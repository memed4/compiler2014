    .sdata
	.align	2
Label19:
	.asciiz	" O\000"
	.align	2
Label21:
	.asciiz	" .\000"
	.align	2
Label24:
	.asciiz	"\n\000"
	.data
	.align 2
gb42:
	.word	8
	.align 2
gb43:
	.space	32
	.align 2
gb44:
	.space	32
	.align 2
gb45:
	.space	120
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label80
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-104
	sw	$ra,	100($sp)
Label4:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	4($sp)
		# li	tv48,	0
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	4
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc46,	tv48
	lw	$t0,	8($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# slt	tv49,	lc46,	gb42
	lw	$t0,	12($sp)
	beq	$t0,	$0,	Label10
		# BEQZ	tv49,	Label10
Label8:
		# Label8
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	16($sp)
		# li	tv50,	0
	addiu	$t0,	$sp,	20
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc47,	tv50
	lw	$t0,	20($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# slt	tv51,	lc47,	gb42
	lw	$t0,	24($sp)
	beq	$t0,	$0,	Label15
		# BEQZ	tv51,	Label15
Label13:
		# Label13
	addiu	$t0,	$sp,	28
	la	$t1,	gb44
	sw	$t1,	0($t0)
		# GET&	tv52,	gb44
	lw	$t0,	8($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# mul	tv54,	lc46,	4
	lw	$t0,	28($sp)
	lw	$t1,	32($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv53,	tv52,	tv54
	lw	$t0,	36($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	ta55,	tv53,	0
	lw	$t0,	40($sp)
	lw	$t0,	0($t0)
	lw	$t1,	20($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	44($sp)
		# eq	tv56,	ta55,	lc47
	lw	$t0,	44($sp)
	beq	$t0,	$0,	Label18
		# BEQZ	tv56,	Label18
	la	$t0,	Label19
	or	$t2,	$0,	$t0
	sw	$t2,	48($sp)
		# la	tv57,	Label19
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr58,	tv57
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr58
	addiu	$t0,	$sp,	52
	sb	$v0,	0($t0)
		# RM	tv59,	$v0
	j	Label20
		# J	Label20
Label18:
		# Label18
	la	$t0,	Label21
	or	$t2,	$0,	$t0
	sw	$t2,	60($sp)
		# la	tv60,	Label21
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	60
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr61,	tv60
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr61
	addiu	$t0,	$sp,	64
	sb	$v0,	0($t0)
		# RM	tv62,	$v0
Label20:
		# Label20
Label14:
		# Label14
	addiu	$t0,	$sp,	68
	addiu	$t1,	$sp,	20
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv63,	lc47
	lw	$t0,	20($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	lc47,	lc47,	1
	lw	$t0,	20($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	72($sp)
		# slt	tv64,	lc47,	gb42
	lw	$t0,	72($sp)
	bne	$t0,	$0,	Label13
		# BNEZ	tv64,	Label13
Label15:
		# Label15
	la	$t0,	Label24
	or	$t2,	$0,	$t0
	sw	$t2,	76($sp)
		# la	tv65,	Label24
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	76
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr66,	tv65
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr66
	addiu	$t0,	$sp,	80
	sb	$v0,	0($t0)
		# RM	tv67,	$v0
Label9:
		# Label9
	addiu	$t0,	$sp,	84
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv68,	lc46
	lw	$t0,	8($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	lc46,	lc46,	1
	lw	$t0,	8($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	88($sp)
		# slt	tv69,	lc46,	gb42
	lw	$t0,	88($sp)
	bne	$t0,	$0,	Label8
		# BNEZ	tv69,	Label8
Label10:
		# Label10
	la	$t0,	Label24
	or	$t2,	$0,	$t0
	sw	$t2,	92($sp)
		# la	tv70,	Label24
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	92
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr71,	tv70
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr71
	addiu	$t0,	$sp,	96
	sb	$v0,	0($t0)
		# RM	tv72,	$v0
Label5:
	lw	$ra,	100($sp)
	addiu	$sp,	$sp,	104
	jr	$ra


Label27:
	addiu	$sp,	$sp,	-716
	sw	$ra,	712($sp)
Label28:
	lw	$t0,	716($sp)
	lw	$t1,	gb42
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	4($sp)
		# eq	tv74,	ce73,	gb42
	lw	$t0,	4($sp)
	beq	$t0,	$0,	Label32
		# BEQZ	tv74,	Label32
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	8
	sb	$v0,	0($t0)
		# RM	tv75,	$v0
	j	Label33
		# J	Label33
Label32:
		# Label32
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	12($sp)
		# li	tv77,	0
	addiu	$t0,	$sp,	16
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc76,	tv77
	lw	$t0,	16($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# slt	tv78,	lc76,	gb42
	lw	$t0,	20($sp)
	beq	$t0,	$0,	Label38
		# BEQZ	tv78,	Label38
Label36:
		# Label36
	addiu	$t0,	$sp,	24
	la	$t1,	gb43
	sw	$t1,	0($t0)
		# GET&	tv79,	gb43
	lw	$t0,	16($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# mul	tv81,	lc76,	4
	lw	$t0,	24($sp)
	lw	$t1,	28($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv80,	tv79,	tv81
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta82,	tv80,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	40($sp)
		# li	tv83,	0
	lw	$t0,	36($sp)
	lw	$t0,	0($t0)
	lw	$t1,	40($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	44($sp)
		# eq	tv84,	ta82,	tv83
	lw	$t0,	44($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label41
		# BEQ	tv84,	0,	Label41
	addiu	$t0,	$sp,	48
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv85,	gb45
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	52($sp)
		# li	tv87,	0
	lw	$t0,	52($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	56($sp)
		# mul	tv88,	tv87,	60
	lw	$t0,	48($sp)
	lw	$t1,	56($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# add	tv86,	tv85,	tv88
	lw	$t0,	16($sp)
	lw	$t1,	716($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	120($sp)
		# add	tv90,	lc76,	ce73
	lw	$t0,	120($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	124($sp)
		# mul	tv91,	tv90,	4
	lw	$t0,	60($sp)
	lw	$t1,	124($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	128($sp)
		# add	tv89,	tv86,	tv91
	lw	$t0,	128($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	132($sp)
		# add	ta92,	tv89,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	136($sp)
		# li	tv93,	0
	lw	$t0,	132($sp)
	lw	$t0,	0($t0)
	lw	$t1,	136($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	140($sp)
		# eq	tv94,	ta92,	tv93
	lw	$t0,	140($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label41
		# BEQ	tv94,	0,	Label41
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	144($sp)
		# li	tv95,	1
	j	Label42
		# J	Label42
Label41:
		# Label41
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	144($sp)
		# li	tv95,	0
Label42:
		# Label42
	lw	$t0,	144($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label39
		# BEQ	tv95,	0,	Label39
	addiu	$t0,	$sp,	148
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv96,	gb45
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	152($sp)
		# li	tv98,	1
	lw	$t0,	152($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	156($sp)
		# mul	tv99,	tv98,	60
	lw	$t0,	148($sp)
	lw	$t1,	156($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	160($sp)
		# add	tv97,	tv96,	tv99
	lw	$t0,	16($sp)
	lw	$t1,	gb42
	add	$t2,	$t0,	$t1
	sw	$t2,	220($sp)
		# add	tv101,	lc76,	gb42
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	224($sp)
		# li	tv102,	1
	lw	$t0,	220($sp)
	lw	$t1,	224($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	228($sp)
		# sub	tv103,	tv101,	tv102
	lw	$t0,	228($sp)
	lw	$t1,	716($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	232($sp)
		# sub	tv104,	tv103,	ce73
	lw	$t0,	232($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	236($sp)
		# mul	tv105,	tv104,	4
	lw	$t0,	160($sp)
	lw	$t1,	236($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	240($sp)
		# add	tv100,	tv97,	tv105
	lw	$t0,	240($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	244($sp)
		# add	ta106,	tv100,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	248($sp)
		# li	tv107,	0
	lw	$t0,	244($sp)
	lw	$t0,	0($t0)
	lw	$t1,	248($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	252($sp)
		# eq	tv108,	ta106,	tv107
	lw	$t0,	252($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label39
		# BEQ	tv108,	0,	Label39
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	256($sp)
		# li	tv109,	1
	j	Label40
		# J	Label40
Label39:
		# Label39
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	256($sp)
		# li	tv109,	0
Label40:
		# Label40
	lw	$t0,	256($sp)
	beq	$t0,	$0,	Label57
		# BEQZ	tv109,	Label57
	addiu	$t0,	$sp,	260
	la	$t1,	gb43
	sw	$t1,	0($t0)
		# GET&	tv110,	gb43
	lw	$t0,	16($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	264($sp)
		# mul	tv112,	lc76,	4
	lw	$t0,	260($sp)
	lw	$t1,	264($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	268($sp)
		# add	tv111,	tv110,	tv112
	lw	$t0,	268($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	272($sp)
		# add	ta113,	tv111,	0
	addiu	$t0,	$sp,	276
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv114,	gb45
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	280($sp)
		# li	tv116,	0
	lw	$t0,	280($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	284($sp)
		# mul	tv117,	tv116,	60
	lw	$t0,	276($sp)
	lw	$t1,	284($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	288($sp)
		# add	tv115,	tv114,	tv117
	lw	$t0,	16($sp)
	lw	$t1,	716($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	348($sp)
		# add	tv119,	lc76,	ce73
	lw	$t0,	348($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	352($sp)
		# mul	tv120,	tv119,	4
	lw	$t0,	288($sp)
	lw	$t1,	352($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	356($sp)
		# add	tv118,	tv115,	tv120
	lw	$t0,	356($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	360($sp)
		# add	ta121,	tv118,	0
	addiu	$t0,	$sp,	364
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv122,	gb45
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	368($sp)
		# li	tv124,	1
	lw	$t0,	368($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	372($sp)
		# mul	tv125,	tv124,	60
	lw	$t0,	364($sp)
	lw	$t1,	372($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	376($sp)
		# add	tv123,	tv122,	tv125
	lw	$t0,	16($sp)
	lw	$t1,	gb42
	add	$t2,	$t0,	$t1
	sw	$t2,	436($sp)
		# add	tv127,	lc76,	gb42
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	440($sp)
		# li	tv128,	1
	lw	$t0,	436($sp)
	lw	$t1,	440($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	444($sp)
		# sub	tv129,	tv127,	tv128
	lw	$t0,	444($sp)
	lw	$t1,	716($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	448($sp)
		# sub	tv130,	tv129,	ce73
	lw	$t0,	448($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	452($sp)
		# mul	tv131,	tv130,	4
	lw	$t0,	376($sp)
	lw	$t1,	452($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	456($sp)
		# add	tv126,	tv123,	tv131
	lw	$t0,	456($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	460($sp)
		# add	ta132,	tv126,	0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	464($sp)
		# li	tv133,	1
	lw	$t0,	460($sp)
	addiu	$t1,	$sp,	464
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta132,	tv133
	lw	$t0,	360($sp)
	lw	$t1,	460($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta121,	ta132
	lw	$t0,	272($sp)
	lw	$t1,	360($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta113,	ta121
	addiu	$t0,	$sp,	468
	la	$t1,	gb44
	sw	$t1,	0($t0)
		# GET&	tv134,	gb44
	lw	$t0,	716($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	472($sp)
		# mul	tv136,	ce73,	4
	lw	$t0,	468($sp)
	lw	$t1,	472($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	476($sp)
		# add	tv135,	tv134,	tv136
	lw	$t0,	476($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	480($sp)
		# add	ta137,	tv135,	0
	lw	$t0,	480($sp)
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta137,	lc76
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	484($sp)
		# li	tv138,	1
	lw	$t0,	716($sp)
	lw	$t1,	484($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	488($sp)
		# add	tv139,	ce73,	tv138
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	488
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr140,	tv139
	lw	$a0,	0($sp)
	jal	Label27
		# CALL	Label27,	cr140
	addiu	$t0,	$sp,	492
	sb	$v0,	0($t0)
		# RM	tv141,	$v0
	addiu	$t0,	$sp,	496
	la	$t1,	gb43
	sw	$t1,	0($t0)
		# GET&	tv142,	gb43
	lw	$t0,	16($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	500($sp)
		# mul	tv144,	lc76,	4
	lw	$t0,	496($sp)
	lw	$t1,	500($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	504($sp)
		# add	tv143,	tv142,	tv144
	lw	$t0,	504($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	508($sp)
		# add	ta145,	tv143,	0
	addiu	$t0,	$sp,	512
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv146,	gb45
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	516($sp)
		# li	tv148,	0
	lw	$t0,	516($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	520($sp)
		# mul	tv149,	tv148,	60
	lw	$t0,	512($sp)
	lw	$t1,	520($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	524($sp)
		# add	tv147,	tv146,	tv149
	lw	$t0,	16($sp)
	lw	$t1,	716($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	584($sp)
		# add	tv151,	lc76,	ce73
	lw	$t0,	584($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	588($sp)
		# mul	tv152,	tv151,	4
	lw	$t0,	524($sp)
	lw	$t1,	588($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	592($sp)
		# add	tv150,	tv147,	tv152
	lw	$t0,	592($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	596($sp)
		# add	ta153,	tv150,	0
	addiu	$t0,	$sp,	600
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv154,	gb45
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	604($sp)
		# li	tv156,	1
	lw	$t0,	604($sp)
	li	$t1,	60
	mul	$t2,	$t0,	$t1
	sw	$t2,	608($sp)
		# mul	tv157,	tv156,	60
	lw	$t0,	600($sp)
	lw	$t1,	608($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	612($sp)
		# add	tv155,	tv154,	tv157
	lw	$t0,	16($sp)
	lw	$t1,	gb42
	add	$t2,	$t0,	$t1
	sw	$t2,	672($sp)
		# add	tv159,	lc76,	gb42
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	676($sp)
		# li	tv160,	1
	lw	$t0,	672($sp)
	lw	$t1,	676($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	680($sp)
		# sub	tv161,	tv159,	tv160
	lw	$t0,	680($sp)
	lw	$t1,	716($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	684($sp)
		# sub	tv162,	tv161,	ce73
	lw	$t0,	684($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	688($sp)
		# mul	tv163,	tv162,	4
	lw	$t0,	612($sp)
	lw	$t1,	688($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	692($sp)
		# add	tv158,	tv155,	tv163
	lw	$t0,	692($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	696($sp)
		# add	ta164,	tv158,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	700($sp)
		# li	tv165,	0
	lw	$t0,	696($sp)
	addiu	$t1,	$sp,	700
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta164,	tv165
	lw	$t0,	596($sp)
	lw	$t1,	696($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta153,	ta164
	lw	$t0,	508($sp)
	lw	$t1,	596($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta145,	ta153
Label57:
		# Label57
Label37:
		# Label37
	addiu	$t0,	$sp,	704
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv166,	lc76
	lw	$t0,	16($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	lc76,	lc76,	1
	lw	$t0,	16($sp)
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	708($sp)
		# slt	tv167,	lc76,	gb42
	lw	$t0,	708($sp)
	bne	$t0,	$0,	Label36
		# BNEZ	tv167,	Label36
Label38:
		# Label38
Label33:
		# Label33
Label29:
	lw	$ra,	712($sp)
	addiu	$sp,	$sp,	716
	jr	$ra


Label80:
	addiu	$sp,	$sp,	-20
	sw	$ra,	16($sp)
Label81:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	4($sp)
		# li	tv168,	0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	4
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr169,	tv168
	lw	$a0,	0($sp)
	jal	Label27
		# CALL	Label27,	cr169
	addiu	$t0,	$sp,	8
	sb	$v0,	0($t0)
		# RM	tv170,	$v0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	12($sp)
		# li	tv171,	0
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	tv171
	j	Label82
		# J	Label82
Label82:
	lw	$ra,	16($sp)
	addiu	$sp,	$sp,	20
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
