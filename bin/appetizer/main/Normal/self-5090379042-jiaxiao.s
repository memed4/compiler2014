	.sdata
	.align	2
Label3:
	.asciiz	"#include <stdio.h>%cchar*f=%c%s%c;int main(){printf(f,10,34,f,34,10);}%c\000"
	.data
	.align 2
gb42:
	.word	Label3
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label4
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label4:
	addiu	$sp,	$sp,	-52
	sw	$ra,	48($sp)
Label5:
	li	$t0,	10
	or	$t2,	$0,	$t0
	sw	$t2,	24($sp)
		# li	tv43,	10
	li	$t0,	34
	or	$t2,	$0,	$t0
	sw	$t2,	28($sp)
		# li	tv44,	34
	li	$t0,	34
	or	$t2,	$0,	$t0
	sw	$t2,	32($sp)
		# li	tv45,	34
	li	$t0,	10
	or	$t2,	$0,	$t0
	sw	$t2,	36($sp)
		# li	tv46,	10
	addiu	$t0,	$sp,	0
	la	$t1,	gb42
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr47,	gb42
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	24
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr48,	tv43
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	28
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr49,	tv44
	addiu	$t0,	$sp,	12
	la	$t1,	gb42
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr50,	gb42
	addiu	$t0,	$sp,	16
	addiu	$t1,	$sp,	32
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr51,	tv45
	addiu	$t0,	$sp,	20
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr52,	tv46
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr47,	cr48,	cr49,	cr50,	cr51,	cr52
	addiu	$t0,	$sp,	40
	sb	$v0,	0($t0)
		# RM	tv53,	$v0
Label6:
	lw	$ra,	48($sp)
	addiu	$sp,	$sp,	52
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
