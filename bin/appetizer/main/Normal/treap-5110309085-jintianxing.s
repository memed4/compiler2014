	.sdata
	.align	2
Label97:
	.asciiz	"%d\n\000"
	.data
	.align 2
gb42:
	.word	2000
	.align 2
gb43:
	.word	13579
	.align 2
gb44:
	.word	12345
	.align 2
gb45:
	.word	32768
	.align 2
gb46:
	.word	86421
	.align 2
gb47:
	.space	4
	.align 2
gb48:
	.space	4
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label82
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-28
	sw	$ra,	24($sp)
Label4:
	lw	$t0,	gb43
	lw	$t1,	gb46
	mul	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# mul	tv50,	gb43,	gb46
	lw	$t0,	8($sp)
	lw	$t1,	gb44
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	tv51,	tv50,	gb44
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	4
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc49,	tv51
	lw	$t0,	12($sp)
	lw	$t1,	gb45
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	16($sp)
		# mod	tv52,	lc49,	gb45
	la	$t0,	gb46
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb46,	tv52
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	lc49
	j	Label5
		# J	Label5
Label5:
	lw	$ra,	24($sp)
	addiu	$sp,	$sp,	28
	jr	$ra


Label16:
	addiu	$sp,	$sp,	-28
	sw	$ra,	24($sp)
Label17:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv54,	0
	lw	$t0,	28($sp)
	lw	$t1,	8($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	4($sp)
		# eq	tv55,	ce53,	tv54
	lw	$t0,	4($sp)
	beq	$t0,	$0,	Label21
		# BEQZ	tv55,	Label21
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	12($sp)
		# li	tv56,	0
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	tv56
	j	Label18
		# J	Label18
Label21:
		# Label21
	lw	$t0,	28($sp)
	li	$t1,	8
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv58,	ce53,	8
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta57,	tv58,	0
	lw	$t1,	20($sp)
	lw	$v0,	0($t1)
		# RM	$v0,	ta57
	j	Label18
		# J	Label18
Label18:
	lw	$ra,	24($sp)
	addiu	$sp,	$sp,	28
	jr	$ra


Label22:
	addiu	$sp,	$sp,	-52
	sw	$ra,	48($sp)
Label23:
	lw	$t0,	52($sp)
	li	$t1,	8
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	tv61,	ce59,	8
	lw	$t0,	4($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	ta60,	tv61,	0
	lw	$t0,	52($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	tv63,	ce59,	12
	lw	$t0,	12($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	ta62,	tv63,	0
	addiu	$t0,	$sp,	0
	lw	$t1,	16($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr64,	ta62
	lw	$a0,	0($sp)
	jal	Label16
		# CALL	Label16,	cr64
	addiu	$t0,	$sp,	20
	sw	$v0,	0($t0)
		# RM	tv65,	$v0
	lw	$t0,	52($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv67,	ce59,	16
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta66,	tv67,	0
	addiu	$t0,	$sp,	0
	lw	$t1,	28($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr68,	ta66
	lw	$a0,	0($sp)
	jal	Label16
		# CALL	Label16,	cr68
	addiu	$t0,	$sp,	32
	sw	$v0,	0($t0)
		# RM	tv69,	$v0
	lw	$t0,	20($sp)
	lw	$t1,	32($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv70,	tv65,	tv69
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	40($sp)
		# li	tv71,	1
	lw	$t0,	36($sp)
	lw	$t1,	40($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# add	tv72,	tv70,	tv71
	lw	$t0,	8($sp)
	addiu	$t1,	$sp,	44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta60,	tv72
Label24:
	lw	$ra,	48($sp)
	addiu	$sp,	$sp,	52
	jr	$ra


Label29:
	addiu	$sp,	$sp,	-52
	sw	$ra,	48($sp)
Label30:
	lw	$t0,	52($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	tv76,	ce73,	16
	lw	$t0,	4($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	ta75,	tv76,	0
	addiu	$t0,	$sp,	12
	lw	$t1,	8($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc74,	ta75
	lw	$t0,	52($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv78,	ce73,	16
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta77,	tv78,	0
	lw	$t0,	12($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv80,	lc74,	12
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta79,	tv80,	0
	lw	$t0,	20($sp)
	lw	$t1,	28($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta77,	ta79
	lw	$t0,	12($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv82,	lc74,	12
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta81,	tv82,	0
	lw	$t0,	36($sp)
	addiu	$t1,	$sp,	52
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta81,	ce73
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	52
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr83,	ce73
	lw	$a0,	0($sp)
	jal	Label22
		# CALL	Label22,	cr83
	addiu	$t0,	$sp,	40
	sb	$v0,	0($t0)
		# RM	tv84,	$v0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr85,	lc74
	lw	$a0,	0($sp)
	jal	Label22
		# CALL	Label22,	cr85
	addiu	$t0,	$sp,	44
	sb	$v0,	0($t0)
		# RM	tv86,	$v0
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	lc74
	j	Label31
		# J	Label31
Label31:
	lw	$ra,	48($sp)
	addiu	$sp,	$sp,	52
	jr	$ra


Label32:
	addiu	$sp,	$sp,	-52
	sw	$ra,	48($sp)
Label33:
	lw	$t0,	52($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	tv90,	ce87,	12
	lw	$t0,	4($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	ta89,	tv90,	0
	addiu	$t0,	$sp,	12
	lw	$t1,	8($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc88,	ta89
	lw	$t0,	52($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv92,	ce87,	12
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta91,	tv92,	0
	lw	$t0,	12($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv94,	lc88,	16
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta93,	tv94,	0
	lw	$t0,	20($sp)
	lw	$t1,	28($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta91,	ta93
	lw	$t0,	12($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv96,	lc88,	16
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta95,	tv96,	0
	lw	$t0,	36($sp)
	addiu	$t1,	$sp,	52
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta95,	ce87
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	52
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr97,	ce87
	lw	$a0,	0($sp)
	jal	Label22
		# CALL	Label22,	cr97
	addiu	$t0,	$sp,	40
	sb	$v0,	0($t0)
		# RM	tv98,	$v0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr99,	lc88
	lw	$a0,	0($sp)
	jal	Label22
		# CALL	Label22,	cr99
	addiu	$t0,	$sp,	44
	sb	$v0,	0($t0)
		# RM	tv100,	$v0
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	lc88
	j	Label34
		# J	Label34
Label34:
	lw	$ra,	48($sp)
	addiu	$sp,	$sp,	52
	jr	$ra


Label35:
	addiu	$sp,	$sp,	-188
	sw	$ra,	184($sp)
Label36:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv103,	0
	lw	$t0,	188($sp)
	lw	$t1,	8($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	12($sp)
		# eq	tv104,	ce101,	tv103
	lw	$t0,	12($sp)
	beq	$t0,	$0,	Label40
		# BEQZ	tv104,	Label40
	addiu	$t1,	$sp,	192
	lw	$v0,	0($t1)
		# RM	$v0,	ce102
	j	Label37
		# J	Label37
Label40:
		# Label40
	lw	$t0,	188($sp)
	li	$t1,	0
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv106,	ce101,	0
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta105,	tv106,	0
	lw	$t0,	192($sp)
	li	$t1,	0
	add	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv108,	ce102,	0
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta107,	tv108,	0
	lw	$t0,	28($sp)
	lw	$t0,	0($t0)
	lw	$t1,	20($sp)
	lw	$t1,	0($t1)
	slt	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# slt	tv109,	ta107,	ta105
	lw	$t0,	32($sp)
	beq	$t0,	$0,	Label43
		# BEQZ	tv109,	Label43
	lw	$t0,	188($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv111,	ce101,	12
	lw	$t0,	36($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	ta110,	tv111,	0
	lw	$t0,	188($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# add	tv113,	ce101,	12
	lw	$t0,	44($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# add	ta112,	tv113,	0
	addiu	$t0,	$sp,	0
	lw	$t1,	48($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr114,	ta112
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	192
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr115,	ce102
	lw	$a0,	0($sp)
	jal	Label35
		# CALL	Label35,	cr114,	cr115
	addiu	$t0,	$sp,	52
	sw	$v0,	0($t0)
		# RM	tv116,	$v0
	lw	$t0,	40($sp)
	addiu	$t1,	$sp,	52
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta110,	tv116
	j	Label44
		# J	Label44
Label43:
		# Label43
	lw	$t0,	188($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	56($sp)
		# add	tv118,	ce101,	16
	lw	$t0,	56($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# add	ta117,	tv118,	0
	lw	$t0,	188($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	tv120,	ce101,	16
	lw	$t0,	64($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	68($sp)
		# add	ta119,	tv120,	0
	addiu	$t0,	$sp,	0
	lw	$t1,	68($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr121,	ta119
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	192
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr122,	ce102
	lw	$a0,	0($sp)
	jal	Label35
		# CALL	Label35,	cr121,	cr122
	addiu	$t0,	$sp,	72
	sw	$v0,	0($t0)
		# RM	tv123,	$v0
	lw	$t0,	60($sp)
	addiu	$t1,	$sp,	72
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta117,	tv123
Label44:
		# Label44
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	188
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr124,	ce101
	lw	$a0,	0($sp)
	jal	Label22
		# CALL	Label22,	cr124
	addiu	$t0,	$sp,	76
	sb	$v0,	0($t0)
		# RM	tv125,	$v0
	lw	$t0,	188($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	80($sp)
		# add	tv127,	ce101,	12
	lw	$t0,	80($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	84($sp)
		# add	ta126,	tv127,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	88($sp)
		# li	tv128,	0
	lw	$t0,	84($sp)
	lw	$t0,	0($t0)
	lw	$t1,	88($sp)
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	92($sp)
		# neq	tv129,	ta126,	tv128
	lw	$t0,	92($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label45
		# BEQ	tv129,	0,	Label45
	lw	$t0,	188($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	96($sp)
		# add	tv131,	ce101,	12
	lw	$t0,	96($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	100($sp)
		# add	ta130,	tv131,	0
	lw	$t0,	100($sp)
	lw	$t0,	0($t0)
	li	$t1,	4
	add	$t2,	$t0,	$t1
	sw	$t2,	104($sp)
		# add	tv133,	ta130,	4
	lw	$t0,	104($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	108($sp)
		# add	ta132,	tv133,	0
	lw	$t0,	188($sp)
	li	$t1,	4
	add	$t2,	$t0,	$t1
	sw	$t2,	112($sp)
		# add	tv135,	ce101,	4
	lw	$t0,	112($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	116($sp)
		# add	ta134,	tv135,	0
	lw	$t0,	116($sp)
	lw	$t0,	0($t0)
	lw	$t1,	108($sp)
	lw	$t1,	0($t1)
	slt	$t2,	$t0,	$t1
	sw	$t2,	120($sp)
		# slt	tv136,	ta134,	ta132
	lw	$t0,	120($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label45
		# BEQ	tv136,	0,	Label45
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	124($sp)
		# li	tv137,	1
	j	Label46
		# J	Label46
Label45:
		# Label45
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	124($sp)
		# li	tv137,	0
Label46:
		# Label46
	lw	$t0,	124($sp)
	beq	$t0,	$0,	Label51
		# BEQZ	tv137,	Label51
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	188
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr138,	ce101
	lw	$a0,	0($sp)
	jal	Label32
		# CALL	Label32,	cr138
	addiu	$t0,	$sp,	128
	sw	$v0,	0($t0)
		# RM	tv139,	$v0
	addiu	$t1,	$sp,	128
	lw	$v0,	0($t1)
		# RM	$v0,	tv139
	j	Label37
		# J	Label37
Label51:
		# Label51
	lw	$t0,	188($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	132($sp)
		# add	tv141,	ce101,	16
	lw	$t0,	132($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	136($sp)
		# add	ta140,	tv141,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	140($sp)
		# li	tv142,	0
	lw	$t0,	136($sp)
	lw	$t0,	0($t0)
	lw	$t1,	140($sp)
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	144($sp)
		# neq	tv143,	ta140,	tv142
	lw	$t0,	144($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label52
		# BEQ	tv143,	0,	Label52
	lw	$t0,	188($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	148($sp)
		# add	tv145,	ce101,	16
	lw	$t0,	148($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	152($sp)
		# add	ta144,	tv145,	0
	lw	$t0,	152($sp)
	lw	$t0,	0($t0)
	li	$t1,	4
	add	$t2,	$t0,	$t1
	sw	$t2,	156($sp)
		# add	tv147,	ta144,	4
	lw	$t0,	156($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	160($sp)
		# add	ta146,	tv147,	0
	lw	$t0,	188($sp)
	li	$t1,	4
	add	$t2,	$t0,	$t1
	sw	$t2,	164($sp)
		# add	tv149,	ce101,	4
	lw	$t0,	164($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	168($sp)
		# add	ta148,	tv149,	0
	lw	$t0,	168($sp)
	lw	$t0,	0($t0)
	lw	$t1,	160($sp)
	lw	$t1,	0($t1)
	slt	$t2,	$t0,	$t1
	sw	$t2,	172($sp)
		# slt	tv150,	ta148,	ta146
	lw	$t0,	172($sp)
	li	$t1,	0
	beq	$t0,	$t1,	Label52
		# BEQ	tv150,	0,	Label52
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	176($sp)
		# li	tv151,	1
	j	Label53
		# J	Label53
Label52:
		# Label52
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	176($sp)
		# li	tv151,	0
Label53:
		# Label53
	lw	$t0,	176($sp)
	beq	$t0,	$0,	Label58
		# BEQZ	tv151,	Label58
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	188
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr152,	ce101
	lw	$a0,	0($sp)
	jal	Label29
		# CALL	Label29,	cr152
	addiu	$t0,	$sp,	180
	sw	$v0,	0($t0)
		# RM	tv153,	$v0
	addiu	$t1,	$sp,	180
	lw	$v0,	0($t1)
		# RM	$v0,	tv153
	j	Label37
		# J	Label37
Label58:
		# Label58
	addiu	$t1,	$sp,	188
	lw	$v0,	0($t1)
		# RM	$v0,	ce101
	j	Label37
		# J	Label37
Label37:
	lw	$ra,	184($sp)
	addiu	$sp,	$sp,	188
	jr	$ra


Label59:
	addiu	$sp,	$sp,	-104
	sw	$ra,	100($sp)
Label60:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv157,	0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc156,	tv157
	lw	$t0,	104($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv159,	ce154,	12
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta158,	tv159,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	24($sp)
		# li	tv160,	0
	lw	$t0,	20($sp)
	lw	$t0,	0($t0)
	lw	$t1,	24($sp)
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	28($sp)
		# neq	tv161,	ta158,	tv160
	lw	$t0,	28($sp)
	beq	$t0,	$0,	Label64
		# BEQZ	tv161,	Label64
	lw	$t0,	104($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv163,	ce154,	12
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta162,	tv163,	0
	lw	$t0,	36($sp)
	lw	$t0,	0($t0)
	li	$t1,	8
	add	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	tv165,	ta162,	8
	lw	$t0,	40($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# add	ta164,	tv165,	0
	addiu	$t0,	$sp,	12
	lw	$t1,	44($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc156,	ta164
Label64:
		# Label64
	lw	$t0,	108($sp)
	lw	$t1,	12($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# slt	tv166,	ce155,	lc156
	lw	$t0,	48($sp)
	beq	$t0,	$0,	Label67
		# BEQZ	tv166,	Label67
	lw	$t0,	104($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# add	tv168,	ce154,	12
	lw	$t0,	52($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	56($sp)
		# add	ta167,	tv168,	0
	addiu	$t0,	$sp,	0
	lw	$t1,	56($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr169,	ta167
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	108
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr170,	ce155
	lw	$a0,	0($sp)
	jal	Label59
		# CALL	Label59,	cr169,	cr170
	addiu	$t0,	$sp,	60
	sw	$v0,	0($t0)
		# RM	tv171,	$v0
	addiu	$t1,	$sp,	60
	lw	$v0,	0($t1)
		# RM	$v0,	tv171
	j	Label61
		# J	Label61
Label67:
		# Label67
	lw	$t0,	12($sp)
	lw	$t1,	108($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# slt	tv172,	lc156,	ce155
	lw	$t0,	64($sp)
	beq	$t0,	$0,	Label70
		# BEQZ	tv172,	Label70
	lw	$t0,	104($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	68($sp)
		# add	tv174,	ce154,	16
	lw	$t0,	68($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	72($sp)
		# add	ta173,	tv174,	0
	lw	$t0,	108($sp)
	lw	$t1,	12($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	76($sp)
		# sub	tv175,	ce155,	lc156
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	80($sp)
		# li	tv176,	1
	lw	$t0,	76($sp)
	lw	$t1,	80($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	84($sp)
		# sub	tv177,	tv175,	tv176
	addiu	$t0,	$sp,	0
	lw	$t1,	72($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr178,	ta173
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	84
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr179,	tv177
	lw	$a0,	0($sp)
	jal	Label59
		# CALL	Label59,	cr178,	cr179
	addiu	$t0,	$sp,	88
	sw	$v0,	0($t0)
		# RM	tv180,	$v0
	addiu	$t1,	$sp,	88
	lw	$v0,	0($t1)
		# RM	$v0,	tv180
	j	Label61
		# J	Label61
Label70:
		# Label70
	lw	$t0,	104($sp)
	li	$t1,	0
	add	$t2,	$t0,	$t1
	sw	$t2,	92($sp)
		# add	tv182,	ce154,	0
	lw	$t0,	92($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	96($sp)
		# add	ta181,	tv182,	0
	lw	$t1,	96($sp)
	lw	$v0,	0($t1)
		# RM	$v0,	ta181
	j	Label61
		# J	Label61
Label61:
	lw	$ra,	100($sp)
	addiu	$sp,	$sp,	104
	jr	$ra


Label79:
	addiu	$sp,	$sp,	-76
	sw	$ra,	72($sp)
Label80:
	li	$t0,	20
	or	$t2,	$0,	$t0
	sw	$t2,	4($sp)
		# li	tv184,	20
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	4
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr185,	tv184
	lw	$a0,	0($sp)
	jal	Label2
		# CALL	Label2,	cr185
	addiu	$t0,	$sp,	8
	sw	$v0,	0($t0)
		# RM	tv186,	$v0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc183,	tv186
	lw	$t0,	12($sp)
	li	$t1,	12
	add	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	tv188,	lc183,	12
	lw	$t0,	16($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	ta187,	tv188,	0
	lw	$t0,	12($sp)
	li	$t1,	16
	add	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv190,	lc183,	16
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta189,	tv190,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	32($sp)
		# li	tv191,	0
	lw	$t0,	28($sp)
	addiu	$t1,	$sp,	32
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta189,	tv191
	lw	$t0,	20($sp)
	lw	$t1,	28($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta187,	ta189
	lw	$t0,	12($sp)
	li	$t1,	8
	add	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv193,	lc183,	8
	lw	$t0,	36($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	ta192,	tv193,	0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	44($sp)
		# li	tv194,	1
	lw	$t0,	40($sp)
	addiu	$t1,	$sp,	44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta192,	tv194
	lw	$t0,	12($sp)
	li	$t1,	4
	add	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# add	tv196,	lc183,	4
	lw	$t0,	48($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# add	ta195,	tv196,	0
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	56
	sw	$v0,	0($t0)
		# RM	tv197,	$v0
	lw	$t0,	52($sp)
	addiu	$t1,	$sp,	56
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta195,	tv197
	lw	$t0,	12($sp)
	li	$t1,	0
	add	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# add	tv199,	lc183,	0
	lw	$t0,	60($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	ta198,	tv199,	0
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	68
	sw	$v0,	0($t0)
		# RM	tv200,	$v0
	lw	$t0,	64($sp)
	addiu	$t1,	$sp,	68
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta198,	tv200
	addiu	$t1,	$sp,	12
	lw	$v0,	0($t1)
		# RM	$v0,	lc183
	j	Label81
		# J	Label81
Label81:
	lw	$ra,	72($sp)
	addiu	$sp,	$sp,	76
	jr	$ra


Label82:
	addiu	$sp,	$sp,	-72
	sw	$ra,	68($sp)
Label83:
	lw	$a0,	0($sp)
	jal	Label79
		# CALL	Label79
	addiu	$t0,	$sp,	8
	sw	$v0,	0($t0)
		# RM	tv201,	$v0
	la	$t0,	gb48
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb48,	tv201
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	12($sp)
		# li	tv202,	0
	la	$t0,	gb47
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb47,	tv202
	lw	$t0,	gb47
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# slt	tv203,	gb47,	gb42
	lw	$t0,	16($sp)
	beq	$t0,	$0,	Label89
		# BEQZ	tv203,	Label89
Label87:
		# Label87
	lw	$a0,	0($sp)
	jal	Label79
		# CALL	Label79
	addiu	$t0,	$sp,	20
	sw	$v0,	0($t0)
		# RM	tv204,	$v0
	addiu	$t0,	$sp,	0
	la	$t1,	gb48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr205,	gb48
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	20
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr206,	tv204
	lw	$a0,	0($sp)
	jal	Label35
		# CALL	Label35,	cr205,	cr206
	addiu	$t0,	$sp,	24
	sw	$v0,	0($t0)
		# RM	tv207,	$v0
	la	$t0,	gb48
	addiu	$t1,	$sp,	24
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb48,	tv207
Label88:
		# Label88
	addiu	$t0,	$sp,	28
	la	$t1,	gb47
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv208,	gb47
	lw	$t0,	gb47
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	gb47
		# add	gb47,	gb47,	1
	lw	$t0,	gb47
	lw	$t1,	gb42
	slt	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# slt	tv209,	gb47,	gb42
	lw	$t0,	32($sp)
	bne	$t0,	$0,	Label87
		# BNEZ	tv209,	Label87
Label89:
		# Label89
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	36($sp)
		# li	tv210,	0
	la	$t0,	gb47
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb47,	tv210
	lw	$t0,	gb47
	lw	$t1,	gb42
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	40($sp)
		# leq	tv211,	gb47,	gb42
	lw	$t0,	40($sp)
	beq	$t0,	$0,	Label96
		# BEQZ	tv211,	Label96
Label94:
		# Label94
	la	$t0,	Label97
	or	$t2,	$0,	$t0
	sw	$t2,	44($sp)
		# la	tv212,	Label97
	addiu	$t0,	$sp,	0
	la	$t1,	gb48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr213,	gb48
	addiu	$t0,	$sp,	4
	la	$t1,	gb47
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr214,	gb47
	lw	$a0,	0($sp)
	jal	Label59
		# CALL	Label59,	cr213,	cr214
	addiu	$t0,	$sp,	48
	sw	$v0,	0($t0)
		# RM	tv215,	$v0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr216,	tv212
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr217,	tv215
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr216,	cr217
	addiu	$t0,	$sp,	52
	sb	$v0,	0($t0)
		# RM	tv218,	$v0
Label95:
		# Label95
	addiu	$t0,	$sp,	56
	la	$t1,	gb47
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv219,	gb47
	lw	$t0,	gb47
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	gb47
		# add	gb47,	gb47,	1
	lw	$t0,	gb47
	lw	$t1,	gb42
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	60($sp)
		# leq	tv220,	gb47,	gb42
	lw	$t0,	60($sp)
	bne	$t0,	$0,	Label94
		# BNEZ	tv220,	Label94
Label96:
		# Label96
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	64($sp)
		# li	tv221,	0
	addiu	$t1,	$sp,	64
	lw	$v0,	0($t1)
		# RM	$v0,	tv221
	j	Label84
		# J	Label84
Label84:
	lw	$ra,	68($sp)
	addiu	$sp,	$sp,	72
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
