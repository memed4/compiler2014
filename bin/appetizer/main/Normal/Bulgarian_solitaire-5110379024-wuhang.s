	.sdata
	.align	2
Label65:
	.asciiz	"%d \000"
	.align	2
Label68:
	.asciiz	"%c\000"
	.align	2
Label168:
	.asciiz	"Sorry, the number n must be a number s.t. there exists i satisfying n=1+2+...+i\n\000"
	.align	2
Label169:
	.asciiz	"Let's start!\n\000"
	.align	2
Label174:
	.asciiz	"%d\n\000"
	.align	2
Label206:
	.asciiz	"step %d:\n\000"
	.align	2
Label207:
	.asciiz	"Total: %d step(s)\n\000"
	.data
	.align 2
gb42:
	.word	210
	.align 2
gb43:
	.space	4
	.align 2
gb44:
	.space	4
	.align 2
gb45:
	.space	4000
	.align 2
gb46:
	.word	48271
	.align 2
gb47:
	.word	2147483647
	.align 2
gb48:
	.space	4
	.align 2
gb49:
	.space	4
	.align 2
gb50:
	.word	1
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label160
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-48
	sw	$ra,	44($sp)
Label4:
	lw	$t0,	gb50
	lw	$t1,	gb48
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	8($sp)
		# mod	tv52,	gb50,	gb48
	lw	$t0,	gb46
	lw	$t1,	8($sp)
	mul	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# mul	tv53,	gb46,	tv52
	lw	$t0,	gb50
	lw	$t1,	gb48
	div	$t0,	$t1
	mflo	$t2
	sw	$t2,	12($sp)
		# div	tv54,	gb50,	gb48
	lw	$t0,	gb49
	lw	$t1,	12($sp)
	mul	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# mul	tv55,	gb49,	tv54
	lw	$t0,	4($sp)
	lw	$t1,	16($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# sub	tv56,	tv53,	tv55
	addiu	$t0,	$sp,	24
	addiu	$t1,	$sp,	20
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc51,	tv56
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	28($sp)
		# li	tv57,	0
	lw	$t0,	24($sp)
	lw	$t1,	28($sp)
	slt	$t2,	$t0,	$t1
	xori	$t2,	$t2,	1
	sw	$t2,	32($sp)
		# geq	tv58,	lc51,	tv57
	lw	$t0,	32($sp)
	beq	$t0,	$0,	Label28
		# BEQZ	tv58,	Label28
	la	$t0,	gb50
	addiu	$t1,	$sp,	24
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb50,	lc51
	j	Label29
		# J	Label29
Label28:
		# Label28
	lw	$t0,	24($sp)
	lw	$t1,	gb47
	add	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv59,	lc51,	gb47
	la	$t0,	gb50
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb50,	tv59
Label29:
		# Label29
	la	$t1,	gb50
	lw	$v0,	0($t1)
		# RM	$v0,	gb50
	j	Label5
		# J	Label5
Label5:
	lw	$ra,	44($sp)
	addiu	$sp,	$sp,	48
	jr	$ra


Label32:
	addiu	$sp,	$sp,	-4
	sw	$ra,	0($sp)
Label33:
	la	$t0,	gb50
	addiu	$t1,	$sp,	4
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb50,	ce60
Label34:
	lw	$ra,	0($sp)
	addiu	$sp,	$sp,	4
	jr	$ra


Label35:
	addiu	$sp,	$sp,	-76
	sw	$ra,	72($sp)
Label36:
	addiu	$t0,	$sp,	12
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv64,	gb45
	lw	$t0,	76($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# mul	tv66,	ce61,	4
	lw	$t0,	12($sp)
	lw	$t1,	4($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	tv65,	tv64,	tv66
	lw	$t0,	8($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# add	ta67,	tv65,	0
	addiu	$t0,	$sp,	20
	lw	$t1,	16($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc63,	ta67
	addiu	$t0,	$sp,	24
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv68,	gb45
	lw	$t0,	76($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# mul	tv70,	ce61,	4
	lw	$t0,	24($sp)
	lw	$t1,	28($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv69,	tv68,	tv70
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta71,	tv69,	0
	addiu	$t0,	$sp,	40
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv72,	gb45
	lw	$t0,	80($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# mul	tv74,	ce62,	4
	lw	$t0,	40($sp)
	lw	$t1,	44($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# add	tv73,	tv72,	tv74
	lw	$t0,	48($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# add	ta75,	tv73,	0
	lw	$t0,	36($sp)
	lw	$t1,	52($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta71,	ta75
	addiu	$t0,	$sp,	56
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv76,	gb45
	lw	$t0,	80($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# mul	tv78,	ce62,	4
	lw	$t0,	56($sp)
	lw	$t1,	60($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	tv77,	tv76,	tv78
	lw	$t0,	64($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	68($sp)
		# add	ta79,	tv77,	0
	lw	$t0,	68($sp)
	addiu	$t1,	$sp,	20
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta79,	lc63
Label37:
	lw	$ra,	72($sp)
	addiu	$sp,	$sp,	76
	jr	$ra


Label38:
	addiu	$sp,	$sp,	-48
	sw	$ra,	44($sp)
Label39:
	lw	$t0,	gb43
	lw	$t1,	48($sp)
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	4($sp)
		# leq	tv81,	gb43,	ce80
	lw	$t0,	4($sp)
	beq	$t0,	$0,	Label45
		# BEQZ	tv81,	Label45
Label43:
		# Label43
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv82,	1
	lw	$t0,	gb43
	lw	$t1,	8($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	tv83,	gb43,	tv82
	lw	$t0,	gb43
	lw	$t1,	12($sp)
	mul	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# mul	tv84,	gb43,	tv83
	li	$t0,	2
	or	$t2,	$0,	$t0
	sw	$t2,	20($sp)
		# li	tv85,	2
	lw	$t0,	16($sp)
	lw	$t1,	20($sp)
	div	$t0,	$t1
	mflo	$t2
	sw	$t2,	24($sp)
		# div	tv86,	tv84,	tv85
	lw	$t0,	48($sp)
	lw	$t1,	24($sp)
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	28($sp)
		# eq	tv87,	ce80,	tv86
	lw	$t0,	28($sp)
	beq	$t0,	$0,	Label54
		# BEQZ	tv87,	Label54
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	32($sp)
		# li	tv88,	1
	addiu	$t1,	$sp,	32
	lw	$v0,	0($t1)
		# RM	$v0,	tv88
	j	Label40
		# J	Label40
Label54:
		# Label54
Label44:
		# Label44
	lw	$t0,	gb43
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	gb43
		# add	gb43,	gb43,	1
	lw	$t0,	gb43
	lw	$t1,	48($sp)
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	36($sp)
		# leq	tv89,	gb43,	ce80
	lw	$t0,	36($sp)
	bne	$t0,	$0,	Label43
		# BNEZ	tv89,	Label43
Label45:
		# Label45
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	40($sp)
		# li	tv90,	0
	addiu	$t1,	$sp,	40
	lw	$v0,	0($t1)
		# RM	$v0,	tv90
	j	Label40
		# J	Label40
Label40:
	lw	$ra,	44($sp)
	addiu	$sp,	$sp,	48
	jr	$ra


Label57:
	addiu	$sp,	$sp,	-64
	sw	$ra,	60($sp)
Label58:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv92,	0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc91,	tv92
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# slt	tv93,	lc91,	gb44
	lw	$t0,	16($sp)
	beq	$t0,	$0,	Label64
		# BEQZ	tv93,	Label64
Label62:
		# Label62
	la	$t0,	Label65
	or	$t2,	$0,	$t0
	sw	$t2,	20($sp)
		# la	tv94,	Label65
	addiu	$t0,	$sp,	24
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv95,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# mul	tv97,	lc91,	4
	lw	$t0,	24($sp)
	lw	$t1,	28($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	tv96,	tv95,	tv97
	lw	$t0,	32($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	ta98,	tv96,	0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	20
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr99,	tv94
	addiu	$t0,	$sp,	4
	lw	$t1,	36($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr100,	ta98
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr99,	cr100
	addiu	$t0,	$sp,	40
	sb	$v0,	0($t0)
		# RM	tv101,	$v0
Label63:
		# Label63
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc91,	lc91,	1
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# slt	tv102,	lc91,	gb44
	lw	$t0,	44($sp)
	bne	$t0,	$0,	Label62
		# BNEZ	tv102,	Label62
Label64:
		# Label64
	la	$t0,	Label68
	or	$t2,	$0,	$t0
	sw	$t2,	48($sp)
		# la	tv103,	Label68
	li	$t0,	10
	or	$t2,	$0,	$t0
	sb	$t2,	52($sp)
		# li	tv104,	10
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr105,	tv103
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	52
	lb	$t2,	0($t1)
	sb	$t2,	0($t0)
		# RM	cr106,	tv104
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr105,	cr106
	addiu	$t0,	$sp,	56
	sb	$v0,	0($t0)
		# RM	tv107,	$v0
Label59:
	lw	$ra,	60($sp)
	addiu	$sp,	$sp,	64
	jr	$ra


Label69:
	addiu	$sp,	$sp,	-4264
	sw	$ra,	4260($sp)
Label70:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv110,	0
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc109,	tv110
	lw	$t0,	gb44
	lw	$t1,	gb43
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	12($sp)
		# neq	tv113,	gb44,	gb43
	lw	$t0,	12($sp)
	beq	$t0,	$0,	Label74
		# BEQZ	tv113,	Label74
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	16($sp)
		# li	tv114,	0
	addiu	$t1,	$sp,	16
	lw	$v0,	0($t1)
		# RM	$v0,	tv114
	j	Label71
		# J	Label71
Label74:
		# Label74
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# slt	tv115,	lc109,	gb44
	lw	$t0,	20($sp)
	beq	$t0,	$0,	Label79
		# BEQZ	tv115,	Label79
Label77:
		# Label77
	addiu	$t0,	$sp,	24
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv116,	lc111
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4028($sp)
		# mul	tv118,	lc109,	4
	lw	$t0,	24($sp)
	lw	$t1,	4028($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4032($sp)
		# add	tv117,	tv116,	tv118
	lw	$t0,	4032($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4036($sp)
		# add	ta119,	tv117,	0
	addiu	$t0,	$sp,	4040
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv120,	gb45
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4044($sp)
		# mul	tv122,	lc109,	4
	lw	$t0,	4040($sp)
	lw	$t1,	4044($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4048($sp)
		# add	tv121,	tv120,	tv122
	lw	$t0,	4048($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4052($sp)
		# add	ta123,	tv121,	0
	lw	$t0,	4036($sp)
	lw	$t1,	4052($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta119,	ta123
Label78:
		# Label78
	lw	$t0,	4($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	lc109,	lc109,	1
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	4056($sp)
		# slt	tv124,	lc109,	gb44
	lw	$t0,	4056($sp)
	bne	$t0,	$0,	Label77
		# BNEZ	tv124,	Label77
Label79:
		# Label79
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	4060($sp)
		# li	tv125,	0
	addiu	$t0,	$sp,	4064
	addiu	$t1,	$sp,	4060
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc108,	tv125
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	4068($sp)
		# li	tv126,	1
	lw	$t0,	gb44
	lw	$t1,	4068($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	4072($sp)
		# sub	tv127,	gb44,	tv126
	lw	$t0,	4064($sp)
	lw	$t1,	4072($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	4076($sp)
		# slt	tv128,	lc108,	tv127
	lw	$t0,	4076($sp)
	beq	$t0,	$0,	Label88
		# BEQZ	tv128,	Label88
Label86:
		# Label86
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	4080($sp)
		# li	tv129,	1
	lw	$t0,	4064($sp)
	lw	$t1,	4080($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	4084($sp)
		# add	tv130,	lc108,	tv129
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	4084
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc109,	tv130
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	4088($sp)
		# slt	tv131,	lc109,	gb44
	lw	$t0,	4088($sp)
	beq	$t0,	$0,	Label95
		# BEQZ	tv131,	Label95
Label93:
		# Label93
	addiu	$t0,	$sp,	4092
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv132,	lc111
	lw	$t0,	4064($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4096($sp)
		# mul	tv134,	lc108,	4
	lw	$t0,	4092($sp)
	lw	$t1,	4096($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4100($sp)
		# add	tv133,	tv132,	tv134
	lw	$t0,	4100($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4104($sp)
		# add	ta135,	tv133,	0
	addiu	$t0,	$sp,	4108
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv136,	lc111
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4112($sp)
		# mul	tv138,	lc109,	4
	lw	$t0,	4108($sp)
	lw	$t1,	4112($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4116($sp)
		# add	tv137,	tv136,	tv138
	lw	$t0,	4116($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4120($sp)
		# add	ta139,	tv137,	0
	lw	$t0,	4120($sp)
	lw	$t0,	0($t0)
	lw	$t1,	4104($sp)
	lw	$t1,	0($t1)
	slt	$t2,	$t0,	$t1
	sw	$t2,	4124($sp)
		# slt	tv140,	ta139,	ta135
	lw	$t0,	4124($sp)
	beq	$t0,	$0,	Label98
		# BEQZ	tv140,	Label98
	addiu	$t0,	$sp,	4128
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv141,	lc111
	lw	$t0,	4064($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4132($sp)
		# mul	tv143,	lc108,	4
	lw	$t0,	4128($sp)
	lw	$t1,	4132($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4136($sp)
		# add	tv142,	tv141,	tv143
	lw	$t0,	4136($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4140($sp)
		# add	ta144,	tv142,	0
	addiu	$t0,	$sp,	4144
	lw	$t1,	4140($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc112,	ta144
	addiu	$t0,	$sp,	4148
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv145,	lc111
	lw	$t0,	4064($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4152($sp)
		# mul	tv147,	lc108,	4
	lw	$t0,	4148($sp)
	lw	$t1,	4152($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4156($sp)
		# add	tv146,	tv145,	tv147
	lw	$t0,	4156($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4160($sp)
		# add	ta148,	tv146,	0
	addiu	$t0,	$sp,	4164
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv149,	lc111
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4168($sp)
		# mul	tv151,	lc109,	4
	lw	$t0,	4164($sp)
	lw	$t1,	4168($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4172($sp)
		# add	tv150,	tv149,	tv151
	lw	$t0,	4172($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4176($sp)
		# add	ta152,	tv150,	0
	lw	$t0,	4160($sp)
	lw	$t1,	4176($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta148,	ta152
	addiu	$t0,	$sp,	4180
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv153,	lc111
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4184($sp)
		# mul	tv155,	lc109,	4
	lw	$t0,	4180($sp)
	lw	$t1,	4184($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4188($sp)
		# add	tv154,	tv153,	tv155
	lw	$t0,	4188($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4192($sp)
		# add	ta156,	tv154,	0
	lw	$t0,	4192($sp)
	addiu	$t1,	$sp,	4144
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta156,	lc112
Label98:
		# Label98
Label94:
		# Label94
	lw	$t0,	4($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	4($sp)
		# add	lc109,	lc109,	1
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	4196($sp)
		# slt	tv157,	lc109,	gb44
	lw	$t0,	4196($sp)
	bne	$t0,	$0,	Label93
		# BNEZ	tv157,	Label93
Label95:
		# Label95
Label87:
		# Label87
	lw	$t0,	4064($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	4064($sp)
		# add	lc108,	lc108,	1
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	4200($sp)
		# li	tv158,	1
	lw	$t0,	gb44
	lw	$t1,	4200($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	4204($sp)
		# sub	tv159,	gb44,	tv158
	lw	$t0,	4064($sp)
	lw	$t1,	4204($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	4208($sp)
		# slt	tv160,	lc108,	tv159
	lw	$t0,	4208($sp)
	bne	$t0,	$0,	Label86
		# BNEZ	tv160,	Label86
Label88:
		# Label88
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	4212($sp)
		# li	tv161,	0
	addiu	$t0,	$sp,	4064
	addiu	$t1,	$sp,	4212
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc108,	tv161
	lw	$t0,	4064($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	4216($sp)
		# slt	tv162,	lc108,	gb44
	lw	$t0,	4216($sp)
	beq	$t0,	$0,	Label109
		# BEQZ	tv162,	Label109
Label107:
		# Label107
	addiu	$t0,	$sp,	4220
	addiu	$t1,	$sp,	28
	sw	$t1,	0($t0)
		# GET&	tv163,	lc111
	lw	$t0,	4064($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	4224($sp)
		# mul	tv165,	lc108,	4
	lw	$t0,	4220($sp)
	lw	$t1,	4224($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	4228($sp)
		# add	tv164,	tv163,	tv165
	lw	$t0,	4228($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	4232($sp)
		# add	ta166,	tv164,	0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	4236($sp)
		# li	tv167,	1
	lw	$t0,	4064($sp)
	lw	$t1,	4236($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	4240($sp)
		# add	tv168,	lc108,	tv167
	lw	$t0,	4232($sp)
	lw	$t0,	0($t0)
	lw	$t1,	4240($sp)
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	4244($sp)
		# neq	tv169,	ta166,	tv168
	lw	$t0,	4244($sp)
	beq	$t0,	$0,	Label114
		# BEQZ	tv169,	Label114
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	4248($sp)
		# li	tv170,	0
	addiu	$t1,	$sp,	4248
	lw	$v0,	0($t1)
		# RM	$v0,	tv170
	j	Label71
		# J	Label71
Label114:
		# Label114
Label108:
		# Label108
	lw	$t0,	4064($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	4064($sp)
		# add	lc108,	lc108,	1
	lw	$t0,	4064($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	4252($sp)
		# slt	tv171,	lc108,	gb44
	lw	$t0,	4252($sp)
	bne	$t0,	$0,	Label107
		# BNEZ	tv171,	Label107
Label109:
		# Label109
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	4256($sp)
		# li	tv172,	1
	addiu	$t1,	$sp,	4256
	lw	$v0,	0($t1)
		# RM	$v0,	tv172
	j	Label71
		# J	Label71
Label71:
	lw	$ra,	4260($sp)
	addiu	$sp,	$sp,	4264
	jr	$ra


Label117:
	addiu	$sp,	$sp,	-128
	sw	$ra,	124($sp)
Label118:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv174,	0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc173,	tv174
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# slt	tv175,	lc173,	gb44
	lw	$t0,	16($sp)
	beq	$t0,	$0,	Label124
		# BEQZ	tv175,	Label124
Label122:
		# Label122
	addiu	$t0,	$sp,	20
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv176,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# mul	tv178,	lc173,	4
	lw	$t0,	20($sp)
	lw	$t1,	24($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	tv177,	tv176,	tv178
	lw	$t0,	28($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	ta179,	tv177,	0
	lw	$t0,	32($sp)
	lw	$t0,	0($t0)
	li	$t1,	0
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	36($sp)
		# eq	tv180,	ta179,	0
	lw	$t0,	36($sp)
	beq	$t0,	$0,	Label125
		# BEQZ	tv180,	Label125
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	40($sp)
		# li	tv182,	1
	lw	$t0,	12($sp)
	lw	$t1,	40($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# add	tv183,	lc173,	tv182
	addiu	$t0,	$sp,	48
	addiu	$t1,	$sp,	44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc181,	tv183
	lw	$t0,	48($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# slt	tv184,	lc181,	gb44
	lw	$t0,	52($sp)
	beq	$t0,	$0,	Label132
		# BEQZ	tv184,	Label132
Label130:
		# Label130
	addiu	$t0,	$sp,	56
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv185,	gb45
	lw	$t0,	48($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# mul	tv187,	lc181,	4
	lw	$t0,	56($sp)
	lw	$t1,	60($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	tv186,	tv185,	tv187
	lw	$t0,	64($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	68($sp)
		# add	ta188,	tv186,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	72($sp)
		# li	tv189,	0
	lw	$t0,	68($sp)
	lw	$t0,	0($t0)
	lw	$t1,	72($sp)
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	76($sp)
		# neq	tv190,	ta188,	tv189
	lw	$t0,	76($sp)
	beq	$t0,	$0,	Label135
		# BEQZ	tv190,	Label135
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr191,	lc173
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr192,	lc181
	lw	$a0,	0($sp)
	jal	Label35
		# CALL	Label35,	cr191,	cr192
	addiu	$t0,	$sp,	80
	sb	$v0,	0($t0)
		# RM	tv193,	$v0
	j	Label132
		# J	Label132
Label135:
		# Label135
Label131:
		# Label131
	lw	$t0,	48($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# add	lc181,	lc181,	1
	lw	$t0,	48($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	84($sp)
		# slt	tv194,	lc181,	gb44
	lw	$t0,	84($sp)
	bne	$t0,	$0,	Label130
		# BNEZ	tv194,	Label130
Label132:
		# Label132
Label125:
		# Label125
Label123:
		# Label123
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc173,	lc173,	1
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	88($sp)
		# slt	tv195,	lc173,	gb44
	lw	$t0,	88($sp)
	bne	$t0,	$0,	Label122
		# BNEZ	tv195,	Label122
Label124:
		# Label124
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	92($sp)
		# li	tv196,	0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	92
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc173,	tv196
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	96($sp)
		# slt	tv197,	lc173,	gb44
	lw	$t0,	96($sp)
	beq	$t0,	$0,	Label144
		# BEQZ	tv197,	Label144
Label142:
		# Label142
	addiu	$t0,	$sp,	100
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv198,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	104($sp)
		# mul	tv200,	lc173,	4
	lw	$t0,	100($sp)
	lw	$t1,	104($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	108($sp)
		# add	tv199,	tv198,	tv200
	lw	$t0,	108($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	112($sp)
		# add	ta201,	tv199,	0
	lw	$t0,	112($sp)
	lw	$t0,	0($t0)
	li	$t1,	0
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	116($sp)
		# eq	tv202,	ta201,	0
	lw	$t0,	116($sp)
	beq	$t0,	$0,	Label145
		# BEQZ	tv202,	Label145
	la	$t0,	gb44
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb44,	lc173
	j	Label144
		# J	Label144
Label145:
		# Label145
Label143:
		# Label143
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc173,	lc173,	1
	lw	$t0,	12($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	120($sp)
		# slt	tv203,	lc173,	gb44
	lw	$t0,	120($sp)
	bne	$t0,	$0,	Label142
		# BNEZ	tv203,	Label142
Label144:
		# Label144
Label119:
	lw	$ra,	124($sp)
	addiu	$sp,	$sp,	128
	jr	$ra


Label148:
	addiu	$sp,	$sp,	-68
	sw	$ra,	64($sp)
Label149:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv205,	0
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc204,	tv205
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# slt	tv206,	lc204,	gb44
	lw	$t0,	12($sp)
	beq	$t0,	$0,	Label155
		# BEQZ	tv206,	Label155
Label153:
		# Label153
Label154:
		# Label154
	addiu	$t0,	$sp,	16
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv207,	gb45
	lw	$t0,	4($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# mul	tv209,	lc204,	4
	lw	$t0,	16($sp)
	lw	$t1,	20($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# add	tv208,	tv207,	tv209
	lw	$t0,	24($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	ta210,	tv208,	0
	lw	$t0,	28($sp)
	lw	$t0,	0($t0)
	li	$t1,	1
	sub	$t2,	$t0,	$t1
	lw	$t0,	28($sp)
	sw	$t2,	0($t0)
		# sub	ta210,	ta210,	1
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	32($sp)
		# li	tv211,	1
	lw	$t0,	4($sp)
	lw	$t1,	32($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv212,	lc204,	tv211
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc204,	tv212
	lw	$t0,	4($sp)
	lw	$t1,	gb44
	slt	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# slt	tv213,	lc204,	gb44
	lw	$t0,	40($sp)
	bne	$t0,	$0,	Label153
		# BNEZ	tv213,	Label153
Label155:
		# Label155
	addiu	$t0,	$sp,	44
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv214,	gb45
	lw	$t0,	gb44
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# mul	tv216,	gb44,	4
	lw	$t0,	44($sp)
	lw	$t1,	48($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# add	tv215,	tv214,	tv216
	lw	$t0,	52($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	56($sp)
		# add	ta217,	tv215,	0
	lw	$t0,	56($sp)
	la	$t1,	gb44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta217,	gb44
	addiu	$t0,	$sp,	60
	la	$t1,	gb44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv218,	gb44
	lw	$t0,	gb44
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	gb44
		# add	gb44,	gb44,	1
Label150:
	lw	$ra,	64($sp)
	addiu	$sp,	$sp,	68
	jr	$ra


Label160:
	addiu	$sp,	$sp,	-320
	sw	$ra,	316($sp)
Label161:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv220,	0
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc219,	tv220
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	16($sp)
		# li	tv222,	0
	addiu	$t0,	$sp,	20
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc221,	tv222
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	24($sp)
		# li	tv224,	0
	addiu	$t0,	$sp,	28
	addiu	$t1,	$sp,	24
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc223,	tv224
	lw	$t0,	gb47
	lw	$t1,	gb46
	div	$t0,	$t1
	mflo	$t2
	sw	$t2,	32($sp)
		# div	tv225,	gb47,	gb46
	la	$t0,	gb48
	addiu	$t1,	$sp,	32
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb48,	tv225
	lw	$t0,	gb47
	lw	$t1,	gb46
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	36($sp)
		# mod	tv226,	gb47,	gb46
	la	$t0,	gb49
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb49,	tv226
	addiu	$t0,	$sp,	0
	la	$t1,	gb42
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr227,	gb42
	lw	$a0,	0($sp)
	jal	Label38
		# CALL	Label38,	cr227
	addiu	$t0,	$sp,	40
	sw	$v0,	0($t0)
		# RM	tv228,	$v0
	lw	$t0,	40($sp)
	li	$t1,	0
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	44($sp)
		# eq	tv229,	tv228,	0
	lw	$t0,	44($sp)
	beq	$t0,	$0,	Label167
		# BEQZ	tv229,	Label167
	la	$t0,	Label168
	or	$t2,	$0,	$t0
	sw	$t2,	48($sp)
		# la	tv230,	Label168
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr231,	tv230
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr231
	addiu	$t0,	$sp,	52
	sb	$v0,	0($t0)
		# RM	tv232,	$v0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	56($sp)
		# li	tv233,	1
	addiu	$t1,	$sp,	56
	lw	$v0,	0($t1)
		# RM	$v0,	tv233
	j	Label162
		# J	Label162
Label167:
		# Label167
	la	$t0,	Label169
	or	$t2,	$0,	$t0
	sw	$t2,	60($sp)
		# la	tv234,	Label169
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	60
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr235,	tv234
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr235
	addiu	$t0,	$sp,	64
	sb	$v0,	0($t0)
		# RM	tv236,	$v0
	li	$t0,	3654898
	or	$t2,	$0,	$t0
	sw	$t2,	68($sp)
		# li	tv237,	3654898
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	68
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr238,	tv237
	lw	$a0,	0($sp)
	jal	Label32
		# CALL	Label32,	cr238
	addiu	$t0,	$sp,	72
	sb	$v0,	0($t0)
		# RM	tv239,	$v0
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	76
	sw	$v0,	0($t0)
		# RM	tv240,	$v0
	li	$t0,	10
	or	$t2,	$0,	$t0
	sw	$t2,	80($sp)
		# li	tv241,	10
	lw	$t0,	76($sp)
	lw	$t1,	80($sp)
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	84($sp)
		# mod	tv242,	tv240,	tv241
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	88($sp)
		# li	tv243,	1
	lw	$t0,	84($sp)
	lw	$t1,	88($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	92($sp)
		# add	tv244,	tv242,	tv243
	la	$t0,	gb44
	addiu	$t1,	$sp,	92
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb44,	tv244
	la	$t0,	Label174
	or	$t2,	$0,	$t0
	sw	$t2,	96($sp)
		# la	tv245,	Label174
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	96
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr246,	tv245
	addiu	$t0,	$sp,	4
	la	$t1,	gb44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr247,	gb44
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr246,	cr247
	addiu	$t0,	$sp,	100
	sb	$v0,	0($t0)
		# RM	tv248,	$v0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	104($sp)
		# li	tv249,	1
	lw	$t0,	gb44
	lw	$t1,	104($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	108($sp)
		# sub	tv250,	gb44,	tv249
	lw	$t0,	12($sp)
	lw	$t1,	108($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	112($sp)
		# slt	tv251,	lc219,	tv250
	lw	$t0,	112($sp)
	beq	$t0,	$0,	Label181
		# BEQZ	tv251,	Label181
Label179:
		# Label179
	addiu	$t0,	$sp,	116
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv252,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	120($sp)
		# mul	tv254,	lc219,	4
	lw	$t0,	116($sp)
	lw	$t1,	120($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	124($sp)
		# add	tv253,	tv252,	tv254
	lw	$t0,	124($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	128($sp)
		# add	ta255,	tv253,	0
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	132
	sw	$v0,	0($t0)
		# RM	tv256,	$v0
	li	$t0,	10
	or	$t2,	$0,	$t0
	sw	$t2,	136($sp)
		# li	tv257,	10
	lw	$t0,	132($sp)
	lw	$t1,	136($sp)
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	140($sp)
		# mod	tv258,	tv256,	tv257
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	144($sp)
		# li	tv259,	1
	lw	$t0,	140($sp)
	lw	$t1,	144($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	148($sp)
		# add	tv260,	tv258,	tv259
	lw	$t0,	128($sp)
	addiu	$t1,	$sp,	148
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta255,	tv260
Label186:
		# Label186
	addiu	$t0,	$sp,	152
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv261,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	156($sp)
		# mul	tv263,	lc219,	4
	lw	$t0,	152($sp)
	lw	$t1,	156($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	160($sp)
		# add	tv262,	tv261,	tv263
	lw	$t0,	160($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	164($sp)
		# add	ta264,	tv262,	0
	lw	$t0,	164($sp)
	lw	$t0,	0($t0)
	lw	$t1,	20($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	168($sp)
		# add	tv265,	ta264,	lc221
	lw	$t0,	gb42
	lw	$t1,	168($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	172($sp)
		# slt	tv266,	gb42,	tv265
	lw	$t0,	172($sp)
	beq	$t0,	$0,	Label187
		# BEQZ	tv266,	Label187
	addiu	$t0,	$sp,	176
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv267,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	180($sp)
		# mul	tv269,	lc219,	4
	lw	$t0,	176($sp)
	lw	$t1,	180($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	184($sp)
		# add	tv268,	tv267,	tv269
	lw	$t0,	184($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	188($sp)
		# add	ta270,	tv268,	0
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3
	addiu	$t0,	$sp,	192
	sw	$v0,	0($t0)
		# RM	tv271,	$v0
	li	$t0,	10
	or	$t2,	$0,	$t0
	sw	$t2,	196($sp)
		# li	tv272,	10
	lw	$t0,	192($sp)
	lw	$t1,	196($sp)
	div	$t0,	$t1
	mfhi	$t2
	sw	$t2,	200($sp)
		# mod	tv273,	tv271,	tv272
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	204($sp)
		# li	tv274,	1
	lw	$t0,	200($sp)
	lw	$t1,	204($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	208($sp)
		# add	tv275,	tv273,	tv274
	lw	$t0,	188($sp)
	addiu	$t1,	$sp,	208
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta270,	tv275
	j	Label186
		# J	Label186
Label187:
		# Label187
	addiu	$t0,	$sp,	212
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv276,	gb45
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	216($sp)
		# mul	tv278,	lc219,	4
	lw	$t0,	212($sp)
	lw	$t1,	216($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	220($sp)
		# add	tv277,	tv276,	tv278
	lw	$t0,	220($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	224($sp)
		# add	ta279,	tv277,	0
	lw	$t0,	20($sp)
	lw	$t1,	224($sp)
	lw	$t1,	0($t1)
	add	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	lc221,	lc221,	ta279
Label180:
		# Label180
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc219,	lc219,	1
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	228($sp)
		# li	tv280,	1
	lw	$t0,	gb44
	lw	$t1,	228($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	232($sp)
		# sub	tv281,	gb44,	tv280
	lw	$t0,	12($sp)
	lw	$t1,	232($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	236($sp)
		# slt	tv282,	lc219,	tv281
	lw	$t0,	236($sp)
	bne	$t0,	$0,	Label179
		# BNEZ	tv282,	Label179
Label181:
		# Label181
	addiu	$t0,	$sp,	240
	la	$t1,	gb45
	sw	$t1,	0($t0)
		# GET&	tv283,	gb45
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	244($sp)
		# li	tv285,	1
	lw	$t0,	gb44
	lw	$t1,	244($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	248($sp)
		# sub	tv286,	gb44,	tv285
	lw	$t0,	248($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	252($sp)
		# mul	tv287,	tv286,	4
	lw	$t0,	240($sp)
	lw	$t1,	252($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	256($sp)
		# add	tv284,	tv283,	tv287
	lw	$t0,	256($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	260($sp)
		# add	ta288,	tv284,	0
	lw	$t0,	gb42
	lw	$t1,	20($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	264($sp)
		# sub	tv289,	gb42,	lc221
	lw	$t0,	260($sp)
	addiu	$t1,	$sp,	264
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta288,	tv289
	lw	$a0,	0($sp)
	jal	Label57
		# CALL	Label57
	addiu	$t0,	$sp,	268
	sb	$v0,	0($t0)
		# RM	tv290,	$v0
	lw	$a0,	0($sp)
	jal	Label117
		# CALL	Label117
	addiu	$t0,	$sp,	272
	sb	$v0,	0($t0)
		# RM	tv291,	$v0
Label204:
		# Label204
	lw	$a0,	0($sp)
	jal	Label69
		# CALL	Label69
	addiu	$t0,	$sp,	276
	sw	$v0,	0($t0)
		# RM	tv292,	$v0
	lw	$t0,	276($sp)
	li	$t1,	0
	xor	$t2,	$t0,	$t1
	sltiu	$t2,	$t2,	1
	sw	$t2,	280($sp)
		# eq	tv293,	tv292,	0
	lw	$t0,	280($sp)
	beq	$t0,	$0,	Label205
		# BEQZ	tv293,	Label205
	la	$t0,	Label206
	or	$t2,	$0,	$t0
	sw	$t2,	284($sp)
		# la	tv294,	Label206
	lw	$t0,	28($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	lc223,	lc223,	1
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	284
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr295,	tv294
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	28
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr296,	lc223
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr295,	cr296
	addiu	$t0,	$sp,	288
	sb	$v0,	0($t0)
		# RM	tv297,	$v0
	lw	$a0,	0($sp)
	jal	Label148
		# CALL	Label148
	addiu	$t0,	$sp,	292
	sb	$v0,	0($t0)
		# RM	tv298,	$v0
	lw	$a0,	0($sp)
	jal	Label117
		# CALL	Label117
	addiu	$t0,	$sp,	296
	sb	$v0,	0($t0)
		# RM	tv299,	$v0
	lw	$a0,	0($sp)
	jal	Label57
		# CALL	Label57
	addiu	$t0,	$sp,	300
	sb	$v0,	0($t0)
		# RM	tv300,	$v0
	j	Label204
		# J	Label204
Label205:
		# Label205
	la	$t0,	Label207
	or	$t2,	$0,	$t0
	sw	$t2,	304($sp)
		# la	tv301,	Label207
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	304
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr302,	tv301
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	28
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr303,	lc223
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr302,	cr303
	addiu	$t0,	$sp,	308
	sb	$v0,	0($t0)
		# RM	tv304,	$v0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	312($sp)
		# li	tv305,	0
	addiu	$t1,	$sp,	312
	lw	$v0,	0($t1)
		# RM	$v0,	tv305
	j	Label162
		# J	Label162
Label162:
	lw	$ra,	316($sp)
	addiu	$sp,	$sp,	320
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
