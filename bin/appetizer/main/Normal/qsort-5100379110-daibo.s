	.sdata
	.align	2
Label50:
	.asciiz	"%d \000"
	.align	2
Label53:
	.asciiz	"\n\000"
	.data
	.align 2
gb42:
	.space	40400
	.align 2
gb43:
	.word	10000
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label31
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-208
	sw	$ra,	204($sp)
Label4:
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	208
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc46,	ce44
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	212
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc47,	ce45
	addiu	$t0,	$sp,	16
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv49,	gb42
	lw	$t0,	208($sp)
	lw	$t1,	212($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# add	tv51,	ce44,	ce45
	li	$t0,	2
	or	$t2,	$0,	$t0
	sw	$t2,	24($sp)
		# li	tv52,	2
	lw	$t0,	20($sp)
	lw	$t1,	24($sp)
	div	$t0,	$t1
	mflo	$t2
	sw	$t2,	28($sp)
		# div	tv53,	tv51,	tv52
	lw	$t0,	28($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# mul	tv54,	tv53,	4
	lw	$t0,	16($sp)
	lw	$t1,	32($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	36($sp)
		# add	tv50,	tv49,	tv54
	lw	$t0,	36($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	ta55,	tv50,	0
	addiu	$t0,	$sp,	44
	lw	$t1,	40($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc48,	ta55
Label10:
		# Label10
	lw	$t0,	8($sp)
	lw	$t1,	12($sp)
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	48($sp)
		# leq	tv56,	lc46,	lc47
	lw	$t0,	48($sp)
	beq	$t0,	$0,	Label11
		# BEQZ	tv56,	Label11
Label14:
		# Label14
	addiu	$t0,	$sp,	52
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv57,	gb42
	lw	$t0,	8($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	56($sp)
		# mul	tv59,	lc46,	4
	lw	$t0,	52($sp)
	lw	$t1,	56($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# add	tv58,	tv57,	tv59
	lw	$t0,	60($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	ta60,	tv58,	0
	lw	$t0,	64($sp)
	lw	$t0,	0($t0)
	lw	$t1,	44($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	68($sp)
		# slt	tv61,	ta60,	lc48
	lw	$t0,	68($sp)
	beq	$t0,	$0,	Label15
		# BEQZ	tv61,	Label15
	addiu	$t0,	$sp,	72
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv62,	lc46
	lw	$t0,	8($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	lc46,	lc46,	1
	j	Label14
		# J	Label14
Label15:
		# Label15
Label18:
		# Label18
	addiu	$t0,	$sp,	76
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv63,	gb42
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	80($sp)
		# mul	tv65,	lc47,	4
	lw	$t0,	76($sp)
	lw	$t1,	80($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	84($sp)
		# add	tv64,	tv63,	tv65
	lw	$t0,	84($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	88($sp)
		# add	ta66,	tv64,	0
	lw	$t0,	44($sp)
	lw	$t1,	88($sp)
	lw	$t1,	0($t1)
	slt	$t2,	$t0,	$t1
	sw	$t2,	92($sp)
		# slt	tv67,	lc48,	ta66
	lw	$t0,	92($sp)
	beq	$t0,	$0,	Label19
		# BEQZ	tv67,	Label19
	addiu	$t0,	$sp,	96
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv68,	lc47
	lw	$t0,	12($sp)
	li	$t1,	1
	sub	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# sub	lc47,	lc47,	1
	j	Label18
		# J	Label18
Label19:
		# Label19
	lw	$t0,	8($sp)
	lw	$t1,	12($sp)
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	100($sp)
		# leq	tv69,	lc46,	lc47
	lw	$t0,	100($sp)
	beq	$t0,	$0,	Label24
		# BEQZ	tv69,	Label24
	addiu	$t0,	$sp,	104
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv71,	gb42
	lw	$t0,	8($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	108($sp)
		# mul	tv73,	lc46,	4
	lw	$t0,	104($sp)
	lw	$t1,	108($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	112($sp)
		# add	tv72,	tv71,	tv73
	lw	$t0,	112($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	116($sp)
		# add	ta74,	tv72,	0
	addiu	$t0,	$sp,	120
	lw	$t1,	116($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc70,	ta74
	addiu	$t0,	$sp,	124
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv75,	gb42
	lw	$t0,	8($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	128($sp)
		# mul	tv77,	lc46,	4
	lw	$t0,	124($sp)
	lw	$t1,	128($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	132($sp)
		# add	tv76,	tv75,	tv77
	lw	$t0,	132($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	136($sp)
		# add	ta78,	tv76,	0
	addiu	$t0,	$sp,	140
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv79,	gb42
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	144($sp)
		# mul	tv81,	lc47,	4
	lw	$t0,	140($sp)
	lw	$t1,	144($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	148($sp)
		# add	tv80,	tv79,	tv81
	lw	$t0,	148($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	152($sp)
		# add	ta82,	tv80,	0
	lw	$t0,	136($sp)
	lw	$t1,	152($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta78,	ta82
	addiu	$t0,	$sp,	156
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv83,	gb42
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	160($sp)
		# mul	tv85,	lc47,	4
	lw	$t0,	156($sp)
	lw	$t1,	160($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	164($sp)
		# add	tv84,	tv83,	tv85
	lw	$t0,	164($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	168($sp)
		# add	ta86,	tv84,	0
	lw	$t0,	168($sp)
	addiu	$t1,	$sp,	120
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta86,	lc70
	addiu	$t0,	$sp,	172
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv87,	lc46
	lw	$t0,	8($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	8($sp)
		# add	lc46,	lc46,	1
	addiu	$t0,	$sp,	176
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv88,	lc47
	lw	$t0,	12($sp)
	li	$t1,	1
	sub	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# sub	lc47,	lc47,	1
Label24:
		# Label24
	j	Label10
		# J	Label10
Label11:
		# Label11
	lw	$t0,	208($sp)
	lw	$t1,	12($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	180($sp)
		# slt	tv89,	ce44,	lc47
	lw	$t0,	180($sp)
	beq	$t0,	$0,	Label27
		# BEQZ	tv89,	Label27
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	208
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr90,	ce44
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr91,	lc47
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr90,	cr91
	addiu	$t0,	$sp,	184
	sw	$v0,	0($t0)
		# RM	tv92,	$v0
Label27:
		# Label27
	lw	$t0,	8($sp)
	lw	$t1,	212($sp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	192($sp)
		# slt	tv93,	lc46,	ce45
	lw	$t0,	192($sp)
	beq	$t0,	$0,	Label30
		# BEQZ	tv93,	Label30
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr94,	lc46
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	212
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr95,	ce45
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr94,	cr95
	addiu	$t0,	$sp,	196
	sw	$v0,	0($t0)
		# RM	tv96,	$v0
Label30:
		# Label30
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	200($sp)
		# li	tv97,	0
	addiu	$t1,	$sp,	200
	lw	$v0,	0($t1)
		# RM	$v0,	tv97
	j	Label5
		# J	Label5
Label5:
	lw	$ra,	204($sp)
	addiu	$sp,	$sp,	208
	jr	$ra


Label31:
	addiu	$sp,	$sp,	-120
	sw	$ra,	116($sp)
Label32:
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv99,	1
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc98,	tv99
	lw	$t0,	12($sp)
	lw	$t1,	gb43
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	16($sp)
		# leq	tv100,	lc98,	gb43
	lw	$t0,	16($sp)
	beq	$t0,	$0,	Label38
		# BEQZ	tv100,	Label38
Label36:
		# Label36
	addiu	$t0,	$sp,	20
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv101,	gb42
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	24($sp)
		# mul	tv103,	lc98,	4
	lw	$t0,	20($sp)
	lw	$t1,	24($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# add	tv102,	tv101,	tv103
	lw	$t0,	28($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# add	ta104,	tv102,	0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	36($sp)
		# li	tv105,	1
	lw	$t0,	gb43
	lw	$t1,	36($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# add	tv106,	gb43,	tv105
	lw	$t0,	40($sp)
	lw	$t1,	12($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	44($sp)
		# sub	tv107,	tv106,	lc98
	lw	$t0,	32($sp)
	addiu	$t1,	$sp,	44
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	ta104,	tv107
Label37:
		# Label37
	addiu	$t0,	$sp,	48
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv108,	lc98
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc98,	lc98,	1
	lw	$t0,	12($sp)
	lw	$t1,	gb43
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	52($sp)
		# leq	tv109,	lc98,	gb43
	lw	$t0,	52($sp)
	bne	$t0,	$0,	Label36
		# BNEZ	tv109,	Label36
Label38:
		# Label38
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	56($sp)
		# li	tv110,	1
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	56
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr111,	tv110
	addiu	$t0,	$sp,	4
	la	$t1,	gb43
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr112,	gb43
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr111,	cr112
	addiu	$t0,	$sp,	60
	sw	$v0,	0($t0)
		# RM	tv113,	$v0
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	64($sp)
		# li	tv114,	1
	addiu	$t0,	$sp,	12
	addiu	$t1,	$sp,	64
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc98,	tv114
	lw	$t0,	12($sp)
	lw	$t1,	gb43
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	68($sp)
		# leq	tv115,	lc98,	gb43
	lw	$t0,	68($sp)
	beq	$t0,	$0,	Label49
		# BEQZ	tv115,	Label49
Label47:
		# Label47
	la	$t0,	Label50
	or	$t2,	$0,	$t0
	sw	$t2,	72($sp)
		# la	tv116,	Label50
	addiu	$t0,	$sp,	76
	la	$t1,	gb42
	sw	$t1,	0($t0)
		# GET&	tv117,	gb42
	lw	$t0,	12($sp)
	li	$t1,	4
	mul	$t2,	$t0,	$t1
	sw	$t2,	80($sp)
		# mul	tv119,	lc98,	4
	lw	$t0,	76($sp)
	lw	$t1,	80($sp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	84($sp)
		# add	tv118,	tv117,	tv119
	lw	$t0,	84($sp)
	li	$t1,	0
	addu	$t2,	$t0,	$t1
	sw	$t2,	88($sp)
		# add	ta120,	tv118,	0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	72
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr121,	tv116
	addiu	$t0,	$sp,	4
	lw	$t1,	88($sp)
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr122,	ta120
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr121,	cr122
	addiu	$t0,	$sp,	92
	sb	$v0,	0($t0)
		# RM	tv123,	$v0
Label48:
		# Label48
	addiu	$t0,	$sp,	96
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv124,	lc98
	lw	$t0,	12($sp)
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	12($sp)
		# add	lc98,	lc98,	1
	lw	$t0,	12($sp)
	lw	$t1,	gb43
	slt	$t2,	$t1,	$t0
	xori	$t2,	$t2,	1
	sw	$t2,	100($sp)
		# leq	tv125,	lc98,	gb43
	lw	$t0,	100($sp)
	bne	$t0,	$0,	Label47
		# BNEZ	tv125,	Label47
Label49:
		# Label49
	la	$t0,	Label53
	or	$t2,	$0,	$t0
	sw	$t2,	104($sp)
		# la	tv126,	Label53
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	104
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr127,	tv126
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr127
	addiu	$t0,	$sp,	108
	sb	$v0,	0($t0)
		# RM	tv128,	$v0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	112($sp)
		# li	tv129,	0
	addiu	$t1,	$sp,	112
	lw	$v0,	0($t1)
		# RM	$v0,	tv129
	j	Label33
		# J	Label33
Label33:
	lw	$ra,	116($sp)
	addiu	$sp,	$sp,	120
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
