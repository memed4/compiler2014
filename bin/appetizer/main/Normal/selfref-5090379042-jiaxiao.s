	.sdata
	.align	2
Label9:
	.asciiz	"%d\n\000"
	.data
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label6
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-16
	sw	$ra,	12($sp)
Label4:
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	8($sp)
		# li	tv43,	0
	addiu	$t1,	$sp,	8
	lw	$v0,	0($t1)
		# RM	$v0,	tv43
	j	Label5
		# J	Label5
Label5:
	lw	$ra,	12($sp)
	addiu	$sp,	$sp,	16
	jr	$ra


Label6:
	addiu	$sp,	$sp,	-28
	sw	$ra,	24($sp)
Label7:
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr45,	lc44
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr45
	addiu	$t0,	$sp,	12
	sw	$v0,	0($t0)
		# RM	tv46,	$v0
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc44,	tv46
	la	$t0,	Label9
	or	$t2,	$0,	$t0
	sw	$t2,	16($sp)
		# la	tv47,	Label9
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	16
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr48,	tv47
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	8
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr49,	lc44
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr48,	cr49
	addiu	$t0,	$sp,	20
	sb	$v0,	0($t0)
		# RM	tv50,	$v0
	addiu	$t1,	$sp,	8
	lw	$v0,	0($t1)
		# RM	$v0,	lc44
	j	Label8
		# J	Label8
Label8:
	lw	$ra,	24($sp)
	addiu	$sp,	$sp,	28
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
