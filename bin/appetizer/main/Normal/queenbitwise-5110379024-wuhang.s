	.sdata
	.align	2
Label51:
	.asciiz	"%d queens:\n\000"
	.align	2
Label56:
	.asciiz	"%d\n\000"
	.data
	.align 2
gb42:
	.word	8
	.align 2
gb43:
	.word	0
	.align 2
gb44:
	.word	1
static_end:
	.word	static_end + 4
	.text
main:
	jal	Label48
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
syscall


Label3:
	addiu	$sp,	$sp,	-92
	sw	$ra,	88($sp)
Label4:
	lw	$t0,	92($sp)
	lw	$t1,	gb44
	xor	$t2,	$t0,	$t1
	sltu	$t2,	$0,	$t2
	sw	$t2,	12($sp)
		# neq	tv48,	ce45,	gb44
	lw	$t0,	12($sp)
	beq	$t0,	$0,	Label8
		# BEQZ	tv48,	Label8
	lw	$t0,	92($sp)
	lw	$t1,	96($sp)
	or	$t2,	$t0,	$t1
	sw	$t2,	16($sp)
		# or	tv50,	ce45,	ce46
	lw	$t0,	16($sp)
	lw	$t1,	100($sp)
	or	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# or	tv51,	tv50,	ce47
	lw	$t0,	20($sp)
	not	$t2,	$t0
	sw	$t2,	24($sp)
		# not	tv52,	tv51
	lw	$t0,	gb44
	lw	$t1,	24($sp)
	and	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# and	tv53,	gb44,	tv52
	addiu	$t0,	$sp,	32
	addiu	$t1,	$sp,	28
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc49,	tv53
Label21:
		# Label21
	lw	$t0,	32($sp)
	beq	$t0,	$0,	Label22
		# BEQZ	lc49,	Label22
	lw	$t0,	32($sp)
	neg	$t2,	$t0
	sw	$t2,	36($sp)
		# neg	tv55,	lc49
	lw	$t0,	32($sp)
	lw	$t1,	36($sp)
	and	$t2,	$t0,	$t1
	sw	$t2,	40($sp)
		# and	tv56,	lc49,	tv55
	addiu	$t0,	$sp,	44
	addiu	$t1,	$sp,	40
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	lc54,	tv56
	lw	$t0,	32($sp)
	lw	$t1,	44($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	32($sp)
		# sub	lc49,	lc49,	lc54
	lw	$t0,	92($sp)
	lw	$t1,	44($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	48($sp)
		# add	tv57,	ce45,	lc54
	lw	$t0,	96($sp)
	lw	$t1,	44($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	52($sp)
		# add	tv58,	ce46,	lc54
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	56($sp)
		# li	tv59,	1
	lw	$t0,	52($sp)
	lw	$t1,	56($sp)
	sll	$t2,	$t0,	$t1
	sw	$t2,	60($sp)
		# sll	tv60,	tv58,	tv59
	lw	$t0,	100($sp)
	lw	$t1,	44($sp)
	add	$t2,	$t0,	$t1
	sw	$t2,	64($sp)
		# add	tv61,	ce47,	lc54
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	68($sp)
		# li	tv62,	1
	lw	$t0,	64($sp)
	lw	$t1,	68($sp)
	sra	$t2,	$t0,	$t1
	sw	$t2,	72($sp)
		# sra	tv63,	tv61,	tv62
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr64,	tv57
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	60
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr65,	tv60
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	72
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr66,	tv63
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr64,	cr65,	cr66
	addiu	$t0,	$sp,	76
	sb	$v0,	0($t0)
		# RM	tv67,	$v0
	j	Label21
		# J	Label21
Label22:
		# Label22
	j	Label47
		# J	Label47
Label8:
		# Label8
	addiu	$t0,	$sp,	84
	la	$t1,	gb43
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	tv68,	gb43
	lw	$t0,	gb43
	li	$t1,	1
	add	$t2,	$t0,	$t1
	sw	$t2,	gb43
		# add	gb43,	gb43,	1
Label47:
		# Label47
Label5:
	lw	$ra,	88($sp)
	addiu	$sp,	$sp,	92
	jr	$ra


Label48:
	addiu	$sp,	$sp,	-64
	sw	$ra,	60($sp)
Label49:
	la	$t0,	Label51
	or	$t2,	$0,	$t0
	sw	$t2,	12($sp)
		# la	tv71,	Label51
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	12
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr72,	tv71
	addiu	$t0,	$sp,	4
	la	$t1,	gb42
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr73,	gb42
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr72,	cr73
	addiu	$t0,	$sp,	16
	sb	$v0,	0($t0)
		# RM	tv74,	$v0
	lw	$t0,	gb44
	lw	$t1,	gb42
	sll	$t2,	$t0,	$t1
	sw	$t2,	20($sp)
		# sll	tv75,	gb44,	gb42
	li	$t0,	1
	or	$t2,	$0,	$t0
	sw	$t2,	24($sp)
		# li	tv76,	1
	lw	$t0,	20($sp)
	lw	$t1,	24($sp)
	sub	$t2,	$t0,	$t1
	sw	$t2,	28($sp)
		# sub	tv77,	tv75,	tv76
	la	$t0,	gb44
	addiu	$t1,	$sp,	28
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	gb44,	tv77
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	32($sp)
		# li	tv78,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	36($sp)
		# li	tv79,	0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	40($sp)
		# li	tv80,	0
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	32
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr81,	tv78
	addiu	$t0,	$sp,	4
	addiu	$t1,	$sp,	36
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr82,	tv79
	addiu	$t0,	$sp,	8
	addiu	$t1,	$sp,	40
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr83,	tv80
	lw	$a0,	0($sp)
	jal	Label3
		# CALL	Label3,	cr81,	cr82,	cr83
	addiu	$t0,	$sp,	44
	sb	$v0,	0($t0)
		# RM	tv84,	$v0
	la	$t0,	Label56
	or	$t2,	$0,	$t0
	sw	$t2,	48($sp)
		# la	tv85,	Label56
	addiu	$t0,	$sp,	0
	addiu	$t1,	$sp,	48
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr86,	tv85
	addiu	$t0,	$sp,	4
	la	$t1,	gb43
	lw	$t2,	0($t1)
	sw	$t2,	0($t0)
		# RM	cr87,	gb43
	lw	$a0,	0($sp)
	jal	Label1
		# CALL	Label1,	cr86,	cr87
	addiu	$t0,	$sp,	52
	sb	$v0,	0($t0)
		# RM	tv88,	$v0
	li	$t0,	0
	or	$t2,	$0,	$t0
	sw	$t2,	56($sp)
		# li	tv89,	0
	addiu	$t1,	$sp,	56
	lw	$v0,	0($t1)
		# RM	$v0,	tv89
	j	Label50
		# J	Label50
Label50:
	lw	$ra,	60($sp)
	addiu	$sp,	$sp,	64
	jr	$ra

Label1:
printf:

stprint:
    or      $t0, $0, $a0
    addiu   $t1, $sp, 4         # first item to print
loop_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    beq     $a0, $0, endprint       # if \000, end 
    xori    $t2, $a0, '%'
    beqz    $t2, format_pr      # if %, goto format_pr, else goto plain_pr

plain_pr:
    li      $v0, 11
    syscall                     # print character in $a0
    j       loop_pr

format_pr:
    lb      $a0, 0($t0)
    addiu   $t0, $t0, 1
    xori    $t2, $a0, 'd'
    beq     $t2, $0, int_pr         # if %d, goto int_pr
    xori    $t2, $a0, 'c'
    beq     $t2, $0, char_pr        # if %c, goto char_pr
    xori    $t2, $a0, 's'
    beq     $t2, $0, string_pr      # if %s, goto string_pr
    xori    $t2, $a0, '0'
    beq     $t2, $0, prec_pr        # if %04d, goto prec_pr

int_pr:
    lw      $a0, 0($t1)         # integer to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 1
    syscall                     # print integer in $a0
    j       loop_pr

char_pr:
    lb      $a0, 0($t1)         # character to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 11
    syscall                     # print character in $a0
    j       loop_pr

string_pr:
    lw      $a0, 0($t1)         # string to print
    addiu   $t1, $t1, 4         # next item
    ori     $v0, $0, 4
    syscall                     # print string in $a0
    j       loop_pr

prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)			# integer to print
	addiu	$t1, $t1, 4			# next item
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero		# >= 1000, goto non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero		# >= 100, goto one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero		# >= 10, goto two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or		$a0, $t2, $0
	syscall
    j       loop_pr

endprint:
    jr      $ra                 # return


Label2:
malloc:
    lw      $v0, static_end
    addu    $a0, $v0, $a0
    addiu   $a0, $a0, 3
    srl     $a0, $a0, 2
    sll     $a0, $a0, 2 #align 2
    sw      $a0, static_end
    jr      $ra


#Finished
