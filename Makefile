all: 
	java -classpath lib/antlr-3.5.1.jar org.antlr.Tool -o src/appetizer/syntactic/ src/appetizer/syntactic/C.g
	mkdir -p bin
	javac -classpath lib/antlr-3.5.1.jar:bin/ -d bin/ src/appetizer/*/*.java 

clean:
	rm -rf bin/

