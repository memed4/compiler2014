package appetizer.main;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;

import java.io.*;
import java.util.*;

import appetizer.environ.*;
import appetizer.syntactic.*;
import appetizer.semantic.*;
import appetizer.translate.*;

public class Main {
	public static void main(String[] args) throws Exception {
		//System.setOut(new PrintStream(new FileOutputStream("out.s")));
		//InputStream in = new FileInputStream("src/appetizer/main/Normal/superloop-5090379042-jiaxiao.c");
		InputStream in = new FileInputStream(args[0]);
		//InputStream in = new FileInputStream("src/appetizer/main/test.c");
		//InputStream in = new FileInputStream("src/appetizer/main/test1.c");
		ANTLRInputStream input = new ANTLRInputStream(in);
		CLexer lexer = new CLexer(input);
		CommonTokenStream tokens = new CommonTokenStream(lexer);			
 		CParser parser = new CParser(tokens);
		CParser.program_return result = parser.program();
		CommonTree t = (CommonTree)result.getTree();
		
		/*String AST = t.toStringTree();
		int space = 1;
		int stack[] = new int[10000];
		int sp = -1;
		char c;
		for(int i = 0; i < AST.length(); ++i) {
			c = AST.charAt(i);
			if (c == '(') {
				System.out.println();
				stack[++sp] = space;
				System.out.print(String.format("%0" + space + "d", 0).replace('0', ' ')); 
				System.out.print(c);
				space += 2;
			} else if (c ==')') {
				space = stack[sp--];
				if (AST.charAt(i-1) == ')') {
					System.out.println();
					System.out.print(String.format("%0" + space + "d", 0).replace('0', ' '));
				}
				System.out.print(c);
			} else {
				System.out.print(c);
			}
		}*/
		
		CommonTree rt;
		
		if (t.getText() == "FUNC") {
			rt = new CommonTree();
			rt.addChild(t);
		} else {
			rt = t;
		}
		
		Checker semanticChecker = new Checker();
		Environ iniEnviron = new Environ(null, true);
		AddStd(iniEnviron);
		semanticChecker.DoChecking(rt, iniEnviron, null);
		
		//semanticChecker.program.printIR();
		
		//System.out.println("----- IR END -----");
		Translator translator = new Translator(semanticChecker.program);
		
		System.exit(0);

	}
	
	public static void AddStd(Environ env) {
		List l = new ArrayList<Type>();
		l.add(new Pointer(Char.getCharInstance()));
		l.add(new Pointer(Char.getCharInstance()));
		Type funcType = new Function(l, Vvoid.getVoidInstance());
		env.addSymbol("strcpy", new SymbolInfo(funcType, "strcpy", null));
		
		l.clear();
		l.add(new Pointer(Char.getCharInstance()));
		l.add(new Pointer(Char.getCharInstance()));
		funcType = new Function(l, Vvoid.getVoidInstance());
		env.addSymbol("strcmp", new SymbolInfo(funcType, "strcmp", null));
		
		l.clear();
		l.add(new Pointer(Char.getCharInstance()));
		l.add(new Pointer(Char.getCharInstance()));
		funcType = new Function(l, Vvoid.getVoidInstance());
		env.addSymbol("strcat", new SymbolInfo(funcType, "strcat", null));
		
		l.clear();
		l.add(new Pointer(Char.getCharInstance()));
		funcType = new Function(l, Vvoid.getVoidInstance());
		env.addSymbol("printf", new SymbolInfo(funcType, "printf", null));
	
		l.clear();
		l.add(new Pointer(Char.getCharInstance()));
		funcType = new Function(l, Vvoid.getVoidInstance());
		env.addSymbol("scanf", new SymbolInfo(funcType, "scanf", null));
	
		l.clear();
		l.add(Int.getIntInstance());
		funcType = new Function(l, new Pointer(Vvoid.getVoidInstance()));
		env.addSymbol("malloc", new SymbolInfo(funcType, "malloc", null));
	}
}

