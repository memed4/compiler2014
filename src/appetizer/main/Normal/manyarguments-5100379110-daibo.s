    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "%d "
	.align 2
g2:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label3
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t1 = printf( g1 arg1 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	8($fp)
	sw	$t0,	4($sp)
	jal	printf
#t2 = printf( g1 arg2 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	jal	printf
#t3 = printf( g1 arg3 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	16($fp)
	sw	$t0,	4($sp)
	jal	printf
#t4 = printf( g1 arg4 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	20($fp)
	sw	$t0,	4($sp)
	jal	printf
#t5 = printf( g1 arg5 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	24($fp)
	sw	$t0,	4($sp)
	jal	printf
#t6 = printf( g2 arg6 )
	la	$t0,	g2
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	28($fp)
	sw	$t0,	4($sp)
	jal	printf
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-56
#t7 = 5+6
	li	$t0,	5
	addiu	$v1,	$t0,	6
#t8 = manyArguments( 0 1 2 3 4 t7 )
	li	$t0,	0
	sw	$t0,	0($sp)
	li	$t0,	1
	sw	$t0,	4($sp)
	li	$t0,	2
	sw	$t0,	8($sp)
	li	$t0,	3
	sw	$t0,	12($sp)
	li	$t0,	4
	sw	$t0,	16($sp)
	sw	$v1,	20($sp)
	jal	label1
	sw	$v0,	-20($fp)
#t9 = 3+4
	li	$t0,	3
	addiu	$v1,	$t0,	4
#t10 = t9+5
	addiu	$a3,	$v1,	5
#t11 = 5-6
	li	$t0,	5
	li	$t1,	6
	subu	$v1,	$t0,	$t1
#t12 = manyArguments( 0 1 2 t10 4 t11 )
	li	$t0,	0
	sw	$t0,	0($sp)
	li	$t0,	1
	sw	$t0,	4($sp)
	li	$t0,	2
	sw	$t0,	8($sp)
	sw	$a3,	12($sp)
	li	$t0,	4
	sw	$t0,	16($sp)
	sw	$v1,	20($sp)
	jal	label1
	sw	$v0,	-4($fp)
#ret 0
	li	$v0,	0
	j label3exit
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
