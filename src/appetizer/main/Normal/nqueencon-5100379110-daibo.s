    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz " ."
	.align 2
g2:
	.asciiz " O"
	.align 2
g3:
	.asciiz "\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label22
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-48
#t1 = 1
	li	$t0,	1
	sw	$t0,	-36($fp)
#label3:
label3:
#t2 = arg1-1
	lw	$t0,	8($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-32($fp)
#t3 = t2<t1
	lw	$t0,	-32($fp)
	lw	$t1,	-36($fp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	-28($fp)
#t3 = t3==0
	lw	$t0,	-28($fp)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$t2,	$s0,	1
	sw	$t2,	-28($fp)
#if t3==0 j label4
	lw	$t0,	-28($fp)
	beq	$t0,	$0,	label4
#t4 = printf( g1 )
	la	$a0,	g1
	li	$v0,	4
	syscall
#label5:
label5:
#t1 = t1+1
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-36($fp)
#j label3
	j label3
#label4:
label4:
#t5 = printf( g2 )
	la	$a0,	g2
	li	$v0,	4
	syscall
#t6 = arg1+1
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	1
#t1 = t6
	sw	$v1,	-36($fp)
#label6:
label6:
#t7 = arg2<t1
	lw	$t0,	12($fp)
	lw	$t1,	-36($fp)
	slt	$v1,	$t0,	$t1
#t7 = t7==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t7==0 j label7
	beq	$v1,	$0,	label7
#t8 = printf( g1 )
	la	$a0,	g1
	li	$v0,	4
	syscall
#label8:
label8:
#t1 = t1+1
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-36($fp)
#j label6
	j label6
#label7:
label7:
#t10 = printf( g3 )
	la	$a0,	g3
	li	$v0,	4
	syscall
#ret 0
	li	$v0,	0
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label9:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-172
#t11 = 0
	li	$t0,	0
	sw	$t0,	-156($fp)
#t12 = arg1
	lw	$t0,	8($fp)
	sw	$t0,	-152($fp)
#t14 = arg1%2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mfhi	$v1
#t13 = t14
	sw	$v1,	-148($fp)
#t15 = arg1/2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mflo	$v1
#t16 = t15%3
	li	$t1,	3
	divu	$v1,	$t1
	mfhi	$a3
#t17 = t16!=1
	li	$t1,	1
	xor $s0,	$a3,	$t1
	sltu	$v1,	$0,	$s0
#if t17==0 j label11
	beq	$v1,	$0,	label11
#t18 = printrow( 2 t12 )
	li	$t0,	2
	sw	$t0,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-128($fp)
#t11 = 4
	li	$t0,	4
	sw	$t0,	-156($fp)
#label12:
label12:
#t19 = arg1<t11
	lw	$t0,	8($fp)
	lw	$t1,	-156($fp)
	slt	$v1,	$t0,	$t1
#t19 = t19==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t19==0 j label13
	beq	$v1,	$0,	label13
#t20 = printrow( t11 t12 )
	lw	$t0,	-156($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-120($fp)
#t11 = t11+2
	lw	$t0,	-156($fp)
	addiu	$t2,	$t0,	2
	sw	$t2,	-156($fp)
#j label12
	j label12
#label13:
label13:
#t11 = 1
	li	$t0,	1
	sw	$t0,	-156($fp)
#label14:
label14:
#t21 = arg1<t11
	lw	$t0,	8($fp)
	lw	$t1,	-156($fp)
	slt	$v1,	$t0,	$t1
#t21 = t21==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t21==0 j label15
	beq	$v1,	$0,	label15
#t22 = printrow( t11 t12 )
	lw	$t0,	-156($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-112($fp)
#t11 = t11+2
	lw	$t0,	-156($fp)
	addiu	$t2,	$t0,	2
	sw	$t2,	-156($fp)
#j label14
	j label14
#label15:
label15:
#j label16
	j label16
#label11:
label11:
#t23 = arg1-t13
	lw	$t0,	8($fp)
	lw	$t1,	-148($fp)
	subu	$v1,	$t0,	$t1
#arg1 = t23
	sw	$v1,	8($fp)
#t24 = arg1/2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mflo	$v1
#t25 = printrow( t24 t12 )
	sw	$v1,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-100($fp)
#t26 = arg1/2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mflo	$v1
#t27 = t26+1
	addiu	$a3,	$v1,	1
#t11 = t27
	sw	$a3,	-156($fp)
#label17:
label17:
#t28 = arg1/2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mflo	$v1
#t29 = t28-1
	li	$t1,	1
	subu	$a3,	$v1,	$t1
#t30 = t11!=t29
	lw	$t0,	-156($fp)
	xor $s0,	$t0,	$a3
	sltu	$v1,	$0,	$s0
#if t30==0 j label18
	beq	$v1,	$0,	label18
#t31 = t11+1
	lw	$t0,	-156($fp)
	addiu	$v1,	$t0,	1
#t32 = printrow( t31 t12 )
	sw	$v1,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-72($fp)
#t33 = t11+2
	lw	$t0,	-156($fp)
	addiu	$v1,	$t0,	2
#t34 = t33%arg1
	lw	$t1,	8($fp)
	divu	$v1,	$t1
	mfhi	$a3
#t11 = t34
	sw	$a3,	-156($fp)
#j label17
	j label17
#label18:
label18:
#t35 = t11-2
	lw	$t0,	-156($fp)
	li	$t1,	2
	subu	$v1,	$t0,	$t1
#t36 = t35%arg1
	lw	$t1,	8($fp)
	divu	$v1,	$t1
	mfhi	$a3
#t11 = t36
	sw	$a3,	-156($fp)
#label19:
label19:
#t37 = arg1/2
	lw	$t0,	8($fp)
	li	$t1,	2
	divu	$t0,	$t1
	mflo	$v1
#t38 = t37-1
	li	$t1,	1
	subu	$a3,	$v1,	$t1
#t39 = t11!=t38
	lw	$t0,	-156($fp)
	xor $s0,	$t0,	$a3
	sltu	$v1,	$0,	$s0
#if t39==0 j label20
	beq	$v1,	$0,	label20
#t40 = arg1-t11
	lw	$t0,	8($fp)
	lw	$t1,	-156($fp)
	subu	$v1,	$t0,	$t1
#t41 = printrow( t40 t12 )
	sw	$v1,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-36($fp)
#t42 = t11-2
	lw	$t0,	-156($fp)
	li	$t1,	2
	subu	$v1,	$t0,	$t1
#t43 = t42+arg1
	lw	$t1,	8($fp)
	addu	$a3,	$v1,	$t1
#t44 = t43%arg1
	lw	$t1,	8($fp)
	divu	$a3,	$t1
	mfhi	$v1
#t11 = t44
	sw	$v1,	-156($fp)
#j label19
	j label19
#label20:
label20:
#t45 = arg1-t11
	lw	$t0,	8($fp)
	lw	$t1,	-156($fp)
	subu	$v1,	$t0,	$t1
#t46 = printrow( t45 t12 )
	sw	$v1,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-16($fp)
#if t13==0 j label21
	lw	$t0,	-148($fp)
	beq	$t0,	$0,	label21
#t47 = arg1+1
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	1
#t48 = printrow( t47 t12 )
	sw	$v1,	0($sp)
	lw	$t0,	-152($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-8($fp)
#label21:
label21:
#label16:
label16:
#t49 = printf( g3 )
	la	$a0,	g3
	li	$v0,	4
	syscall
#ret 0
	li	$v0,	0
	j label9exit
label9exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label10:

label22:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-28
#t50 = 6
	li	$t0,	6
	sw	$t0,	-16($fp)
#label24:
label24:
#t51 = 11<t50
	li	$t0,	11
	lw	$t1,	-16($fp)
	slt	$v1,	$t0,	$t1
#t51 = t51==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t51==0 j label25
	beq	$v1,	$0,	label25
#t52 = nqueen( t50 )
	lw	$t0,	-16($fp)
	sw	$t0,	0($sp)
	jal	label9
	sw	$v0,	-8($fp)
#label26:
label26:
#t53 = t50
	lw	$t0,	-16($fp)
	sw	$t0,	-4($fp)
#t50 = t50+1
	lw	$t0,	-16($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-16($fp)
#j label24
	j label24
#label25:
label25:
#ret 0
	li	$v0,	0
	j label22exit
label22exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label23:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
