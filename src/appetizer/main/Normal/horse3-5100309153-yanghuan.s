    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g19:
	.asciiz "%d\n"
	.align 2
g20:
	.asciiz "no solution!\n"
	.align 2
	.data
	.align 2
g1:
	.word 100
	.space 4
	.align 2
g2:
	.space 4
	.align 2
g3:
	.space 4
	.align 2
g4:
	.space 4
	.align 2
g5:
	.space 4
	.align 2
g6:
	.space 4
	.align 2
g7:
	.space 4
	.align 2
g8:
	.space 4
	.align 2
g9:
	.space 40000
	.align 2
g10:
	.space 40000
	.align 2
g11:
	.space 4
	.align 2
g12:
	.space 4
	.align 2
g13:
	.space 4
	.align 2
g14:
	.space 32
	.align 2
g15:
	.space 36
	.align 2
g16:
	.space 40000
	.align 2
g17:
	.space 4
	.align 2
g18:
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label11
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-20
#t1 = 0
	addiu	$v1,	$0,	0
#t2 = arg1<g1
	lw	$t0,	8($fp)
	lw	$t1,	g1
	slt	$a3,	$t0,	$t1
#if t2==0 j label3
	beq	$a3,	$0,	label3
#t3 = arg1<0
	lw	$t0,	8($fp)
	li	$t1,	0
	slt	$a3,	$t0,	$t1
#t3 = t3==0
	li	$t1,	0
	xor	$s0,	$a3,	$t1
	sltiu	$a3,	$s0,	1
#if t3==0 j label3
	beq	$a3,	$0,	label3
#t1 = 1
	addiu	$v1,	$0,	1
#label3:
label3:
#ret t1
	addu	$v0,	$0,	$v1
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label4:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-104
#t4 = 0
	addiu	$t6,	$0,	0
#t5 = 0
	addiu	$a3,	$0,	0
#t6 = check( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label1
	sw	$v0,	-84($fp)
#t7 = t6==1
	lw	$t0,	-84($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t7==0 j label6
	beq	$v1,	$0,	label6
#t8 = check( arg2 )
	lw	$t0,	12($fp)
	sw	$t0,	0($sp)
	jal	label1
	sw	$v0,	-76($fp)
#t9 = t8==1
	lw	$t0,	-76($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t9==0 j label6
	beq	$v1,	$0,	label6
#t5 = 1
	addiu	$a3,	$0,	1
#label7:
label7:
#if t5==0 j label6
	beq	$a3,	$0,	label6
#t10 = &g16
	la	$t3,	g16
#t11 = arg1*400
	lw	$t0,	8($fp)
	li	$t1,	400
	mul	$t4,	$t0,	$t1
#t10 = t10+t11
	addu	$t3,	$t3,	$t4
#t12 = arg2*4
	lw	$t0,	12($fp)
	sll	$t5,	$t0,	2
#t10 = t10+t12
	addu	$t3,	$t3,	$t5
#t13 = -1
	li	$t0,	1
	subu	$t5,	$0,	$t0
#t14 = [t10]==t13
	lw	$t0,	0($t3)
	xor	$s0,	$t0,	$t5
	sltiu	$t6,	$s0,	1
#if t14==0 j label6
	beq	$t6,	$0,	label6
#t4 = 1
	addiu	$t6,	$0,	1
#label8:
label8:
#if t4==0 j label6
	beq	$t6,	$0,	label6
#t15 = g11
	lw	$t0,	g11
	sw	$t0,	-48($fp)
#g11 = g11+1
	lw	$t0,	g11
	addiu	$t2,	$t0,	1
	sw	$t2,	g11
#t16 = &g9
	la	$t1,	g9
	sw	$t1,	-44($fp)
#t17 = g11*4
	lw	$t0,	g11
	sll	$v1,	$t0,	2
#t16 = t16+t17
	lw	$t0,	-44($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-44($fp)
#[t16] = arg1
	lw	$t0,	8($fp)
	lw	$t1,	-44($fp)
	sw	$t0,	0($t1)
#t18 = &g10
	la	$v1,	g10
#t19 = g11*4
	lw	$t0,	g11
	sll	$t3,	$t0,	2
#t18 = t18+t19
	addu	$v1,	$v1,	$t3
#[t18] = arg2
	lw	$t0,	12($fp)
	sw	$t0,	0($v1)
#t20 = &g16
	la	$v1,	g16
#t21 = arg1*400
	lw	$t0,	8($fp)
	li	$t1,	400
	mul	$t3,	$t0,	$t1
#t20 = t20+t21
	addu	$v1,	$v1,	$t3
#t22 = arg2*4
	lw	$t0,	12($fp)
	sll	$t3,	$t0,	2
#t20 = t20+t22
	addu	$v1,	$v1,	$t3
#t23 = g13+1
	lw	$t0,	g13
	addiu	$t3,	$t0,	1
#[t20] = t23
	sw	$t3,	0($v1)
#t24 = 0
	addiu	$v1,	$0,	0
#t25 = arg1==g5
	lw	$t0,	8($fp)
	lw	$t1,	g5
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t25==0 j label9
	beq	$v1,	$0,	label9
#t26 = arg2==g6
	lw	$t0,	12($fp)
	lw	$t1,	g6
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t26==0 j label9
	beq	$v1,	$0,	label9
#t24 = 1
	addiu	$v1,	$0,	1
#label10:
label10:
#if t24==0 j label9
	beq	$v1,	$0,	label9
#g12 = 1
	li	$t0,	1
	sw	$t0,	g12
#label9:
label9:
#label6:
label6:
label4exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label5:

label11:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-236
#t27 = g1-1
	lw	$t0,	g1
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#g6 = t27
	sw	$v1,	g6
#g5 = g6
	lw	$t0,	g6
	sw	$t0,	g5
#g17 = 0
	li	$t0,	0
	sw	$t0,	g17
#label13:
label13:
#t28 = g17<g1
	lw	$t0,	g17
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t28==0 j label14
	beq	$v1,	$0,	label14
#g18 = 0
	li	$t0,	0
	sw	$t0,	g18
#label16:
label16:
#t29 = g18<g1
	lw	$t0,	g18
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t29==0 j label17
	beq	$v1,	$0,	label17
#t30 = &g16
	la	$v1,	g16
#t31 = g17*400
	lw	$t0,	g17
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t30 = t30+t31
	addu	$v1,	$v1,	$a3
#t32 = g18*4
	lw	$t0,	g18
	sll	$a3,	$t0,	2
#t30 = t30+t32
	addu	$v1,	$v1,	$a3
#t33 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t30] = t33
	sw	$a3,	0($v1)
#label18:
label18:
#g18 = g18+1
	lw	$t0,	g18
	addiu	$t2,	$t0,	1
	sw	$t2,	g18
#j label16
	j label16
#label17:
label17:
#label15:
label15:
#g17 = g17+1
	lw	$t0,	g17
	addiu	$t2,	$t0,	1
	sw	$t2,	g17
#j label13
	j label13
#label14:
label14:
#t36 = &g14
	la	$v1,	g14
#t37 = -2
	li	$t0,	2
	subu	$a3,	$0,	$t0
#[t36] = t37
	sw	$a3,	0($v1)
#t38 = &g15
	la	$v1,	g15
#t39 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t38] = t39
	sw	$a3,	0($v1)
#t40 = &g14
	la	$v1,	g14
#t40 = t40+4
	addiu	$v1,	$v1,	4
#t41 = -2
	li	$t0,	2
	subu	$a3,	$0,	$t0
#[t40] = t41
	sw	$a3,	0($v1)
#t42 = &g15
	la	$v1,	g15
#t42 = t42+4
	addiu	$v1,	$v1,	4
#[t42] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t43 = &g14
	la	$v1,	g14
#t43 = t43+8
	addiu	$v1,	$v1,	8
#[t43] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#t44 = &g15
	la	$v1,	g15
#t44 = t44+8
	addiu	$v1,	$v1,	8
#t45 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t44] = t45
	sw	$a3,	0($v1)
#t46 = &g14
	la	$v1,	g14
#t46 = t46+12
	addiu	$v1,	$v1,	12
#[t46] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#t47 = &g15
	la	$v1,	g15
#t47 = t47+12
	addiu	$v1,	$v1,	12
#[t47] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t48 = &g14
	la	$v1,	g14
#t48 = t48+16
	addiu	$v1,	$v1,	16
#t49 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t48] = t49
	sw	$a3,	0($v1)
#t50 = &g15
	la	$v1,	g15
#t50 = t50+16
	addiu	$v1,	$v1,	16
#t51 = -2
	li	$t0,	2
	subu	$a3,	$0,	$t0
#[t50] = t51
	sw	$a3,	0($v1)
#t52 = &g14
	la	$v1,	g14
#t52 = t52+20
	addiu	$v1,	$v1,	20
#t53 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t52] = t53
	sw	$a3,	0($v1)
#t54 = &g15
	la	$v1,	g15
#t54 = t54+20
	addiu	$v1,	$v1,	20
#[t54] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#t55 = &g14
	la	$v1,	g14
#t55 = t55+24
	addiu	$v1,	$v1,	24
#[t55] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t56 = &g15
	la	$v1,	g15
#t56 = t56+24
	addiu	$v1,	$v1,	24
#t57 = -2
	li	$t0,	2
	subu	$a3,	$0,	$t0
#[t56] = t57
	sw	$a3,	0($v1)
#t58 = &g14
	la	$v1,	g14
#t58 = t58+28
	addiu	$v1,	$v1,	28
#[t58] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t59 = &g15
	la	$v1,	g15
#t59 = t59+28
	addiu	$v1,	$v1,	28
#[t59] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#label19:
label19:
#t60 = g11<g2
	lw	$t0,	g11
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#t60 = t60==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t60==0 j label20
	beq	$v1,	$0,	label20
#t61 = &g9
	la	$v1,	g9
#t62 = g2*4
	lw	$t0,	g2
	sll	$a3,	$t0,	2
#t61 = t61+t62
	addu	$v1,	$v1,	$a3
#g7 = [t61]
	lw	$t0,	0($v1)
	sw	$t0,	g7
#t63 = &g10
	la	$v1,	g10
#t64 = g2*4
	lw	$t0,	g2
	sll	$a3,	$t0,	2
#t63 = t63+t64
	addu	$v1,	$v1,	$a3
#g8 = [t63]
	lw	$t0,	0($v1)
	sw	$t0,	g8
#t65 = &g16
	la	$v1,	g16
#t66 = g7*400
	lw	$t0,	g7
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t65 = t65+t66
	addu	$v1,	$v1,	$a3
#t67 = g8*4
	lw	$t0,	g8
	sll	$a3,	$t0,	2
#t65 = t65+t67
	addu	$v1,	$v1,	$a3
#g13 = [t65]
	lw	$t0,	0($v1)
	sw	$t0,	g13
#g18 = 0
	li	$t0,	0
	sw	$t0,	g18
#label21:
label21:
#t68 = g18<8
	lw	$t0,	g18
	li	$t1,	8
	slt	$v1,	$t0,	$t1
#if t68==0 j label22
	beq	$v1,	$0,	label22
#t69 = &g14
	la	$v1,	g14
#t70 = g18*4
	lw	$t0,	g18
	sll	$a3,	$t0,	2
#t69 = t69+t70
	addu	$v1,	$v1,	$a3
#t71 = g7+[t69]
	lw	$t0,	g7
	lw	$t1,	0($v1)
	addu	$a3,	$t0,	$t1
#t72 = &g15
	la	$v1,	g15
#t73 = g18*4
	lw	$t0,	g18
	sll	$t3,	$t0,	2
#t72 = t72+t73
	addu	$v1,	$v1,	$t3
#t74 = g8+[t72]
	lw	$t0,	g8
	lw	$t1,	0($v1)
	addu	$t3,	$t0,	$t1
#t75 = addList( t71 t74 )
	sw	$a3,	0($sp)
	sw	$t3,	4($sp)
	jal	label4
	sw	$v0,	-36($fp)
#label23:
label23:
#g18 = g18+1
	lw	$t0,	g18
	addiu	$t2,	$t0,	1
	sw	$t2,	g18
#j label21
	j label21
#label22:
label22:
#t77 = g12==1
	lw	$t0,	g12
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t77==0 j label24
	beq	$v1,	$0,	label24
#j label20
	j label20
#label24:
label24:
#t78 = g2
	lw	$t0,	g2
	sw	$t0,	-28($fp)
#g2 = g2+1
	lw	$t0,	g2
	addiu	$t2,	$t0,	1
	sw	$t2,	g2
#j label19
	j label19
#label20:
label20:
#t79 = g12==1
	lw	$t0,	g12
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t79==0 j label25
	beq	$v1,	$0,	label25
#t80 = &g16
	la	$v1,	g16
#t81 = g5*400
	lw	$t0,	g5
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t80 = t80+t81
	addu	$v1,	$v1,	$a3
#t82 = g6*4
	lw	$t0,	g6
	sll	$a3,	$t0,	2
#t80 = t80+t82
	addu	$v1,	$v1,	$a3
#t83 = printf( g19 [t80] )
	la	$t0,	g19
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#j label26
	j label26
#label25:
label25:
#t84 = printf( g20 )
	la	$a0,	g20
	li	$v0,	4
	syscall
#label26:
label26:
#ret 0
	li	$v0,	0
	j label11exit
label11exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label12:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
