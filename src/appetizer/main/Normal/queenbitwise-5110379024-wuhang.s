    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g4:
	.asciiz "%d queens:\n"
	.align 2
g5:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
g1:
	.word 8
	.space 4
	.align 2
g2:
	.word 0
	.space 4
	.align 2
g3:
	.word 1
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label7
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-84
#t1 = arg1!=g3
	lw	$t0,	8($fp)
	lw	$t1,	g3
	xor $s0,	$t0,	$t1
	sltu	$t2,	$0,	$s0
	sw	$t2,	-64($fp)
#if t1==0 j label3
	lw	$t0,	-64($fp)
	beq	$t0,	$0,	label3
#t3 = arg1|arg2
	lw	$t0,	8($fp)
	lw	$t1,	12($fp)
	or	$t2,	$t0,	$t1
	sw	$t2,	-56($fp)
#t4 = t3|arg3
	lw	$t0,	-56($fp)
	lw	$t1,	16($fp)
	or	$v1,	$t0,	$t1
#t5 = ~t4
	nor	$a3,	$v1,	$0
#t6 = g3&t5
	lw	$t0,	g3
	and	$v1,	$t0,	$a3
#t2 = t6
	sw	$v1,	-60($fp)
#label4:
label4:
#if t2==0 j label5
	lw	$t0,	-60($fp)
	beq	$t0,	$0,	label5
#t8 = -t2
	lw	$t0,	-60($fp)
	subu	$v1,	$0,	$t0
#t9 = t2&t8
	lw	$t0,	-60($fp)
	and	$a3,	$t0,	$v1
#t7 = t9
	addu	$v1,	$0,	$a3
#t2 = t2-t7
	lw	$t0,	-60($fp)
	subu	$t2,	$t0,	$v1
	sw	$t2,	-60($fp)
#t10 = arg1+t7
	lw	$t0,	8($fp)
	addu	$a3,	$t0,	$v1
#t11 = arg2+t7
	lw	$t0,	12($fp)
	addu	$t3,	$t0,	$v1
#t12 = t11<<1
	li	$t1,	1
	sllv	$t4,	$t3,	$t1
#t13 = arg3+t7
	lw	$t0,	16($fp)
	addu	$t3,	$t0,	$v1
#t14 = t13>>1
	li	$t1,	1
	srav	$v1,	$t3,	$t1
#t15 = test( t10 t12 t14 )
	sw	$a3,	0($sp)
	sw	$t4,	4($sp)
	sw	$v1,	8($sp)
	jal	label1
	sw	$v0,	-8($fp)
#j label4
	j label4
#label5:
label5:
#j label6
	j label6
#label3:
label3:
#t16 = g2
	lw	$t0,	g2
	sw	$t0,	-4($fp)
#g2 = g2+1
	lw	$t0,	g2
	addiu	$t2,	$t0,	1
	sw	$t2,	g2
#label6:
label6:
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label7:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t17 = printf( g4 g1 )
	la	$t0,	g4
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	g1
	sw	$t0,	4($sp)
	jal	printf
#t18 = g3<<g1
	lw	$t0,	g3
	lw	$t1,	g1
	sllv	$v1,	$t0,	$t1
#t19 = t18-1
	li	$t1,	1
	subu	$a3,	$v1,	$t1
#g3 = t19
	sw	$a3,	g3
#t20 = test( 0 0 0 )
	li	$t0,	0
	sw	$t0,	0($sp)
	li	$t0,	0
	sw	$t0,	4($sp)
	li	$t0,	0
	sw	$t0,	8($sp)
	jal	label1
	sw	$v0,	-8($fp)
#t21 = printf( g5 g2 )
	la	$t0,	g5
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	g2
	sw	$t0,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label7exit
label7exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label8:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
