    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g3:
	.asciiz "%d "
	.align 2
g4:
	.asciiz "\n"
	.align 2
	.data
	.align 2
g1:
	.word 10000
	.space 4
	.align 2
g2:
	.space 40000
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label23
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-44
#t2 = arg1*4
	lw	$t0,	8($fp)
	sll	$v1,	$t0,	2
#t3 = g2+t2
	lw	$t0,	g2
	addu	$a3,	$t0,	$v1
#t1 = [t3]
	lw	$t0,	0($a3)
	addu	$t3,	$0,	$t0
#t4 = arg1*4
	lw	$t0,	8($fp)
	sll	$a3,	$t0,	2
#t5 = g2+t4
	lw	$t0,	g2
	addu	$t4,	$t0,	$a3
#t6 = arg2*4
	lw	$t0,	12($fp)
	sll	$a3,	$t0,	2
#t7 = g2+t6
	lw	$t0,	g2
	addu	$t5,	$t0,	$a3
#[t5] = [t7]
	lw	$t0,	0($t5)
	sw	$t0,	0($t4)
#t8 = arg2*4
	lw	$t0,	12($fp)
	sll	$a3,	$t0,	2
#t9 = g2+t8
	lw	$t0,	g2
	addu	$t4,	$t0,	$a3
#[t9] = t1
	sw	$t3,	0($t4)
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-128
#t13 = g1-1
	lw	$t0,	g1
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t14 = t13/2
	li	$t1,	2
	divu	$v1,	$t1
	mflo	$a3
#t10 = t14
	sw	$a3,	-112($fp)
#t12 = 0
	addiu	$v1,	$0,	0
#t11 = t12
	sw	$v1,	-108($fp)
#label5:
label5:
#t15 = t10<0
	lw	$t0,	-112($fp)
	li	$t1,	0
	slt	$a3,	$t0,	$t1
#t15 = t15==0
	li	$t1,	0
	xor	$s0,	$a3,	$t1
	sltiu	$a3,	$s0,	1
#if t15==0 j label6
	beq	$a3,	$0,	label6
#t16 = t10*2
	lw	$t0,	-112($fp)
	sll	$a3,	$t0,	1
#t12 = t16
	addu	$v1,	$0,	$a3
#t17 = 0
	addiu	$a3,	$0,	0
#t18 = t10*2
	lw	$t0,	-112($fp)
	sll	$a3,	$t0,	1
#t19 = t18+1
	addiu	$t3,	$a3,	1
#t20 = t19<g1
	lw	$t1,	g1
	slt	$a3,	$t3,	$t1
#if t20==0 j label7
	beq	$a3,	$0,	label7
#t21 = &g2
	la	$a3,	g2
#t22 = t10*2
	lw	$t0,	-112($fp)
	sll	$t3,	$t0,	1
#t23 = t22+1
	addiu	$t4,	$t3,	1
#t24 = t23*4
	sll	$t3,	$t4,	2
#t21 = t21+t24
	addu	$a3,	$a3,	$t3
#t25 = &g2
	la	$t3,	g2
#t26 = t10*2
	lw	$t0,	-112($fp)
	sll	$t4,	$t0,	1
#t27 = t26*4
	sll	$t5,	$t4,	2
#t25 = t25+t27
	addu	$t3,	$t3,	$t5
#t28 = [t21]<[t25]
	lw	$t0,	0($a3)
	lw	$t1,	0($t3)
	slt	$t4,	$t0,	$t1
#if t28==0 j label7
	beq	$t4,	$0,	label7
#t17 = 1
	addiu	$a3,	$0,	1
#label8:
label8:
#if t17==0 j label7
	beq	$a3,	$0,	label7
#t29 = t10*2
	lw	$t0,	-112($fp)
	sll	$a3,	$t0,	1
#t30 = t29+1
	addiu	$t3,	$a3,	1
#t12 = t30
	addu	$v1,	$0,	$t3
#label7:
label7:
#t31 = &g2
	la	$a3,	g2
#t32 = t10*4
	lw	$t0,	-112($fp)
	sll	$t3,	$t0,	2
#t31 = t31+t32
	addu	$a3,	$a3,	$t3
#t33 = &g2
	la	$t3,	g2
#t34 = t12*4
	sll	$t4,	$v1,	2
#t33 = t33+t34
	addu	$t3,	$t3,	$t4
#t35 = [t33]<[t31]
	lw	$t0,	0($t3)
	lw	$t1,	0($a3)
	slt	$t4,	$t0,	$t1
#if t35==0 j label9
	beq	$t4,	$0,	label9
#t36 = exchange( t10 t12 )
	lw	$t0,	-112($fp)
	sw	$t0,	0($sp)
	sw	$v1,	4($sp)
	jal	label1
	sw	$v0,	-8($fp)
#label9:
label9:
#t37 = t10-1
	lw	$t0,	-112($fp)
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t10 = t37
	sw	$v1,	-112($fp)
#j label5
	j label5
#label6:
label6:
#ret 0
	li	$v0,	0
	j label3exit
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

label10:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-156
#t40 = 0
	addiu	$v1,	$0,	0
#t39 = t40
	addu	$a3,	$0,	$v1
#t38 = t39
	addu	$v1,	$0,	$a3
#label12:
label12:
#t41 = t38*2
	sll	$t3,	$v1,	1
#t42 = t41<arg1
	lw	$t1,	8($fp)
	slt	$t4,	$t3,	$t1
#if t42==0 j label13
	beq	$t4,	$0,	label13
#t43 = t38*2
	sll	$t3,	$v1,	1
#t39 = t43
	addu	$a3,	$0,	$t3
#t44 = 0
	addiu	$t3,	$0,	0
#t45 = t38*2
	sll	$t3,	$v1,	1
#t46 = t45+1
	addiu	$t4,	$t3,	1
#t47 = t46<arg1
	lw	$t1,	8($fp)
	slt	$t3,	$t4,	$t1
#if t47==0 j label14
	beq	$t3,	$0,	label14
#t48 = &g2
	la	$t3,	g2
#t49 = t38*2
	sll	$t4,	$v1,	1
#t50 = t49+1
	addiu	$t5,	$t4,	1
#t51 = t50*4
	sll	$t4,	$t5,	2
#t48 = t48+t51
	addu	$t3,	$t3,	$t4
#t52 = &g2
	la	$t4,	g2
#t53 = t38*2
	sll	$t5,	$v1,	1
#t54 = t53*4
	sll	$t6,	$t5,	2
#t52 = t52+t54
	addu	$t4,	$t4,	$t6
#t55 = [t48]<[t52]
	lw	$t0,	0($t3)
	lw	$t1,	0($t4)
	slt	$t5,	$t0,	$t1
#if t55==0 j label14
	beq	$t5,	$0,	label14
#t44 = 1
	addiu	$t3,	$0,	1
#label15:
label15:
#if t44==0 j label14
	beq	$t3,	$0,	label14
#t56 = t38*2
	sll	$t3,	$v1,	1
#t57 = t56+1
	addiu	$t4,	$t3,	1
#t39 = t57
	addu	$a3,	$0,	$t4
#label14:
label14:
#t58 = &g2
	la	$t3,	g2
#t59 = t38*4
	sll	$t4,	$v1,	2
#t58 = t58+t59
	addu	$t3,	$t3,	$t4
#t60 = &g2
	la	$t4,	g2
#t61 = t39*4
	sll	$t5,	$a3,	2
#t60 = t60+t61
	addu	$t4,	$t4,	$t5
#t62 = [t60]<[t58]
	lw	$t0,	0($t4)
	lw	$t1,	0($t3)
	slt	$t5,	$t0,	$t1
#if t62==0 j label16
	beq	$t5,	$0,	label16
#t63 = &g2
	la	$t3,	g2
#t64 = t38*4
	sll	$t4,	$v1,	2
#t63 = t63+t64
	addu	$t3,	$t3,	$t4
#t65 = &g2
	la	$t4,	g2
#t66 = t39*4
	sll	$t5,	$a3,	2
#t65 = t65+t66
	addu	$t4,	$t4,	$t5
#[t63] = [t63]^[t65]
	lw	$t0,	0($t3)
	lw	$t1,	0($t4)
	xor	$t2,	$t0,	$t1
	sw	$t2,	0($t3)
#t67 = &g2
	la	$t3,	g2
#t68 = t39*4
	sll	$t4,	$a3,	2
#t67 = t67+t68
	addu	$t3,	$t3,	$t4
#t69 = &g2
	la	$t4,	g2
#t70 = t38*4
	sll	$t5,	$v1,	2
#t69 = t69+t70
	addu	$t4,	$t4,	$t5
#[t67] = [t67]^[t69]
	lw	$t0,	0($t3)
	lw	$t1,	0($t4)
	xor	$t2,	$t0,	$t1
	sw	$t2,	0($t3)
#t71 = &g2
	la	$t3,	g2
#t72 = t38*4
	sll	$t4,	$v1,	2
#t71 = t71+t72
	addu	$t3,	$t3,	$t4
#t73 = &g2
	la	$t4,	g2
#t74 = t39*4
	sll	$t5,	$a3,	2
#t73 = t73+t74
	addu	$t4,	$t4,	$t5
#[t71] = [t71]^[t73]
	lw	$t0,	0($t3)
	lw	$t1,	0($t4)
	xor	$t2,	$t0,	$t1
	sw	$t2,	0($t3)
#t38 = t39
	addu	$v1,	$0,	$a3
#j label17
	j label17
#label16:
label16:
#j label13
	j label13
#label17:
label17:
#j label12
	j label12
#label13:
label13:
#ret 0
	li	$v0,	0
	j label10exit
label10exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label11:

label18:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-80
#t75 = 0
	addiu	$a3,	$0,	0
#t76 = 0
	li	$t0,	0
	sw	$t0,	-64($fp)
#label20:
label20:
#t77 = t76<g1
	lw	$t0,	-64($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t77==0 j label21
	beq	$v1,	$0,	label21
#t78 = &g2
	la	$v1,	g2
#t75 = [t78]
	lw	$t0,	0($v1)
	addu	$a3,	$0,	$t0
#t79 = &g2
	la	$v1,	g2
#t80 = &g2
	la	$t3,	g2
#t81 = g1-t76
	lw	$t0,	g1
	lw	$t1,	-64($fp)
	subu	$t4,	$t0,	$t1
#t82 = t81-1
	li	$t1,	1
	subu	$t5,	$t4,	$t1
#t83 = t82*4
	sll	$t4,	$t5,	2
#t80 = t80+t83
	addu	$t3,	$t3,	$t4
#[t79] = [t80]
	lw	$t0,	0($t3)
	sw	$t0,	0($v1)
#t84 = &g2
	la	$v1,	g2
#t85 = g1-t76
	lw	$t0,	g1
	lw	$t1,	-64($fp)
	subu	$t3,	$t0,	$t1
#t86 = t85-1
	li	$t1,	1
	subu	$t4,	$t3,	$t1
#t87 = t86*4
	sll	$t3,	$t4,	2
#t84 = t84+t87
	addu	$v1,	$v1,	$t3
#[t84] = t75
	sw	$a3,	0($v1)
#t88 = g1-t76
	lw	$t0,	g1
	lw	$t1,	-64($fp)
	subu	$v1,	$t0,	$t1
#t89 = t88-1
	li	$t1,	1
	subu	$a3,	$v1,	$t1
#t90 = adjustHeap( t89 )
	sw	$a3,	0($sp)
	jal	label10
	sw	$v0,	-8($fp)
#label22:
label22:
#t91 = t76+1
	lw	$t0,	-64($fp)
	addiu	$v1,	$t0,	1
#t76 = t91
	sw	$v1,	-64($fp)
#j label20
	j label20
#label21:
label21:
#ret 0
	li	$v0,	0
	j label18exit
label18exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label19:

label23:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-68
#t92 = 0
	li	$t0,	0
	sw	$t0,	-52($fp)
#label25:
label25:
#t93 = t92<g1
	lw	$t0,	-52($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t93==0 j label26
	beq	$v1,	$0,	label26
#t94 = &g2
	la	$v1,	g2
#t95 = t92*4
	lw	$t0,	-52($fp)
	sll	$a3,	$t0,	2
#t94 = t94+t95
	addu	$v1,	$v1,	$a3
#[t94] = t92
	lw	$t0,	-52($fp)
	sw	$t0,	0($v1)
#label27:
label27:
#t96 = t92+1
	lw	$t0,	-52($fp)
	addiu	$v1,	$t0,	1
#t92 = t96
	sw	$v1,	-52($fp)
#j label25
	j label25
#label26:
label26:
#t97 = makeHeap( )
	jal	label3
	sw	$v0,	-32($fp)
#t98 = heapSort( )
	jal	label18
	sw	$v0,	-28($fp)
#t92 = 0
	li	$t0,	0
	sw	$t0,	-52($fp)
#label28:
label28:
#t99 = t92<g1
	lw	$t0,	-52($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t99==0 j label29
	beq	$v1,	$0,	label29
#t100 = &g2
	la	$v1,	g2
#t101 = t92*4
	lw	$t0,	-52($fp)
	sll	$a3,	$t0,	2
#t100 = t100+t101
	addu	$v1,	$v1,	$a3
#t102 = printf( g3 [t100] )
	la	$t0,	g3
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#label30:
label30:
#t103 = t92+1
	lw	$t0,	-52($fp)
	addiu	$v1,	$t0,	1
#t92 = t103
	sw	$v1,	-52($fp)
#j label28
	j label28
#label29:
label29:
#t104 = printf( g4 )
	la	$a0,	g4
	li	$v0,	4
	syscall
#ret 0
	li	$v0,	0
	j label23exit
label23exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label24:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
