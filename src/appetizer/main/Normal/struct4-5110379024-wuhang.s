    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g2:
	.asciiz "%c%c\t"
	.align 2
g3:
	.asciiz "%d %d %d %d\n"
	.align 2
g4:
	.asciiz "%c %c\n"
	.align 2
g5:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
g1:
	.space 12
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label1
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-120
#t2 = &t1
	la	$v1,	-84($fp)
#t2 = t2+0
	addiu	$v1,	$v1,	0
#[t2] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t3 = &t1
	la	$t1,	-84($fp)
	sw	$t1,	-68($fp)
#t3 = t3+4
	lw	$t0,	-68($fp)
	addiu	$t2,	$t0,	4
	sw	$t2,	-68($fp)
#[t3] = 69
	li	$t0,	69
	lw	$t1,	-68($fp)
	sw	$t0,	0($t1)
#t4 = &t1
	la	$t1,	-84($fp)
	sw	$t1,	-64($fp)
#t4 = t4+8
	lw	$t0,	-64($fp)
	addiu	$t2,	$t0,	8
	sw	$t2,	-64($fp)
#[t4] = 10
	li	$t0,	10
	lw	$t1,	-64($fp)
	sw	$t0,	0($t1)
#t5 = &t1
	la	$t1,	-84($fp)
	sw	$t1,	-60($fp)
#t5 = t5+4
	lw	$t0,	-60($fp)
	addiu	$t2,	$t0,	4
	sw	$t2,	-60($fp)
#t6 = &t1
	la	$a3,	-84($fp)
#t6 = t6+8
	addiu	$a3,	$a3,	8
#t7 = printf( g2 [t5] [t6] )
	la	$t0,	g2
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-60($fp)
	lw	$t1,	0($t0)
	sw	$t1,	4($sp)
	lw	$t1,	0($a3)
	sw	$t1,	8($sp)
	jal	printf
#t8 = printf( g3 12 12 4 4 )
	la	$t0,	g3
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	li	$t0,	12
	sw	$t0,	4($sp)
	li	$t0,	12
	sw	$t0,	8($sp)
	li	$t0,	4
	sw	$t0,	12($sp)
	li	$t0,	4
	sw	$t0,	16($sp)
	jal	printf
#t9 = &g1
	la	$v1,	g1
#t9 = t9+0
	addiu	$v1,	$v1,	0
#t10 = &t1
	la	$a3,	-84($fp)
#t10 = t10+4
	addiu	$a3,	$a3,	4
#[t9] = [t10]
	addu	$a2,	$0,	$a3
	addu	$a1,	$0,	$v1
	li	$a0,	12
	jal	memcpy
#t11 = &g1
	la	$v1,	g1
#t11 = t11+0
	addiu	$v1,	$v1,	0
#t12 = &t1
	la	$a3,	-84($fp)
#t12 = t12+8
	addiu	$a3,	$a3,	8
#[t11] = [t12]
	addu	$a2,	$0,	$a3
	addu	$a1,	$0,	$v1
	li	$a0,	12
	jal	memcpy
#t13 = &g1
	la	$v1,	g1
#t13 = t13+0
	addiu	$v1,	$v1,	0
#t14 = &g1
	la	$a3,	g1
#t14 = t14+0
	addiu	$a3,	$a3,	0
#t15 = printf( g4 [t13] [t14] )
	la	$t0,	g4
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	lw	$t1,	0($a3)
	sw	$t1,	8($sp)
	jal	printf
#t16 = &g1
	la	$v1,	g1
#t16 = t16+0
	addiu	$v1,	$v1,	0
#[t16] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#t17 = &g1
	la	$v1,	g1
#t17 = t17+0
	addiu	$v1,	$v1,	0
#t18 = [t17]==2
	lw	$t0,	0($v1)
	li	$t1,	2
	xor	$s0,	$t0,	$t1
	sltiu	$a3,	$s0,	1
#t19 = printf( g5 t18 )
	la	$t0,	g5
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	sw	$a3,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
