    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label3
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-64
#t1 = arg1+arg2
	lw	$t0,	8($fp)
	lw	$t1,	12($fp)
	addu	$v1,	$t0,	$t1
#t2 = t1+arg3
	lw	$t1,	16($fp)
	addu	$t2,	$v1,	$t1
	sw	$t2,	-52($fp)
#t3 = t2+arg4
	lw	$t0,	-52($fp)
	lw	$t1,	20($fp)
	addu	$a3,	$t0,	$t1
#t4 = t3+arg5
	lw	$t1,	24($fp)
	addu	$t3,	$a3,	$t1
#t5 = t4+arg6
	lw	$t1,	28($fp)
	addu	$t4,	$t3,	$t1
#t6 = t5+arg7
	lw	$t1,	32($fp)
	addu	$t5,	$t4,	$t1
#t7 = t6+arg8
	lw	$t1,	36($fp)
	addu	$t6,	$t5,	$t1
#t8 = t7+arg9
	lw	$t1,	40($fp)
	addu	$t7,	$t6,	$t1
#t9 = t8+arg10
	lw	$t1,	44($fp)
	addu	$t8,	$t7,	$t1
#t10 = t9+arg11
	lw	$t1,	48($fp)
	addu	$t9,	$t8,	$t1
#t11 = t10+arg12
	lw	$t1,	52($fp)
	addu	$s1,	$t9,	$t1
#t12 = t11+arg13
	lw	$t1,	56($fp)
	addu	$s2,	$s1,	$t1
#t13 = t12+arg14
	lw	$t1,	60($fp)
	addu	$s3,	$s2,	$t1
#t14 = t13+arg15
	lw	$t1,	64($fp)
	addu	$t2,	$s3,	$t1
	sw	$t2,	-4($fp)
#ret t14
	lw	$v0,	-4($fp)
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-76
#t15 = a( 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 )
	li	$t0,	1
	sw	$t0,	0($sp)
	li	$t0,	2
	sw	$t0,	4($sp)
	li	$t0,	3
	sw	$t0,	8($sp)
	li	$t0,	4
	sw	$t0,	12($sp)
	li	$t0,	5
	sw	$t0,	16($sp)
	li	$t0,	6
	sw	$t0,	20($sp)
	li	$t0,	7
	sw	$t0,	24($sp)
	li	$t0,	8
	sw	$t0,	28($sp)
	li	$t0,	9
	sw	$t0,	32($sp)
	li	$t0,	10
	sw	$t0,	36($sp)
	li	$t0,	11
	sw	$t0,	40($sp)
	li	$t0,	12
	sw	$t0,	44($sp)
	li	$t0,	13
	sw	$t0,	48($sp)
	li	$t0,	14
	sw	$t0,	52($sp)
	li	$t0,	15
	sw	$t0,	56($sp)
	jal	label1
	sw	$v0,	-8($fp)
#t16 = printf( g1 t15 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-8($fp)
	sw	$t0,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label3exit
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
