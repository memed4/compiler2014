    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "move %c --> %c\n"
	.align 2
g2:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label5
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-64
#t1 = arg1==1
	lw	$t0,	8($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$t2,	$s0,	1
	sw	$t2,	-36($fp)
#if t1==0 j label3
	lw	$t0,	-36($fp)
	beq	$t0,	$0,	label3
#t2 = printf( g1 arg2 arg4 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	lw	$t0,	20($fp)
	sw	$t0,	8($sp)
	jal	printf
#t3 = arg5
	lw	$t0,	24($fp)
	sw	$t0,	-28($fp)
#arg5 = arg5+1
	lw	$t0,	24($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	24($fp)
#j label4
	j label4
#label3:
label3:
#t4 = arg1-1
	lw	$t0,	8($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-24($fp)
#t5 = cd( t4 arg2 arg4 arg3 arg5 )
	lw	$t0,	-24($fp)
	sw	$t0,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	lw	$t0,	20($fp)
	sw	$t0,	8($sp)
	lw	$t0,	16($fp)
	sw	$t0,	12($sp)
	lw	$t0,	24($fp)
	sw	$t0,	16($sp)
	jal	label1
	sw	$v0,	-20($fp)
#arg5 = t5
	lw	$t0,	-20($fp)
	sw	$t0,	24($fp)
#t6 = printf( g1 arg2 arg4 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	lw	$t0,	20($fp)
	sw	$t0,	8($sp)
	jal	printf
#t7 = arg1-1
	lw	$t0,	8($fp)
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t8 = cd( t7 arg3 arg2 arg4 arg5 )
	sw	$v1,	0($sp)
	lw	$t0,	16($fp)
	sw	$t0,	4($sp)
	lw	$t0,	12($fp)
	sw	$t0,	8($sp)
	lw	$t0,	20($fp)
	sw	$t0,	12($sp)
	lw	$t0,	24($fp)
	sw	$t0,	16($sp)
	jal	label1
	sw	$v0,	-8($fp)
#arg5 = t8
	lw	$t0,	-8($fp)
	sw	$t0,	24($fp)
#t9 = arg5
	lw	$t0,	24($fp)
	sw	$t0,	-4($fp)
#arg5 = arg5+1
	lw	$t0,	24($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	24($fp)
#label4:
label4:
#ret arg5
	lw	$v0,	24($fp)
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label5:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-56
#t10 = 65
	addiu	$v1,	$0,	65
#t11 = 66
	addiu	$a3,	$0,	66
#t12 = 67
	addiu	$t3,	$0,	67
#t13 = 10
	addiu	$t4,	$0,	10
#t15 = cd( t13 t10 t11 t12 0 )
	sw	$t4,	0($sp)
	sw	$v1,	4($sp)
	sw	$a3,	8($sp)
	sw	$t3,	12($sp)
	li	$t0,	0
	sw	$t0,	16($sp)
	jal	label1
	sw	$v0,	-8($fp)
#t14 = t15
	lw	$t0,	-8($fp)
	addu	$v1,	$0,	$t0
#t16 = printf( g2 t14 )
	la	$t0,	g2
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	sw	$v1,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label5exit
label5exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label6:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
