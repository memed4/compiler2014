#!/bin/bash

CCHK="java -cp /Users/Kay/project/compiler/compiler/bin:/Users/Kay/project/compiler/compiler/lib/antlr-3.5.1.jar appetizer.main.Main"

for source in `ls *.c`; do
  cn=${source%.c}
  $CCHK $source > ${cn}.s
done

