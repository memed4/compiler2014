    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g10:
	.asciiz "%d "
	.align 2
g11:
	.asciiz "%c"
	.align 2
g12:
	.asciiz "Sorry, the number n must be a number s.t. there exists i satisfying n=1+2+...+i\n"
	.align 2
g13:
	.asciiz "Let's start!\n"
	.align 2
g14:
	.asciiz "%d\n"
	.align 2
g15:
	.asciiz "step %d:\n"
	.align 2
g16:
	.asciiz "Total: %d step(s)\n"
	.align 2
	.data
	.align 2
g1:
	.word 210
	.space 4
	.align 2
g2:
	.space 4
	.align 2
g3:
	.space 4
	.align 2
g4:
	.space 4000
	.align 2
g5:
	.word 48271
	.space 4
	.align 2
g6:
	.word 2147483647
	.space 4
	.align 2
g7:
	.space 4
	.align 2
g8:
	.space 4
	.align 2
g9:
	.word 1
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label56
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t2 = g9%g7
	lw	$t0,	g9
	lw	$t1,	g7
	divu	$t0,	$t1
	mfhi	$v1
#t3 = g5*t2
	lw	$t0,	g5
	mul	$t5,	$t0,	$v1
#t4 = g9/g7
	lw	$t0,	g9
	lw	$t1,	g7
	divu	$t0,	$t1
	mflo	$v1
#t5 = g8*t4
	lw	$t0,	g8
	mul	$a3,	$t0,	$v1
#t6 = t3-t5
	subu	$v1,	$t5,	$a3
#t1 = t6
	addu	$a3,	$0,	$v1
#t7 = t1<0
	li	$t1,	0
	slt	$t3,	$a3,	$t1
#t7 = t7==0
	li	$t1,	0
	xor	$s0,	$t3,	$t1
	sltiu	$t3,	$s0,	1
#if t7==0 j label3
	beq	$t3,	$0,	label3
#g9 = t1
	sw	$a3,	g9
#j label4
	j label4
#label3:
label3:
#t8 = t1+g6
	lw	$t1,	g6
	addu	$t4,	$a3,	$t1
#g9 = t8
	sw	$t4,	g9
#label4:
label4:
#ret g9
	lw	$v0,	g9
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label5:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-8
#g9 = arg1
	lw	$t0,	8($fp)
	sw	$t0,	g9
label5exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label6:

label7:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-44
#t10 = &g4
	la	$v1,	g4
#t11 = arg1*4
	lw	$t0,	8($fp)
	sll	$a3,	$t0,	2
#t10 = t10+t11
	addu	$v1,	$v1,	$a3
#t9 = [t10]
	lw	$t0,	0($v1)
	addu	$a3,	$0,	$t0
#t12 = &g4
	la	$v1,	g4
#t13 = arg1*4
	lw	$t0,	8($fp)
	sll	$t3,	$t0,	2
#t12 = t12+t13
	addu	$v1,	$v1,	$t3
#t14 = &g4
	la	$t3,	g4
#t15 = arg2*4
	lw	$t0,	12($fp)
	sll	$t4,	$t0,	2
#t14 = t14+t15
	addu	$t3,	$t3,	$t4
#[t12] = [t14]
	lw	$t0,	0($t3)
	sw	$t0,	0($v1)
#t16 = &g4
	la	$v1,	g4
#t17 = arg2*4
	lw	$t0,	12($fp)
	sll	$t3,	$t0,	2
#t16 = t16+t17
	addu	$v1,	$v1,	$t3
#[t16] = t9
	sw	$a3,	0($v1)
label7exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label8:

label9:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-28
#label11:
label11:
#t18 = arg1<g2
	lw	$t0,	8($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#t18 = t18==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t18==0 j label12
	beq	$v1,	$0,	label12
#t19 = g2+1
	lw	$t0,	g2
	addiu	$v1,	$t0,	1
#t20 = g2*t19
	lw	$t0,	g2
	mul	$a3,	$t0,	$v1
#t21 = t20/2
	li	$t1,	2
	divu	$a3,	$t1
	mflo	$v1
#t22 = arg1==t21
	lw	$t0,	8($fp)
	xor	$s0,	$t0,	$v1
	sltiu	$a3,	$s0,	1
#if t22==0 j label14
	beq	$a3,	$0,	label14
#ret 1
	li	$v0,	1
	j label9exit
#label14:
label14:
#label13:
label13:
#g2 = g2+1
	lw	$t0,	g2
	addiu	$t2,	$t0,	1
	sw	$t2,	g2
#j label11
	j label11
#label12:
label12:
#ret 0
	li	$v0,	0
	j label9exit
label9exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label10:

label15:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t23 = 0
	li	$t0,	0
	sw	$t0,	-24($fp)
#label17:
label17:
#t24 = t23<g3
	lw	$t0,	-24($fp)
	lw	$t1,	g3
	slt	$v1,	$t0,	$t1
#if t24==0 j label18
	beq	$v1,	$0,	label18
#t25 = &g4
	la	$v1,	g4
#t26 = t23*4
	lw	$t0,	-24($fp)
	sll	$a3,	$t0,	2
#t25 = t25+t26
	addu	$v1,	$v1,	$a3
#t27 = printf( g10 [t25] )
	la	$t0,	g10
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#label19:
label19:
#t23 = t23+1
	lw	$t0,	-24($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-24($fp)
#j label17
	j label17
#label18:
label18:
#t28 = printf( g11 10 )
	la	$t0,	g11
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	li	$t0,	10
	sw	$t0,	4($sp)
	jal	printf
label15exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label16:

label20:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-4132
#t30 = 0
	addiu	$v1,	$0,	0
#t33 = g3!=g2
	lw	$t0,	g3
	lw	$t1,	g2
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t33==0 j label22
	beq	$a3,	$0,	label22
#ret 0
	li	$v0,	0
	j label20exit
#label22:
label22:
#label23:
label23:
#t34 = t30<g3
	lw	$t1,	g3
	slt	$a3,	$v1,	$t1
#if t34==0 j label24
	beq	$a3,	$0,	label24
#t35 = &t31
	la	$a3,	-4116($fp)
#t36 = t30*4
	sll	$t3,	$v1,	2
#t35 = t35+t36
	addu	$a3,	$a3,	$t3
#t37 = &g4
	la	$t3,	g4
#t38 = t30*4
	sll	$t4,	$v1,	2
#t37 = t37+t38
	addu	$t3,	$t3,	$t4
#[t35] = [t37]
	lw	$t0,	0($t3)
	sw	$t0,	0($a3)
#label25:
label25:
#t30 = t30+1
	addiu	$v1,	$v1,	1
#j label23
	j label23
#label24:
label24:
#t29 = 0
	addiu	$a3,	$0,	0
#label26:
label26:
#t39 = g3-1
	lw	$t0,	g3
	li	$t1,	1
	subu	$t3,	$t0,	$t1
#t40 = t29<t39
	slt	$t4,	$a3,	$t3
#if t40==0 j label27
	beq	$t4,	$0,	label27
#t41 = t29+1
	addiu	$t3,	$a3,	1
#t30 = t41
	addu	$v1,	$0,	$t3
#label29:
label29:
#t42 = t30<g3
	lw	$t1,	g3
	slt	$t3,	$v1,	$t1
#if t42==0 j label30
	beq	$t3,	$0,	label30
#t43 = &t31
	la	$t3,	-4116($fp)
#t44 = t29*4
	sll	$t4,	$a3,	2
#t43 = t43+t44
	addu	$t3,	$t3,	$t4
#t45 = &t31
	la	$t4,	-4116($fp)
#t46 = t30*4
	sll	$t5,	$v1,	2
#t45 = t45+t46
	addu	$t4,	$t4,	$t5
#t47 = [t45]<[t43]
	lw	$t0,	0($t4)
	lw	$t1,	0($t3)
	slt	$t5,	$t0,	$t1
#if t47==0 j label32
	beq	$t5,	$0,	label32
#t48 = &t31
	la	$t3,	-4116($fp)
#t49 = t29*4
	sll	$t4,	$a3,	2
#t48 = t48+t49
	addu	$t3,	$t3,	$t4
#t32 = [t48]
	lw	$t0,	0($t3)
	addu	$t4,	$0,	$t0
#t50 = &t31
	la	$t3,	-4116($fp)
#t51 = t29*4
	sll	$t5,	$a3,	2
#t50 = t50+t51
	addu	$t3,	$t3,	$t5
#t52 = &t31
	la	$t5,	-4116($fp)
#t53 = t30*4
	sll	$t6,	$v1,	2
#t52 = t52+t53
	addu	$t5,	$t5,	$t6
#[t50] = [t52]
	lw	$t0,	0($t5)
	sw	$t0,	0($t3)
#t54 = &t31
	la	$t3,	-4116($fp)
#t55 = t30*4
	sll	$t5,	$v1,	2
#t54 = t54+t55
	addu	$t3,	$t3,	$t5
#[t54] = t32
	sw	$t4,	0($t3)
#label32:
label32:
#label31:
label31:
#t30 = t30+1
	addiu	$v1,	$v1,	1
#j label29
	j label29
#label30:
label30:
#label28:
label28:
#t29 = t29+1
	addiu	$a3,	$a3,	1
#j label26
	j label26
#label27:
label27:
#t29 = 0
	addiu	$a3,	$0,	0
#label33:
label33:
#t56 = t29<g3
	lw	$t1,	g3
	slt	$v1,	$a3,	$t1
#if t56==0 j label34
	beq	$v1,	$0,	label34
#t57 = &t31
	la	$v1,	-4116($fp)
#t58 = t29*4
	sll	$t3,	$a3,	2
#t57 = t57+t58
	addu	$v1,	$v1,	$t3
#t59 = t29+1
	addiu	$t3,	$a3,	1
#t60 = [t57]!=t59
	lw	$t0,	0($v1)
	xor $s0,	$t0,	$t3
	sltu	$t4,	$0,	$s0
#if t60==0 j label36
	beq	$t4,	$0,	label36
#ret 0
	li	$v0,	0
	j label20exit
#label36:
label36:
#label35:
label35:
#t29 = t29+1
	addiu	$a3,	$a3,	1
#j label33
	j label33
#label34:
label34:
#ret 1
	li	$v0,	1
	j label20exit
label20exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label21:

label37:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-80
#t61 = 0
	li	$t0,	0
	sw	$t0,	-64($fp)
#label39:
label39:
#t62 = t61<g3
	lw	$t0,	-64($fp)
	lw	$t1,	g3
	slt	$v1,	$t0,	$t1
#if t62==0 j label40
	beq	$v1,	$0,	label40
#t63 = &g4
	la	$v1,	g4
#t64 = t61*4
	lw	$t0,	-64($fp)
	sll	$a3,	$t0,	2
#t63 = t63+t64
	addu	$v1,	$v1,	$a3
#t65 = ![t63]
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$0
	sltiu	$a3,	$s0,	1
#if t65==0 j label42
	beq	$a3,	$0,	label42
#t67 = t61+1
	lw	$t0,	-64($fp)
	addiu	$v1,	$t0,	1
#t66 = t67
	sw	$v1,	-44($fp)
#label43:
label43:
#t68 = t66<g3
	lw	$t0,	-44($fp)
	lw	$t1,	g3
	slt	$v1,	$t0,	$t1
#if t68==0 j label44
	beq	$v1,	$0,	label44
#t69 = &g4
	la	$v1,	g4
#t70 = t66*4
	lw	$t0,	-44($fp)
	sll	$a3,	$t0,	2
#t69 = t69+t70
	addu	$v1,	$v1,	$a3
#t71 = [t69]!=0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t71==0 j label46
	beq	$a3,	$0,	label46
#t72 = swap( t61 t66 )
	lw	$t0,	-64($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-44($fp)
	sw	$t0,	4($sp)
	jal	label7
	sw	$v0,	-20($fp)
#j label44
	j label44
#label46:
label46:
#label45:
label45:
#t66 = t66+1
	lw	$t0,	-44($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-44($fp)
#j label43
	j label43
#label44:
label44:
#label42:
label42:
#label41:
label41:
#t61 = t61+1
	lw	$t0,	-64($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-64($fp)
#j label39
	j label39
#label40:
label40:
#t61 = 0
	li	$t0,	0
	sw	$t0,	-64($fp)
#label47:
label47:
#t73 = t61<g3
	lw	$t0,	-64($fp)
	lw	$t1,	g3
	slt	$v1,	$t0,	$t1
#if t73==0 j label48
	beq	$v1,	$0,	label48
#t74 = &g4
	la	$v1,	g4
#t75 = t61*4
	lw	$t0,	-64($fp)
	sll	$a3,	$t0,	2
#t74 = t74+t75
	addu	$v1,	$v1,	$a3
#t76 = ![t74]
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$0
	sltiu	$a3,	$s0,	1
#if t76==0 j label50
	beq	$a3,	$0,	label50
#g3 = t61
	lw	$t0,	-64($fp)
	sw	$t0,	g3
#j label48
	j label48
#label50:
label50:
#label49:
label49:
#t61 = t61+1
	lw	$t0,	-64($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-64($fp)
#j label47
	j label47
#label48:
label48:
label37exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label38:

label51:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t77 = 0
	addiu	$v1,	$0,	0
#label53:
label53:
#t78 = t77<g3
	lw	$t1,	g3
	slt	$a3,	$v1,	$t1
#if t78==0 j label54
	beq	$a3,	$0,	label54
#label55:
label55:
#t79 = &g4
	la	$a3,	g4
#t80 = t77*4
	sll	$t3,	$v1,	2
#t79 = t79+t80
	addu	$a3,	$a3,	$t3
#[t79] = [t79]-1
	lw	$t0,	0($a3)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	0($a3)
#t81 = t77+1
	addiu	$a3,	$v1,	1
#t77 = t81
	addu	$v1,	$0,	$a3
#j label53
	j label53
#label54:
label54:
#t82 = &g4
	la	$v1,	g4
#t83 = g3*4
	lw	$t0,	g3
	sll	$a3,	$t0,	2
#t82 = t82+t83
	addu	$v1,	$v1,	$a3
#[t82] = g3
	lw	$t0,	g3
	sw	$t0,	0($v1)
#t84 = g3
	lw	$t0,	g3
	sw	$t0,	-4($fp)
#g3 = g3+1
	lw	$t0,	g3
	addiu	$t2,	$t0,	1
	sw	$t2,	g3
label51exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label52:

label56:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-196
#t85 = 0
	li	$t0,	0
	sw	$t0,	-180($fp)
#t86 = 0
	li	$t0,	0
	sw	$t0,	-176($fp)
#t87 = 0
	li	$t0,	0
	sw	$t0,	-172($fp)
#t88 = g6/g5
	lw	$t0,	g6
	lw	$t1,	g5
	divu	$t0,	$t1
	mflo	$v1
#g7 = t88
	sw	$v1,	g7
#t89 = g6%g5
	lw	$t0,	g6
	lw	$t1,	g5
	divu	$t0,	$t1
	mfhi	$v1
#g8 = t89
	sw	$v1,	g8
#t90 = pd( g1 )
	lw	$t0,	g1
	sw	$t0,	0($sp)
	jal	label9
	sw	$v0,	-160($fp)
#t91 = !t90
	lw	$t0,	-160($fp)
	xor	$s0,	$t0,	$0
	sltiu	$v1,	$s0,	1
#if t91==0 j label58
	beq	$v1,	$0,	label58
#t92 = printf( g12 )
	la	$a0,	g12
	li	$v0,	4
	syscall
#ret 1
	li	$v0,	1
	j label56exit
#label58:
label58:
#t93 = printf( g13 )
	la	$a0,	g13
	li	$v0,	4
	syscall
#t94 = initialize( 3654898 )
	li	$t0,	3654898
	sw	$t0,	0($sp)
	jal	label5
	sw	$v0,	-144($fp)
#t95 = random( )
	jal	label1
	sw	$v0,	-140($fp)
#t96 = t95%10
	lw	$t0,	-140($fp)
	li	$t1,	10
	divu	$t0,	$t1
	mfhi	$v1
#t97 = t96+1
	addiu	$a3,	$v1,	1
#g3 = t97
	sw	$a3,	g3
#t98 = printf( g14 g3 )
	la	$t0,	g14
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	g3
	sw	$t0,	4($sp)
	jal	printf
#label59:
label59:
#t99 = g3-1
	lw	$t0,	g3
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t100 = t85<t99
	lw	$t0,	-180($fp)
	slt	$a3,	$t0,	$v1
#if t100==0 j label60
	beq	$a3,	$0,	label60
#t101 = &g4
	la	$t1,	g4
	sw	$t1,	-116($fp)
#t102 = t85*4
	lw	$t0,	-180($fp)
	sll	$v1,	$t0,	2
#t101 = t101+t102
	lw	$t0,	-116($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-116($fp)
#t103 = random( )
	jal	label1
	sw	$v0,	-108($fp)
#t104 = t103%10
	lw	$t0,	-108($fp)
	li	$t1,	10
	divu	$t0,	$t1
	mfhi	$v1
#t105 = t104+1
	addiu	$a3,	$v1,	1
#[t101] = t105
	lw	$t1,	-116($fp)
	sw	$a3,	0($t1)
#label62:
label62:
#t106 = &g4
	la	$v1,	g4
#t107 = t85*4
	lw	$t0,	-180($fp)
	sll	$a3,	$t0,	2
#t106 = t106+t107
	addu	$v1,	$v1,	$a3
#t108 = [t106]+t86
	lw	$t0,	0($v1)
	lw	$t1,	-176($fp)
	addu	$a3,	$t0,	$t1
#t109 = g1<t108
	lw	$t0,	g1
	slt	$v1,	$t0,	$a3
#if t109==0 j label63
	beq	$v1,	$0,	label63
#t110 = &g4
	la	$t1,	g4
	sw	$t1,	-80($fp)
#t111 = t85*4
	lw	$t0,	-180($fp)
	sll	$v1,	$t0,	2
#t110 = t110+t111
	lw	$t0,	-80($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-80($fp)
#t112 = random( )
	jal	label1
	sw	$v0,	-72($fp)
#t113 = t112%10
	lw	$t0,	-72($fp)
	li	$t1,	10
	divu	$t0,	$t1
	mfhi	$v1
#t114 = t113+1
	addiu	$a3,	$v1,	1
#[t110] = t114
	lw	$t1,	-80($fp)
	sw	$a3,	0($t1)
#j label62
	j label62
#label63:
label63:
#t115 = &g4
	la	$v1,	g4
#t116 = t85*4
	lw	$t0,	-180($fp)
	sll	$a3,	$t0,	2
#t115 = t115+t116
	addu	$v1,	$v1,	$a3
#t86 = t86+[t115]
	lw	$t0,	-176($fp)
	lw	$t1,	0($v1)
	addu	$t2,	$t0,	$t1
	sw	$t2,	-176($fp)
#label61:
label61:
#t85 = t85+1
	lw	$t0,	-180($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-180($fp)
#j label59
	j label59
#label60:
label60:
#t117 = &g4
	la	$v1,	g4
#t118 = g3-1
	lw	$t0,	g3
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t119 = t118*4
	sll	$t3,	$a3,	2
#t117 = t117+t119
	addu	$v1,	$v1,	$t3
#t120 = g1-t86
	lw	$t0,	g1
	lw	$t1,	-176($fp)
	subu	$a3,	$t0,	$t1
#[t117] = t120
	sw	$a3,	0($v1)
#t121 = show( )
	jal	label15
	sw	$v0,	-36($fp)
#t122 = merge( )
	jal	label37
	sw	$v0,	-32($fp)
#label64:
label64:
#t123 = win( )
	jal	label20
	sw	$v0,	-28($fp)
#t124 = !t123
	lw	$t0,	-28($fp)
	xor	$s0,	$t0,	$0
	sltiu	$v1,	$s0,	1
#if t124==0 j label65
	beq	$v1,	$0,	label65
#t87 = t87+1
	lw	$t0,	-172($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-172($fp)
#t125 = printf( g15 t87 )
	la	$t0,	g15
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-172($fp)
	sw	$t0,	4($sp)
	jal	printf
#t126 = move( )
	jal	label51
	sw	$v0,	-16($fp)
#t127 = merge( )
	jal	label37
	sw	$v0,	-12($fp)
#t128 = show( )
	jal	label15
	sw	$v0,	-8($fp)
#j label64
	j label64
#label65:
label65:
#t129 = printf( g16 t87 )
	la	$t0,	g16
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-172($fp)
	sw	$t0,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label56exit
label56exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label57:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
