    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "%04d"
	.align 2
g2:
	.asciiz "\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label1
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-11316
#t1 = 10000
	li	$t0,	10000
	sw	$t0,	-11300($fp)
#t2 = 0
	li	$t0,	0
	sw	$t0,	-11296($fp)
#t3 = 2800
	li	$t0,	2800
	sw	$t0,	-11292($fp)
#t4 = 0
	li	$t0,	0
	sw	$t0,	-11288($fp)
#t5 = 0
	li	$t0,	0
	sw	$t0,	-11284($fp)
#t7 = 0
	addiu	$a3,	$0,	0
#label3:
label3:
#t8 = t2-t3
	lw	$t0,	-11296($fp)
	lw	$t1,	-11292($fp)
	subu	$v1,	$t0,	$t1
#if t8==0 j label4
	beq	$v1,	$0,	label4
#t9 = &t6
	la	$v1,	-11280($fp)
#t10 = t2
	lw	$t0,	-11296($fp)
	addu	$a3,	$0,	$t0
#t2 = t2+1
	lw	$t0,	-11296($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-11296($fp)
#t11 = t10*4
	sll	$t3,	$a3,	2
#t9 = t9+t11
	addu	$v1,	$v1,	$t3
#t12 = t1/5
	lw	$t0,	-11300($fp)
	li	$t1,	5
	divu	$t0,	$t1
	mflo	$a3
#[t9] = t12
	sw	$a3,	0($v1)
#label5:
label5:
#j label3
	j label3
#label4:
label4:
#label6:
label6:
#t4 = 0
	li	$t0,	0
	sw	$t0,	-11288($fp)
#t13 = t3*2
	lw	$t0,	-11292($fp)
	sll	$v1,	$t0,	1
#t7 = t13
	addu	$a3,	$0,	$v1
#if t7==0 j label7
	beq	$a3,	$0,	label7
#t2 = t3
	lw	$t0,	-11292($fp)
	sw	$t0,	-11296($fp)
#label9:
label9:
#t14 = &t6
	la	$v1,	-11280($fp)
#t15 = t2*4
	lw	$t0,	-11296($fp)
	sll	$t3,	$t0,	2
#t14 = t14+t15
	addu	$v1,	$v1,	$t3
#t16 = [t14]*t1
	lw	$t0,	0($v1)
	lw	$t1,	-11300($fp)
	mul	$t3,	$t0,	$t1
#t4 = t4+t16
	lw	$t0,	-11288($fp)
	addu	$t2,	$t0,	$t3
	sw	$t2,	-11288($fp)
#t17 = &t6
	la	$v1,	-11280($fp)
#t18 = t2*4
	lw	$t0,	-11296($fp)
	sll	$t3,	$t0,	2
#t17 = t17+t18
	addu	$v1,	$v1,	$t3
#t7 = t7-1
	li	$t1,	1
	subu	$a3,	$a3,	$t1
#t19 = t4%t7
	lw	$t0,	-11288($fp)
	divu	$t0,	$a3
	mfhi	$t3
#[t17] = t19
	sw	$t3,	0($v1)
#t20 = t7
	addu	$v1,	$0,	$a3
#t7 = t7-1
	li	$t1,	1
	subu	$a3,	$a3,	$t1
#t4 = t4/t20
	lw	$t0,	-11288($fp)
	divu	$t0,	$v1
	mflo	$t2
	sw	$t2,	-11288($fp)
#t2 = t2-1
	lw	$t0,	-11296($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-11296($fp)
#if t2==0 j label10
	lw	$t0,	-11296($fp)
	beq	$t0,	$0,	label10
#label11:
label11:
#t4 = t4*t2
	lw	$t0,	-11288($fp)
	lw	$t1,	-11296($fp)
	mul	$t2,	$t0,	$t1
	sw	$t2,	-11288($fp)
#j label9
	j label9
#label10:
label10:
#label8:
label8:
#t3 = t3-14
	lw	$t0,	-11292($fp)
	li	$t1,	14
	subu	$t2,	$t0,	$t1
	sw	$t2,	-11292($fp)
#t21 = t4/t1
	lw	$t0,	-11288($fp)
	lw	$t1,	-11300($fp)
	divu	$t0,	$t1
	mflo	$v1
#t22 = t5+t21
	lw	$t0,	-11284($fp)
	addu	$a3,	$t0,	$v1
#t23 = printf( g1 t22 )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	sw	$a3,	4($sp)
	jal	printf
#t24 = t4%t1
	lw	$t0,	-11288($fp)
	lw	$t1,	-11300($fp)
	divu	$t0,	$t1
	mfhi	$v1
#t5 = t24
	sw	$v1,	-11284($fp)
#j label6
	j label6
#label7:
label7:
#t25 = printf( g2 )
	la	$a0,	g2
	li	$v0,	4
	syscall
#ret 0
	li	$v0,	0
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
