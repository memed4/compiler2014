    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g3:
	.asciiz "%d %d\n"
	.align 2
	.data
	.align 2
g1:
	.word 100
	.space 4
	.align 2
g2:
	.space 400
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label13
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-16
#t1 = arg1*237
	lw	$t0,	8($fp)
	li	$t1,	237
	mul	$v1,	$t0,	$t1
#t2 = t1%g1
	lw	$t1,	g1
	divu	$v1,	$t1
	mfhi	$a3
#ret t2
	addu	$v0,	$0,	$a3
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-120
#t4 = 0
	li	$t0,	0
	sw	$t0,	-104($fp)
#t5 = getHash( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label1
	sw	$v0,	-100($fp)
#t3 = t5
	lw	$t0,	-100($fp)
	sw	$t0,	-108($fp)
#t6 = &g2
	la	$v1,	g2
#t7 = t3*4
	lw	$t0,	-108($fp)
	sll	$a3,	$t0,	2
#t6 = t6+t7
	addu	$v1,	$v1,	$a3
#t8 = [t6]==0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$a3,	$s0,	1
#if t8==0 j label5
	beq	$a3,	$0,	label5
#t9 = &g2
	la	$t1,	g2
	sw	$t1,	-84($fp)
#t10 = t3*4
	lw	$t0,	-108($fp)
	sll	$v1,	$t0,	2
#t9 = t9+t10
	lw	$t0,	-84($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-84($fp)
#t11 = malloc( 12 )
	li	$t0,	12
	addu	$a0,	$0,	$t0
	jal	malloc
	sw	$v0,	-76($fp)
#[t9] = t11
	lw	$t0,	-76($fp)
	lw	$t1,	-84($fp)
	sw	$t0,	0($t1)
#t12 = &g2
	la	$v1,	g2
#t13 = t3*4
	lw	$t0,	-108($fp)
	sll	$a3,	$t0,	2
#t12 = t12+t13
	addu	$v1,	$v1,	$a3
#t12 = [t12]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#[t12] = arg1
	lw	$t0,	8($fp)
	sw	$t0,	0($v1)
#t14 = &g2
	la	$v1,	g2
#t15 = t3*4
	lw	$t0,	-108($fp)
	sll	$a3,	$t0,	2
#t14 = t14+t15
	addu	$v1,	$v1,	$a3
#t14 = [t14]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t14 = t14+4
	addiu	$v1,	$v1,	4
#[t14] = arg2
	lw	$t0,	12($fp)
	sw	$t0,	0($v1)
#t16 = &g2
	la	$v1,	g2
#t17 = t3*4
	lw	$t0,	-108($fp)
	sll	$a3,	$t0,	2
#t16 = t16+t17
	addu	$v1,	$v1,	$a3
#t16 = [t16]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t16 = t16+8
	addiu	$v1,	$v1,	8
#[t16] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#ret
	j label3exit
#label5:
label5:
#t18 = &g2
	la	$v1,	g2
#t19 = t3*4
	lw	$t0,	-108($fp)
	sll	$a3,	$t0,	2
#t18 = t18+t19
	addu	$v1,	$v1,	$a3
#t4 = [t18]
	lw	$t0,	0($v1)
	sw	$t0,	-104($fp)
#label6:
label6:
#t20 = t4
	lw	$t0,	-104($fp)
	addu	$v1,	$0,	$t0
#t21 = [t20]!=arg1
	lw	$t0,	0($v1)
	lw	$t1,	8($fp)
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t21==0 j label7
	beq	$a3,	$0,	label7
#t22 = t4+8
	lw	$t0,	-104($fp)
	addiu	$v1,	$t0,	8
#t23 = [t22]==0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$a3,	$s0,	1
#if t23==0 j label8
	beq	$a3,	$0,	label8
#t24 = t4+8
	lw	$t0,	-104($fp)
	addiu	$t2,	$t0,	8
	sw	$t2,	-24($fp)
#t25 = malloc( 12 )
	li	$t0,	12
	addu	$a0,	$0,	$t0
	jal	malloc
	sw	$v0,	-20($fp)
#[t24] = t25
	lw	$t0,	-20($fp)
	lw	$t1,	-24($fp)
	sw	$t0,	0($t1)
#t26 = t4+8
	lw	$t0,	-104($fp)
	addiu	$v1,	$t0,	8
#t26 = [t26]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#[t26] = arg1
	lw	$t0,	8($fp)
	sw	$t0,	0($v1)
#t27 = t4+8
	lw	$t0,	-104($fp)
	addiu	$v1,	$t0,	8
#t27 = [t27]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t27 = t27+8
	addiu	$v1,	$v1,	8
#[t27] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#label8:
label8:
#t28 = t4+8
	lw	$t0,	-104($fp)
	addiu	$v1,	$t0,	8
#t4 = [t28]
	lw	$t0,	0($v1)
	sw	$t0,	-104($fp)
#j label6
	j label6
#label7:
label7:
#t29 = t4+4
	lw	$t0,	-104($fp)
	addiu	$v1,	$t0,	4
#[t29] = arg2
	lw	$t0,	12($fp)
	sw	$t0,	0($v1)
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

label9:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-44
#t31 = 0
	addiu	$v1,	$0,	0
#t32 = &g2
	la	$t1,	g2
	sw	$t1,	-28($fp)
#t33 = getHash( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label1
	sw	$v0,	-24($fp)
#t34 = t33*4
	lw	$t0,	-24($fp)
	sll	$v1,	$t0,	2
#t32 = t32+t34
	lw	$t0,	-28($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-28($fp)
#t31 = [t32]
	lw	$t1,	-28($fp)
	lw	$t0,	0($t1)
	addu	$v1,	$0,	$t0
#label11:
label11:
#t35 = t31
	addu	$a3,	$0,	$v1
#t36 = [t35]!=arg1
	lw	$t0,	0($a3)
	lw	$t1,	8($fp)
	xor $s0,	$t0,	$t1
	sltu	$t3,	$0,	$s0
#if t36==0 j label12
	beq	$t3,	$0,	label12
#t37 = t31+8
	addiu	$a3,	$v1,	8
#t31 = [t37]
	lw	$t0,	0($a3)
	addu	$v1,	$0,	$t0
#j label11
	j label11
#label12:
label12:
#t38 = t31+4
	addiu	$a3,	$v1,	4
#ret [t38]
	lw	$v0,	0($a3)
	j label9exit
label9exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label10:

label13:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-56
#t39 = 0
	li	$t0,	0
	sw	$t0,	-36($fp)
#label15:
label15:
#t40 = t39<g1
	lw	$t0,	-36($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t40==0 j label16
	beq	$v1,	$0,	label16
#t41 = &g2
	la	$v1,	g2
#t42 = t39*4
	lw	$t0,	-36($fp)
	sll	$a3,	$t0,	2
#t41 = t41+t42
	addu	$v1,	$v1,	$a3
#[t41] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#label17:
label17:
#t39 = t39+1
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-36($fp)
#j label15
	j label15
#label16:
label16:
#t39 = 0
	li	$t0,	0
	sw	$t0,	-36($fp)
#label18:
label18:
#t44 = t39<1000
	lw	$t0,	-36($fp)
	li	$t1,	1000
	slt	$v1,	$t0,	$t1
#if t44==0 j label19
	beq	$v1,	$0,	label19
#t45 = put( t39 t39 )
	lw	$t0,	-36($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-36($fp)
	sw	$t0,	4($sp)
	jal	label3
	sw	$v0,	-16($fp)
#label20:
label20:
#t39 = t39+1
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-36($fp)
#j label18
	j label18
#label19:
label19:
#t39 = 0
	li	$t0,	0
	sw	$t0,	-36($fp)
#label21:
label21:
#t47 = t39<1000
	lw	$t0,	-36($fp)
	li	$t1,	1000
	slt	$v1,	$t0,	$t1
#if t47==0 j label22
	beq	$v1,	$0,	label22
#t48 = get( t39 )
	lw	$t0,	-36($fp)
	sw	$t0,	0($sp)
	jal	label9
	sw	$v0,	-8($fp)
#t49 = printf( g3 t39 t48 )
	la	$t0,	g3
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-36($fp)
	sw	$t0,	4($sp)
	lw	$t0,	-8($fp)
	sw	$t0,	8($sp)
	jal	printf
#label23:
label23:
#t39 = t39+1
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-36($fp)
#j label21
	j label21
#label22:
label22:
label13exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label14:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
