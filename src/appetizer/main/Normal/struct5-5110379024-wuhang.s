    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g4:
	.asciiz "%d\t"
	.align 2
g5:
	.asciiz "\n"
	.align 2
g6:
	.asciiz "%c %c %d, %d\n"
	.align 2
g7:
	.asciiz "%d "
	.align 2
	.data
	.align 2
g1:
	.word 5
	.space 4
	.align 2
g2:
	.word 5
	.space 4
	.align 2
g3:
	.space 580
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label39
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-20
#t1 = &arg1
	la	$v1,	8($fp)
#t1 = t1+0
	addiu	$v1,	$v1,	0
#t2 = &arg2
	la	$a3,	12($fp)
#t2 = t2+0
	addiu	$a3,	$a3,	0
#t3 = [t1]!=[t2]
	lw	$t0,	0($v1)
	lw	$t1,	0($a3)
	xor $s0,	$t0,	$t1
	sltu	$t3,	$0,	$s0
#if t3==0 j label3
	beq	$t3,	$0,	label3
#ret 0
	li	$v0,	0
	j label1exit
#j label4
	j label4
#label3:
label3:
#ret 1
	li	$v0,	1
	j label1exit
#label4:
label4:
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label5:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-332
#t5 = 0
	addiu	$a3,	$0,	0
#t4 = 0
	addiu	$v1,	$0,	0
#label7:
label7:
#t6 = t4<g1
	lw	$t1,	g1
	slt	$a3,	$v1,	$t1
#if t6==0 j label8
	beq	$a3,	$0,	label8
#t5 = 0
	addiu	$a3,	$0,	0
#label10:
label10:
#t7 = t5<g1
	lw	$t1,	g1
	slt	$t3,	$a3,	$t1
#if t7==0 j label11
	beq	$t3,	$0,	label11
#t8 = &arg1
	la	$t3,	8($fp)
#t8 = t8+0
	addiu	$t3,	$t3,	0
#t9 = t4*20
	li	$t1,	20
	mul	$t4,	$v1,	$t1
#t8 = t8+t9
	addu	$t3,	$t3,	$t4
#t10 = t5*4
	sll	$t4,	$a3,	2
#t8 = t8+t10
	addu	$t3,	$t3,	$t4
#t11 = &arg2
	la	$t4,	124($fp)
#t11 = t11+0
	addiu	$t4,	$t4,	0
#t12 = t4*20
	li	$t1,	20
	mul	$t5,	$v1,	$t1
#t11 = t11+t12
	addu	$t4,	$t4,	$t5
#t13 = t5*4
	sll	$t5,	$a3,	2
#t11 = t11+t13
	addu	$t4,	$t4,	$t5
#t14 = [t8]!=[t11]
	lw	$t0,	0($t3)
	lw	$t1,	0($t4)
	xor $s0,	$t0,	$t1
	sltu	$t5,	$0,	$s0
#if t14==0 j label13
	beq	$t5,	$0,	label13
#ret 0
	li	$v0,	0
	j label5exit
#label13:
label13:
#label12:
label12:
#t5 = t5+1
	addiu	$a3,	$a3,	1
#j label10
	j label10
#label11:
label11:
#label9:
label9:
#t4 = t4+1
	addiu	$v1,	$v1,	1
#j label7
	j label7
#label8:
label8:
#t15 = &arg1
	la	$v1,	8($fp)
#t15 = t15+100
	addiu	$v1,	$v1,	100
#t16 = 0*4
	li	$t0,	0
	sll	$a3,	$t0,	2
#t15 = t15+t16
	addu	$v1,	$v1,	$a3
#t17 = &arg2
	la	$a3,	124($fp)
#t17 = t17+100
	addiu	$a3,	$a3,	100
#t18 = 0*4
	li	$t0,	0
	sll	$t3,	$t0,	2
#t17 = t17+t18
	addu	$a3,	$a3,	$t3
#t19 = [t15]!=[t17]
	lw	$t0,	0($v1)
	lw	$t1,	0($a3)
	xor $s0,	$t0,	$t1
	sltu	$t3,	$0,	$s0
#if t19==0 j label14
	beq	$t3,	$0,	label14
#ret 0
	li	$v0,	0
	j label5exit
#j label15
	j label15
#label14:
label14:
#t20 = &arg1
	la	$v1,	8($fp)
#t20 = t20+100
	addiu	$v1,	$v1,	100
#t20 = t20+4
	addiu	$v1,	$v1,	4
#t22 = &arg2
	la	$a3,	124($fp)
#t22 = t22+100
	addiu	$a3,	$a3,	100
#t22 = t22+4
	addiu	$a3,	$a3,	4
#t24 = [t20]!=[t22]
	lw	$t0,	0($v1)
	lw	$t1,	0($a3)
	xor $s0,	$t0,	$t1
	sltu	$t3,	$0,	$s0
#if t24==0 j label16
	beq	$t3,	$0,	label16
#ret 0
	li	$v0,	0
	j label5exit
#j label17
	j label17
#label16:
label16:
#t25 = &arg1
	la	$t1,	8($fp)
	sw	$t1,	-16($fp)
#t25 = t25+112
	lw	$t0,	-16($fp)
	addiu	$t2,	$t0,	112
	sw	$t2,	-16($fp)
#t26 = &arg2
	la	$t1,	124($fp)
	sw	$t1,	-12($fp)
#t26 = t26+112
	lw	$t0,	-12($fp)
	addiu	$t2,	$t0,	112
	sw	$t2,	-12($fp)
#t27 = comp1( [t25] [t26] )
	lw	$t0,	-16($fp)
	lw	$t1,	-16($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$t2,	$s0,	1
	sw	$t2,	-8($fp)
#ret t27
	lw	$v0,	-8($fp)
	j label5exit
#label17:
label17:
#label15:
label15:
#t28 = -1
	li	$t0,	1
	subu	$v1,	$0,	$t0
#ret t28
	addu	$v0,	$0,	$v1
	j label5exit
label5exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label6:

label18:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-24
#t29 = &g3
	la	$v1,	g3
#t30 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t29 = t29+t30
	addu	$v1,	$v1,	$a3
#t29 = t29+108
	addiu	$v1,	$v1,	108
#[t29] = [t29]+1
	lw	$t0,	0($v1)
	addiu	$t2,	$t0,	1
	sw	$t2,	0($v1)
#t31 = &g3
	la	$v1,	g3
#t32 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t31 = t31+t32
	addu	$v1,	$v1,	$a3
#ret [t31]
	addu	$v0,	$0,	$v1
label18exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label19:

label20:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-124
#t33 = [arg1]
	addu	$a1,	$0,	$v1
	lw	$a2,	8($fp)
	li	$a0,	116
	jal	memcpy
#[arg1] = [arg2]
	lw	$a2,	12($fp)
	lw	$a1,	8($fp)
	li	$a0,	116
	jal	memcpy
#[arg2] = t33
	addu	$a2,	$0,	$v1
	lw	$a1,	12($fp)
	li	$a0,	116
	jal	memcpy
label20exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label21:

label22:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-544
#t35 = getNode( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label18
	sw	$v0,	-412($fp)
#t34 = t35
	lw	$a2,	-412($fp)
	la	$a1,	-528($fp)
	li	$a0,	116
	jal	memcpy
#t37 = getNode( arg2 )
	lw	$t0,	12($fp)
	sw	$t0,	0($sp)
	jal	label18
	sw	$v0,	-180($fp)
#t36 = t37
	lw	$a2,	-180($fp)
	la	$a1,	-296($fp)
	li	$a0,	116
	jal	memcpy
#t38 = 0
	li	$t0,	0
	sw	$t0,	-64($fp)
#t39 = 0
	li	$t0,	0
	sw	$t0,	-60($fp)
#label24:
label24:
#t40 = t38<g1
	lw	$t0,	-64($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t40==0 j label25
	beq	$v1,	$0,	label25
#label27:
label27:
#t41 = t39<g1
	lw	$t0,	-60($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t41==0 j label28
	beq	$v1,	$0,	label28
#t42 = &t34
	la	$v1,	-528($fp)
#t42 = t42+0
	addiu	$v1,	$v1,	0
#t43 = t38*20
	lw	$t0,	-64($fp)
	li	$t1,	20
	mul	$a3,	$t0,	$t1
#t42 = t42+t43
	addu	$v1,	$v1,	$a3
#t44 = t39*4
	lw	$t0,	-60($fp)
	sll	$a3,	$t0,	2
#t42 = t42+t44
	addu	$v1,	$v1,	$a3
#t45 = &t36
	la	$a3,	-296($fp)
#t45 = t45+0
	addiu	$a3,	$a3,	0
#t46 = t38*20
	lw	$t0,	-64($fp)
	li	$t1,	20
	mul	$t3,	$t0,	$t1
#t45 = t45+t46
	addu	$a3,	$a3,	$t3
#t47 = t39*4
	lw	$t0,	-60($fp)
	sll	$t3,	$t0,	2
#t45 = t45+t47
	addu	$a3,	$a3,	$t3
#t48 = [t45]<[t42]
	lw	$t0,	0($a3)
	lw	$t1,	0($v1)
	slt	$t3,	$t0,	$t1
#if t48==0 j label30
	beq	$t3,	$0,	label30
#t49 = &t34
	la	$v1,	-528($fp)
#t50 = &t36
	la	$a3,	-296($fp)
#t51 = exchange( t49 t50 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label20
	sw	$v0,	-12($fp)
#t52 = &t34
	la	$v1,	-528($fp)
#t52 = t52+112
	addiu	$v1,	$v1,	112
#[t52] = [t52]+1
	lw	$t0,	0($v1)
	addiu	$t2,	$t0,	1
	sw	$t2,	0($v1)
#t53 = &t36
	la	$v1,	-296($fp)
#t53 = t53+112
	addiu	$v1,	$v1,	112
#[t53] = [t53]-1
	lw	$t0,	0($v1)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	0($v1)
#label30:
label30:
#label29:
label29:
#t39 = t39+1
	lw	$t0,	-60($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-60($fp)
#j label27
	j label27
#label28:
label28:
#label26:
label26:
#t38 = t38+1
	lw	$t0,	-64($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-64($fp)
#j label24
	j label24
#label25:
label25:
label22exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label23:

label31:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-104
#t54 = 0
	li	$t0,	0
	sw	$t0,	-76($fp)
#label33:
label33:
#t55 = t54<g1
	lw	$t0,	-76($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t55==0 j label34
	beq	$v1,	$0,	label34
#t56 = 0
	li	$t0,	0
	sw	$t0,	-68($fp)
#label36:
label36:
#t57 = t56<g1
	lw	$t0,	-68($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t57==0 j label37
	beq	$v1,	$0,	label37
#t58 = &g3
	la	$v1,	g3
#t59 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t58 = t58+t59
	addu	$v1,	$v1,	$a3
#t60 = t54*20
	lw	$t0,	-76($fp)
	li	$t1,	20
	mul	$a3,	$t0,	$t1
#t58 = t58+t60
	addu	$v1,	$v1,	$a3
#t61 = t56*4
	lw	$t0,	-68($fp)
	sll	$a3,	$t0,	2
#t58 = t58+t61
	addu	$v1,	$v1,	$a3
#t62 = printf( g4 [t58] )
	la	$t0,	g4
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#label38:
label38:
#t56 = t56+1
	lw	$t0,	-68($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-68($fp)
#j label36
	j label36
#label37:
label37:
#t63 = printf( g5 )
	la	$a0,	g5
	li	$v0,	4
	syscall
#label35:
label35:
#t54 = t54+1
	lw	$t0,	-76($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-76($fp)
#j label33
	j label33
#label34:
label34:
#t64 = &g3
	la	$v1,	g3
#t65 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t64 = t64+t65
	addu	$v1,	$v1,	$a3
#t64 = t64+100
	addiu	$v1,	$v1,	100
#t64 = t64+0
	addiu	$v1,	$v1,	0
#t67 = &g3
	la	$a3,	g3
#t68 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$t3,	$t0,	$t1
#t67 = t67+t68
	addu	$a3,	$a3,	$t3
#t67 = t67+100
	addiu	$a3,	$a3,	100
#t67 = t67+4
	addiu	$a3,	$a3,	4
#t70 = &g3
	la	$t3,	g3
#t71 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$t4,	$t0,	$t1
#t70 = t70+t71
	addu	$t3,	$t3,	$t4
#t70 = t70+108
	addiu	$t3,	$t3,	108
#t72 = &g3
	la	$t4,	g3
#t73 = arg1*116
	lw	$t0,	8($fp)
	li	$t1,	116
	mul	$t5,	$t0,	$t1
#t72 = t72+t73
	addu	$t4,	$t4,	$t5
#t72 = t72+112
	addiu	$t4,	$t4,	112
#t74 = printf( g6 [t64] [t67] [t70] [t72] )
	la	$t0,	g6
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	lw	$t1,	0($a3)
	sw	$t1,	8($sp)
	lw	$t1,	0($t3)
	sw	$t1,	12($sp)
	lw	$t1,	0($t4)
	sw	$t1,	16($sp)
	jal	printf
label31exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label32:

label39:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-984
#t75 = 7
	li	$t0,	7
	sw	$t0,	-744($fp)
#t76 = 0
	li	$t0,	0
	sw	$t0,	-740($fp)
#label41:
label41:
#t79 = t76<g2
	lw	$t0,	-740($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t79==0 j label42
	beq	$v1,	$0,	label42
#t77 = 0
	li	$t0,	0
	sw	$t0,	-736($fp)
#label44:
label44:
#t80 = t77<g1
	lw	$t0,	-736($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t80==0 j label45
	beq	$v1,	$0,	label45
#t78 = 0
	li	$t0,	0
	sw	$t0,	-732($fp)
#label47:
label47:
#t81 = t78<g1
	lw	$t0,	-732($fp)
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t81==0 j label48
	beq	$v1,	$0,	label48
#t82 = &g3
	la	$v1,	g3
#t83 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t82 = t82+t83
	addu	$v1,	$v1,	$a3
#t84 = t77*20
	lw	$t0,	-736($fp)
	li	$t1,	20
	mul	$a3,	$t0,	$t1
#t82 = t82+t84
	addu	$v1,	$v1,	$a3
#t85 = t78*4
	lw	$t0,	-732($fp)
	sll	$a3,	$t0,	2
#t82 = t82+t85
	addu	$v1,	$v1,	$a3
#t86 = t77*5110
	lw	$t0,	-736($fp)
	li	$t1,	5110
	mul	$a3,	$t0,	$t1
#t87 = t86+t78
	lw	$t1,	-732($fp)
	addu	$t3,	$a3,	$t1
#t88 = 34-t76
	li	$t0,	34
	lw	$t1,	-740($fp)
	subu	$a3,	$t0,	$t1
#t89 = t87%t88
	divu	$t3,	$a3
	mfhi	$t4
#t90 = t89+1
	addiu	$a3,	$t4,	1
#[t82] = t90
	sw	$a3,	0($v1)
#t91 = &g3
	la	$v1,	g3
#t92 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t91 = t91+t92
	addu	$v1,	$v1,	$a3
#t91 = t91+100
	addiu	$v1,	$v1,	100
#t91 = t91+0
	addiu	$v1,	$v1,	0
#t94 = t77*t77
	lw	$t0,	-736($fp)
	lw	$t1,	-736($fp)
	mul	$a3,	$t0,	$t1
#t95 = t94*t77
	lw	$t1,	-736($fp)
	mul	$t3,	$a3,	$t1
#t96 = t76+t95
	lw	$t0,	-740($fp)
	addu	$a3,	$t0,	$t3
#[t91] = t96
	sw	$a3,	0($v1)
#t97 = &g3
	la	$v1,	g3
#t98 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t97 = t97+t98
	addu	$v1,	$v1,	$a3
#t97 = t97+100
	addiu	$v1,	$v1,	100
#t97 = t97+4
	addiu	$v1,	$v1,	4
#t100 = t78+t77
	lw	$t0,	-732($fp)
	lw	$t1,	-736($fp)
	addu	$a3,	$t0,	$t1
#t101 = t100+t76
	lw	$t1,	-740($fp)
	addu	$t3,	$a3,	$t1
#t102 = t101<<1
	li	$t1,	1
	sllv	$a3,	$t3,	$t1
#[t97] = t102
	sw	$a3,	0($v1)
#t103 = &g3
	la	$v1,	g3
#t104 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t103 = t103+t104
	addu	$v1,	$v1,	$a3
#t103 = t103+112
	addiu	$v1,	$v1,	112
#t105 = ~t77
	lw	$t0,	-736($fp)
	nor	$a3,	$t0,	$0
#t106 = t76+t105
	lw	$t0,	-740($fp)
	addu	$t3,	$t0,	$a3
#t107 = t106|t78
	lw	$t1,	-732($fp)
	or	$a3,	$t3,	$t1
#[t103] = t107
	sw	$a3,	0($v1)
#t108 = &g3
	la	$v1,	g3
#t109 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t108 = t108+t109
	addu	$v1,	$v1,	$a3
#t108 = t108+100
	addiu	$v1,	$v1,	100
#t108 = t108+0
	addiu	$v1,	$v1,	0
#t111 = &g3
	la	$a3,	g3
#t112 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$t3,	$t0,	$t1
#t111 = t111+t112
	addu	$a3,	$a3,	$t3
#t111 = t111+100
	addiu	$a3,	$a3,	100
#t111 = t111+0
	addiu	$a3,	$a3,	0
#t114 = 122-97
	li	$t0,	122
	li	$t1,	97
	subu	$t3,	$t0,	$t1
#t115 = t114+1
	addiu	$t4,	$t3,	1
#t116 = [t111]%t115
	lw	$t0,	0($a3)
	divu	$t0,	$t4
	mfhi	$t3
#t117 = t116+97
	addiu	$a3,	$t3,	97
#[t108] = t117
	sw	$a3,	0($v1)
#t118 = &g3
	la	$v1,	g3
#t119 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$a3,	$t0,	$t1
#t118 = t118+t119
	addu	$v1,	$v1,	$a3
#t118 = t118+100
	addiu	$v1,	$v1,	100
#t118 = t118+4
	addiu	$v1,	$v1,	4
#t121 = &g3
	la	$a3,	g3
#t122 = t76*116
	lw	$t0,	-740($fp)
	li	$t1,	116
	mul	$t3,	$t0,	$t1
#t121 = t121+t122
	addu	$a3,	$a3,	$t3
#t121 = t121+100
	addiu	$a3,	$a3,	100
#t121 = t121+4
	addiu	$a3,	$a3,	4
#t124 = 90-65
	li	$t0,	90
	li	$t1,	65
	subu	$t3,	$t0,	$t1
#t125 = t124+1
	addiu	$t4,	$t3,	1
#t126 = [t121]%t125
	lw	$t0,	0($a3)
	divu	$t0,	$t4
	mfhi	$t3
#t127 = t126+65
	addiu	$a3,	$t3,	65
#[t118] = t127
	sw	$a3,	0($v1)
#label49:
label49:
#t78 = t78+1
	lw	$t0,	-732($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-732($fp)
#j label47
	j label47
#label48:
label48:
#label46:
label46:
#t77 = t77+1
	lw	$t0,	-736($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-736($fp)
#j label44
	j label44
#label45:
label45:
#label43:
label43:
#t76 = t76+1
	lw	$t0,	-740($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-740($fp)
#j label41
	j label41
#label42:
label42:
#t77 = 0
	li	$t0,	0
	sw	$t0,	-736($fp)
#label50:
label50:
#t128 = t77<g2
	lw	$t0,	-736($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t128==0 j label51
	beq	$v1,	$0,	label51
#t78 = 0
	li	$t0,	0
	sw	$t0,	-732($fp)
#label53:
label53:
#t129 = t78<g2
	lw	$t0,	-732($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t129==0 j label54
	beq	$v1,	$0,	label54
#t130 = comp( t77 t78 )
	lw	$t0,	-736($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-732($fp)
	sw	$t0,	4($sp)
	jal	label22
	sw	$v0,	-548($fp)
#label55:
label55:
#t78 = t78+1
	lw	$t0,	-732($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-732($fp)
#j label53
	j label53
#label54:
label54:
#label52:
label52:
#t77 = t77+1
	lw	$t0,	-736($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-736($fp)
#j label50
	j label50
#label51:
label51:
#t76 = 0
	li	$t0,	0
	sw	$t0,	-740($fp)
#label56:
label56:
#t131 = t76<g2
	lw	$t0,	-740($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t131==0 j label57
	beq	$v1,	$0,	label57
#t132 = show( t76 )
	lw	$t0,	-740($fp)
	sw	$t0,	0($sp)
	jal	label31
	sw	$v0,	-540($fp)
#label58:
label58:
#t76 = t76+1
	lw	$t0,	-740($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-740($fp)
#j label56
	j label56
#label57:
label57:
#t133 = g2-1
	lw	$t0,	g2
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t77 = t133
	sw	$v1,	-736($fp)
#label59:
label59:
#t134 = -1
	li	$t0,	1
	subu	$v1,	$0,	$t0
#t135 = t134<t77
	lw	$t1,	-736($fp)
	slt	$a3,	$v1,	$t1
#if t135==0 j label60
	beq	$a3,	$0,	label60
#t136 = t77%3
	lw	$t0,	-736($fp)
	li	$t1,	3
	divu	$t0,	$t1
	mfhi	$v1
#t137 = t136==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$a3,	$s0,	1
#if t137==0 j label62
	beq	$a3,	$0,	label62
#t138 = &g3
	la	$t1,	g3
	sw	$t1,	-516($fp)
#t139 = t77*116
	lw	$t0,	-736($fp)
	li	$t1,	116
	mul	$v1,	$t0,	$t1
#t138 = t138+t139
	lw	$t0,	-516($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-516($fp)
#t140 = t77+3
	lw	$t0,	-736($fp)
	addiu	$v1,	$t0,	3
#t141 = t140%g2
	lw	$t1,	g2
	divu	$v1,	$t1
	mfhi	$a3
#t142 = getNode( t141 )
	sw	$a3,	0($sp)
	jal	label18
	sw	$v0,	-500($fp)
#[t138] = t142
	lw	$a2,	-500($fp)
	lw	$a1,	-516($fp)
	li	$a0,	116
	jal	memcpy
#j label63
	j label63
#label62:
label62:
#t143 = &g3
	la	$t1,	g3
	sw	$t1,	-384($fp)
#t144 = t77*116
	lw	$t0,	-736($fp)
	li	$t1,	116
	mul	$v1,	$t0,	$t1
#t143 = t143+t144
	lw	$t0,	-384($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-384($fp)
#t145 = getNode( t77 )
	lw	$t0,	-736($fp)
	sw	$t0,	0($sp)
	jal	label18
	sw	$v0,	-376($fp)
#[t143] = t145
	lw	$a2,	-376($fp)
	lw	$a1,	-384($fp)
	li	$a0,	116
	jal	memcpy
#label63:
label63:
#label61:
label61:
#t146 = t77
	lw	$t0,	-736($fp)
	sw	$t0,	-260($fp)
#t77 = t77-1
	lw	$t0,	-736($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-736($fp)
#j label59
	j label59
#label60:
label60:
#t147 = printf( g5 )
	la	$a0,	g5
	li	$v0,	4
	syscall
#t77 = 0
	li	$t0,	0
	sw	$t0,	-736($fp)
#label64:
label64:
#t148 = t77<g2
	lw	$t0,	-736($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t148==0 j label65
	beq	$v1,	$0,	label65
#t78 = 0
	li	$t0,	0
	sw	$t0,	-732($fp)
#label67:
label67:
#t149 = t78<g2
	lw	$t0,	-732($fp)
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#if t149==0 j label68
	beq	$v1,	$0,	label68
#t150 = getNode( t77 )
	lw	$t0,	-736($fp)
	sw	$t0,	0($sp)
	jal	label18
	sw	$v0,	-244($fp)
#t151 = getNode( t78 )
	lw	$t0,	-732($fp)
	sw	$t0,	0($sp)
	jal	label18
	sw	$v0,	-128($fp)
#t152 = compare( t150 t151 )
	lw	$a2,	-244($fp)
	la	$a1,	0($sp)
	li	$a0,	116
	jal	memcpy
	lw	$a2,	-128($fp)
	la	$a1,	116($sp)
	li	$a0,	116
	jal	memcpy
	jal	label5
	sw	$v0,	-12($fp)
#t153 = printf( g7 t152 )
	la	$t0,	g7
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-12($fp)
	sw	$t0,	4($sp)
	jal	printf
#label69:
label69:
#t78 = t78+1
	lw	$t0,	-732($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-732($fp)
#j label67
	j label67
#label68:
label68:
#t154 = printf( g5 )
	la	$a0,	g5
	li	$v0,	4
	syscall
#label66:
label66:
#t77 = t77+1
	lw	$t0,	-736($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-736($fp)
#j label64
	j label64
#label65:
label65:
#ret 0
	li	$v0,	0
	j label39exit
label39exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label40:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
