    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g3:
	.asciiz "%d "
	.align 2
g4:
	.asciiz "\n"
	.align 2
	.data
	.align 2
g1:
	.space 40400
	.align 2
g2:
	.word 10000
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label12
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-136
#t1 = arg1
	lw	$t0,	8($fp)
	sw	$t0,	-120($fp)
#t2 = arg2
	lw	$t0,	12($fp)
	sw	$t0,	-116($fp)
#t4 = &g1
	la	$v1,	g1
#t5 = arg1+arg2
	lw	$t0,	8($fp)
	lw	$t1,	12($fp)
	addu	$a3,	$t0,	$t1
#t6 = t5/2
	li	$t1,	2
	divu	$a3,	$t1
	mflo	$t3
#t7 = t6*4
	sll	$a3,	$t3,	2
#t4 = t4+t7
	addu	$v1,	$v1,	$a3
#t3 = [t4]
	lw	$t0,	0($v1)
	addu	$a3,	$0,	$t0
#label3:
label3:
#t8 = t2<t1
	lw	$t0,	-116($fp)
	lw	$t1,	-120($fp)
	slt	$v1,	$t0,	$t1
#t8 = t8==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t8==0 j label4
	beq	$v1,	$0,	label4
#label5:
label5:
#t9 = &g1
	la	$v1,	g1
#t10 = t1*4
	lw	$t0,	-120($fp)
	sll	$t3,	$t0,	2
#t9 = t9+t10
	addu	$v1,	$v1,	$t3
#t11 = [t9]<t3
	lw	$t0,	0($v1)
	slt	$t3,	$t0,	$a3
#if t11==0 j label6
	beq	$t3,	$0,	label6
#t1 = t1+1
	lw	$t0,	-120($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-120($fp)
#j label5
	j label5
#label6:
label6:
#label7:
label7:
#t13 = &g1
	la	$v1,	g1
#t14 = t2*4
	lw	$t0,	-116($fp)
	sll	$t3,	$t0,	2
#t13 = t13+t14
	addu	$v1,	$v1,	$t3
#t15 = t3<[t13]
	lw	$t1,	0($v1)
	slt	$t3,	$a3,	$t1
#if t15==0 j label8
	beq	$t3,	$0,	label8
#t16 = t2
	lw	$t0,	-116($fp)
	sw	$t0,	-64($fp)
#t2 = t2-1
	lw	$t0,	-116($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-116($fp)
#j label7
	j label7
#label8:
label8:
#t17 = t2<t1
	lw	$t0,	-116($fp)
	lw	$t1,	-120($fp)
	slt	$v1,	$t0,	$t1
#t17 = t17==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t17==0 j label9
	beq	$v1,	$0,	label9
#t19 = &g1
	la	$v1,	g1
#t20 = t1*4
	lw	$t0,	-120($fp)
	sll	$t3,	$t0,	2
#t19 = t19+t20
	addu	$v1,	$v1,	$t3
#t18 = [t19]
	lw	$t0,	0($v1)
	addu	$t3,	$0,	$t0
#t21 = &g1
	la	$v1,	g1
#t22 = t1*4
	lw	$t0,	-120($fp)
	sll	$t4,	$t0,	2
#t21 = t21+t22
	addu	$v1,	$v1,	$t4
#t23 = &g1
	la	$t4,	g1
#t24 = t2*4
	lw	$t0,	-116($fp)
	sll	$t5,	$t0,	2
#t23 = t23+t24
	addu	$t4,	$t4,	$t5
#[t21] = [t23]
	lw	$t0,	0($t4)
	sw	$t0,	0($v1)
#t25 = &g1
	la	$v1,	g1
#t26 = t2*4
	lw	$t0,	-116($fp)
	sll	$t4,	$t0,	2
#t25 = t25+t26
	addu	$v1,	$v1,	$t4
#[t25] = t18
	sw	$t3,	0($v1)
#t1 = t1+1
	lw	$t0,	-120($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-120($fp)
#t28 = t2
	lw	$t0,	-116($fp)
	sw	$t0,	-20($fp)
#t2 = t2-1
	lw	$t0,	-116($fp)
	li	$t1,	1
	subu	$t2,	$t0,	$t1
	sw	$t2,	-116($fp)
#label9:
label9:
#j label3
	j label3
#label4:
label4:
#t29 = arg1<t2
	lw	$t0,	8($fp)
	lw	$t1,	-116($fp)
	slt	$v1,	$t0,	$t1
#if t29==0 j label10
	beq	$v1,	$0,	label10
#t30 = qsrt( arg1 t2 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-116($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-12($fp)
#label10:
label10:
#t31 = t1<arg2
	lw	$t0,	-120($fp)
	lw	$t1,	12($fp)
	slt	$v1,	$t0,	$t1
#if t31==0 j label11
	beq	$v1,	$0,	label11
#t32 = qsrt( t1 arg2 )
	lw	$t0,	-120($fp)
	sw	$t0,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-4($fp)
#label11:
label11:
#ret 0
	li	$v0,	0
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label12:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-64
#t33 = 1
	li	$t0,	1
	sw	$t0,	-48($fp)
#label14:
label14:
#t34 = g2<t33
	lw	$t0,	g2
	lw	$t1,	-48($fp)
	slt	$v1,	$t0,	$t1
#t34 = t34==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t34==0 j label15
	beq	$v1,	$0,	label15
#t35 = &g1
	la	$v1,	g1
#t36 = t33*4
	lw	$t0,	-48($fp)
	sll	$a3,	$t0,	2
#t35 = t35+t36
	addu	$v1,	$v1,	$a3
#t37 = g2+1
	lw	$t0,	g2
	addiu	$a3,	$t0,	1
#t38 = t37-t33
	lw	$t1,	-48($fp)
	subu	$t3,	$a3,	$t1
#[t35] = t38
	sw	$t3,	0($v1)
#label16:
label16:
#t33 = t33+1
	lw	$t0,	-48($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-48($fp)
#j label14
	j label14
#label15:
label15:
#t40 = qsrt( 1 g2 )
	li	$t0,	1
	sw	$t0,	0($sp)
	lw	$t0,	g2
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-24($fp)
#t33 = 1
	li	$t0,	1
	sw	$t0,	-48($fp)
#label17:
label17:
#t41 = g2<t33
	lw	$t0,	g2
	lw	$t1,	-48($fp)
	slt	$v1,	$t0,	$t1
#t41 = t41==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t41==0 j label18
	beq	$v1,	$0,	label18
#t42 = &g1
	la	$v1,	g1
#t43 = t33*4
	lw	$t0,	-48($fp)
	sll	$a3,	$t0,	2
#t42 = t42+t43
	addu	$v1,	$v1,	$a3
#t44 = printf( g3 [t42] )
	la	$t0,	g3
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#label19:
label19:
#t33 = t33+1
	lw	$t0,	-48($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-48($fp)
#j label17
	j label17
#label18:
label18:
#t46 = printf( g4 )
	la	$a0,	g4
	li	$v0,	4
	syscall
#ret 0
	li	$v0,	0
	j label12exit
label12exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label13:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
