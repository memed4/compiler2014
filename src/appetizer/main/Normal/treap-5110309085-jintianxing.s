    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g8:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
g1:
	.word 2000
	.space 4
	.align 2
g2:
	.word 13579
	.space 4
	.align 2
g3:
	.word 12345
	.space 4
	.align 2
g4:
	.word 32768
	.space 4
	.align 2
g5:
	.word 86421
	.space 4
	.align 2
g6:
	.space 4
	.align 2
g7:
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label28
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-24
#t2 = g2*g5
	lw	$t0,	g2
	lw	$t1,	g5
	mul	$v1,	$t0,	$t1
#t3 = t2+g3
	lw	$t1,	g3
	addu	$a3,	$v1,	$t1
#t1 = t3
	addu	$v1,	$0,	$a3
#t4 = t1%g4
	lw	$t1,	g4
	divu	$v1,	$t1
	mfhi	$t3
#g5 = t4
	sw	$t3,	g5
#ret t1
	addu	$v0,	$0,	$v1
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-16
#t5 = arg1==0
	lw	$t0,	8($fp)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t5==0 j label5
	beq	$v1,	$0,	label5
#ret 0
	li	$v0,	0
	j label3exit
#label5:
label5:
#t6 = arg1+8
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	8
#ret [t6]
	lw	$v0,	0($v1)
	j label3exit
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

label6:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t7 = arg1+8
	lw	$t0,	8($fp)
	addiu	$t2,	$t0,	8
	sw	$t2,	-28($fp)
#t8 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t9 = sum_of( [t8] )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	jal	label3
	sw	$v0,	-20($fp)
#t10 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t11 = sum_of( [t10] )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	jal	label3
	sw	$v0,	-12($fp)
#t12 = t9+t11
	lw	$t0,	-20($fp)
	lw	$t1,	-12($fp)
	addu	$v1,	$t0,	$t1
#t13 = t12+1
	addiu	$a3,	$v1,	1
#[t7] = t13
	lw	$t1,	-28($fp)
	sw	$a3,	0($t1)
label6exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label7:

label8:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t15 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t14 = [t15]
	lw	$t0,	0($v1)
	sw	$t0,	-28($fp)
#t16 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t17 = t14+12
	lw	$t0,	-28($fp)
	addiu	$a3,	$t0,	12
#[t16] = [t17]
	lw	$t0,	0($a3)
	sw	$t0,	0($v1)
#t18 = t14+12
	lw	$t0,	-28($fp)
	addiu	$v1,	$t0,	12
#[t18] = arg1
	lw	$t0,	8($fp)
	sw	$t0,	0($v1)
#t19 = update( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label6
	sw	$v0,	-8($fp)
#t20 = update( t14 )
	lw	$t0,	-28($fp)
	sw	$t0,	0($sp)
	jal	label6
	sw	$v0,	-4($fp)
#ret t14
	lw	$v0,	-28($fp)
	j label8exit
label8exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label9:

label10:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t22 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t21 = [t22]
	lw	$t0,	0($v1)
	sw	$t0,	-28($fp)
#t23 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t24 = t21+16
	lw	$t0,	-28($fp)
	addiu	$a3,	$t0,	16
#[t23] = [t24]
	lw	$t0,	0($a3)
	sw	$t0,	0($v1)
#t25 = t21+16
	lw	$t0,	-28($fp)
	addiu	$v1,	$t0,	16
#[t25] = arg1
	lw	$t0,	8($fp)
	sw	$t0,	0($v1)
#t26 = update( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label6
	sw	$v0,	-8($fp)
#t27 = update( t21 )
	lw	$t0,	-28($fp)
	sw	$t0,	0($sp)
	jal	label6
	sw	$v0,	-4($fp)
#ret t21
	lw	$v0,	-28($fp)
	j label10exit
label10exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label11:

label12:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-116
#t28 = arg1==0
	lw	$t0,	8($fp)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t28==0 j label14
	beq	$v1,	$0,	label14
#ret arg2
	lw	$v0,	12($fp)
	j label12exit
#label14:
label14:
#t29 = arg1
	lw	$t0,	8($fp)
	addu	$v1,	$0,	$t0
#t30 = arg2
	lw	$t0,	12($fp)
	addu	$a3,	$0,	$t0
#t31 = [t30]<[t29]
	lw	$t0,	0($a3)
	lw	$t1,	0($v1)
	slt	$t3,	$t0,	$t1
#if t31==0 j label15
	beq	$t3,	$0,	label15
#t32 = arg1+12
	lw	$t0,	8($fp)
	addiu	$t2,	$t0,	12
	sw	$t2,	-84($fp)
#t33 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t34 = insert_node( [t33] arg2 )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	jal	label12
	sw	$v0,	-76($fp)
#[t32] = t34
	lw	$t0,	-76($fp)
	lw	$t1,	-84($fp)
	sw	$t0,	0($t1)
#j label16
	j label16
#label15:
label15:
#t35 = arg1+16
	lw	$t0,	8($fp)
	addiu	$t2,	$t0,	16
	sw	$t2,	-72($fp)
#t36 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t37 = insert_node( [t36] arg2 )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	jal	label12
	sw	$v0,	-64($fp)
#[t35] = t37
	lw	$t0,	-64($fp)
	lw	$t1,	-72($fp)
	sw	$t0,	0($t1)
#label16:
label16:
#t38 = update( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label6
	sw	$v0,	-60($fp)
#t39 = 0
	addiu	$v1,	$0,	0
#t40 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t41 = [t40]!=0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t41==0 j label17
	beq	$a3,	$0,	label17
#t42 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t42 = [t42]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t42 = t42+4
	addiu	$v1,	$v1,	4
#t43 = arg1+4
	lw	$t0,	8($fp)
	addiu	$a3,	$t0,	4
#t44 = [t43]<[t42]
	lw	$t0,	0($a3)
	lw	$t1,	0($v1)
	slt	$t3,	$t0,	$t1
#if t44==0 j label17
	beq	$t3,	$0,	label17
#t39 = 1
	addiu	$v1,	$0,	1
#label18:
label18:
#if t39==0 j label17
	beq	$v1,	$0,	label17
#t45 = rotate_right( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label10
	sw	$v0,	-32($fp)
#ret t45
	lw	$v0,	-32($fp)
	j label12exit
#label17:
label17:
#t46 = 0
	addiu	$v1,	$0,	0
#t47 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t48 = [t47]!=0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t48==0 j label19
	beq	$a3,	$0,	label19
#t49 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t49 = [t49]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t49 = t49+4
	addiu	$v1,	$v1,	4
#t50 = arg1+4
	lw	$t0,	8($fp)
	addiu	$a3,	$t0,	4
#t51 = [t50]<[t49]
	lw	$t0,	0($a3)
	lw	$t1,	0($v1)
	slt	$t3,	$t0,	$t1
#if t51==0 j label19
	beq	$t3,	$0,	label19
#t46 = 1
	addiu	$v1,	$0,	1
#label20:
label20:
#if t46==0 j label19
	beq	$v1,	$0,	label19
#t52 = rotate_left( arg1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	jal	label8
	sw	$v0,	-4($fp)
#ret t52
	lw	$v0,	-4($fp)
	j label12exit
#label19:
label19:
#ret arg1
	lw	$v0,	8($fp)
	j label12exit
label12exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label13:

label21:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-68
#t53 = 0
	li	$t0,	0
	sw	$t0,	-52($fp)
#t54 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t55 = [t54]!=0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor $s0,	$t0,	$t1
	sltu	$a3,	$0,	$s0
#if t55==0 j label23
	beq	$a3,	$0,	label23
#t56 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t56 = [t56]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t56 = t56+8
	addiu	$v1,	$v1,	8
#t53 = [t56]
	lw	$t0,	0($v1)
	sw	$t0,	-52($fp)
#label23:
label23:
#t57 = arg2<t53
	lw	$t0,	12($fp)
	lw	$t1,	-52($fp)
	slt	$v1,	$t0,	$t1
#if t57==0 j label24
	beq	$v1,	$0,	label24
#t58 = arg1+12
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	12
#t59 = get_kth_element( [t58] arg2 )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	lw	$t0,	12($fp)
	sw	$t0,	4($sp)
	jal	label21
	sw	$v0,	-28($fp)
#ret t59
	lw	$v0,	-28($fp)
	j label21exit
#label24:
label24:
#t60 = t53<arg2
	lw	$t0,	-52($fp)
	lw	$t1,	12($fp)
	slt	$v1,	$t0,	$t1
#if t60==0 j label25
	beq	$v1,	$0,	label25
#t61 = arg1+16
	lw	$t0,	8($fp)
	addiu	$v1,	$t0,	16
#t62 = arg2-t53
	lw	$t0,	12($fp)
	lw	$t1,	-52($fp)
	subu	$a3,	$t0,	$t1
#t63 = t62-1
	li	$t1,	1
	subu	$t3,	$a3,	$t1
#t64 = get_kth_element( [t61] t63 )
	lw	$t1,	0($v1)
	sw	$t1,	0($sp)
	sw	$t3,	4($sp)
	jal	label21
	sw	$v0,	-8($fp)
#ret t64
	lw	$v0,	-8($fp)
	j label21exit
#label25:
label25:
#t65 = arg1
	lw	$t0,	8($fp)
	addu	$v1,	$0,	$t0
#ret [t65]
	lw	$v0,	0($v1)
	j label21exit
label21exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label22:

label26:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-48
#t67 = malloc( 20 )
	li	$t0,	20
	addu	$a0,	$0,	$t0
	jal	malloc
	sw	$v0,	-32($fp)
#t66 = t67
	lw	$t0,	-32($fp)
	sw	$t0,	-36($fp)
#t68 = t66+12
	lw	$t0,	-36($fp)
	addiu	$v1,	$t0,	12
#t69 = t66+16
	lw	$t0,	-36($fp)
	addiu	$a3,	$t0,	16
#[t69] = 0
	li	$t0,	0
	sw	$t0,	0($a3)
#[t68] = [t69]
	lw	$t0,	0($a3)
	sw	$t0,	0($v1)
#t70 = t66+8
	lw	$t0,	-36($fp)
	addiu	$v1,	$t0,	8
#[t70] = 1
	li	$t0,	1
	sw	$t0,	0($v1)
#t71 = t66+4
	lw	$t0,	-36($fp)
	addiu	$t2,	$t0,	4
	sw	$t2,	-16($fp)
#t72 = get_random( )
	jal	label1
	sw	$v0,	-12($fp)
#[t71] = t72
	lw	$t0,	-12($fp)
	lw	$t1,	-16($fp)
	sw	$t0,	0($t1)
#t73 = t66
	lw	$t0,	-36($fp)
	sw	$t0,	-8($fp)
#t74 = get_random( )
	jal	label1
	sw	$v0,	-4($fp)
#[t73] = t74
	lw	$t0,	-4($fp)
	lw	$t1,	-8($fp)
	sw	$t0,	0($t1)
#ret t66
	lw	$v0,	-36($fp)
	j label26exit
label26exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label27:

label28:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-44
#t75 = alloc_node( )
	jal	label26
	sw	$v0,	-28($fp)
#g7 = t75
	lw	$t0,	-28($fp)
	sw	$t0,	g7
#g6 = 0
	li	$t0,	0
	sw	$t0,	g6
#label30:
label30:
#t76 = g6<g1
	lw	$t0,	g6
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t76==0 j label31
	beq	$v1,	$0,	label31
#t77 = alloc_node( )
	jal	label26
	sw	$v0,	-20($fp)
#t78 = insert_node( g7 t77 )
	lw	$t0,	g7
	sw	$t0,	0($sp)
	lw	$t0,	-20($fp)
	sw	$t0,	4($sp)
	jal	label12
	sw	$v0,	-16($fp)
#g7 = t78
	lw	$t0,	-16($fp)
	sw	$t0,	g7
#label32:
label32:
#g6 = g6+1
	lw	$t0,	g6
	addiu	$t2,	$t0,	1
	sw	$t2,	g6
#j label30
	j label30
#label31:
label31:
#g6 = 0
	li	$t0,	0
	sw	$t0,	g6
#label33:
label33:
#t80 = g1<g6
	lw	$t0,	g1
	lw	$t1,	g6
	slt	$v1,	$t0,	$t1
#t80 = t80==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t80==0 j label34
	beq	$v1,	$0,	label34
#t81 = get_kth_element( g7 g6 )
	lw	$t0,	g7
	sw	$t0,	0($sp)
	lw	$t0,	g6
	sw	$t0,	4($sp)
	jal	label21
	sw	$v0,	-8($fp)
#t82 = printf( g8 t81 )
	la	$t0,	g8
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-8($fp)
	sw	$t0,	4($sp)
	jal	printf
#label35:
label35:
#g6 = g6+1
	lw	$t0,	g6
	addiu	$t2,	$t0,	1
	sw	$t2,	g6
#j label33
	j label33
#label34:
label34:
#ret 0
	li	$v0,	0
	j label28exit
label28exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label29:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
