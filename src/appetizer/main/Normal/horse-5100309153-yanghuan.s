    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g17:
	.asciiz "%d\n"
	.align 2
g18:
	.asciiz "no solution!\n"
	.align 2
	.data
	.align 2
g1:
	.word 100
	.space 4
	.align 2
g2:
	.space 4
	.align 2
g3:
	.space 4
	.align 2
g4:
	.space 4
	.align 2
g5:
	.space 4
	.align 2
g6:
	.space 4
	.align 2
g7:
	.space 4
	.align 2
g8:
	.space 4
	.align 2
g9:
	.space 40000
	.align 2
g10:
	.space 40000
	.align 2
g11:
	.space 4
	.align 2
g12:
	.space 4
	.align 2
g13:
	.space 4
	.align 2
g14:
	.space 40000
	.align 2
g15:
	.space 4
	.align 2
g16:
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label11
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-20
#t1 = 0
	addiu	$v1,	$0,	0
#t2 = arg1<arg2
	lw	$t0,	8($fp)
	lw	$t1,	12($fp)
	slt	$a3,	$t0,	$t1
#if t2==0 j label3
	beq	$a3,	$0,	label3
#t3 = arg1<0
	lw	$t0,	8($fp)
	li	$t1,	0
	slt	$a3,	$t0,	$t1
#t3 = t3==0
	li	$t1,	0
	xor	$s0,	$a3,	$t1
	sltiu	$a3,	$s0,	1
#if t3==0 j label3
	beq	$a3,	$0,	label3
#t1 = 1
	addiu	$v1,	$0,	1
#label3:
label3:
#ret t1
	addu	$v0,	$0,	$v1
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label4:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-108
#t4 = 0
	addiu	$t6,	$0,	0
#t5 = 0
	addiu	$a3,	$0,	0
#t6 = check( arg1 g1 )
	lw	$t0,	8($fp)
	sw	$t0,	0($sp)
	lw	$t0,	g1
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-84($fp)
#t7 = t6==1
	lw	$t0,	-84($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t7==0 j label6
	beq	$v1,	$0,	label6
#t8 = check( arg2 g1 )
	lw	$t0,	12($fp)
	sw	$t0,	0($sp)
	lw	$t0,	g1
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-76($fp)
#t9 = t8==1
	lw	$t0,	-76($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t9==0 j label6
	beq	$v1,	$0,	label6
#t5 = 1
	addiu	$a3,	$0,	1
#label7:
label7:
#if t5==0 j label6
	beq	$a3,	$0,	label6
#t10 = &g14
	la	$t3,	g14
#t11 = arg1*400
	lw	$t0,	8($fp)
	li	$t1,	400
	mul	$t4,	$t0,	$t1
#t10 = t10+t11
	addu	$t3,	$t3,	$t4
#t12 = arg2*4
	lw	$t0,	12($fp)
	sll	$t5,	$t0,	2
#t10 = t10+t12
	addu	$t3,	$t3,	$t5
#t13 = -1
	li	$t0,	1
	subu	$t5,	$0,	$t0
#t14 = [t10]==t13
	lw	$t0,	0($t3)
	xor	$s0,	$t0,	$t5
	sltiu	$t2,	$s0,	1
	sw	$t2,	-52($fp)
#if t14==0 j label6
	lw	$t0,	-52($fp)
	beq	$t0,	$0,	label6
#t4 = 1
	addiu	$t6,	$0,	1
#label8:
label8:
#if t4==0 j label6
	beq	$t6,	$0,	label6
#t15 = g11+1
	lw	$t0,	g11
	addiu	$t6,	$t0,	1
#g11 = t15
	sw	$t6,	g11
#t16 = &g9
	la	$t6,	g9
#t17 = g11*4
	lw	$t0,	g11
	sll	$v1,	$t0,	2
#t16 = t16+t17
	addu	$t6,	$t6,	$v1
#[t16] = arg1
	lw	$t0,	8($fp)
	sw	$t0,	0($t6)
#t18 = &g10
	la	$v1,	g10
#t19 = g11*4
	lw	$t0,	g11
	sll	$t3,	$t0,	2
#t18 = t18+t19
	addu	$v1,	$v1,	$t3
#[t18] = arg2
	lw	$t0,	12($fp)
	sw	$t0,	0($v1)
#t20 = &g14
	la	$v1,	g14
#t21 = arg1*400
	lw	$t0,	8($fp)
	li	$t1,	400
	mul	$t3,	$t0,	$t1
#t20 = t20+t21
	addu	$v1,	$v1,	$t3
#t22 = arg2*4
	lw	$t0,	12($fp)
	sll	$t3,	$t0,	2
#t20 = t20+t22
	addu	$v1,	$v1,	$t3
#t23 = g13+1
	lw	$t0,	g13
	addiu	$t3,	$t0,	1
#[t20] = t23
	sw	$t3,	0($v1)
#t24 = 0
	addiu	$v1,	$0,	0
#t25 = arg1==g5
	lw	$t0,	8($fp)
	lw	$t1,	g5
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t25==0 j label9
	beq	$v1,	$0,	label9
#t26 = arg2==g6
	lw	$t0,	12($fp)
	lw	$t1,	g6
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t26==0 j label9
	beq	$v1,	$0,	label9
#t24 = 1
	addiu	$v1,	$0,	1
#label10:
label10:
#if t24==0 j label9
	beq	$v1,	$0,	label9
#g12 = 1
	li	$t0,	1
	sw	$t0,	g12
#label9:
label9:
#label6:
label6:
label4exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label5:

label11:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-204
#t27 = g1-1
	lw	$t0,	g1
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#g6 = t27
	sw	$v1,	g6
#g5 = g6
	lw	$t0,	g6
	sw	$t0,	g5
#g15 = 0
	li	$t0,	0
	sw	$t0,	g15
#label13:
label13:
#t28 = g15<g1
	lw	$t0,	g15
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t28==0 j label14
	beq	$v1,	$0,	label14
#g16 = 0
	li	$t0,	0
	sw	$t0,	g16
#label16:
label16:
#t29 = g16<g1
	lw	$t0,	g16
	lw	$t1,	g1
	slt	$v1,	$t0,	$t1
#if t29==0 j label17
	beq	$v1,	$0,	label17
#t30 = &g14
	la	$v1,	g14
#t31 = g15*400
	lw	$t0,	g15
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t30 = t30+t31
	addu	$v1,	$v1,	$a3
#t32 = g16*4
	lw	$t0,	g16
	sll	$a3,	$t0,	2
#t30 = t30+t32
	addu	$v1,	$v1,	$a3
#t33 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#[t30] = t33
	sw	$a3,	0($v1)
#label18:
label18:
#g16 = g16+1
	lw	$t0,	g16
	addiu	$t2,	$t0,	1
	sw	$t2,	g16
#j label16
	j label16
#label17:
label17:
#label15:
label15:
#g15 = g15+1
	lw	$t0,	g15
	addiu	$t2,	$t0,	1
	sw	$t2,	g15
#j label13
	j label13
#label14:
label14:
#label19:
label19:
#t36 = g11<g2
	lw	$t0,	g11
	lw	$t1,	g2
	slt	$v1,	$t0,	$t1
#t36 = t36==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t36==0 j label20
	beq	$v1,	$0,	label20
#t37 = &g9
	la	$v1,	g9
#t38 = g2*4
	lw	$t0,	g2
	sll	$a3,	$t0,	2
#t37 = t37+t38
	addu	$v1,	$v1,	$a3
#g7 = [t37]
	lw	$t0,	0($v1)
	sw	$t0,	g7
#t39 = &g10
	la	$v1,	g10
#t40 = g2*4
	lw	$t0,	g2
	sll	$a3,	$t0,	2
#t39 = t39+t40
	addu	$v1,	$v1,	$a3
#g8 = [t39]
	lw	$t0,	0($v1)
	sw	$t0,	g8
#t41 = &g14
	la	$v1,	g14
#t42 = g7*400
	lw	$t0,	g7
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t41 = t41+t42
	addu	$v1,	$v1,	$a3
#t43 = g8*4
	lw	$t0,	g8
	sll	$a3,	$t0,	2
#t41 = t41+t43
	addu	$v1,	$v1,	$a3
#g13 = [t41]
	lw	$t0,	0($v1)
	sw	$t0,	g13
#t44 = g7-1
	lw	$t0,	g7
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t45 = g8-2
	lw	$t0,	g8
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t46 = addList( t44 t45 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-120($fp)
#t47 = g7-1
	lw	$t0,	g7
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t48 = g8+2
	lw	$t0,	g8
	addiu	$a3,	$t0,	2
#t49 = addList( t47 t48 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-108($fp)
#t50 = g7+1
	lw	$t0,	g7
	addiu	$v1,	$t0,	1
#t51 = g8-2
	lw	$t0,	g8
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t52 = addList( t50 t51 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-96($fp)
#t53 = g7+1
	lw	$t0,	g7
	addiu	$v1,	$t0,	1
#t54 = g8+2
	lw	$t0,	g8
	addiu	$a3,	$t0,	2
#t55 = addList( t53 t54 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-84($fp)
#t56 = g7-2
	lw	$t0,	g7
	li	$t1,	2
	subu	$v1,	$t0,	$t1
#t57 = g8-1
	lw	$t0,	g8
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t58 = addList( t56 t57 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-72($fp)
#t59 = g7-2
	lw	$t0,	g7
	li	$t1,	2
	subu	$v1,	$t0,	$t1
#t60 = g8+1
	lw	$t0,	g8
	addiu	$a3,	$t0,	1
#t61 = addList( t59 t60 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-60($fp)
#t62 = g7+2
	lw	$t0,	g7
	addiu	$v1,	$t0,	2
#t63 = g8-1
	lw	$t0,	g8
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t64 = addList( t62 t63 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-48($fp)
#t65 = g7+2
	lw	$t0,	g7
	addiu	$v1,	$t0,	2
#t66 = g8+1
	lw	$t0,	g8
	addiu	$a3,	$t0,	1
#t67 = addList( t65 t66 )
	sw	$v1,	0($sp)
	sw	$a3,	4($sp)
	jal	label4
	sw	$v0,	-36($fp)
#t68 = g12==1
	lw	$t0,	g12
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t68==0 j label21
	beq	$v1,	$0,	label21
#j label20
	j label20
#label21:
label21:
#t69 = g2+1
	lw	$t0,	g2
	addiu	$v1,	$t0,	1
#g2 = t69
	sw	$v1,	g2
#j label19
	j label19
#label20:
label20:
#t70 = g12==1
	lw	$t0,	g12
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t70==0 j label22
	beq	$v1,	$0,	label22
#t71 = &g14
	la	$v1,	g14
#t72 = g5*400
	lw	$t0,	g5
	li	$t1,	400
	mul	$a3,	$t0,	$t1
#t71 = t71+t72
	addu	$v1,	$v1,	$a3
#t73 = g6*4
	lw	$t0,	g6
	sll	$a3,	$t0,	2
#t71 = t71+t73
	addu	$v1,	$v1,	$a3
#t74 = printf( g17 [t71] )
	la	$t0,	g17
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#j label23
	j label23
#label22:
label22:
#t75 = printf( g18 )
	la	$a0,	g18
	li	$v0,	4
	syscall
#label23:
label23:
#ret 0
	li	$v0,	0
	j label11exit
label11exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label12:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
