    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g4:
	.asciiz "%d %d\n"
	.align 2
g5:
	.asciiz "Total: %d\n"
	.align 2
	.data
	.align 2
g1:
	.word 15000
	.space 4
	.align 2
g2:
	.space 60004
	.align 2
g3:
	.word 0
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label1
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-108
#t1 = 1
	li	$t0,	1
	sw	$t0,	-88($fp)
#label3:
label3:
#t2 = g1<t1
	lw	$t0,	g1
	lw	$t1,	-88($fp)
	slt	$t2,	$t0,	$t1
	sw	$t2,	-84($fp)
#t2 = t2==0
	lw	$t0,	-84($fp)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$t2,	$s0,	1
	sw	$t2,	-84($fp)
#if t2==0 j label4
	lw	$t0,	-84($fp)
	beq	$t0,	$0,	label4
#t3 = &g2
	la	$t1,	g2
	sw	$t1,	-80($fp)
#t4 = t1*4
	lw	$t0,	-88($fp)
	sll	$t2,	$t0,	2
	sw	$t2,	-76($fp)
#t3 = t3+t4
	lw	$t0,	-80($fp)
	lw	$t1,	-76($fp)
	addu	$t2,	$t0,	$t1
	sw	$t2,	-80($fp)
#[t3] = 1
	li	$t0,	1
	lw	$t1,	-80($fp)
	sw	$t0,	0($t1)
#label5:
label5:
#t1 = t1+1
	lw	$t0,	-88($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-88($fp)
#j label3
	j label3
#label4:
label4:
#t1 = 2
	li	$t0,	2
	sw	$t0,	-88($fp)
#label6:
label6:
#t6 = g1<t1
	lw	$t0,	g1
	lw	$t1,	-88($fp)
	slt	$v1,	$t0,	$t1
#t6 = t6==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t6==0 j label7
	beq	$v1,	$0,	label7
#t7 = &g2
	la	$v1,	g2
#t8 = t1*4
	lw	$t0,	-88($fp)
	sll	$a3,	$t0,	2
#t7 = t7+t8
	addu	$v1,	$v1,	$a3
#if [t7]==0 j label9
	lw	$t0,	0($v1)
	beq	$t0,	$0,	label9
#t9 = 2
	li	$t0,	2
	sw	$t0,	-60($fp)
#t10 = 0
	addiu	$v1,	$0,	0
#t11 = 3<t1
	li	$t0,	3
	lw	$t1,	-88($fp)
	slt	$v1,	$t0,	$t1
#if t11==0 j label10
	beq	$v1,	$0,	label10
#t12 = &g2
	la	$v1,	g2
#t13 = t1-2
	lw	$t0,	-88($fp)
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t14 = t13*4
	sll	$t3,	$a3,	2
#t12 = t12+t14
	addu	$v1,	$v1,	$t3
#if [t12]==0 j label10
	lw	$t0,	0($v1)
	beq	$t0,	$0,	label10
#t10 = 1
	addiu	$v1,	$0,	1
#label11:
label11:
#if t10==0 j label10
	beq	$v1,	$0,	label10
#t15 = g3
	lw	$t0,	g3
	sw	$t0,	-36($fp)
#g3 = g3+1
	lw	$t0,	g3
	addiu	$t2,	$t0,	1
	sw	$t2,	g3
#t16 = t1-2
	lw	$t0,	-88($fp)
	li	$t1,	2
	subu	$v1,	$t0,	$t1
#t17 = printf( g4 t16 t1 )
	la	$t0,	g4
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	sw	$v1,	4($sp)
	lw	$t0,	-88($fp)
	sw	$t0,	8($sp)
	jal	printf
#label10:
label10:
#label12:
label12:
#t18 = t1*t9
	lw	$t0,	-88($fp)
	lw	$t1,	-60($fp)
	mul	$v1,	$t0,	$t1
#t19 = g1<t18
	lw	$t0,	g1
	slt	$a3,	$t0,	$v1
#t19 = t19==0
	li	$t1,	0
	xor	$s0,	$a3,	$t1
	sltiu	$a3,	$s0,	1
#if t19==0 j label13
	beq	$a3,	$0,	label13
#t20 = &g2
	la	$v1,	g2
#t21 = t1*t9
	lw	$t0,	-88($fp)
	lw	$t1,	-60($fp)
	mul	$a3,	$t0,	$t1
#t22 = t21*4
	sll	$t3,	$a3,	2
#t20 = t20+t22
	addu	$v1,	$v1,	$t3
#[t20] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#t9 = t9+1
	lw	$t0,	-60($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-60($fp)
#j label12
	j label12
#label13:
label13:
#label9:
label9:
#label8:
label8:
#t1 = t1+1
	lw	$t0,	-88($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-88($fp)
#j label6
	j label6
#label7:
label7:
#t25 = printf( g5 g3 )
	la	$t0,	g5
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	g3
	sw	$t0,	4($sp)
	jal	printf
#ret 0
	li	$v0,	0
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
