    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g2:
	.asciiz "%d %d\n"
	.align 2
	.data
	.align 2
g1:
	.word 0
	.space 4
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label8
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-12
#t1 = [arg1]+1
	lw	$t1,	8($fp)
	lw	$t0,	0($t1)
	addiu	$v1,	$t0,	1
#[arg1] = t1
	lw	$t1,	8($fp)
	sw	$v1,	0($t1)
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label3:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-40
#t2 = 0
	li	$t0,	0
	sw	$t0,	-28($fp)
#t3 = 1
	li	$t0,	1
	sw	$t0,	-24($fp)
#label5:
label5:
#t4 = 10<t3
	li	$t0,	10
	lw	$t1,	-24($fp)
	slt	$v1,	$t0,	$t1
#t4 = t4==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t4==0 j label6
	beq	$v1,	$0,	label6
#t5 = &t2
	la	$v1,	-28($fp)
#t6 = addSmall( t5 )
	sw	$v1,	0($sp)
	jal	label1
	sw	$v0,	-12($fp)
#t7 = t2+1
	lw	$t0,	-28($fp)
	addiu	$v1,	$t0,	1
#t2 = t7
	sw	$v1,	-28($fp)
#label7:
label7:
#t3 = t3+1
	lw	$t0,	-24($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-24($fp)
#j label5
	j label5
#label6:
label6:
#t9 = [arg1]+t2
	lw	$t1,	8($fp)
	lw	$t0,	0($t1)
	lw	$t1,	-28($fp)
	addu	$v1,	$t0,	$t1
#[arg1] = t9
	lw	$t1,	8($fp)
	sw	$v1,	0($t1)
label3exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label4:

label8:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-60
#t12 = &g1
	la	$v1,	g1
#t11 = t12
	sw	$v1,	-36($fp)
#t10 = 1
	li	$t0,	1
	sw	$t0,	-40($fp)
#label10:
label10:
#t13 = 10<t10
	li	$t0,	10
	lw	$t1,	-40($fp)
	slt	$v1,	$t0,	$t1
#t13 = t13==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t13==0 j label11
	beq	$v1,	$0,	label11
#t14 = 0
	li	$t0,	0
	sw	$t0,	-24($fp)
#t15 = &t14
	la	$v1,	-24($fp)
#t16 = addMiddle( t15 )
	sw	$v1,	0($sp)
	jal	label3
	sw	$v0,	-16($fp)
#t17 = [t11]+t14
	lw	$t1,	-36($fp)
	lw	$t0,	0($t1)
	lw	$t1,	-24($fp)
	addu	$v1,	$t0,	$t1
#[t11] = t17
	lw	$t1,	-36($fp)
	sw	$v1,	0($t1)
#t18 = g1+1
	lw	$t0,	g1
	addiu	$v1,	$t0,	1
#g1 = t18
	sw	$v1,	g1
#t19 = printf( g2 t10 g1 )
	la	$t0,	g2
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t0,	-40($fp)
	sw	$t0,	4($sp)
	lw	$t0,	g1
	sw	$t0,	8($sp)
	jal	printf
#label12:
label12:
#t10 = t10+1
	lw	$t0,	-40($fp)
	addiu	$t2,	$t0,	1
	sw	$t2,	-40($fp)
#j label10
	j label10
#label11:
label11:
label8exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label9:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
