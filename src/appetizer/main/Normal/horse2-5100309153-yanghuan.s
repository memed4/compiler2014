    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "%d\n"
	.align 2
g2:
	.asciiz "no solution!\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label4
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-20
#t1 = 0
	addiu	$v1,	$0,	0
#t2 = arg1<arg2
	lw	$t0,	8($fp)
	lw	$t1,	12($fp)
	slt	$a3,	$t0,	$t1
#if t2==0 j label3
	beq	$a3,	$0,	label3
#t3 = arg1<0
	lw	$t0,	8($fp)
	li	$t1,	0
	slt	$a3,	$t0,	$t1
#t3 = t3==0
	li	$t1,	0
	xor	$s0,	$a3,	$t1
	sltiu	$a3,	$s0,	1
#if t3==0 j label3
	beq	$a3,	$0,	label3
#t1 = 1
	addiu	$v1,	$0,	1
#label3:
label3:
#ret t1
	addu	$v0,	$0,	$v1
	j label1exit
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

label4:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-1220
#t4 = 100
	li	$t0,	100
	sw	$t0,	-1204($fp)
#t7 = 0
	li	$t0,	0
	sw	$t0,	-1192($fp)
#t6 = t7
	lw	$t0,	-1192($fp)
	sw	$t0,	-1196($fp)
#t10 = t6
	lw	$t0,	-1196($fp)
	sw	$t0,	-1180($fp)
#t5 = t10
	lw	$t0,	-1180($fp)
	sw	$t0,	-1200($fp)
#t20 = t4-1
	lw	$t0,	-1204($fp)
	li	$t1,	1
	subu	$v1,	$t0,	$t1
#t9 = t20
	sw	$v1,	-1184($fp)
#t8 = t9
	lw	$t0,	-1184($fp)
	sw	$t0,	-1188($fp)
#t14 = 0
	li	$t0,	0
	sw	$t0,	-1164($fp)
#t13 = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	-1168($fp)
#t11 = 0
	li	$t0,	0
	sw	$t0,	-1176($fp)
#t12 = t11
	lw	$t0,	-1176($fp)
	sw	$t0,	-1172($fp)
#t21 = t4*t4
	lw	$t0,	-1204($fp)
	lw	$t1,	-1204($fp)
	mul	$v1,	$t0,	$t1
#t22 = t21*4
	sll	$a3,	$v1,	2
#t23 = malloc( t22 )
	addu	$a0,	$0,	$a3
	jal	malloc
	sw	$v0,	-1128($fp)
#t15 = t23
	lw	$t0,	-1128($fp)
	sw	$t0,	-1160($fp)
#t18 = 0
	li	$t0,	0
	sw	$t0,	-1148($fp)
#label6:
label6:
#t24 = t4*t4
	lw	$t0,	-1204($fp)
	lw	$t1,	-1204($fp)
	mul	$v1,	$t0,	$t1
#t25 = t18<t24
	lw	$t0,	-1148($fp)
	slt	$a3,	$t0,	$v1
#if t25==0 j label7
	beq	$a3,	$0,	label7
#t26 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t27 = t18*4
	lw	$t0,	-1148($fp)
	sll	$a3,	$t0,	2
#t26 = t26+t27
	addu	$v1,	$v1,	$a3
#[t26] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#label8:
label8:
#t28 = t18+1
	lw	$t0,	-1148($fp)
	addiu	$v1,	$t0,	1
#t18 = t28
	sw	$v1,	-1148($fp)
#j label6
	j label6
#label7:
label7:
#t29 = t4*t4
	lw	$t0,	-1204($fp)
	lw	$t1,	-1204($fp)
	mul	$v1,	$t0,	$t1
#t30 = t29*4
	sll	$a3,	$v1,	2
#t31 = malloc( t30 )
	addu	$a0,	$0,	$a3
	jal	malloc
	sw	$v0,	-1096($fp)
#t16 = t31
	lw	$t0,	-1096($fp)
	sw	$t0,	-1156($fp)
#t18 = 0
	li	$t0,	0
	sw	$t0,	-1148($fp)
#label9:
label9:
#t32 = t4*t4
	lw	$t0,	-1204($fp)
	lw	$t1,	-1204($fp)
	mul	$v1,	$t0,	$t1
#t33 = t18<t32
	lw	$t0,	-1148($fp)
	slt	$a3,	$t0,	$v1
#if t33==0 j label10
	beq	$a3,	$0,	label10
#t34 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t35 = t18*4
	lw	$t0,	-1148($fp)
	sll	$a3,	$t0,	2
#t34 = t34+t35
	addu	$v1,	$v1,	$a3
#[t34] = 0
	li	$t0,	0
	sw	$t0,	0($v1)
#label11:
label11:
#t36 = t18+1
	lw	$t0,	-1148($fp)
	addiu	$v1,	$t0,	1
#t18 = t36
	sw	$v1,	-1148($fp)
#j label9
	j label9
#label10:
label10:
#t37 = t4*4
	lw	$t0,	-1204($fp)
	sll	$v1,	$t0,	2
#t38 = malloc( t37 )
	addu	$a0,	$0,	$v1
	jal	malloc
	sw	$v0,	-1068($fp)
#t17 = t38
	lw	$t0,	-1068($fp)
	sw	$t0,	-1152($fp)
#t18 = 0
	li	$t0,	0
	sw	$t0,	-1148($fp)
#label12:
label12:
#t39 = t18<t4
	lw	$t0,	-1148($fp)
	lw	$t1,	-1204($fp)
	slt	$v1,	$t0,	$t1
#if t39==0 j label13
	beq	$v1,	$0,	label13
#t40 = t17
	lw	$t0,	-1152($fp)
	sw	$t0,	-1060($fp)
#t41 = t18*4
	lw	$t0,	-1148($fp)
	sll	$v1,	$t0,	2
#t40 = t40+t41
	lw	$t0,	-1060($fp)
	addu	$t2,	$t0,	$v1
	sw	$t2,	-1060($fp)
#t42 = t4*4
	lw	$t0,	-1204($fp)
	sll	$v1,	$t0,	2
#t43 = malloc( t42 )
	addu	$a0,	$0,	$v1
	jal	malloc
	sw	$v0,	-1048($fp)
#[t40] = t43
	lw	$t0,	-1048($fp)
	lw	$t1,	-1060($fp)
	sw	$t0,	0($t1)
#t19 = 0
	addiu	$v1,	$0,	0
#label15:
label15:
#t44 = t19<t4
	lw	$t1,	-1204($fp)
	slt	$a3,	$v1,	$t1
#if t44==0 j label16
	beq	$a3,	$0,	label16
#t45 = t17
	lw	$t0,	-1152($fp)
	addu	$a3,	$0,	$t0
#t46 = t18*4
	lw	$t0,	-1148($fp)
	sll	$t3,	$t0,	2
#t45 = t45+t46
	addu	$a3,	$a3,	$t3
#t45 = [t45]
	lw	$t0,	0($a3)
	addu	$a3,	$0,	$t0
#t47 = t19*4
	sll	$t3,	$v1,	2
#t45 = t45+t47
	addu	$a3,	$a3,	$t3
#t48 = -1
	li	$t0,	1
	subu	$t3,	$0,	$t0
#[t45] = t48
	sw	$t3,	0($a3)
#label17:
label17:
#t49 = t19+1
	addiu	$a3,	$v1,	1
#t19 = t49
	addu	$v1,	$0,	$a3
#j label15
	j label15
#label16:
label16:
#label14:
label14:
#t50 = t18+1
	lw	$t0,	-1148($fp)
	addiu	$v1,	$t0,	1
#t18 = t50
	sw	$v1,	-1148($fp)
#j label12
	j label12
#label13:
label13:
#t51 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#[t51] = t6
	lw	$t0,	-1196($fp)
	sw	$t0,	0($v1)
#t52 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#[t52] = t7
	lw	$t0,	-1192($fp)
	sw	$t0,	0($v1)
#t53 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t54 = t6*4
	lw	$t0,	-1196($fp)
	sll	$a3,	$t0,	2
#t53 = t53+t54
	addu	$v1,	$v1,	$a3
#t53 = [t53]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t55 = t7*4
	lw	$t0,	-1192($fp)
	sll	$a3,	$t0,	2
#t53 = t53+t55
	addu	$v1,	$v1,	$a3
#t56 = [t53]==0
	lw	$t0,	0($v1)
	li	$t1,	0
	xor	$s0,	$t0,	$t1
	sltiu	$t2,	$s0,	1
	sw	$t2,	-996($fp)
#label18:
label18:
#t57 = t10<t5
	lw	$t0,	-1180($fp)
	lw	$t1,	-1200($fp)
	slt	$v1,	$t0,	$t1
#t57 = t57==0
	li	$t1,	0
	xor	$s0,	$v1,	$t1
	sltiu	$v1,	$s0,	1
#if t57==0 j label19
	beq	$v1,	$0,	label19
#t58 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t59 = t15
	lw	$t0,	-1160($fp)
	addu	$a3,	$0,	$t0
#t60 = t5*4
	lw	$t0,	-1200($fp)
	sll	$t3,	$t0,	2
#t59 = t59+t60
	addu	$a3,	$a3,	$t3
#t61 = [t59]*4
	lw	$t0,	0($a3)
	sll	$t3,	$t0,	2
#t58 = t58+t61
	addu	$v1,	$v1,	$t3
#t58 = [t58]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t62 = t16
	lw	$t0,	-1156($fp)
	addu	$a3,	$0,	$t0
#t63 = t5*4
	lw	$t0,	-1200($fp)
	sll	$t3,	$t0,	2
#t62 = t62+t63
	addu	$a3,	$a3,	$t3
#t64 = [t62]*4
	lw	$t0,	0($a3)
	sll	$t3,	$t0,	2
#t58 = t58+t64
	addu	$v1,	$v1,	$t3
#t12 = [t58]
	lw	$t0,	0($v1)
	sw	$t0,	-1172($fp)
#t65 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t66 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t65 = t65+t66
	addu	$v1,	$v1,	$a3
#t67 = [t65]-1
	lw	$t0,	0($v1)
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t13 = t67
	sw	$a3,	-1168($fp)
#t68 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t69 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t68 = t68+t69
	addu	$v1,	$v1,	$a3
#t70 = [t68]-2
	lw	$t0,	0($v1)
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t14 = t70
	sw	$a3,	-1164($fp)
#t71 = 0
	addiu	$v1,	$0,	0
#t72 = 0
	addiu	$v1,	$0,	0
#t73 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-928($fp)
#t74 = t73==1
	lw	$t0,	-928($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t74==0 j label20
	beq	$v1,	$0,	label20
#t75 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-920($fp)
#t76 = t75==1
	lw	$t0,	-920($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t76==0 j label20
	beq	$v1,	$0,	label20
#t72 = 1
	addiu	$v1,	$0,	1
#label21:
label21:
#if t72==0 j label20
	beq	$v1,	$0,	label20
#t77 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t78 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t77 = t77+t78
	addu	$v1,	$v1,	$a3
#t77 = [t77]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t79 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t77 = t77+t79
	addu	$v1,	$v1,	$a3
#t80 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t81 = [t77]==t80
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t81==0 j label20
	beq	$t3,	$0,	label20
#t71 = 1
	addiu	$v1,	$0,	1
#label22:
label22:
#if t71==0 j label20
	beq	$v1,	$0,	label20
#t82 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t82
	sw	$v1,	-1180($fp)
#t83 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t84 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t83 = t83+t84
	addu	$v1,	$v1,	$a3
#[t83] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t85 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t86 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t85 = t85+t86
	addu	$v1,	$v1,	$a3
#[t85] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t87 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t88 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t87 = t87+t88
	addu	$v1,	$v1,	$a3
#t87 = [t87]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t89 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t87 = t87+t89
	addu	$v1,	$v1,	$a3
#t90 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t87] = t90
	sw	$a3,	0($v1)
#t91 = 0
	addiu	$v1,	$0,	0
#t92 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t92==0 j label23
	beq	$v1,	$0,	label23
#t93 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t93==0 j label23
	beq	$v1,	$0,	label23
#t91 = 1
	addiu	$v1,	$0,	1
#label24:
label24:
#if t91==0 j label23
	beq	$v1,	$0,	label23
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label23:
label23:
#label20:
label20:
#t94 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t95 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t94 = t94+t95
	addu	$v1,	$v1,	$a3
#t96 = [t94]-1
	lw	$t0,	0($v1)
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t13 = t96
	sw	$a3,	-1168($fp)
#t97 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t98 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t97 = t97+t98
	addu	$v1,	$v1,	$a3
#t99 = [t97]+2
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	2
#t14 = t99
	sw	$a3,	-1164($fp)
#t100 = 0
	addiu	$v1,	$0,	0
#t101 = 0
	addiu	$v1,	$0,	0
#t102 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-812($fp)
#t103 = t102==1
	lw	$t0,	-812($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t103==0 j label25
	beq	$v1,	$0,	label25
#t104 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-804($fp)
#t105 = t104==1
	lw	$t0,	-804($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t105==0 j label25
	beq	$v1,	$0,	label25
#t101 = 1
	addiu	$v1,	$0,	1
#label26:
label26:
#if t101==0 j label25
	beq	$v1,	$0,	label25
#t106 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t107 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t106 = t106+t107
	addu	$v1,	$v1,	$a3
#t106 = [t106]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t108 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t106 = t106+t108
	addu	$v1,	$v1,	$a3
#t109 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t110 = [t106]==t109
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t110==0 j label25
	beq	$t3,	$0,	label25
#t100 = 1
	addiu	$v1,	$0,	1
#label27:
label27:
#if t100==0 j label25
	beq	$v1,	$0,	label25
#t111 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t111
	sw	$v1,	-1180($fp)
#t112 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t113 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t112 = t112+t113
	addu	$v1,	$v1,	$a3
#[t112] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t114 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t115 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t114 = t114+t115
	addu	$v1,	$v1,	$a3
#[t114] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t116 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t117 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t116 = t116+t117
	addu	$v1,	$v1,	$a3
#t116 = [t116]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t118 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t116 = t116+t118
	addu	$v1,	$v1,	$a3
#t119 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t116] = t119
	sw	$a3,	0($v1)
#t120 = 0
	addiu	$v1,	$0,	0
#t121 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t121==0 j label28
	beq	$v1,	$0,	label28
#t122 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t122==0 j label28
	beq	$v1,	$0,	label28
#t120 = 1
	addiu	$v1,	$0,	1
#label29:
label29:
#if t120==0 j label28
	beq	$v1,	$0,	label28
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label28:
label28:
#label25:
label25:
#t123 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t124 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t123 = t123+t124
	addu	$v1,	$v1,	$a3
#t125 = [t123]+1
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	1
#t13 = t125
	sw	$a3,	-1168($fp)
#t126 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t127 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t126 = t126+t127
	addu	$v1,	$v1,	$a3
#t128 = [t126]-2
	lw	$t0,	0($v1)
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t14 = t128
	sw	$a3,	-1164($fp)
#t129 = 0
	addiu	$v1,	$0,	0
#t130 = 0
	addiu	$v1,	$0,	0
#t131 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-696($fp)
#t132 = t131==1
	lw	$t0,	-696($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t132==0 j label30
	beq	$v1,	$0,	label30
#t133 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-688($fp)
#t134 = t133==1
	lw	$t0,	-688($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t134==0 j label30
	beq	$v1,	$0,	label30
#t130 = 1
	addiu	$v1,	$0,	1
#label31:
label31:
#if t130==0 j label30
	beq	$v1,	$0,	label30
#t135 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t136 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t135 = t135+t136
	addu	$v1,	$v1,	$a3
#t135 = [t135]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t137 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t135 = t135+t137
	addu	$v1,	$v1,	$a3
#t138 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t139 = [t135]==t138
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t139==0 j label30
	beq	$t3,	$0,	label30
#t129 = 1
	addiu	$v1,	$0,	1
#label32:
label32:
#if t129==0 j label30
	beq	$v1,	$0,	label30
#t140 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t140
	sw	$v1,	-1180($fp)
#t141 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t142 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t141 = t141+t142
	addu	$v1,	$v1,	$a3
#[t141] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t143 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t144 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t143 = t143+t144
	addu	$v1,	$v1,	$a3
#[t143] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t145 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t146 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t145 = t145+t146
	addu	$v1,	$v1,	$a3
#t145 = [t145]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t147 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t145 = t145+t147
	addu	$v1,	$v1,	$a3
#t148 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t145] = t148
	sw	$a3,	0($v1)
#t149 = 0
	addiu	$v1,	$0,	0
#t150 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t150==0 j label33
	beq	$v1,	$0,	label33
#t151 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t151==0 j label33
	beq	$v1,	$0,	label33
#t149 = 1
	addiu	$v1,	$0,	1
#label34:
label34:
#if t149==0 j label33
	beq	$v1,	$0,	label33
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label33:
label33:
#label30:
label30:
#t152 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t153 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t152 = t152+t153
	addu	$v1,	$v1,	$a3
#t154 = [t152]+1
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	1
#t13 = t154
	sw	$a3,	-1168($fp)
#t155 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t156 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t155 = t155+t156
	addu	$v1,	$v1,	$a3
#t157 = [t155]+2
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	2
#t14 = t157
	sw	$a3,	-1164($fp)
#t158 = 0
	addiu	$v1,	$0,	0
#t159 = 0
	addiu	$v1,	$0,	0
#t160 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-580($fp)
#t161 = t160==1
	lw	$t0,	-580($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t161==0 j label35
	beq	$v1,	$0,	label35
#t162 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-572($fp)
#t163 = t162==1
	lw	$t0,	-572($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t163==0 j label35
	beq	$v1,	$0,	label35
#t159 = 1
	addiu	$v1,	$0,	1
#label36:
label36:
#if t159==0 j label35
	beq	$v1,	$0,	label35
#t164 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t165 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t164 = t164+t165
	addu	$v1,	$v1,	$a3
#t164 = [t164]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t166 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t164 = t164+t166
	addu	$v1,	$v1,	$a3
#t167 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t168 = [t164]==t167
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t168==0 j label35
	beq	$t3,	$0,	label35
#t158 = 1
	addiu	$v1,	$0,	1
#label37:
label37:
#if t158==0 j label35
	beq	$v1,	$0,	label35
#t169 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t169
	sw	$v1,	-1180($fp)
#t170 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t171 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t170 = t170+t171
	addu	$v1,	$v1,	$a3
#[t170] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t172 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t173 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t172 = t172+t173
	addu	$v1,	$v1,	$a3
#[t172] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t174 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t175 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t174 = t174+t175
	addu	$v1,	$v1,	$a3
#t174 = [t174]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t176 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t174 = t174+t176
	addu	$v1,	$v1,	$a3
#t177 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t174] = t177
	sw	$a3,	0($v1)
#t178 = 0
	addiu	$v1,	$0,	0
#t179 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t179==0 j label38
	beq	$v1,	$0,	label38
#t180 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t180==0 j label38
	beq	$v1,	$0,	label38
#t178 = 1
	addiu	$v1,	$0,	1
#label39:
label39:
#if t178==0 j label38
	beq	$v1,	$0,	label38
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label38:
label38:
#label35:
label35:
#t181 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t182 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t181 = t181+t182
	addu	$v1,	$v1,	$a3
#t183 = [t181]-2
	lw	$t0,	0($v1)
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t13 = t183
	sw	$a3,	-1168($fp)
#t184 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t185 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t184 = t184+t185
	addu	$v1,	$v1,	$a3
#t186 = [t184]-1
	lw	$t0,	0($v1)
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t14 = t186
	sw	$a3,	-1164($fp)
#t187 = 0
	addiu	$v1,	$0,	0
#t188 = 0
	addiu	$v1,	$0,	0
#t189 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-464($fp)
#t190 = t189==1
	lw	$t0,	-464($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t190==0 j label40
	beq	$v1,	$0,	label40
#t191 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-456($fp)
#t192 = t191==1
	lw	$t0,	-456($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t192==0 j label40
	beq	$v1,	$0,	label40
#t188 = 1
	addiu	$v1,	$0,	1
#label41:
label41:
#if t188==0 j label40
	beq	$v1,	$0,	label40
#t193 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t194 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t193 = t193+t194
	addu	$v1,	$v1,	$a3
#t193 = [t193]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t195 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t193 = t193+t195
	addu	$v1,	$v1,	$a3
#t196 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t197 = [t193]==t196
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t197==0 j label40
	beq	$t3,	$0,	label40
#t187 = 1
	addiu	$v1,	$0,	1
#label42:
label42:
#if t187==0 j label40
	beq	$v1,	$0,	label40
#t198 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t198
	sw	$v1,	-1180($fp)
#t199 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t200 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t199 = t199+t200
	addu	$v1,	$v1,	$a3
#[t199] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t201 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t202 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t201 = t201+t202
	addu	$v1,	$v1,	$a3
#[t201] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t203 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t204 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t203 = t203+t204
	addu	$v1,	$v1,	$a3
#t203 = [t203]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t205 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t203 = t203+t205
	addu	$v1,	$v1,	$a3
#t206 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t203] = t206
	sw	$a3,	0($v1)
#t207 = 0
	addiu	$v1,	$0,	0
#t208 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t208==0 j label43
	beq	$v1,	$0,	label43
#t209 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t209==0 j label43
	beq	$v1,	$0,	label43
#t207 = 1
	addiu	$v1,	$0,	1
#label44:
label44:
#if t207==0 j label43
	beq	$v1,	$0,	label43
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label43:
label43:
#label40:
label40:
#t210 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t211 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t210 = t210+t211
	addu	$v1,	$v1,	$a3
#t212 = [t210]-2
	lw	$t0,	0($v1)
	li	$t1,	2
	subu	$a3,	$t0,	$t1
#t13 = t212
	sw	$a3,	-1168($fp)
#t213 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t214 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t213 = t213+t214
	addu	$v1,	$v1,	$a3
#t215 = [t213]+1
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	1
#t14 = t215
	sw	$a3,	-1164($fp)
#t216 = 0
	addiu	$v1,	$0,	0
#t217 = 0
	addiu	$v1,	$0,	0
#t218 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-348($fp)
#t219 = t218==1
	lw	$t0,	-348($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t219==0 j label45
	beq	$v1,	$0,	label45
#t220 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-340($fp)
#t221 = t220==1
	lw	$t0,	-340($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t221==0 j label45
	beq	$v1,	$0,	label45
#t217 = 1
	addiu	$v1,	$0,	1
#label46:
label46:
#if t217==0 j label45
	beq	$v1,	$0,	label45
#t222 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t223 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t222 = t222+t223
	addu	$v1,	$v1,	$a3
#t222 = [t222]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t224 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t222 = t222+t224
	addu	$v1,	$v1,	$a3
#t225 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t226 = [t222]==t225
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t226==0 j label45
	beq	$t3,	$0,	label45
#t216 = 1
	addiu	$v1,	$0,	1
#label47:
label47:
#if t216==0 j label45
	beq	$v1,	$0,	label45
#t227 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t227
	sw	$v1,	-1180($fp)
#t228 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t229 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t228 = t228+t229
	addu	$v1,	$v1,	$a3
#[t228] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t230 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t231 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t230 = t230+t231
	addu	$v1,	$v1,	$a3
#[t230] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t232 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t233 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t232 = t232+t233
	addu	$v1,	$v1,	$a3
#t232 = [t232]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t234 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t232 = t232+t234
	addu	$v1,	$v1,	$a3
#t235 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t232] = t235
	sw	$a3,	0($v1)
#t236 = 0
	addiu	$v1,	$0,	0
#t237 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t237==0 j label48
	beq	$v1,	$0,	label48
#t238 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t238==0 j label48
	beq	$v1,	$0,	label48
#t236 = 1
	addiu	$v1,	$0,	1
#label49:
label49:
#if t236==0 j label48
	beq	$v1,	$0,	label48
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label48:
label48:
#label45:
label45:
#t239 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t240 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t239 = t239+t240
	addu	$v1,	$v1,	$a3
#t241 = [t239]+2
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	2
#t13 = t241
	sw	$a3,	-1168($fp)
#t242 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t243 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t242 = t242+t243
	addu	$v1,	$v1,	$a3
#t244 = [t242]-1
	lw	$t0,	0($v1)
	li	$t1,	1
	subu	$a3,	$t0,	$t1
#t14 = t244
	sw	$a3,	-1164($fp)
#t245 = 0
	addiu	$v1,	$0,	0
#t246 = 0
	addiu	$v1,	$0,	0
#t247 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-232($fp)
#t248 = t247==1
	lw	$t0,	-232($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t248==0 j label50
	beq	$v1,	$0,	label50
#t249 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-224($fp)
#t250 = t249==1
	lw	$t0,	-224($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t250==0 j label50
	beq	$v1,	$0,	label50
#t246 = 1
	addiu	$v1,	$0,	1
#label51:
label51:
#if t246==0 j label50
	beq	$v1,	$0,	label50
#t251 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t252 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t251 = t251+t252
	addu	$v1,	$v1,	$a3
#t251 = [t251]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t253 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t251 = t251+t253
	addu	$v1,	$v1,	$a3
#t254 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t255 = [t251]==t254
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t255==0 j label50
	beq	$t3,	$0,	label50
#t245 = 1
	addiu	$v1,	$0,	1
#label52:
label52:
#if t245==0 j label50
	beq	$v1,	$0,	label50
#t256 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t256
	sw	$v1,	-1180($fp)
#t257 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t258 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t257 = t257+t258
	addu	$v1,	$v1,	$a3
#[t257] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t259 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t260 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t259 = t259+t260
	addu	$v1,	$v1,	$a3
#[t259] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t261 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t262 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t261 = t261+t262
	addu	$v1,	$v1,	$a3
#t261 = [t261]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t263 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t261 = t261+t263
	addu	$v1,	$v1,	$a3
#t264 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t261] = t264
	sw	$a3,	0($v1)
#t265 = 0
	addiu	$v1,	$0,	0
#t266 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t266==0 j label53
	beq	$v1,	$0,	label53
#t267 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t267==0 j label53
	beq	$v1,	$0,	label53
#t265 = 1
	addiu	$v1,	$0,	1
#label54:
label54:
#if t265==0 j label53
	beq	$v1,	$0,	label53
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label53:
label53:
#label50:
label50:
#t268 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t269 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t268 = t268+t269
	addu	$v1,	$v1,	$a3
#t270 = [t268]+2
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	2
#t13 = t270
	sw	$a3,	-1168($fp)
#t271 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t272 = t5*4
	lw	$t0,	-1200($fp)
	sll	$a3,	$t0,	2
#t271 = t271+t272
	addu	$v1,	$v1,	$a3
#t273 = [t271]+1
	lw	$t0,	0($v1)
	addiu	$a3,	$t0,	1
#t14 = t273
	sw	$a3,	-1164($fp)
#t274 = 0
	addiu	$v1,	$0,	0
#t275 = 0
	addiu	$v1,	$0,	0
#t276 = check( t13 t4 )
	lw	$t0,	-1168($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-116($fp)
#t277 = t276==1
	lw	$t0,	-116($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t277==0 j label55
	beq	$v1,	$0,	label55
#t278 = check( t14 t4 )
	lw	$t0,	-1164($fp)
	sw	$t0,	0($sp)
	lw	$t0,	-1204($fp)
	sw	$t0,	4($sp)
	jal	label1
	sw	$v0,	-108($fp)
#t279 = t278==1
	lw	$t0,	-108($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t279==0 j label55
	beq	$v1,	$0,	label55
#t275 = 1
	addiu	$v1,	$0,	1
#label56:
label56:
#if t275==0 j label55
	beq	$v1,	$0,	label55
#t280 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t281 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t280 = t280+t281
	addu	$v1,	$v1,	$a3
#t280 = [t280]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t282 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t280 = t280+t282
	addu	$v1,	$v1,	$a3
#t283 = -1
	li	$t0,	1
	subu	$a3,	$0,	$t0
#t284 = [t280]==t283
	lw	$t0,	0($v1)
	xor	$s0,	$t0,	$a3
	sltiu	$t3,	$s0,	1
#if t284==0 j label55
	beq	$t3,	$0,	label55
#t274 = 1
	addiu	$v1,	$0,	1
#label57:
label57:
#if t274==0 j label55
	beq	$v1,	$0,	label55
#t285 = t10+1
	lw	$t0,	-1180($fp)
	addiu	$v1,	$t0,	1
#t10 = t285
	sw	$v1,	-1180($fp)
#t286 = t15
	lw	$t0,	-1160($fp)
	addu	$v1,	$0,	$t0
#t287 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t286 = t286+t287
	addu	$v1,	$v1,	$a3
#[t286] = t13
	lw	$t0,	-1168($fp)
	sw	$t0,	0($v1)
#t288 = t16
	lw	$t0,	-1156($fp)
	addu	$v1,	$0,	$t0
#t289 = t10*4
	lw	$t0,	-1180($fp)
	sll	$a3,	$t0,	2
#t288 = t288+t289
	addu	$v1,	$v1,	$a3
#[t288] = t14
	lw	$t0,	-1164($fp)
	sw	$t0,	0($v1)
#t290 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t291 = t13*4
	lw	$t0,	-1168($fp)
	sll	$a3,	$t0,	2
#t290 = t290+t291
	addu	$v1,	$v1,	$a3
#t290 = [t290]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t292 = t14*4
	lw	$t0,	-1164($fp)
	sll	$a3,	$t0,	2
#t290 = t290+t292
	addu	$v1,	$v1,	$a3
#t293 = t12+1
	lw	$t0,	-1172($fp)
	addiu	$a3,	$t0,	1
#[t290] = t293
	sw	$a3,	0($v1)
#t294 = 0
	addiu	$v1,	$0,	0
#t295 = t13==t8
	lw	$t0,	-1168($fp)
	lw	$t1,	-1188($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t295==0 j label58
	beq	$v1,	$0,	label58
#t296 = t14==t9
	lw	$t0,	-1164($fp)
	lw	$t1,	-1184($fp)
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t296==0 j label58
	beq	$v1,	$0,	label58
#t294 = 1
	addiu	$v1,	$0,	1
#label59:
label59:
#if t294==0 j label58
	beq	$v1,	$0,	label58
#t11 = 1
	li	$t0,	1
	sw	$t0,	-1176($fp)
#label58:
label58:
#label55:
label55:
#t297 = t11==1
	lw	$t0,	-1176($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t297==0 j label60
	beq	$v1,	$0,	label60
#j label19
	j label19
#label60:
label60:
#t298 = t5+1
	lw	$t0,	-1200($fp)
	addiu	$v1,	$t0,	1
#t5 = t298
	sw	$v1,	-1200($fp)
#j label18
	j label18
#label19:
label19:
#t299 = t11==1
	lw	$t0,	-1176($fp)
	li	$t1,	1
	xor	$s0,	$t0,	$t1
	sltiu	$v1,	$s0,	1
#if t299==0 j label61
	beq	$v1,	$0,	label61
#t300 = t17
	lw	$t0,	-1152($fp)
	addu	$v1,	$0,	$t0
#t301 = t8*4
	lw	$t0,	-1188($fp)
	sll	$a3,	$t0,	2
#t300 = t300+t301
	addu	$v1,	$v1,	$a3
#t300 = [t300]
	lw	$t0,	0($v1)
	addu	$v1,	$0,	$t0
#t302 = t9*4
	lw	$t0,	-1184($fp)
	sll	$a3,	$t0,	2
#t300 = t300+t302
	addu	$v1,	$v1,	$a3
#t303 = printf( g1 [t300] )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
#j label62
	j label62
#label61:
label61:
#t304 = printf( g2 )
	la	$a0,	g2
	li	$v0,	4
	syscall
#label62:
label62:
#ret 0
	li	$v0,	0
	j label4exit
label4exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label5:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
