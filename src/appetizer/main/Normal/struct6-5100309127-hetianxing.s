    .data 0x10000000
    .space 0xFFFFF
	.sdata 0x10000000
	.align 2
g1:
	.asciiz "%d %d\n"
	.align 2
g2:
	.asciiz "%d\n"
	.align 2
	.data
	.align 2
static_end:
	.word	static_end + 4

	.text
main:
	jal label1
	or	$a0,	$v0,	$0
	or	$v0,	$0,	17
	syscall

label1:
	sw	$ra,	-4($sp)
	sw	$fp,	-8($sp)
	addiu	$fp,	$sp,	-8
	addiu	$sp,	$sp,	-432
#t1 = 3
	li	$t0,	3
	sw	$t0,	-332($fp)
#t5 = &t2
	la	$v1,	-328($fp)
#t5 = t5+0
	addiu	$v1,	$v1,	0
#t6 = &t1
	la	$a3,	-332($fp)
#[t5] = t6
	sw	$a3,	0($v1)
#t7 = &t2
	la	$v1,	-328($fp)
#t7 = t7+4
	addiu	$v1,	$v1,	4
#t8 = 0*4
	li	$t0,	0
	sll	$a3,	$t0,	2
#t7 = t7+t8
	addu	$v1,	$v1,	$a3
#[t7] = 2
	li	$t0,	2
	sw	$t0,	0($v1)
#t9 = &t3
	la	$v1,	-244($fp)
#t10 = &t2
	la	$a3,	-328($fp)
#t11 = memcpy( t9 t10 84 )
	addu	$a1,	$0,	$v1
	addu	$a2,	$0,	$a3
	li	$a0,	84
	jal	memcpy
#t12 = &t4
	la	$v1,	-160($fp)
#t13 = &t3
	la	$a3,	-244($fp)
#t14 = memcpy( t12 t13 84 )
	addu	$a1,	$0,	$v1
	addu	$a2,	$0,	$a3
	li	$a0,	84
	jal	memcpy
#t15 = &t4
	la	$v1,	-160($fp)
#t15 = t15+4
	addiu	$v1,	$v1,	4
#t16 = 0*4
	li	$t0,	0
	sll	$a3,	$t0,	2
#t15 = t15+t16
	addu	$v1,	$v1,	$a3
#[t15] = 3
	li	$t0,	3
	sw	$t0,	0($v1)
#t17 = &t4
	la	$v1,	-160($fp)
#t17 = t17+4
	addiu	$v1,	$v1,	4
#t18 = 0*4
	li	$t0,	0
	sll	$a3,	$t0,	2
#t17 = t17+t18
	addu	$v1,	$v1,	$a3
#t19 = &t4
	la	$a3,	-160($fp)
#t19 = t19+0
	addiu	$a3,	$a3,	0
#t20 = printf( g1 [t17] [[t19]] )
	la	$t0,	g1
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	lw	$t0,	0($a3)
	lw	$t1,	0($t0)
	sw	$t1,	8($sp)
	jal	printf
#t21 = &t2
	la	$v1,	-328($fp)
#t21 = t21+4
	addiu	$v1,	$v1,	4
#t22 = 0*4
	li	$t0,	0
	sll	$a3,	$t0,	2
#t21 = t21+t22
	addu	$v1,	$v1,	$a3
#t23 = printf( g2 [t21] )
	la	$t0,	g2
	addu	$a0,	$0,	$t0
	sw	$a0,	0($sp)
	lw	$t1,	0($v1)
	sw	$t1,	4($sp)
	jal	printf
label1exit:
	lw	$ra,	4($fp)
	addiu	$sp,	$fp,	8
	lw	$fp,	0($fp)
	jr	$ra
label2:

printf:
stprintf:
	or	$t0,	$0,	$a0
	addiu   $t1, $sp, 4
loop_pr:
	lb      $a0, 0($t0)
	addiu   $t0, $t0, 1
	beq     $a0, $0, endprint
	xori    $t2, $a0, '%'
	beqz    $t2, format_pr
plain_pr:
	li      $v0, 11
	syscall
	j	loop_pr
format_pr:
	lb $a0,	0($t0)
	addiu   $t0, $t0, 1
	xori    $t2, $a0, 'd'
	beq     $t2, $0, int_pr
	xori    $t2, $a0, 'c'
	beq     $t2, $0, char_pr
	xori    $t2, $a0, 's'
	beq     $t2, $0, string_pr
	xori    $t2, $a0, '0'
	beq     $t2, $0, prec_pr
int_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 1
	syscall
	j	loop_pr
char_pr:
	lb      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 11
	syscall
	j	loop_pr
string_pr:
	lw      $a0, 0($t1)
	addiu   $t1, $t1, 4
	ori     $v0, $0, 4
	syscall
	j	loop_pr
prec_pr:
	addiu	$t0, $t0, 2
	lw		$t2, 0($t1)
	addiu	$t1, $t1, 4
	or		$a0, $0, $0
	ori		$v0, $0, 1
	slti	$t3, $t2, 1000
	beqz	$t3, non_zero
	slti	$t3, $t2, 100
	beqz	$t3, one_zero
	slti	$t3, $t2, 10
	beqz	$t3, two_zero
three_zero:
	syscall
two_zero:
	syscall
one_zero:
	syscall
non_zero:
	or	$a0, $t2, $0
	syscall
	j	loop_pr
endprint:
	jr	$ra

malloc:
	lw	$v0,	static_end
	addu	$a0,	$v0,	$a0
	addiu	$a0,	$a0,	3
	srl	$a0,	$a0,	2
	sll	$a0,	$a0,	2
	sw	$a0,	static_end
	jr	$ra

memcpy:
	lw	$t8,	0($a2)
	addiu	$a2,	$a2,	4
	sw	$t8,	0($a1)
	addiu	$a1,	$a1,	4
	addiu	$a0,	$a0,	-4
	bne	$a0,	$0,	memcpy
	jr	$ra
