package appetizer.translate;

import appetizer.interform.*;
import appetizer.environ.*;

public class Asm {
	static public String push(Register reg) {
		return null;
	}
	
	static public String getAlloc(int i) {
		return Utils.regs[i];
	}
	
	static public void push(String s) {
		System.out.println("	sw" 
			 + "	" + s + ","
		     + "	" + "-4" + "($sp)");
	}
	
	static public void assignReg(String dest, String src) {
		System.out.println("	addu"
			 + "	" + dest + ","
			 + "	" + "$0" + ","
			 + "	" + src);
	}
	
	static public void label(String s) {
		System.out.println(s + ":");
	}
	
	static public void jmp(String s) {
		System.out.println("	j" + " " + s);
	}
	
	private static String getReg(Register reg) {
		if (reg.getMemloc().isArgs()) {
			return Integer.toString(reg.getMemloc().offset) + "($fp)";
		} else {
			return "-" + Integer.toString(reg.getMemloc().offset) + "($fp)";
		}
	}
	
	private static String getGlobal(Register reg) {
		return reg.getIR();
	}
	
	private static String getConst(Const con) {
		return con.getIR();
	}
	
	private static String getMem(Mem memi) {
		if (memi.oprand instanceof Register) {
			String s = getReg((Register)memi.oprand);
			return s;
		} else if (memi.oprand instanceof Mem) {
			Mem xmem = (Mem)memi.oprand;
			Register reg = (Register)xmem.oprand;
			Asm.movMtoR(getReg(reg), "$t0");
			Asm.movMemRtoR("$t0", "$t1");
			return "$t1";
		}
		return null;
	}
	
	public static void addiu(String dest, String src, String imm) {
		System.out.println("	addiu	" + dest + ",	" + src + ",	" + imm);
	}
	
	public static void movRtoR(String r1, String r2) {
		System.out.println("	addu	" + r2 + ",	$0,	" + r1);
	}
	
	public static void movMtoM(String m1, String m2) {
		System.out.println("	lw	$t0,	" + m1);
		System.out.println("	sw	$t0,	" + m2);
	}
	
	public static void movMtoR(String m, String r) {
		System.out.println("	lw	" + r + ",	" + m);
	}
	
	public static void movRtoM(String r, String m) {
		System.out.println("	sw	" + r + ",	" + m);
	}
	
	public static void movCtoR(String num, String r) {
		System.out.println("	li	" + r + ",	" + num);
	}
	
	public static void movAtoR(String addr, String r) {
		System.out.println("	la	" + r + ",	" + addr);
	}
	
	public static void movMemRtoR(String r1, String r2) {
		System.out.println("	lw	" + r2 + ",	" + "0(" + r1 + ")");
	}
	
	public static void movRtoMemR(String r1, String r2) {
		System.out.println("	sw	" + r1 + ",	" + "0(" + r2 + ")");
	}
	
	static public void malloc(Func func, Oprand result, FunctionEntry funcEntry) {
		String reg = null;
		Oprand op = func.argument.get(0);
		if (op instanceof Register) {
			if (((Register) op).getMemloc().isGlobal()) {
				reg = getGlobal((Register)op);
				Asm.movMtoR(reg, "$t0");
				Asm.movRtoR("$t0", "$a0");
			} else {
				int i = ((Register)op).memloc.getNum();
				if (funcEntry.allo.containsKey(i) && !((Register)op).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(i)), "$a0");
				} else {
					reg = getReg((Register)op);
					Asm.movMtoR(reg, "$t0");
					Asm.movRtoR("$t0", "$a0");
				}
			}
		} else if (op instanceof Mem) {
			Mem memi = (Mem)op;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int i = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(i), "$a0");
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int i = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(i), "$t0");
				Asm.movMemRtoR("$t0", "$a0");
			} else {
				reg = getMem((Mem)op);
				Asm.movMtoR(reg, "$t0");
				Asm.movMemRtoR("$t0", "$t1");
				Asm.movRtoR("$t1", "$a0");
			}
		} else if (op instanceof Const) {
			reg = getConst((Const)op);
			Asm.movCtoR(reg, "$t0");
			Asm.movRtoR("$t0", "$a0");
		}
		
		System.out.println("	jal	malloc");
		
		if (result instanceof Register) {
			if (((Register) result).getMemloc().isGlobal()) {
				reg = getGlobal((Register)result);
				Asm.movRtoM("$v0", reg);
			} else {
				int i = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(i) && !((Register)result).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(i)), "$a0");
				} else {
					reg = getReg((Register)result);
					Asm.movRtoM("$v0", reg);
				}
			}
		} else if (result instanceof Mem) {
			Mem memi = (Mem)result;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int i = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movRtoMemR("$v0", getAlloc(i));
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int i = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(i), "$t0");
				Asm.movRtoMemR("$v0", "$t0");
			} else {
				reg = getMem((Mem) result);
				Asm.movMtoR(reg, "$t0");
				Asm.movRtoMemR("$v0", "$t0");
			}
		}
	}
	
	static public void printf(Func func, FunctionEntry funcEntry) {
		int offset = 0;
		String reg;
		if (func.argument.size() == 1) {
			Oprand op = func.argument.get(0);
			reg = getGlobal((Register)op);
			Asm.movAtoR(reg, "$a0");
			Asm.movCtoR("4", "$v0");
			System.out.println("	syscall");
		} else {
		for (int i = 0; i < func.argument.size(); ++i) {
			Oprand op = func.argument.get(i);
			if (op instanceof Register) {
				MemLoc memloc = ((Register) op).getMemloc();
				if (memloc.getType() instanceof Array) {
					if (((Register) op).getMemloc().isGlobal()) {
						reg = getGlobal((Register)op);
						Asm.movAtoR(reg, "$t0");
						if ( i > 0 ) {
							Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						} else {
							Asm.movRtoR("$t0", "$a0");
							Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
						}
						offset += 4;
					} else {
						reg = getReg((Register)op);
						Asm.movAtoR(reg, "$t0");
						
						//offset += getAlignOffset(((Register) op).getMemloc().getType().getSize());
						if ( i > 0 ) {
							Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						} else {
							Asm.movRtoR("$t0", "$a0");
							Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
						}
						offset += 4;
					}
				} else {
					if (((Register) op).getMemloc().isGlobal()) {
						reg = getGlobal((Register)op);
						Asm.movMtoR(reg, "$t0");
						if ( i > 0 ) {
							Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						} else {
							Asm.movRtoR("$t0", "$a0");
							Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
						}
						offset += 4;
					} else {
						int x = ((Register)op).memloc.getNum();
						if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
							if ( i > 0 ){
								Asm.movRtoM(getAlloc(funcEntry.allo.get(x)), Integer.toString(offset) + "($sp)");
							} else {
								Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a0");
								Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
							}
							
						} else {
							reg = getReg((Register)op);
							Asm.movMtoR(reg, "$t0");
							//offset += getAlignOffset(((Register) op).getMemloc().getType().getSize());
							if ( i > 0 ) {
								Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
							} else {
								Asm.movRtoR("$t0", "$a0");
								Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
							}
						}
						offset += 4;
					}
				}
			} else if (op instanceof Mem) {
				Mem memi = (Mem)op;
				if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
					//String s = getReg((Register)memi.oprand);
					int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
					Asm.movMemRtoR(getAlloc(x), "$t1");
					if ( i > 0 ) {
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					} else {
						Asm.movRtoR("$t1", "$a0");
						Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
					}
				} else if (memi.oprand instanceof Mem 
				&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
					int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
					Asm.movMemRtoR(getAlloc(x), "$t0");
					Asm.movMemRtoR("$t0", "$t1");
					if ( i > 0 ) {
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					} else {
						Asm.movRtoR("$t1", "$a0");
						Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
					}
				} else {
					reg = getMem((Mem)op);
					if (((Mem)op).oprand instanceof Register)
						Asm.movMtoR(reg, "$t0");
					else
						Asm.movRtoR(reg, "$t0");
					Asm.movMemRtoR("$t0", "$t1");
					if ( i > 0 ) {
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					} else {
						Asm.movRtoR("$t1", "$a0");
						Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
					}
				}
				offset += 4;
			} else if (op instanceof Const) {
				reg = getConst((Const)op);
				Asm.movCtoR(reg, "$t0");
				if ( i > 0) {
					Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
				} else {
					Asm.movRtoR("$t0", "$a0");
					Asm.movRtoM("$a0", Integer.toString(offset) + "($sp)");
				}
				offset += 4;
			}
		}
		System.out.println("	jal	printf");
		}
	}
	
	static public void comp1(Func func, Oprand result) {
		Oprand op1 = func.argument.get(0);
		Register r1 = (Register)((Mem) op1).oprand;
		Oprand op2 = func.argument.get(0);
		Register r2 = (Register)((Mem) op1).oprand;
		Asm.movMtoR(getReg((Register)r1), "$t0");
		Asm.movMtoR(getReg((Register)r2), "$t1");
		Asm.eq("$t0", "$t1", "$t2");
		
		Asm.movRtoM("$t2", getReg((Register) result));
		
	}
	
 	static public void assignFunc(Func func, Oprand result, FunctionEntry funcEntry) {
		int offset = 0;
		String reg;
		for (int i = 0; i < func.argument.size(); ++i) {
			Oprand op = func.argument.get(i);
			if (op instanceof Register) {
				MemLoc memloc = ((Register) op).getMemloc();
				if (memloc.getType() instanceof Struct) {
					Asm.movMtoR(getReg((Register)op), "$a2");
					Asm.movAtoR(Integer.toString(offset) + "($sp)", "$a1");
					Asm.movCtoR(getConst(new Const(memloc.getType().getSize())), "$a0");
					System.out.println("	jal	memcpy");
					offset += memloc.getType().getSize();
					continue;
				}
				if (memloc.getType() instanceof Array) {
					if (((Register) op).getMemloc().isGlobal()) {
						reg = getGlobal((Register)op);
						Asm.movAtoR(reg, "$t0");
						Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						offset += 4;
					} else {
						reg = getReg((Register)op);
						if (memloc.isArgs()) {
							Asm.movMtoR(reg, "$t0");
						} else {
							Asm.movAtoR(reg, "$t0");
						}
						//offset += getAlignOffset(((Register) op).getMemloc().getType().getSize());
						Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						offset += 4;
					}
				} else {
					if (((Register) op).getMemloc().isGlobal()) {
						reg = getGlobal((Register)op);
						Asm.movMtoR(reg, "$t0");
						Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
						offset += 4;
					} else {
						int x = ((Register)op).memloc.getNum();
						if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
							Asm.movRtoM(getAlloc(funcEntry.allo.get(x)), Integer.toString(offset) + "($sp)");							
						} else {
							reg = getReg((Register)op);
							//offset += getAlignOffset(((Register) op).getMemloc().getType().getSize());
							Asm.movMtoM(reg, Integer.toString(offset) + "($sp)");
						}
						offset += 4;
					}
				}
			} else if (op instanceof Mem) {
				reg = getMem((Mem)op);
				if ( ((Mem)op).oprand instanceof Register && ((Register) ((Mem)op).oprand).memloc.getType() instanceof Pointer)
				{ 
					if (((Pointer) ((Register) ((Mem)op).oprand).memloc.getType()).getelementType() instanceof Struct) {
						Type tmpType = ((Pointer) ((Register) ((Mem)op).oprand).memloc.getType()).getelementType();
						int size = ((Struct) tmpType).getSize();
						
						Asm.movAtoR(reg, "$a2");
						Asm.movAtoR(Integer.toString(offset) + "($sp)", "$a1");
						Asm.movCtoR(getConst(new Const(size)), "$a0");
						System.out.println("	jal memcpy");
						offset += size;
					} else {
						Mem memi = (Mem)op;
						if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
							//String s = getReg((Register)memi.oprand);
							int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
							Asm.movMemRtoR(getAlloc(x), "$t1");
							Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
						} else if (memi.oprand instanceof Mem 
						&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
							int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
							Asm.movMemRtoR(getAlloc(x), "$t0");
							Asm.movMemRtoR("$t0", "$t1");
							Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
						} else {
							Asm.movMtoR(reg, "$t0");
							Asm.movMemRtoR("$t0", "$t1");
							Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
						}
						offset += 4;
					}
				} else {
					Mem memi = (Mem)op;
					if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
						//String s = getReg((Register)memi.oprand);
						int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
						Asm.movMemRtoR(getAlloc(x), "$t1");
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					} else if (memi.oprand instanceof Mem 
					&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
						int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
						Asm.movMemRtoR(getAlloc(x), "$t0");
						Asm.movMemRtoR("$t0", "$t1");
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					} else {
						Asm.movMtoR(reg, "$t0");
						Asm.movMemRtoR("$t0", "$t1");
						Asm.movRtoM("$t1", Integer.toString(offset) + "($sp)");
					}
					offset += 4;
				}
			} else if (op instanceof Const) {
				reg = getConst((Const)op);
				Asm.movCtoR(reg, "$t0");
				Asm.movRtoM("$t0", Integer.toString(offset) + "($sp)");
				offset += 4;
			}
		}
		System.out.println("	jal	" + func.entry.fbegin.oprand1.getIR());
		if (result instanceof Register) {
			if (((Register) result).getMemloc().isGlobal()) {
				reg = getGlobal((Register)result);
				Asm.movRtoM("$v0", reg);
			} else {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					Asm.movRtoR("$v0", getAlloc(funcEntry.allo.get(x)));							
				} else {
					reg = getReg((Register)result);
					Asm.movRtoM("$v0", reg);
				}
			}
		} else if (result instanceof Mem) {
			Mem memi = (Mem)result;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movRtoMemR("$v0", getAlloc(x));
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
				Asm.movRtoMemR("$v0", "$t0");
			} else {
				reg = getMem((Mem) result);
				Asm.movMtoR(reg, "$t0");
				Asm.movRtoMemR("$v0", "$t0");
			}
		}
	}
	
 	static public void operation1(Operation op, Oprand oprand1, Oprand result, FunctionEntry funcEntry) {
 		String reg = null;
 		String src = "$t0";
 		String dest = "$t1";
 		
 		if (oprand1 instanceof Register) {
			if (((Register) oprand1).getMemloc().isGlobal()) {
				reg = getGlobal((Register) oprand1);
			} else {
				reg = getReg((Register) oprand1);
			}
			if (!op.getOpName().equals("&")) {
				int x = ((Register)oprand1).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)oprand1).memloc.isArgs()) {
					src = getAlloc(funcEntry.allo.get(x));							
				} else {
					Asm.movMtoR(reg, "$t0");
				}
			}
		} else if (oprand1 instanceof Mem) {
			Mem memi = (Mem)oprand1;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t1");
				Asm.movMemRtoR("$t1", "$t0");
			} else {
				reg = getMem((Mem) oprand1);
				Asm.movMtoR(reg, "$t1");
				Asm.movMemRtoR("$t1", "$t0");
			}
		} else if (oprand1 instanceof Const) {
			reg = getConst((Const)oprand1);
			Asm.movCtoR(getConst((Const)oprand1), "$t0");
		}
 		
 		if (result instanceof Register) {
			if (!((Register) result).getMemloc().isGlobal()) {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					dest = getAlloc(funcEntry.allo.get(x));
				}
			}
		} 
 		
 		switch(op.getOpName()) {
 			case "&":
 				Asm.movAtoR(reg, dest);
 				break;
 			case "!":
 				Asm.not(src, dest);
 				break;
 			case "-":
 				Asm.fu(src, dest);
 				break;
 			case "~":
 				Asm.rer(src, dest);
 			default:
 				break;
 		}
 		
 		if (result instanceof Register) {
			if (((Register) result).getMemloc().isGlobal()) {
				reg = getGlobal((Register)result);
				Asm.movRtoM(dest, reg);
			} else {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					return;
				} else {
					reg = getReg((Register)result);
					Asm.movRtoM(dest, reg);
				}
			}
		} else if (result instanceof Mem) {
			Mem memi = (Mem)result;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movRtoMemR(dest, getAlloc(x));
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
				Asm.movRtoMemR(dest, "$t0");
			} else {
				reg = getMem((Mem) result);
				Asm.movMtoR(reg, "$t0");
				Asm.movRtoMemR(dest, "$t0");
			}
		}
 	}
 	
 	static public void rer(String a, String b) {
 		System.out.println("	nor	" + b + ",	" + a + ",	$0");
 	}
 	
 	static public void fu(String a, String b) {
 		System.out.println("	subu	" + b + ",	$0,	" + a);
 	}
 	
 	static public void memcpy(Func func, FunctionEntry funcEntry) {
 		String reg = null;
 		Oprand arg1 = func.argument.get(0);
 		if (arg1 instanceof Register) {
			if (((Register) arg1).getMemloc().isGlobal()) {
				reg = getGlobal((Register)arg1);
				Asm.movMtoR(reg, "$a1");
			} else {
				int x = ((Register)arg1).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)arg1).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a1");						
				} else {
					reg = getReg((Register)arg1);
					Asm.movMtoR(reg, "$a1");
				}
			}
		} else if (arg1 instanceof Mem) {
			reg = getMem((Mem) arg1);
			Asm.movMtoR(reg, "$t0");
			Asm.movMemRtoR("$t0", "$a1");
		}
 		Oprand arg2 = func.argument.get(1);
 		if (arg2 instanceof Register) {
			if (((Register) arg2).getMemloc().isGlobal()) {
				reg = getGlobal((Register)arg2);
				Asm.movMtoR(reg, "$a2");
			} else {
				int x = ((Register)arg2).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)arg2).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a2");						
				} else {
					reg = getReg((Register)arg2);
					Asm.movMtoR(reg, "$a2");
				}
			}
		} else if (arg2 instanceof Mem) {
			reg = getMem((Mem) arg2);
			Asm.movMtoR(reg, "$t0");
			Asm.movMemRtoR("$t0", "$a2");
		}
 		Oprand arg3 = func.argument.get(2);
 		Asm.movCtoR(getConst((Const)arg3), "$a0");
 		
 		System.out.println("	jal	memcpy");
 	}
 	
	static public void operation2(Operation op, Oprand oprand1, Oprand oprand2, Oprand result, FunctionEntry funcEntry) {
		// oprand1 to $t0 oprand2 to $t1 result to $t2
		String reg = null;
		String dest = "$t2", src1 = "$t0", src2 = "$t1";
		boolean muli = false;
		boolean addi = false;
		
		if (oprand1 instanceof Register) {
			if (((Register) oprand1).getMemloc().isGlobal()) {
				reg = getGlobal((Register) oprand1);
				Asm.movMtoR(reg, "$t0");
			} else {
				int x = ((Register)oprand1).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)oprand1).memloc.isArgs()) {
					src1 = getAlloc(funcEntry.allo.get(x));							
				} else {
					reg = getReg((Register) oprand1);
					Asm.movMtoR(reg, "$t0");
				}
			}
		} else if (oprand1 instanceof Mem) {
			Mem memi = (Mem)oprand1;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t1");
				Asm.movMemRtoR("$t1", "$t0");
			} else {
				reg = getMem((Mem) oprand1);
				Asm.movMtoR(reg, "$t1");
				Asm.movMemRtoR("$t1", "$t0");
			}
		} else if (oprand1 instanceof Const) {
			Asm.movCtoR(getConst((Const)oprand1), "$t0");
		}
		
		if (oprand2 instanceof Register) {
			if (((Register) oprand2).getMemloc().isGlobal()) {
				reg = getGlobal((Register) oprand2);
				Asm.movMtoR(reg, "$t1");
			} else {
				int x = ((Register)oprand2).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)oprand2).memloc.isArgs()) {
					src2 = getAlloc(funcEntry.allo.get(x));							
				} else {
					reg = getReg((Register) oprand2);
					Asm.movMtoR(reg, "$t1");
				}
			}
		} else if (oprand2 instanceof Mem) {
			Mem memi = (Mem)oprand2;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t1");
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t2");
				Asm.movMemRtoR("$t2", "$t1");
			} else {
				reg = getMem((Mem) oprand2);
				Asm.movMtoR(reg, "$t2");
				Asm.movMemRtoR("$t2", "$t1");
			}
		} else if (oprand2 instanceof Const) {
			int x = ((Const)oprand2).value;
			if (Integer.highestOneBit(x) == x && op.getOpName().equals("*")) {
				muli = true;
			} else if (op.getOpName().equals("+")) {
				addi = true;
			} else {
				Asm.movCtoR(getConst((Const)oprand2), "$t1");
			}
		}
		
		if (result instanceof Register) {
			if (!((Register) result).getMemloc().isGlobal()) {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					dest = getAlloc(funcEntry.allo.get(x));		
				}
			}
		}
		
		switch (op.getOpName()) {
			case "+":
				if (!addi) {
					addu(src1, src2, dest);
				} else {
					int x = ((Const)oprand2).value;
					addiu(dest, src1, Integer.toString(x));
				}
				break;
			case "-":
				subu(src1, src2, dest);
				break;
			case "*":
				if (!muli) {
					mul(src1, src2, dest);
				} else {
					int x = ((Const)oprand2).value;
					int off = Integer.numberOfTrailingZeros(x);
					muli(src1, Integer.toString(off), dest);
				}
					
				break;
			case "/":
				divu(src1, src2, dest);
				break;
			case "%":
				mod(src1, src2, dest);
				break;
			case "&":
				and(src1, src2, dest);
				break;
			case ">>":
				shr(src1, src2, dest);
				break;
			case "<<":
				shl(src1, src2, dest);
				break;
			case "<":
				slt(src1, src2, dest);
				break;
			case "==":
				eq(src1, src2, dest);
				break;
			case "!=":
				neq(src1, src2, dest);
				break;
			case "^":
				xor(src1, src2, dest);
				break;
			case "|":
				or(src1, src2, dest);
				break;
			default:
				break;
		}
		
		if (result instanceof Register) {
			if (((Register) result).getMemloc().isGlobal()) {
				reg = getGlobal((Register)result);
				Asm.movRtoM(dest, reg);
			} else {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					return;
				} else {
					reg = getReg((Register)result);
					Asm.movRtoM(dest, reg);
				}
			}
		} else if (result instanceof Mem) {
			Mem memi = (Mem)result;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movRtoMemR(dest, getAlloc(x));
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
				Asm.movRtoMemR(dest, "$t0");
			} else {
				reg = getMem((Mem) result);
				Asm.movMtoR(reg, "$t0");
				Asm.movRtoMemR(dest, "$t0");
			}
		}
	}
	
	static public void muli(String a, String b, String c) {
		System.out.println("	sll	" + c + ",	" + a + ",	" + b);
	}
	
	static public void not(String a, String b) {
		eq(a, "$0", b);
	}
	
	static public void or(String a, String b, String c) {
		System.out.println("	or	" + c + ",	" + a + ",	" + b);
	}
	
	static public void xor(String a, String b, String c) {
		System.out.println("	xor	" + c + ",	" + a + ",	" + b);
	}
	
	static public void neq(String a, String b, String c) {
		System.out.println("	xor $s0,	" + a + ",	" + b);
		System.out.println("	sltu	" + c + ",	$0,	$s0");
	}

	static public void eq(String a, String b, String c) {
		System.out.println("	xor	$s0,	" + a + ",	" + b);
		System.out.println("	sltiu	" + c + ",	$s0,	1");
	}
	
	static public void slt(String a, String b, String c) {
		System.out.println("	slt	" + c + ",	" + a + ",	" + b);
	}
	
	static public void shl(String a, String b, String c) {
		System.out.println("	sllv	" + c + ",	" + a + ",	" + b);
	}
	
	static public void shr(String a, String b, String c) {
		System.out.println("	srav	" + c + ",	" + a + ",	" + b);
	}
	
	static public void and(String a, String b, String c) {
		System.out.println("	and	" + c + ",	" + a + ",	" + b);
	}
	
	static public void mod(String a, String b, String c) {
		System.out.println("	divu	" + a + ",	" + b);
		System.out.println("	mfhi	" + c);
	}
	
	static public void divu(String a, String b, String c) {
		System.out.println("	divu	" + a + ",	" + b);
		System.out.println("	mflo	" + c);
	}
	
	static public void mul(String a, String b, String c) {
		System.out.println("	mul	" + c + ",	" + a + ",	" + b);
	}
	
	static public void subu(String a, String b, String c) {
		System.out.println("	subu	" + c + ",	" + a + ",	" + b);
	}
	
	static public void addu(String a, String b, String c) {
		System.out.println("	addu	" + c + ",	" + a + ",	" + b);
	}
	
	static public void ret(Oprand result, FunctionEntry funcEntry) {
		String reg = null;
		if (result != null) {
			
			if (result instanceof Mem) {
				Oprand op = ((Mem) result).oprand;
				if (op instanceof Register && ((Register) op).getMemloc().getType() instanceof Pointer) {
					Type type = ((Register) op).getMemloc().getType();
					if (((Pointer) type).getelementType() instanceof Array) {
						Type xtype = ((Pointer) type).getelementType();
						if (((Array)xtype).getelementType() instanceof Struct) {
							int x = ((Register)op).memloc.getNum();
							if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
								Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$v0");
							} else {
								Asm.movMtoR(getReg((Register)op), "$v0");
							}
							return;
						}
					}
				}
			}
			
			if (result instanceof Register) {
				if (((Register) result).getMemloc().isGlobal()) {
					reg = getGlobal((Register) result);
					Asm.movMtoR(reg, "$v0");
				} else {
					int x = ((Register)result).memloc.getNum();
					if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
						Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$v0");
					} else {
						reg = getReg((Register) result);
						Asm.movMtoR(reg, "$v0");
					}
				}
			} else if (result instanceof Mem) {
				Mem memi = (Mem)result;
				if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
					//String s = getReg((Register)memi.oprand);
					int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
					Asm.movMemRtoR(getAlloc(x), "$v0");
				} else if (memi.oprand instanceof Mem 
				&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
					int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
					Asm.movMemRtoR(getAlloc(x), "$t0");
					Asm.movMemRtoR("$t0", "$v0");
				} else {
					reg = getMem((Mem) result);
					Asm.movMtoR(reg, "$t0");
					Asm.movMemRtoR("$t0", "$v0");
				}
			} else if (result instanceof Const) {
				Asm.movCtoR(getConst((Const)result), "$v0");
			}
		} 
		Asm.jmp(funcEntry.fbegin.oprand1.getIR() + "exit");
	}
	
	static public void assign(Oprand oprand1, Oprand result, FunctionEntry funcEntry) {
		String reg = null;
		
		if (oprand1 instanceof Mem && result instanceof Register) {
			if (((Register)result).getMemloc().getType() instanceof Struct) {
				Oprand op = ((Mem) oprand1).oprand;
				reg = getReg((Register)result);
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a1");
				} else {
					Asm.movMtoR(reg, "$a1");
				}
				reg = getReg((Register)op);
				x = ((Register)op).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a2");
				} else {
					Asm.movMtoR(reg, "$a2");
				}
				int size = ((Register)result).getMemloc().getType().getSize();
				Asm.movCtoR(getConst(new Const(size)), "$a0");
				
				System.out.println("	jal	memcpy");
				return;
			}
		}
		
		if (oprand1 instanceof Mem && result instanceof Mem) {
			Oprand op = ((Mem) oprand1).oprand;
			if (op instanceof Register && ((Register)op).getMemloc().getType() instanceof Pointer) {
				if (((Pointer)((Register)op).getMemloc().getType()).getelementType() instanceof Struct) {
					Oprand op1 = ((Mem) result).oprand;
					reg = getReg((Register) op);
					int x = ((Register)op).memloc.getNum();
					if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
						Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a2");
					} else {
						Asm.movMtoR(reg, "$a2");
					}
					reg = getReg((Register) op1);
					x = ((Register)op1).memloc.getNum();
					if (funcEntry.allo.containsKey(x) && !((Register)op1).memloc.isArgs()) {
						Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a1");
					} else {
						Asm.movMtoR(reg, "$a1");
					}
					int size = ((Pointer)((Register)op).getMemloc().getType()).getelementType().getSize();
					Asm.movCtoR(getConst(new Const(size)), "$a0");
					
					System.out.println("	jal	memcpy");
					return;
				}
			}
		}
		
		if (oprand1 instanceof Register && result instanceof Mem) {
			if (((Register)oprand1).getMemloc().getType() instanceof Struct) {
				Oprand op = ((Mem) result).oprand;
				reg = getReg((Register) oprand1);
				int x = ((Register)oprand1).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)oprand1).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a2");
				} else {
					Asm.movMtoR(reg, "$a2");
				}
				x = ((Register)op).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)op).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a1");
				} else {
					reg = getReg((Register)op);
					Asm.movMtoR(reg, "$a1");
				}
				int size = ((Register)oprand1).getMemloc().getType().getSize();
				Asm.movCtoR(getConst(new Const(size)), "$a0");
				
				System.out.println("	jal	memcpy");
				return;
			}
		}
		
		if (oprand1 instanceof Register && result instanceof Register) {
			if (((Register)result).getMemloc().getType() instanceof Struct) {
				reg = getReg((Register) oprand1);
				int x = ((Register)oprand1).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)oprand1).memloc.isArgs()) {
					Asm.movRtoR(getAlloc(funcEntry.allo.get(x)), "$a2");
				} else {
					Asm.movMtoR(reg, "$a2");
				}
				reg = getReg((Register) result);
				Asm.movAtoR(reg, "$a1");
				int size = ((Register)result).getMemloc().getType().getSize();
				Asm.movCtoR(getConst(new Const(size)), "$a0");
				
				System.out.println("	jal	memcpy");
				return;
			}
		}
				
		String src = "$t0", dest = "$t1";
		boolean assigni = false;
		
		if (oprand1 instanceof Register) {
			MemLoc memloc = ((Register) oprand1).getMemloc();
			if (memloc.getType() instanceof Array) {
				if (((Register) oprand1).getMemloc().isGlobal()) {
					reg = getGlobal((Register) oprand1);
					Asm.movAtoR(reg, "$t0");
				} else {
					reg = getReg((Register) oprand1);
					if (memloc.isArgs()) {
						Asm.movMtoR(reg, "$t0");
					} else {
						Asm.movAtoR(reg, "$t0");
					}
				}
			} else {
				if (((Register) oprand1).getMemloc().isGlobal()) {
					reg = getGlobal((Register) oprand1);
					Asm.movMtoR(reg, "$t0");
				} else {
					int x = ((Register)oprand1).memloc.getNum();
					if (funcEntry.allo.containsKey(x) && !((Register)oprand1).memloc.isArgs()) {
						src = getAlloc(funcEntry.allo.get(x));
					} else {
						reg = getReg((Register) oprand1);
						Asm.movMtoR(reg, "$t0");
					}
				}
			}
		} else if (oprand1 instanceof Mem) {
			Mem memi = (Mem)oprand1;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t2");
				Asm.movMemRtoR("$t2", "$t0");
			} else {
				reg = getMem((Mem) oprand1);
				Asm.movMtoR(reg, "$t1");
				Asm.movMemRtoR("$t1", "$t0");
			}
		} else if (oprand1 instanceof Const) {
			//Asm.movCtoR(getConst((Const) oprand1), "$t0");
			assigni = true;
		}
		
		if (result instanceof Register) {
			if (((Register) result).getMemloc().isGlobal()) {
				reg = getGlobal((Register)result);
				if (assigni) {
					Asm.movCtoR(getConst((Const) oprand1), "$t0");
				} 
				Asm.movRtoM(src, reg);
			} else {
				int x = ((Register)result).memloc.getNum();
				if (funcEntry.allo.containsKey(x) && !((Register)result).memloc.isArgs()) {
					dest = getAlloc(funcEntry.allo.get(x));
					if (!assigni) {
						Asm.movRtoR(src, dest);
					} else {
						Asm.addiu(dest, "$0", Integer.toString(((Const)oprand1).value));
					}
				} else {
					reg = getReg((Register)result);
					if (assigni) {
						Asm.movCtoR(getConst((Const) oprand1), "$t0");
					}
					Asm.movRtoM(src, reg);
				}
			}
		} else if (result instanceof Mem) {
			Mem memi = (Mem)result;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				if (assigni) {
					Asm.movCtoR(getConst((Const) oprand1), "$t0");
				}
				Asm.movRtoMemR(src, getAlloc(x));
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				if (assigni) {
					Asm.movCtoR(getConst((Const) oprand1), "$t0");
				}
				Asm.movMemRtoR(getAlloc(x), "$t1");
				Asm.movRtoMemR(src, "$t1");
			} else {
				if (assigni) {
					Asm.movCtoR(getConst((Const) oprand1), "$t0");
				}
				reg = getMem((Mem) result);
				Asm.movMtoR(reg, "$t1");
				Asm.movRtoMemR(src, "$t1");
			}
		}
	}
	
	static public void jz(Oprand oprand, String s, FunctionEntry funcEntry) {
		if (oprand instanceof Register) {
			int x = ((Register)oprand).memloc.getNum();
			if (funcEntry.allo.containsKey(x) && !((Register)oprand).memloc.isArgs()) {
				System.out.println("	beq	" + getAlloc(funcEntry.allo.get(x)) + ",	$0,	" + s);
			} else{
				String ret = "	lw	$t0,	" + getReg((Register)oprand) + "\n";
				ret += "	beq	$t0,	$0,	" + s;
				System.out.println(ret);
			}
		} else {
			Mem memi = (Mem)oprand;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
				System.out.println("	beq	$t0,	$0,	" + s);
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t2");
				Asm.movMemRtoR("$t2", "$t0");
				System.out.println("	beq	$t0,	$0,	" + s);
			} else {
				String ret = "	lw	$t1,	" + getMem((Mem)oprand) + "\n";
				ret += "	lw	$t0,	0($t1)\n";
				ret += "	beq	$t0,	$0,	" + s;
				System.out.println(ret);
			}
		}
	}
	
	static public void jnz(Oprand oprand, String s, FunctionEntry funcEntry) {
		if (oprand instanceof Register) {
			int x = ((Register)oprand).memloc.getNum();
			if (funcEntry.allo.containsKey(x) && !((Register)oprand).memloc.isArgs()) {
				System.out.println("	bne	" + getAlloc(funcEntry.allo.get(x)) + ",	$0,	" + s);
			} else {
				String ret = "	lw	$t0,	" + getReg((Register)oprand) + "\n";
				ret += "	bne	$t0,	$0,	" + s;
				System.out.println(ret);
			}
		} else {
			Mem memi = (Mem)oprand;
			if (memi.oprand instanceof Register && funcEntry.allo.containsKey(((Register)memi.oprand).memloc.getNum()) && !((Register)memi.oprand).memloc.isArgs()) {
				//String s = getReg((Register)memi.oprand);
				int x = funcEntry.allo.get(((Register)memi.oprand).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t0");
				System.out.println("	bne	$t0,	$0,	" + s);
			} else if (memi.oprand instanceof Mem 
			&& funcEntry.allo.containsKey(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum()) && !((Register)((Register)((Mem)memi.oprand).oprand)).memloc.isArgs()) {
				int x = funcEntry.allo.get(((Register)((Register)((Mem)memi.oprand).oprand)).memloc.getNum());
				Asm.movMemRtoR(getAlloc(x), "$t2");
				Asm.movMemRtoR("$t2", "$t0");
				System.out.println("	bne	$t0,	$0,	" + s);
			} else {
				String ret = "	lw	$t1,	" + getMem((Mem)oprand) + "\n";
				ret += "	lw	$t0,	0($t1)\n";
				ret += "	bne	$t0,	$0,	" + s;
				System.out.println(ret);
			}
		}
	}
	
}
