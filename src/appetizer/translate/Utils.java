package appetizer.translate;

public class Utils {
	public static int getAlignOffset(int i) {
		int tmp = i;
		tmp += 3;
		tmp >>= 2;
		tmp <<= 2;
		return tmp;
	}
	
	public static String regs[] = {
		"$v1", //0
		"$a3", //1
		"$t3", //2
		"$t4", //3
		"$t5", //4
		"$t6", //5
		"$t7", //6
		"$t8", //7
		"$t9", //8
		"$s1", //9
		"$s2", //10
		"$s3", //11
	};
}
