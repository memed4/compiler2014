package appetizer.translate;

import java.util.Stack;

import appetizer.codegen.AssemblyWriter;
import appetizer.interform.*;
import appetizer.environ.*;

public class Translator {

	private Program program;
	private AssemblyWriter asm;
	//private Table vscope;
	private int offset;
	private Stack<Integer> offsets;
	//private Label exit;

	public Translator(Program program) {
		this.program = program;
		offset = 0;
		offsets = new Stack<Integer>();
		//exit = new Label();
		translate(this.program);
	}

	private void translate(Program program) {
		initsdata();
        dataSection();
		System.out.println();
		System.out.println("	.text");
		mainEntry();
		for (int i = 0; i < this.program.funcList.size(); i++) {
			FunctionEntry curEntry = this.program.funcList.get(i);
			
			curEntry.scan();
			
			curEntry.setFuncLabel(program.funcLabel);
			
			allocateArg(curEntry);
			int stacksize = curEntry.getInnerArgSpace();
			stacksize += allocateReg(curEntry) + 8;
			curEntry.stacksize = stacksize;
			
			curEntry.fbegin.printASM(curEntry);
			
			enterCall(curEntry);
			
			callbody(curEntry);
			
			ExitCall(curEntry);
			
			System.out.println();	
		}
		
		addPrintf();
		addMalloc();
		addMemcpy();
	}

    private void initsdata() {
        System.out.println("    .data 0x10000000");
        System.out.println("    .space 0xFFFFF");
    }

	private void ExitCall(FunctionEntry curEntry) {
		System.out.println(curEntry.fbegin.oprand1.getIR() + "exit:");
		Asm.movMtoR("4($fp)", "$ra");
		Asm.addiu("$sp", "$fp", Integer.toString(8));
		Asm.movMtoR("0($fp)", "$fp");
		System.out.println("	jr	$ra");
		System.out.println(curEntry.fend.oprand1.getIR() + ":");
	}
	
	private void callbody(FunctionEntry curEntry) {
		curEntry.printASM();
	}
	
	private int allocateArg(FunctionEntry curEntry) {
		int x = 8;
		for (int i = 0; i < curEntry.argPtr.size(); ++i) {
			MemLoc memloc = curEntry.argPtr.get(i);
			memloc.offset = x;
			//x += getAlignOffset(memloc.getType().getSize());
			if (memloc.getType() instanceof Struct) {
				x += memloc.getType().getSize();
			} else {
				x += 4;
			}
		}
		return curEntry.argPtr.size() * 4;
	}
	
	private void enterCall(FunctionEntry funcEntry) {
		Asm.movRtoM("$ra", "-4($sp)");
		Asm.movRtoM("$fp", "-8($sp)");
		Asm.addiu("$fp", "$sp", "-8");
		if (funcEntry.stacksize > 32767) {
			Asm.movCtoR(Integer.toString(funcEntry.stacksize), "$t0");
			Asm.addu("$sp", "$sp", "$t0");
		} else {
			Asm.addiu("$sp", "$sp", "-" + Integer.toString(funcEntry.stacksize));
		}
	}
	
	private int allocateReg(FunctionEntry funcEntry) {
		int size = 0;
		for (int i = funcEntry.maxreg; i >= funcEntry.minreg; --i) {
			Register reg = funcEntry.regMap.get(i);
			if (reg == null) {
				continue;
			}
			size += Utils.getAlignOffset(reg.getMemloc().getType().getSize());
			//System.out.println("Size of $t" + Integer.toString(i) + ":	" + reg.getMemloc().getType().getSize());
			reg.getMemloc().offset = size;
		}
		return size;
	}
	
	private void mainEntry() {
		String lmain = null;
		for (int i = 0; i < this.program.funcList.size(); ++i) {
			if (this.program.funcList.get(i).getName().equals("main")) {
				lmain = this.program.funcList.get(i).getfbegin().oprand1.getIR();
			}
		}
		System.out.println("main:");
		System.out.println("	jal " + lmain);
		System.out.println("	or	$a0,	$v0,	$0");
		System.out.println("	or	$v0,	$0,	17");
		System.out.println("	syscall");
		System.out.println();
	}
	
	private void addMalloc() {
		System.out.println("malloc:");
		System.out.println("	lw	$v0,	static_end");
		System.out.println("	addu	$a0,	$v0,	$a0");
		System.out.println("	addiu	$a0,	$a0,	3");
		System.out.println("	srl	$a0,	$a0,	2");
		System.out.println("	sll	$a0,	$a0,	2");
		System.out.println("	sw	$a0,	static_end");
		System.out.println("	jr	$ra");
		System.out.println();
	}
	
	private void addMemcpy() {
		System.out.println("memcpy:");
		System.out.println("	lw	$t8,	0($a2)");
		System.out.println("	addiu	$a2,	$a2,	4");
		System.out.println("	sw	$t8,	0($a1)");
		System.out.println("	addiu	$a1,	$a1,	4");
		System.out.println("	addiu	$a0,	$a0,	-4");
		System.out.println("	bne	$a0,	$0,	memcpy");
		System.out.println("	jr	$ra");
	}
	
	private void addPrintf() {
		
		System.out.println("printf:");
		System.out.println("stprintf:");
		System.out.println("	or	$t0,	$0,	$a0");
		System.out.println("	addiu   $t1, $sp, 4");
		System.out.println("loop_pr:");
		System.out.println("	lb      $a0, 0($t0)");
		System.out.println("	addiu   $t0, $t0, 1");
		System.out.println("	beq     $a0, $0, endprint");
		System.out.println("	xori    $t2, $a0, '%'");
		System.out.println("	beqz    $t2, format_pr");
		System.out.println("plain_pr:");
		System.out.println("	li      $v0, 11");
		System.out.println("	syscall");
		System.out.println("	j	loop_pr");
		System.out.println("format_pr:");
		System.out.println("	lb $a0,	0($t0)");
		System.out.println("	addiu   $t0, $t0, 1");
		System.out.println("	xori    $t2, $a0, 'd'");
		System.out.println("	beq     $t2, $0, int_pr");
		System.out.println("	xori    $t2, $a0, 'c'");
		System.out.println("	beq     $t2, $0, char_pr");
		System.out.println("	xori    $t2, $a0, 's'");
		System.out.println("	beq     $t2, $0, string_pr");
		System.out.println("	xori    $t2, $a0, '0'");
		System.out.println("	beq     $t2, $0, prec_pr");
		System.out.println("int_pr:");
		System.out.println("	lw      $a0, 0($t1)");
		System.out.println("	addiu   $t1, $t1, 4");
		System.out.println("	ori     $v0, $0, 1");
		System.out.println("	syscall");
		System.out.println("	j	loop_pr");
		System.out.println("char_pr:");
		System.out.println("	lb      $a0, 0($t1)");
		System.out.println("	addiu   $t1, $t1, 4");
		System.out.println("	ori     $v0, $0, 11");
		System.out.println("	syscall");
		System.out.println("	j	loop_pr");
		System.out.println("string_pr:");
		System.out.println("	lw      $a0, 0($t1)");
		System.out.println("	addiu   $t1, $t1, 4");
		System.out.println("	ori     $v0, $0, 4");
		System.out.println("	syscall");
		System.out.println("	j	loop_pr");
		System.out.println("prec_pr:");
		System.out.println("	addiu	$t0, $t0, 2");
		System.out.println("	lw		$t2, 0($t1)");
		System.out.println("	addiu	$t1, $t1, 4");
		System.out.println("	or		$a0, $0, $0");
		System.out.println("	ori		$v0, $0, 1");
		System.out.println("	slti	$t3, $t2, 1000");
		System.out.println("	beqz	$t3, non_zero");
		System.out.println("	slti	$t3, $t2, 100");
		System.out.println("	beqz	$t3, one_zero");
		System.out.println("	slti	$t3, $t2, 10");
		System.out.println("	beqz	$t3, two_zero");
		System.out.println("three_zero:");
		System.out.println("	syscall");
		System.out.println("two_zero:");
		System.out.println("	syscall");
		System.out.println("one_zero:");
		System.out.println("	syscall");
		System.out.println("non_zero:");
		System.out.println("	or	$a0, $t2, $0");
		System.out.println("	syscall");
		System.out.println("	j	loop_pr");
		System.out.println("endprint:");
		System.out.println("	jr	$ra");	
		System.out.println();
	}
	
	private void dataSection() {
		Quad quad;
		
		System.out.println("	.sdata 0x10000000");
		System.out.println("	.align 2");
		
		for (int i = 0; i < this.program.init.size(); ++i) {
			quad = this.program.init.get(i);
			if (quad.oprand1 instanceof ConstString) {
				System.out.println(quad.result.getIR() + ":");
				System.out.println("	.asciiz " + quad.oprand1.getIR());
				System.out.println("	.align 2");
			}
		}
		System.out.println("	.data");
		System.out.println("	.align 2");
		for (int i = 0; i < this.program.gPtr.size(); ++i) {
			MemLoc memloc = this.program.gPtr.get(i);
			System.out.println("g" + Integer.toString(memloc.getNum()) + ":");
				for (int j = 0; j < this.program.init.size(); j++) {
					quad = this.program.init.get(j);
					if (((Register)quad.result).getMemloc().equals(memloc)) {
						if (memloc.getType() instanceof Char) {
							System.out.println("	.byte " + quad.oprand1.getIR());
						} else {
							System.out.println("	.word " + quad.oprand1.getIR());
						}
						break;
					}
				}
				System.out.println("	.space " + Integer.toString(memloc.getType().getSize()));
				System.out.println("	.align 2");
		}
		
		System.out.println("static_end:");
		System.out.println("	.word	static_end + 4");
	}
	
	
	/*
	private void translate(Block block) {
		offsets.push(offset);
		vscope.beginScope();
		translate(block.decls);
		translate(block.stmts);
		vscope.endScope();
		offset = offsets.pop();
	}

	private void translate(DeclList decls) {
		if (decls == null) {
			return;
		}
		translate(decls.head);
		translate(decls.tail);
	}
	
	private void translate(Decl decl) {
		offset -= 4;
		vscope.put(decl.sym, new VarInfo(offset));
	}

	private void translate(StmtList stmts) {
		if (stmts == null) {
			return;
		}
		translate(stmts.head);
		translate(stmts.tail);
	}

	private void translate(Stmt stmt) {
		if (stmt instanceof Block) {
			translate((Block) stmt);
		} else if (stmt instanceof Expr) {
			translate((Expr) stmt);
		} else if (stmt instanceof IfStmt) {
			translate((IfStmt) stmt);
		} else if (stmt instanceof ReturnStmt) {
			translate((ReturnStmt) stmt);
		}
	}

	private void translate(Expr expr) {
		if (expr instanceof Num) {
			translate((Num) expr);
		} else if (expr instanceof Op) {
			translate((Op) expr);
		} else if (expr instanceof Var) {
			translate((Var) expr);
		}
	}

	private void transleft(Expr expr) {
		if (expr instanceof Num) {
			throw new Error("Number is not a lvalue.");
		} else if (expr instanceof Op) {
			if (((Op) expr).isLvalue()) {
				translate((Op) expr);
			}
			throw new Error(((Op) expr).opType + " is not a lvalue.");
		} else if (expr instanceof Var) {
			translate((Var) expr);
		}
	}

	private Temp newTemp() {
		offset -= 4;
		return new Temp(offset);
	}

	private void translate(Num num) {
		num.loc = newTemp();
		asm.store(num.loc, num.value);
	}

	private void translate(Op op) {
		if (op.isLvalue()) {
			transleft(op.left);
			translate(op.right);
			op.loc = op.left.loc;
		} else {
			translate(op.left);
			translate(op.right);
			op.loc = newTemp();
		}

		switch (op.opType) {
		case PLUS:
			asm.add(op.loc, op.left.loc, op.right.loc);
			break;
		case MINUS:
			asm.sub(op.loc, op.left.loc, op.right.loc);
			break;
		case TIMES:
			asm.mul(op.loc, op.left.loc, op.right.loc);
			break;
		case DIVIDE:
			asm.div(op.loc, op.left.loc, op.right.loc);
			break;
		case EQ:
			asm.seq(op.loc, op.left.loc, op.right.loc);
			break;
		case NE:
			asm.sne(op.loc, op.left.loc, op.right.loc);
			break;
		case LT:
			asm.slt(op.loc, op.left.loc, op.right.loc);
			break;
		case GT:
			asm.sgt(op.loc, op.left.loc, op.right.loc);
			break;
		case LE:
			asm.sle(op.loc, op.left.loc, op.right.loc);
			break;
		case GE:
			asm.sge(op.loc, op.left.loc, op.right.loc);
			break;
		case ASSIGN:
			asm.move(op.loc, op.right.loc);
			break;
		}
	}

	private void translate(Var var) {
		VarInfo varInfo = (VarInfo) vscope.get(var.sym);
		if (varInfo == null) {
			throw new Error("Undefined variable: " + var.sym);
		}
		var.loc = varInfo.loc;
	}

	private void translate(IfStmt ifStmt) {
		Label next = new Label();
		translate(ifStmt.cond);
		asm.jz(ifStmt.cond.loc, next);
		translate(ifStmt.body);
		asm.emit(next);
	}

	private void translate(ReturnStmt returnStmt) {
		translate(returnStmt.expr);
		asm.returnValue(returnStmt.expr.loc);
		asm.jump(exit);
	}*/
}
