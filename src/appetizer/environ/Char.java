package appetizer.environ;

final public class Char extends Type {

	static Char charInstance = new Char();
	
	public Char() {
		
	}
	
	public static Char getCharInstance() {
		return charInstance;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Char) || (obj instanceof Int) || (obj instanceof Pointer);
	}

	public int getSize() {
		return 4;
	}
}
