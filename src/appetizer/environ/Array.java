package appetizer.environ;

public final class Array extends Pointer {
	int capacity;
	
	public Array(Type elementType, int capacity) {
		super(elementType);
		this.capacity = capacity;
	}
	
	public int getCapa() {
		return this.capacity;
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof Array) && (super.equals(obj)) && (this.capacity == ((Array)obj).capacity));
	}
	
	public int getSize() {
		return this.capacity * elementType.getSize(); 
	}
}
