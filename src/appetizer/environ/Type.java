package appetizer.environ;

abstract public class Type {
	abstract public int getSize();
}
