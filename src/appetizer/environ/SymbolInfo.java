package appetizer.environ;

import appetizer.interform.*;

public class SymbolInfo {
	Type type;
	String name;
	MemLoc memloc;
	
	public SymbolInfo(Type type, String name, MemLoc memloc) {
		this.type = type;
		this.name = name;
		this.memloc = memloc;
	}
	
	public MemLoc getMemloc() {
		return this.memloc;
	}
	
	public Type getType() {
		return this.type;
	}
	
	public String getName() {
		return this.name;
	}
}
