package appetizer.environ;

import appetizer.interform.*;

public class Expression {
	boolean isLtype;
	boolean fromCall;
	int value;
	Type type;
	Oprand oprand;
	
	public Expression(boolean isLtype, int value, Type type, boolean fromCall, Oprand oprand) {
		this.isLtype = isLtype;
		this.value = value;
		this.type = type;
		this.fromCall = fromCall;
		this.oprand = oprand;
	}
	
	public Oprand getOprand() {
		return this.oprand;
	}
	
	public boolean getisLtype() {
		return this.isLtype;
	}
	
	public int getvalue() {
		return this.value;
	}
	
	public Type gettype() {
		return this.type;
	}
	
	public boolean getFronCall() {
		return this.fromCall;
	}
}
