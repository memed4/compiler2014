package appetizer.environ;

final public class Vvoid extends Type {

	private Vvoid() {
		
	}
	
	static Vvoid voidInstance = new Vvoid();
	
	public static Vvoid getVoidInstance() {
		return voidInstance;
	}
	
	@Override
	public boolean equals(Object obj) {
		return obj instanceof Vvoid;
	}
	
	public int getSize() {
		return 4;
	}
	
}
