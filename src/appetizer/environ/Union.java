package appetizer.environ;

import java.util.ArrayList;
import java.util.List;

public final class Union extends Record {
	
	String name;
	
	public String getName() {
		return this.name;
	}
	
	public Union(String s) {
		this.name = s;
		this.fields = new ArrayList<RecordField>();
	}
	
	public void addFields(Type type, String name) {
		this.fields.add(new RecordField(type, name));
	}
	
	public Type getRecordType(String name) {
		for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(name)) {
				return this.fields.get(i).type;
			}
		}
		return null;
	}
	
	public boolean DupName(String name) {
		for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) && this.name.equals(((Union)obj).getName());
	}
	
	public int getSize() {
		int t = 0;
		for (int i = 0; i < this.fields.size(); i++) {
			t += this.fields.get(i).type.getSize();
		}
		return t;
	}
	
	public int getOffset(String s) {
		int off = 0;
		/*for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(s))
				break;
			off += this.fields.get(i).type.getSize();
		}*/
		return off;
	}
	
}
