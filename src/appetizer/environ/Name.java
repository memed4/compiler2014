package appetizer.environ;

public final class Name extends Type {
	String name;
	
	public Name(String name) {
		this.name = name;
	}

	@Override
	public boolean equals(Object obj) {
		return this.name.equals(((Name)obj).name);
	}
	
	public int getSize() {
		return 4;
	}
}
