package appetizer.environ;

import java.util.ArrayList;
import java.util.List;

final public class Struct extends Record {
	
	String name;
	public Struct(String name) {
		this.fields = new ArrayList<RecordField>();
		this.name = name;
	}
	
	public void addFields(Type type, String name) {
		this.fields.add(new RecordField(type, name));
	}
	
	public Type getRecordType(String name) {
		for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(name)) {
				return this.fields.get(i).type;
			}
		}
		return null;
	}
	
	public boolean DupName(String name) {
		for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(name)) {
				return true;
			}
		}
		return false;
	}
	
	public String getName() {
		return this.name;
	}
	
	@Override
	public boolean equals(Object obj) {
		return super.equals(obj) && this.name.equals(((Struct)obj).getName());
	}

	public int getSize() {
		int t = 0;
		for (int i = 0; i < this.fields.size(); i++) {
			t += this.fields.get(i).type.getSize();
		}
		return t;
	}
	
	public int getOffset(String s) {
		int off = 0;
		for (int i = 0; i < this.fields.size(); i++) {
			if (this.fields.get(i).name.equals(s))
				break;
			off += this.fields.get(i).type.getSize();
		}
		return off;
	}
}
