package appetizer.environ;

import java.util.List;

abstract class Record extends Type {
	class RecordField {
		Type type;
		String name;
		
		public RecordField(Type type, String name) {
			this.type = type;
			this.name = name;
		}
	}
	
	public List<RecordField> fields;

	@Override
	public boolean equals(Object obj) {
		if (this.fields.size() != ((Record)obj).fields.size()) return false;
		for (int i = 0; i < this.fields.size(); i++) {
			if (!(this.fields.get(i).type.equals(((Record)obj).fields.get(i).type)) 
			   || !(this.fields.get(i).name.equals(((Record)obj).fields.get(i).name))) {
				return false;
			}
		}
		return true;
	}
}
