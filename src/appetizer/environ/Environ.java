package appetizer.environ;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Environ {
		
	public Environ upEnviron;
	public Type retType;
	public int loop;
	public boolean global;
	
	public Environ(Environ upEnviron, boolean global) {
		this.upEnviron = upEnviron;
		this.global = global;
		loop = 0;
	}
	
	public boolean getGlobal() {
		return this.global;
	}
	
	public void addSymbol(String name, SymbolInfo symInfo) {
		dict.put(name, symInfo);
	}
	
	public void addStructSymbol(String name, SymbolInfo symInfo) {
		structDict.put(name, symInfo);
	}
	
	public Type getStructType(String name) {
		Environ CurrentEnviron = this;
		while (CurrentEnviron != null) {
			SymbolInfo symInfo = CurrentEnviron.structDict.get(name);
			if (symInfo != null) return symInfo.type;
			CurrentEnviron = CurrentEnviron.upEnviron;
		}
		return null;
	}
	
	public boolean checkDupDecl(String name) {
		return (this.dict.get(name) != null) ? false : true;
	}
	
	public boolean checksDupDecl(String name) {
		return (this.structDict.get(name) != null) ? false : true;
	}
	
	public SymbolInfo inEnv(String name) {
		Environ CurrentEnviron = this;
		while (CurrentEnviron != null) {
			SymbolInfo symInfo = CurrentEnviron.dict.get(name);
			if (symInfo != null) return symInfo;
			symInfo = CurrentEnviron.structDict.get(name);
			if (symInfo != null) return symInfo;
			CurrentEnviron = CurrentEnviron.upEnviron;
		}
		return null;
	}
	
	public void addStructType(Struct s) {
		this.structTypeDict.add(s);
	}
	
	public void addUnionType(Union s) {
		this.unionTypeDict.add(s);
	}
	
	public Struct findStructType(String name) {
		Environ CurrentEnviron = this;
		while (CurrentEnviron != null) {
			for (int i = 0; i < CurrentEnviron.structTypeDict.size(); i++){
				if (CurrentEnviron.structTypeDict.get(i).getName().equals(name)) {
					return CurrentEnviron.structTypeDict.get(i);
				}
			}
			CurrentEnviron = CurrentEnviron.upEnviron;
		}
		return null;
	}
	
	public Union findUnionType(String name) {
		Environ CurrentEnviron = this;
		while (CurrentEnviron != null) {
			for (int i = 0; i < CurrentEnviron.unionTypeDict.size(); i++){
				if (CurrentEnviron.unionTypeDict.get(i).getName().equals(name)) {
					return CurrentEnviron.unionTypeDict.get(i);
				}
			}
			CurrentEnviron = CurrentEnviron.upEnviron;
		}
		return null;
	}
	
	public HashMap<String, SymbolInfo> dict = new HashMap<String, SymbolInfo>();
	public HashMap<String, SymbolInfo> structDict = new HashMap<String, SymbolInfo>();
	public List<Struct> structTypeDict = new ArrayList<Struct>();
	public List<Union> unionTypeDict = new ArrayList<Union>();
}
