package appetizer.environ;

final public class Int extends Type{
	
	private Int() {
		
	}
	
	static Int intInstance = new Int();
	
	public static Int getIntInstance() {
		return intInstance;
	}
	
	@Override
	public boolean equals(Object obj) {
		return (obj instanceof Int) || (obj instanceof Char) || (obj instanceof Pointer);
	}
	
	public int getSize() {
		return 4;
	}
}
