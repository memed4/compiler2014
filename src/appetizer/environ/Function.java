package appetizer.environ;

import java.util.List;

public final class Function extends Type {
	List<Type> argumentType;
	Type returnType;
	
	public Function(List<Type> argumentType, Type returnType) {
		this.argumentType = argumentType;
		this.returnType = returnType;
	}
	
	public List<Type> getArguType() {
		return this.argumentType;
	}
	
	public Type getRetType() {
		return this.returnType;
	}
	
	public int getSize() {
		return 0;
	}
}
