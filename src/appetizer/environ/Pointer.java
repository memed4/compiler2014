package appetizer.environ;

public class Pointer extends Type {
	Type elementType;

	public Pointer(Type elementType) {
		this.elementType = elementType;
	}
	
	public Type getelementType() {
		return this.elementType;
	}
	
	@Override
	public boolean equals(Object obj) {
		return ((obj instanceof Pointer)
			|| (obj instanceof Array)
			|| (obj instanceof Int)
			|| (obj instanceof Char));
	}
	
	public int getSize() {
		return 4;
	}
}
