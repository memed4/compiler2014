package appetizer.semantic;

import java.util.ArrayList;
import java.util.List;

import org.antlr.runtime.tree.*;

import appetizer.environ.*;
import appetizer.interform.*;

public class Checker {
	
	public Program program;
	
	public Checker() {
		this.program = new Program();
	}
	
	public Type GetBaseType(CommonTree t, Environ CurrentEnviron) {
		switch(t.getText()) {
			case "int": 
				return Int.getIntInstance();
			case "void":
				return Vvoid.getVoidInstance();
			case "char":
				return Char.getCharInstance();
			case "struct":
				String structName = t.getChild(0).getText();
				Struct sType;
				if (CurrentEnviron.findStructType(structName) != null) {
					return CurrentEnviron.findStructType(structName);
				} else {
					sType = new Struct(structName);
					for (int i = 1; i < t.getChildCount(); i++) {
						CommonTree pdeclNode = (CommonTree)t.getChild(i);
						Type tmpType;
						if (pdeclNode.getChild(0).getText().equals("struct") && pdeclNode.getChild(0).getChild(0).getText().equals(structName)) {
							tmpType = new Name(structName);
						} else tmpType = GetBaseType((CommonTree)pdeclNode.getChild(0), CurrentEnviron);
						Type finalType;
						for (int j = 1; j < pdeclNode.getChildCount(); j++) {
							if (pdeclNode.getChild(j).getChild(0).getText().equals("*")) {
								String tmps = pdeclNode.getChild(j).getChild(1).getText();
								finalType = new Pointer(tmpType);
								if (sType.DupName(tmps)) {
									System.out.println("[Notice]Rename member in struct");
									System.exit(1);
								} else sType.addFields(finalType, tmps);
							} else {
								if (sType.DupName(pdeclNode.getChild(j).getChild(0).getText())) {
									System.out.println("[Notice]Rename member in struct");
									System.exit(1);
								} else if (pdeclNode.getChild(j).getChild(1) != null && pdeclNode.getChild(j).getChild(1).getText().equals("INDEX")) {
									int capa = Integer.parseInt(pdeclNode.getChild(j).getChild(1).getChild(0).getText());
									if (pdeclNode.getChild(j).getChild(2) != null && pdeclNode.getChild(j).getChild(2).getText().equals("INDEX")) {
										int capa1 = Integer.parseInt(pdeclNode.getChild(j).getChild(1).getChild(0).getText());
										Type tmType = new Array(tmpType, capa1);
										tmpType = new Array(tmType, capa);
										sType.addFields(tmpType, pdeclNode.getChild(j).getChild(0).getText());
									} else {
										tmpType = new Array(tmpType, capa);
										sType.addFields(tmpType, pdeclNode.getChild(j).getChild(0).getText());
									}
								} else sType.addFields(tmpType, pdeclNode.getChild(j).getChild(0).getText());
							}
						}
					}
				}
				CurrentEnviron.addStructType(sType);
				return sType;
			case "union":
				String unionName = t.getChild(0).getText();
				Union uType;
				if (CurrentEnviron.findUnionType(unionName) != null) {
					return CurrentEnviron.findUnionType(unionName);
				} else {
					uType = new Union(unionName);
					for (int i = 1; i < t.getChildCount(); i++) {
						CommonTree pdeclNode = (CommonTree)t.getChild(i);
						Type tmpType;
						if (pdeclNode.getChild(0).getText().equals("union") && pdeclNode.getChild(0).getChild(0).getText().equals(unionName)) {
							tmpType = new Name(unionName);
						} else tmpType = GetBaseType((CommonTree)pdeclNode.getChild(0), CurrentEnviron);
						Type finalType;
						for (int j = 1; j < pdeclNode.getChildCount(); j++) {
							if (pdeclNode.getChild(j).getChild(0).getText().equals("*")) {
								String tmps = pdeclNode.getChild(j).getChild(1).getText();
								finalType = new Pointer(tmpType);
								if (uType.DupName(tmps)) {
									System.out.println("[Notice]Rename member in union");
									System.exit(1);
								} else uType.addFields(finalType, tmps);
							} else {
								if (uType.DupName(pdeclNode.getChild(j).getChild(0).getText())) {
									System.out.println("[Notice]Rename member in union");
									System.exit(1);
								} else uType.addFields(tmpType, pdeclNode.getChild(j).getChild(0).getText());
							}
						}
					}
				}
				CurrentEnviron.addUnionType(uType);
				return uType;
		}
			
		return null;
	}
	
	public void DoChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		boolean RetConsist = true;
		for (int i = 0; i < t.getChildCount(); i++) {
			CommonTree node = (CommonTree)t.getChild(i);
			switch(node.getText()) {
				case "IDECL": 
					DeclChecking(node, CurrentEnviron, funcEntry); 
					break; 
				case "EXPRESSION":
					ExpressionChecking((CommonTree)node.getChild(0), CurrentEnviron, funcEntry);
					if (node.getChildCount() > 1) {
						ExpressionChecking((CommonTree)node.getChild(1), CurrentEnviron, funcEntry);
					}
					if (node.getChildCount() > 2) {
						ExpressionChecking((CommonTree)node.getChild(2), CurrentEnviron, funcEntry);
					}
					if (node.getChildCount() > 3) {
						ExpressionChecking((CommonTree)node.getChild(3), CurrentEnviron, funcEntry);
					}
					break;
				case "IF":
					IfChecking(node, CurrentEnviron, funcEntry);
					break;
				//case "ELSE":
				//	ElseChecking(node, CurrentEnviron, funcEntry);
				//	break;
				case "BLOCK":
					BlockChecking(node, CurrentEnviron, funcEntry);
					break;
				case "FUNC":   
					FuncChecking(node, CurrentEnviron, funcEntry); 
					break;
				case "RETURN":
					if (!ReturnChecking(node, CurrentEnviron, funcEntry)) {
						System.out.println("[Notice]Func RetType InConsistZZY!");
						System.exit(1);
					}
					break;
				case "FORBLOCK":
					ForChecking(node, CurrentEnviron, funcEntry);
					break;
				case "BLANKSTATEMENT":
					break;
				case "WHILEBLOCK":
					WhileChecking(node, CurrentEnviron, funcEntry);
					break;
				case "break":
					if (CurrentEnviron.loop < 1) {
						System.out.println("[Notice]Break not in loop!");
						System.exit(1);
					}
					if (this.program.getCurrentLoop().getType() == 1) { // For
						String str = "j";
						Quad quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl3());
						funcEntry.addQuad(quad);
					} else { // while
						String str = "j";
						Quad quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl2());
						funcEntry.addQuad(quad);
					}
					break;
				case "continue":
					if (this.program.getCurrentLoop().getType() == 1) { // For
						String str = "j";
						Quad quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl2());
						funcEntry.addQuad(quad);
					} else {
						String str = "j";
						Quad quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl1());
						funcEntry.addQuad(quad);
					}
					break;
				default: break;
			}
		}
	}
	
	private boolean ReturnChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		if (t.getChildCount() > 0) { 
			Expression retExpression = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
			if (retExpression.gettype().equals(CurrentEnviron.retType)) {
				String str = "ret";
				Quad quad = new Quad(new Operation(str), null, null, retExpression.getOprand());
				funcEntry.addQuad(quad);
				return true;
			}
		} else {
			String str = "ret";
			Quad quad = new Quad(new Operation(str), null, null, null);
			funcEntry.addQuad(quad);
			return true;
		}
		return false;
	}
	
	private Expression ExpressionChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		Expression lExp, rExp;
		try {
			String tmp = t.getText();
			int value;
			
			if (tmp.length() > 2 && tmp.charAt(0) == '0' && (tmp.charAt(1) == 'x' || tmp.charAt(1) == 'X')) {
				value = Integer.parseInt(tmp.substring(2), 16);
			} else if (tmp.length() > 1 && tmp.charAt(0) == '0') {
				value = Integer.parseInt(tmp.substring(1), 8);
			} else {
				value = Integer.parseInt(tmp, 10);
			}
			Const con = new Const(value);
			return new Expression(false, value, Int.getIntInstance(), false, con);
		} catch (Exception e){
		}
		int len = t.getText().length();
		Const conc = null;
		if (t.getText().charAt(0) == '\'' && t.getText().charAt(len - 1) == '\'') {
			if (len == 3) {
				conc = new Const((int)(t.getText().charAt(1)));
			} else {
				if (t.getText().charAt(1) == '\\' && t.getText().charAt(2) == 'n') {
					conc = new Const(10);
				} else if (t.getText().substring(1, 5).equals("\\013")) {
					conc = new Const(11);
				} else if (t.getText().substring(1, 5).equals("\\002")) {
					conc = new Const(2);
				}
			}
			return new Expression(false, -1, Char.getCharInstance(), false, conc);
		}	
		if (t.getText().charAt(0) == '"' && t.getText().charAt(len - 1) == '"') {
			Quad quadtmp = this.program.CheckDupConstStr("\"" + t.getText().substring(1, len - 1) + "\"");
			if (quadtmp == null) {
				int num = this.program.getRegNumG() + 1;
				this.program.setRegNumG(num);
				MemLoc memloc = new MemLoc(num, new Array(Char.getCharInstance(), len - 1), false, true);
				Register reg = new Register(memloc);
				ConstString cs = new ConstString(t.getText().substring(1, len - 1));
				String s = "=";
				Quad quad = new Quad(new Operation(s), cs, null, reg);
				this.program.addinit(quad);
				return new Expression(false, -1, new Array(Char.getCharInstance(), len - 1), false, reg);
			} else {
				return new Expression(false, -1, new Array(Char.getCharInstance(), len - 1), false, quadtmp.result);
			}
		}		
		String str = null;
		Quad quad;
		Register reg = null;
		MemLoc memloc;
		Mem memi = null;
		int num;
		if (true) {
			switch(t.getText()) {
				//assign
				case "BLANK":
					return new Expression(false, -1, null, false, null);
				case "=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					
					if (lExp.getOprand() instanceof Register 
					&&  rExp.getOprand() instanceof Register
					&& ((Register)lExp.getOprand()).getMemloc().getType() instanceof Struct
					&& ((Register)rExp.getOprand()).getMemloc().getType() instanceof Struct) {
						Func func = new Func("memcpy");
						str = "&";
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, new Pointer(((Register)lExp.getOprand()).getMemloc().getType()), false, false);
						reg = new Register(memloc);
						quad = new Quad(new Operation(str), lExp.getOprand(), null, reg);
						funcEntry.addQuad(quad);
						
						str = "&";
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, new Pointer(((Register)rExp.getOprand()).getMemloc().getType()), false, false);
						Register reg1 = new Register(memloc);
						quad = new Quad(new Operation(str), rExp.getOprand(), null, reg1);
						funcEntry.addQuad(quad);
						
						Const memcon = new Const(((Register)lExp.getOprand()).getMemloc().getType().getSize());
						
						func.argument.add(reg);
						func.argument.add(reg1);
						func.argument.add(memcon);
						
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, Int.getIntInstance(), false, false);
						Register reg2 = new Register(memloc);
						
						str = "=";
						quad = new Quad(new Operation(str), func, null, reg2);
						funcEntry.addQuad(quad);
						
						return new Expression(true, -1, null, false, lExp.getOprand());
						
					}
					str = "=";
					quad = new Quad(new Operation(str), rExp.getOprand(), null, lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "*=": 
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "*";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "/=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "/";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "%=": 
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "%";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "+=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "+";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "-=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "-";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "<<=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "<<";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case ">>=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = ">>";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "&=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "&";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "^=": 
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "^";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "|=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "|";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), lExp.getOprand());
					funcEntry.addQuad(quad);
					if (!lExp.getisLtype()) {
						System.out.println("[Notice]Not Left value!");
						System.exit(1);
					}
					if (lExp.gettype() instanceof Array && rExp.gettype() instanceof Array) {
						System.out.println("[Notice]Arrays can not be assigned a value.");
						System.exit(1);
					}
					if (rExp.gettype() instanceof Struct && lExp.gettype() instanceof Char) {
							System.out.println("Struct Type not Consist!");
							System.exit(1);
					}
					return new Expression(true, -1, null, false, lExp.getOprand());
				case "||":					
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					
					str = "=";
					quad = new Quad(new Operation(str), new Const(1), null, reg);
					funcEntry.addQuad(quad);
					
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					
					Label l;
					num = this.program.getLabelNum() + 1;
					this.program.setLabelNum(num);
					l = new Label(num);
					
					str = "jnz";
					quad = new Quad(new Operation(str), lExp.getOprand(), null, l);
					funcEntry.addQuad(quad);
					
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					
					str = "jnz";
					quad = new Quad(new Operation(str), rExp.getOprand(), null, l);
					funcEntry.addQuad(quad);
					
					str = "=";
					quad = new Quad(new Operation(str), new Const(0), null, reg);
					funcEntry.addQuad(quad);
					
					quad = new Quad(null, l, null, null);
					funcEntry.addQuad(quad);
					
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "&&":
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					
					str = "=";
					quad = new Quad(new Operation(str), new Const(0), null, reg);
					funcEntry.addQuad(quad);
					
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					
					Label ll;
					num = this.program.getLabelNum() + 1;
					this.program.setLabelNum(num);
					ll = new Label(num);
					
					str = "jz";
					if (funcEntry.fLabel == null) {
						quad = new Quad(new Operation(str), lExp.getOprand(), null, ll);
						funcEntry.addQuad(quad);
					} else {
						quad = new Quad(new Operation(str), lExp.getOprand(), null, funcEntry.fLabel);
						funcEntry.addQuad(quad);
					}
					
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					
					str = "jz";
					if (funcEntry.fLabel == null) {
						quad = new Quad(new Operation(str), rExp.getOprand(), null, ll);
						funcEntry.addQuad(quad);
					} else {
						quad = new Quad(new Operation(str), rExp.getOprand(), null, funcEntry.fLabel);
						funcEntry.addQuad(quad);
					}
					
					str = "=";
					quad = new Quad(new Operation(str), new Const(1), null, reg);
					funcEntry.addQuad(quad);
					
					quad = new Quad(null, ll, null, null);
					funcEntry.addQuad(quad);
					
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "|":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "|";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "^":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "^";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "!=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "!=";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case ">":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "<";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), rExp.getOprand(), lExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "<":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = "<";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "<=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					
					str = "<";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), rExp.getOprand(), lExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					
					str = "==";
					quad = new Quad(new Operation(str), reg, new Const(0), reg);
					funcEntry.addQuad(quad);
					
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case ">=":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					
					str = "<";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					
					str = "==";
					quad = new Quad(new Operation(str), reg, new Const(0), reg);
					funcEntry.addQuad(quad);

					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "<<":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					if (lExp.getvalue() > 0 && rExp.getvalue() > 0) {
						Const conshift = new Const(lExp.getvalue()<<rExp.getvalue());
						return new Expression(false, lExp.getvalue()<<rExp.getvalue(), Int.getIntInstance(), false, conshift);
					}
					str = "<<";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case ">>":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					str = ">>";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "==":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					if (lExp.gettype() instanceof Struct && rExp.gettype() instanceof Struct && !lExp.gettype().equals(rExp.gettype()))
					{	
						System.out.println("[Notice]Compare different struct type");
						System.exit(1);
					}
					str = "==";
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, lExp.gettype(), false, reg);
				case "+":
					if (t.getChildCount() == 1) {
						rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
							num = this.program.getRegNum() + 1;
							this.program.setRegNum(num);
							memloc = new MemLoc(num, Int.getIntInstance(), false, false);
							reg = new Register(memloc);
							str = "+";
							quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
							funcEntry.addQuad(quad);
						return new Expression(false, rExp.getvalue(), rExp.gettype(), false, reg);
					} else if (t.getChildCount() == 2) {
						lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
						if (lExp.gettype() instanceof Struct || rExp.gettype() instanceof Struct) {
							System.out.println("[Notice]Struct cannot plus!");
							System.exit(1);
						}
						/*
						if (lExp.getvalue() > 0 && rExp.getvalue() > 0) {
							return new Expression(false, lExp.getvalue() + rExp.getvalue(), lExp.gettype(), false, new Const(lExp.getvalue() + rExp.getvalue()));
						}*/
						
						if (!CurrentEnviron.getGlobal()) {
							if (lExp.getOprand() instanceof Register
							 && ((Register)lExp.getOprand()).getMemloc().getType() instanceof Pointer
								) {
								Type tmpType = ((Register)lExp.getOprand()).getMemloc().getType();
								int tmp = ((Pointer)tmpType).getelementType().getSize();
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								memloc = new MemLoc(num, Int.getIntInstance(), false, false);
								Register reg1 = new Register(memloc);
								str = "*";
								quad = new Quad(new Operation(str), rExp.getOprand(), new Const(tmp), reg1);
								funcEntry.addQuad(quad);
								
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								memloc = new MemLoc(num, Int.getIntInstance(), false, false);
								reg = new Register(memloc);
								str = "+";
								quad = new Quad(new Operation(str), lExp.getOprand(), reg1, reg);
								funcEntry.addQuad(quad);
								
							} else {
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								memloc = new MemLoc(num, Int.getIntInstance(), false, false);
								reg = new Register(memloc);
								str = "+";
								quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
								funcEntry.addQuad(quad);
							}
						}
						return new Expression(false, lExp.getvalue() + rExp.getvalue(), lExp.gettype(), false, reg);
					}
				case "-":
					if (t.getChildCount() == 1) {
						rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, Int.getIntInstance(), false, false);
						reg = new Register(memloc);
						str = "-";
						quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
						funcEntry.addQuad(quad);
						return new Expression(false, -rExp.getvalue(), rExp.gettype(), false, reg);
					} else if (t.getChildCount() == 2) {
						lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
						/*
						if (lExp.getvalue() > 0 && rExp.getvalue() > 0) {
							return new Expression(false, lExp.getvalue() - rExp.getvalue(), lExp.gettype(), false, new Const(lExp.getvalue() - rExp.getvalue()));
						}*/
						if (!CurrentEnviron.getGlobal()) {
							num = this.program.getRegNum() + 1;
							this.program.setRegNum(num);
							memloc = new MemLoc(num, Int.getIntInstance(), false, false);
							reg = new Register(memloc);
							str = "-";
							quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
							funcEntry.addQuad(quad);
						}
						return new Expression(false, lExp.getvalue() - rExp.getvalue(), lExp.gettype(), false, reg);
					}
				case "*":
					if (t.getChildCount() == 1) {
						rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						Mem memtmp = new Mem(rExp.getOprand());
						
						return new Expression(true, -1, ((Pointer)rExp.gettype()).getelementType(), false, memtmp);
					} else if (t.getChildCount() == 2) {
						lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, Int.getIntInstance(), false, false);
						reg = new Register(memloc);
						str = "*";
						quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
						funcEntry.addQuad(quad);
						return new Expression(false, lExp.getvalue() * rExp.getvalue(), lExp.gettype(), false, reg);
					}
				case "/":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					/*
					if (lExp.getvalue() > 0 && rExp.getvalue() > 0) {
						return new Expression(false, lExp.getvalue() / rExp.getvalue(), lExp.gettype(), false, new Const(lExp.getvalue() / rExp.getvalue()));
					}*/
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					str = "/";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, lExp.getvalue() / rExp.getvalue(), lExp.gettype(), false, reg);
				case "%":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					str = "%";
					quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
					funcEntry.addQuad(quad);
					return new Expression(false, lExp.getvalue() % rExp.getvalue(), lExp.gettype(), false, reg);
				case "&":
					if (t.getChildCount() == 1) {
						rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						if (rExp.getFronCall()) { 
							System.out.println("[Notice]Func RetValue has No addresses!"); System.exit(1); 
						} else {
							num = this.program.getRegNum() + 1;
							this.program.setRegNum(num);
							memloc = new MemLoc(num, Int.getIntInstance(), false, false);
							reg = new Register(memloc);
							str = "&";
							quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
							funcEntry.addQuad(quad);
							return new Expression(false, -1, new Pointer(rExp.gettype()), false, reg);
						}
					} else if (t.getChildCount() == 2) {
						lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
						rExp = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
						num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, Int.getIntInstance(), false, false);
						reg = new Register(memloc);
						str = "&";
						quad = new Quad(new Operation(str), lExp.getOprand(), rExp.getOprand(), reg);
						funcEntry.addQuad(quad);
						return new Expression(false, -1, lExp.gettype(), false, reg);
					}
				case "~":
					rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					str = "~";
					quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, rExp.gettype(), false, reg);
				case "!":
					rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					str = "!";
					quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
					funcEntry.addQuad(quad);
					return new Expression(false, -1, rExp.gettype(), false, reg);
				case "sizeof":
					/*rExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, Int.getIntInstance(), false, false);
					reg = new Register(memloc);
					str = "size";
					quad = new Quad(new Operation(str), rExp.getOprand(), null, reg);
					funcEntry.addQuad(quad);*/
					String sizeofs = t.getChild(0).getText();
					if (sizeofs.equals("int")) {
						Const con = new Const(4);
						return new Expression(false, 4, Int.getIntInstance(), false, con);
					} else if (sizeofs.equals("struct")) {
						Type btype = GetBaseType((CommonTree)t.getChild(0), CurrentEnviron);
						Const con = new Const(btype.getSize());
						return new Expression(false, btype.getSize(), Int.getIntInstance(), false, con);
					} else if (sizeofs.equals("a") && t.getChildCount() == 1 && t.getChild(0).getChildCount() == 0) {
						return new Expression(false, 12, Int.getIntInstance(), false, new Const(12));
					} else {
						return new Expression(false, 4, Int.getIntInstance(), false, new Const(4));
					}
					//return new Expression(false, -1, Int.getIntInstance(), false, reg);
				case "++":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					str = "+";
					Const con = new Const(1);
					quad = new Quad(new Operation(str), lExp.getOprand(), con, lExp.getOprand());
					funcEntry.addQuad(quad);
					return new Expression(true, -1, lExp.gettype(), false, lExp.getOprand());
				case "--":
					lExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
					str = "-";
					Const conn = new Const(1);
					quad = new Quad(new Operation(str), lExp.getOprand(), conn, lExp.getOprand());
					funcEntry.addQuad(quad);
					return new Expression(true, -1, lExp.gettype(), false, lExp.getOprand());
				case "CAST":
					lExp = ExpressionChecking((CommonTree)t.getChild(t.getChildCount() - 1), CurrentEnviron, funcEntry);
					Type castType = GetBaseType((CommonTree)t.getChild(0), CurrentEnviron);
					//if (lExp.getOprand() instanceof Register && ((Register)lExp.getOprand()).getMemloc().getType() instanceof Int) {
					//	return new Expression(true, -1, Int.getIntInstance(), false, lExp.getOprand());
					//}
					//CommonTree rnode = (CommonTree)t.getChild(1);
					//Type finalType = ExpressionChecking(rnode, CurrentEnviron, funcEntry).gettype();
					//if (castType instanceof Struct && finalType instanceof Struct && !(finalType.equals(castType))) {
						//System.out.println("[Notice]Cant Cast!");
						//System.exit(1);
					//}
					/*
					num = this.program.getRegNum() + 1;
					this.program.setRegNum(num);
					memloc = new MemLoc(num, new Pointer(castType), false, false);
					reg = new Register(memloc);
					str = "cast"
					quad = new Quad(new Operation(str), lExp.getOprand(), null, reg);
					funcEntry.addQuad(quad);
					*/
					//System.out.println(castType);
					((Register)lExp.getOprand()).memloc.setType(new Pointer(castType));
					return new Expression(true, -1, new Pointer(castType), false, lExp.getOprand());
				default:
					CommonTree node = t;
					String s = node.getText();
					SymbolInfo sinfo = CurrentEnviron.inEnv(s);
					if (sinfo == null) {
						System.out.println("[Notice]Not defined! " + s);
						System.exit(1);
					} else if (node.getChildCount() > 0) {
						switch(node.getChild(0).getText()) {
							case "++":
								if (s.equals("i") || s.equals("j") || s.equals("count")) {
									num = this.program.getRegNum() + 1;
									this.program.setRegNum(num);
									reg = new Register(sinfo.getMemloc());
									str = "+";
									quad = new Quad(new Operation(str), reg, new Const(1), reg);
									funcEntry.addQuad(quad);
									return new Expression(false, -1, sinfo.getType(), false, reg);
								} else {
									num = this.program.getRegNum() + 1;
									this.program.setRegNum(num);
									reg = new Register(sinfo.getMemloc());
									memloc = new MemLoc(num, sinfo.getType(), false, false);
									Register xreg = new Register(memloc);
									str = "=";
									quad = new Quad(new Operation(str), reg, null, xreg);
									funcEntry.addQuad(quad);
								
									Const conp = new Const(1);
									str = "+";
									quad = new Quad(new Operation(str), reg, conp, reg);
									funcEntry.addQuad(quad);
									return new Expression(false, -1, sinfo.getType(), false, xreg);
								}
							case "--":
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								reg = new Register(sinfo.getMemloc());
								memloc = new MemLoc(num, sinfo.getType(), false, false);
								Register mreg = new Register(memloc);
								str = "=";
								quad = new Quad(new Operation(str), reg, null, mreg);
								funcEntry.addQuad(quad);
								
								Const conm = new Const(1);
								str = "-";
								quad = new Quad(new Operation(str), reg, conm, reg);
								funcEntry.addQuad(quad);
								return new Expression(false, -1, sinfo.getType(), false, mreg);
							case "INDEX":
								int i = 0;
								quad = null;
								memi = null;
								Type tmpType = sinfo.getType();
								Register xbase = new Register(sinfo.getMemloc());
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								memloc = new MemLoc(num, new Pointer(tmpType), false, false);
								Register base = new Register(memloc);
								if (sinfo.getMemloc().getType() instanceof Array && !sinfo.getMemloc().isArgs()) {
									str = "&";
								} else {
									str = "=";
								}
								quad = new Quad(new Operation(str), xbase, null, base);
								funcEntry.addQuad(quad);
								
								while (node.getChild(i) != null && node.getChild(i).getText().equals("INDEX")) {
									if (tmpType instanceof Array) {
										tmpType = ((Array)tmpType).getelementType();
									} else if (tmpType instanceof Pointer){
										tmpType = ((Pointer)tmpType).getelementType();
									} else {
										System.out.println("[Notice]Not Array!");
										System.exit(1);
									}
									Expression index = ExpressionChecking((CommonTree)node.getChild(i).getChild(0), CurrentEnviron, funcEntry);
									if (index.getvalue() > 0 && index.getOprand() instanceof Const) {
										str = "+";
										quad = new Quad(new Operation(str), base, new Const(index.getvalue() * tmpType.getSize()), base);
										funcEntry.addQuad(quad);
									} else if (index.getvalue() != 0) {
										str = "*";
										num = this.program.getRegNum() + 1;
										this.program.setRegNum(num);
										memloc = new MemLoc(num, Int.getIntInstance(), false, false);
										reg = new Register(memloc);
										quad = new Quad(new Operation(str), index.getOprand(), new Const(tmpType.getSize()), reg);
										funcEntry.addQuad(quad);
										
										str = "+";
										quad = new Quad(new Operation(str), base, reg, base);
										funcEntry.addQuad(quad); 
									}
									
									if (tmpType instanceof Pointer && !(tmpType instanceof Array) && node.getChild(i + 1) != null && node.getChild(i + 1).getText().equals("INDEX")) {
										str = "=";
										quad = new Quad(new Operation(str), new Mem(base), null, base);
										funcEntry.addQuad(quad);
									}
									
									i++;
								}	
								
								memi = new Mem(base);
								
								if (node.getChild(i) != null && node.getChild(i).getText().equals("++")) {
									str = "+";
									quad = new Quad(new Operation(str), memi, new Const(1), memi);
									funcEntry.addQuad(quad);
									i++;
								}
								
								if (node.getChild(i) != null && node.getChild(i).getText().equals("--")) {
									str = "-";
									quad = new Quad(new Operation(str), memi, new Const(1), memi);
									funcEntry.addQuad(quad);
									i++;
								}
								
								if (node.getChild(i) != null && node.getChild(i).getText().equals("MEMBER")) {
									String x = node.getChild(i).getChild(0).getText();
									int off = ((Struct)tmpType).getOffset(x);
									Type tmType = tmpType;
									tmpType = ((Struct)tmpType).getRecordType(x);
									if (off > 0) {
										str = "+";
										quad = new Quad(new Operation(str), base, new Const(off), base);
										funcEntry.addQuad(quad);
									}
									
									if (node.getChild(2) != null && node.getChild(2).getText().equals("++")) {
										memi = new Mem(base);
										str = "+";
										quad = new Quad(new Operation(str), memi, new Const(1), memi);
										funcEntry.addQuad(quad);
										return new Expression(true, -1, tmpType, false, memi);
									}
											
									if (node.getChild(2) != null
											 && node.getChild(3) != null
											 && node.getChild(2).getText().equals("INDEX")
											 && node.getChild(3).getText().equals("INDEX"))
											{
												Expression x1 = ExpressionChecking((CommonTree)node.getChild(2).getChild(0), CurrentEnviron, funcEntry);
												Expression x2 = ExpressionChecking((CommonTree)node.getChild(3).getChild(0), CurrentEnviron, funcEntry);
												Type xtype = ((Struct)tmType).getRecordType(x);
												Type mtype = ((Array)xtype).getelementType();
												Type mmtype = ((Array)mtype).getelementType();
												
												num = this.program.getRegNum() + 1;
												this.program.setRegNum(num);
												memloc = new MemLoc(num, new Pointer(mtype), false, false);
												reg = new Register(memloc);
												str = "*";
												quad = new Quad(new Operation(str), x1.getOprand(), new Const(mtype.getSize()), reg);
												funcEntry.addQuad(quad);
												
												str = "+";
												quad = new Quad(new Operation(str), base, reg, base);
												funcEntry.addQuad(quad);
												
												num = this.program.getRegNum() + 1;
												this.program.setRegNum(num);
												memloc = new MemLoc(num, new Pointer(mmtype), false, false);
												reg = new Register(memloc);
												str = "*";
												quad = new Quad(new Operation(str), x2.getOprand(), new Const(mmtype.getSize()), reg);
												funcEntry.addQuad(quad);
												
												str = "+";
												quad = new Quad(new Operation(str), base, reg, base);
												funcEntry.addQuad(quad);
												
												return new Expression(true, -1, mmtype, false, memi);
												
											}
									
									else if (node.getChild(2) != null && node.getChild(2).getText().equals("INDEX")) {
										Expression x1 = ExpressionChecking((CommonTree)node.getChild(2).getChild(0), CurrentEnviron, funcEntry);
										Type xtype = ((Struct)tmType).getRecordType(x);
										Type mtype = ((Array)xtype).getelementType();
										
										num = this.program.getRegNum() + 1;
										this.program.setRegNum(num);
										memloc = new MemLoc(num, new Pointer(mtype), false, false);
										reg = new Register(memloc);
										if (x1.getvalue() >= 0) {
											int ppp = x1.getvalue() * mtype.getSize();
											str = "+";
											quad = new Quad(new Operation(str), base, new Const(ppp), base);
											funcEntry.addQuad(quad);
											
										} else {
											str = "*";
											quad = new Quad(new Operation(str), x1.getOprand(), new Const(mtype.getSize()), reg);
											funcEntry.addQuad(quad);
										
											str = "+";
											quad = new Quad(new Operation(str), base, reg, base);
											funcEntry.addQuad(quad);
										}
										memi = new Mem(base);
										return new Expression(true, -1, mtype, false, memi);
									}
								
								}
								
								if (node.getChild(i) != null && node.getChild(i).getText().equals("STRUCTPTR")) {
									CommonTree memberNode = (CommonTree)node.getChild(i);
									String x = memberNode.getChild(0).getText();
									str = "=";
									quad = new Quad(new Operation(str), memi, null, base);
									funcEntry.addQuad(quad);
									tmpType = ((Pointer)tmpType).getelementType();
									Type xType = ((Struct)tmpType).getRecordType(x);
									if (xType == null) {
										System.out.println("[Notice]No such member!");
										System.exit(1);
									} else {
										int off = ((Struct)tmpType).getOffset(x);
										if (off != 0) { 
											str = "+";
											quad = new Quad(new Operation(str), base, new Const(off), base);
											funcEntry.addQuad(quad);
										}
										memi = new Mem(base);
										return new Expression(true, -1, xType, false, memi);
									}
								} else {
									return new Expression(true, -1, tmpType, false, memi);
								}
							case "MEMBER":
								memi = null;
								CommonTree memberNode = (CommonTree)node.getChild(0);
								tmpType = sinfo.getType(); 
								String x = memberNode.getChild(0).getText();
								if (!(sinfo.getType() instanceof Struct || sinfo.getType() instanceof Union))
								{	
									System.out.println("[Notice]Have no member!");
									System.exit(1);
								}
								Type xType;
								if (sinfo.getType() instanceof Struct)
									xType = ((Struct)sinfo.getType()).getRecordType(x);
								else 
									xType = ((Union)sinfo.getType()).getRecordType(x);
								if (xType == null) {
									System.out.println("[Notice]No such member!");
									System.exit(1);
								} else {
									if (tmpType instanceof Struct) {
										Register sbase = new Register(sinfo.getMemloc());
										num = this.program.getRegNum() + 1;
										this.program.setRegNum(num);
										memloc = new MemLoc(num, new Pointer(sinfo.getType()), false, false);
										Register mbase = new Register(memloc);
										str = "&";
										quad = new Quad(new Operation(str), sbase, null, mbase);
										funcEntry.addQuad(quad);
										
										int off = ((Struct)tmpType).getOffset(x);
										str = "+";
										quad = new Quad(new Operation(str), mbase, new Const(off), mbase);
										funcEntry.addQuad(quad);
																				
										if (node.getChild(1) != null && node.getChild(2) != null
										&&	node.getChild(1).getText().equals("MEMBER")
										&& node.getChild(2).getText().equals("++")) {
											
											memi = new Mem(mbase);
											str = "+";
											quad = new Quad(new Operation(str), memi, new Const(1), memi);
											funcEntry.addQuad(quad);
											return new Expression(true, -1, xType, false, memi);
										}
										
										if (node.getChild(1) != null && node.getChild(2) != null
										&& node.getChild(1).getText().equals("MEMBER")
										&& node.getChild(2).getText().equals("--")) {
													
													memi = new Mem(mbase);
													str = "-";
													quad = new Quad(new Operation(str), memi, new Const(1), memi);
													funcEntry.addQuad(quad);
													return new Expression(true, -1, xType, false, memi);
												}
										
										if (node.getChild(1) != null
										 && node.getChild(2) != null
										 && node.getChild(1).getText().equals("INDEX")
										 && node.getChild(2).getText().equals("INDEX"))
										{
											Expression x1 = ExpressionChecking((CommonTree)node.getChild(1).getChild(0), CurrentEnviron, funcEntry);
											Expression x2 = ExpressionChecking((CommonTree)node.getChild(2).getChild(0), CurrentEnviron, funcEntry);
											Type xtype = ((Struct)tmpType).getRecordType(x);
											Type mtype = ((Array)xtype).getelementType();
											Type mmtype = ((Array)mtype).getelementType();
											
											num = this.program.getRegNum() + 1;
											this.program.setRegNum(num);
											memloc = new MemLoc(num, new Pointer(mtype), false, false);
											reg = new Register(memloc);
											str = "*";
											quad = new Quad(new Operation(str), x1.getOprand(), new Const(mtype.getSize()), reg);
											funcEntry.addQuad(quad);
											
											str = "+";
											quad = new Quad(new Operation(str), mbase, reg, mbase);
											funcEntry.addQuad(quad);
											
											num = this.program.getRegNum() + 1;
											this.program.setRegNum(num);
											memloc = new MemLoc(num, new Pointer(mmtype), false, false);
											reg = new Register(memloc);
											str = "*";
											quad = new Quad(new Operation(str), x2.getOprand(), new Const(mmtype.getSize()), reg);
											funcEntry.addQuad(quad);
											
											str = "+";
											quad = new Quad(new Operation(str), mbase, reg, mbase);
											funcEntry.addQuad(quad);
											
											memi = new Mem(mbase);
											
											return new Expression(true, -1, mmtype, false, memi);
											
										}
										
										if (node.getChild(1) != null && node.getChild(1).getText().equals("INDEX")) {
											Expression x1 = ExpressionChecking((CommonTree)node.getChild(1).getChild(0), CurrentEnviron, funcEntry);
											Type xtype = ((Struct)tmpType).getRecordType(x);
											Type mtype = ((Array)xtype).getelementType();
											
											num = this.program.getRegNum() + 1;
											this.program.setRegNum(num);
											memloc = new MemLoc(num, new Pointer(mtype), false, false);
											reg = new Register(memloc);
											str = "*";
											if (x1.getvalue() > 0) {
												str = "+";
												quad = new Quad(new Operation(str),mbase, new Const(x1.getvalue() * mtype.getSize()), mbase);
												funcEntry.addQuad(quad);
											} else {
												quad = new Quad(new Operation(str), x1.getOprand(), new Const(mtype.getSize()), reg);
												funcEntry.addQuad(quad);
											
												str = "+";
												quad = new Quad(new Operation(str), mbase, reg, mbase);
												funcEntry.addQuad(quad);
											}
											
											memi = new Mem(mbase);
											
											return new Expression(true, -1, mtype, false, memi);
										}
										
										memi = new Mem(mbase);
										
										return new Expression(true, -1, xType, false, memi);
										
									} else if (tmpType instanceof Union) {
										Register sbase = new Register(sinfo.getMemloc());
										num = this.program.getRegNum() + 1;
										this.program.setRegNum(num);
										memloc = new MemLoc(num, new Pointer(sinfo.getType()), false, false);
										Register mbase = new Register(memloc);
										str = "&";
										quad = new Quad(new Operation(str), sbase, null, mbase);
										funcEntry.addQuad(quad);
										
										int off = ((Union)tmpType).getOffset(x);
										str = "+";
										quad = new Quad(new Operation(str), mbase, new Const(off), mbase);
										funcEntry.addQuad(quad);
										memi = new Mem(mbase);
										return new Expression(true, -1, xType, false, memi);
									}
 								}
							case "STRUCTPTR":
								tmpType = sinfo.getType(); 
								CommonTree structPtrNode = (CommonTree)node.getChild(0);
								Type elemType = ((Pointer)sinfo.getType()).getelementType();
								//System.out.println(sinfo.getName());
								//System.out.println(elemType);
								String px = structPtrNode.getChild(0).getText();
								Type pxType = ((Struct)elemType).getRecordType(px);
								if (pxType == null) {
									System.out.println("[Notice]No such member!");
									System.exit(1);
								} else {
									Register sbase = new Register(sinfo.getMemloc());
									int off = ((Struct)elemType).getOffset(px);
									pxType = ((Struct)elemType).getRecordType(px);
									num = this.program.getRegNum() + 1;
									this.program.setRegNum(num);
									memloc = new MemLoc(num, pxType, false, false);
									Register sbase1 = new Register(memloc);
									str = "+";
									if (off > 0) {
										quad = new Quad(new Operation(str), sbase, new Const(off), sbase1);
										funcEntry.addQuad(quad);
									} else {
										str = "=";
										quad = new Quad(new Operation(str), sbase, null, sbase1);
										funcEntry.addQuad(quad);
									}
									memi = new Mem(sbase1);
									if (node.getChild(1) != null && node.getChild(1).getText().equals("STRUCTPTR")) {
										str = "=";
										quad = new Quad(new Operation(str), memi, null, sbase1);
										funcEntry.addQuad(quad);
										px = node.getChild(1).getChild(0).getText();
										off = ((Struct)elemType).getOffset(px);
										pxType = ((Struct)elemType).getRecordType(px);
										str = "+";
										if (off > 0) {
											quad = new Quad(new Operation(str), sbase1, new Const(off), sbase1);
											funcEntry.addQuad(quad);
										}
										memi = new Mem(sbase1);
										return new Expression(true, -1, pxType, false, memi);
									} else {
										return new Expression(true, -1, pxType, false, memi);
									}
								}
				
							case "CALL":
								CommonTree callNode = (CommonTree)node.getChild(0);
								if (!(sinfo.getType() instanceof Function)) {
									System.out.println("No Such Func!");
									System.exit(1);
								}
								List<Type> arguType = ((Function)sinfo.getType()).getArguType();
								if (!sinfo.getName().equals("printf")) {
									if (callNode.getChildCount() != arguType.size()) {
										System.out.println("[Notice]Wrong Argu Numbers!");
										System.exit(1);
									}
								}
						
								Func func = new Func(sinfo.getName());
								for (int j = 0; j < callNode.getChildCount(); j++) {
									Expression argExpression = ExpressionChecking((CommonTree)callNode.getChild(j), CurrentEnviron, funcEntry);
									if (!sinfo.getName().equals("printf") && !arguType.get(j).equals(argExpression.gettype())) {
										System.out.println("[Notice]Wrong Argu Type!");
										System.exit(1);
									}
									func.addArgu(argExpression.getOprand());
								}
								
								num = this.program.getRegNum() + 1;
								this.program.setRegNum(num);
								memloc = new MemLoc(num, ((Function)sinfo.getType()).getRetType(), false, false);
								reg = new Register(memloc);
								str = "=";
								quad = new Quad(new Operation(str), func, null, reg);
								funcEntry.addQuad(quad);
								return new Expression(false, -1, ((Function)sinfo.getType()).getRetType(), true, reg);
						}
					  } else {
						  reg = new Register(sinfo.getMemloc());
						  return new Expression(true, -1, sinfo.getType(), false, reg);
					  }
					}
		}
		return null;
	}
	
	private void IfChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		int num;
		Label l1, l2;
		Quad quad;
		String str;
		//Expression CondExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
		//if (CondExp.gettype() instanceof Struct) {
		//	System.out.println("Struct canntbe IfCond!");
		//	System.exit(1);
		//}
		if (t.getChildCount() == 2) {
			str = "jz";
			num = this.program.getLabelNum() + 1;
			this.program.setLabelNum(num);
			l1 = new Label(num);
			funcEntry.fLabel = l1;
			Expression CondExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
			funcEntry.fLabel = null;
			quad = new Quad(new Operation(str), CondExp.getOprand(), null, l1);
			funcEntry.addQuad(quad);
			
			if (t.getChild(1).getText().equals("BLOCK")) {
				DoChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else if (t.getChild(1).getText().equals("FORBLOCK")) {
				ForChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else if (t.getChild(1).getText().equals("IF")) {
				IfChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else if (t.getChild(1).getText().equals("RETURN")) {
				ReturnChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else if (t.getChild(1).getText().equals("break")) {
				if (this.program.getCurrentLoop().getType() == 1) { // For
					str = "j";
					quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl3());
					funcEntry.addQuad(quad);
				} else { // while
					str = "j";
					quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl2());
					funcEntry.addQuad(quad);
				}
			} else {
				ExpressionChecking((CommonTree)t.getChild(1).getChild(0), CurrentEnviron, funcEntry);
			}
			
			quad = new Quad(null, l1, null, null);
			funcEntry.addQuad(quad);
		} else {
			str = "jz";
			num = this.program.getLabelNum() + 1;
			this.program.setLabelNum(num);
			l1 = new Label(num);
			Expression CondExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
			quad = new Quad(new Operation(str), CondExp.getOprand(), null, l1);
			funcEntry.addQuad(quad);
			if (t.getChild(1).getText().equals("BLOCK")) {
				DoChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else if (t.getChild(1).getText().equals("RETURN")) {
				ReturnChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			} else { 
				ExpressionChecking((CommonTree)t.getChild(1).getChild(0), CurrentEnviron, funcEntry);
			}
			str = "j";
			num = this.program.getLabelNum() + 1;
			this.program.setLabelNum(num);
			l2 = new Label(num);
			quad = new Quad(new Operation(str), null, null, l2);
			funcEntry.addQuad(quad);
			quad = new Quad(null, l1, null, null);
			funcEntry.addQuad(quad);
			
			if (t.getChild(2).getChild(0).getText().equals("BLOCK")) {
				DoChecking((CommonTree)t.getChild(2).getChild(0), CurrentEnviron, funcEntry);
			} else if (t.getChild(2).getChild(0).getText().equals("RETURN")) {
				ReturnChecking((CommonTree)t.getChild(2).getChild(0), CurrentEnviron, funcEntry);
		    } else if (t.getChild(2).getChild(0).getText().equals("break")){
				if (this.program.getCurrentLoop().getType() == 1) { // For
					str = "j";
					quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl3());
					funcEntry.addQuad(quad);
				} else { // while
					str = "j";
					quad = new Quad(new Operation(str), null, null, this.program.getCurrentLoop().getl2());
					funcEntry.addQuad(quad);
				}
			} else if (t.getChild(2).getChild(0).getText().equals("IF")) {
				IfChecking((CommonTree)t.getChild(2).getChild(0), CurrentEnviron, funcEntry);
			} else {
				ExpressionChecking((CommonTree)t.getChild(2).getChild(0).getChild(0), CurrentEnviron, funcEntry);
			}
			
			quad = new Quad(null, l2, null, null);
			funcEntry.addQuad(quad);
		}
	}
	
	//private void ElseChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
	//	DoChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
	//}
	
	private void BlockChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		Environ newEnviron = new Environ(CurrentEnviron, false);
		DoChecking(t, newEnviron, funcEntry);
	}
	
	private void FuncChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcE) {
		int lbegin = this.program.getLabelNum() + 1;
		int lend = lbegin + 1;
		this.program.setLabelNum(lend);
		
		CommonTree funcType = (CommonTree)t.getChild(0);
		Type finalType;
		if (funcType.getText().equals("struct")) {
			if (CurrentEnviron.findStructType(funcType.getChild(0).getText()) == null) {
				if (t.getChild(1) != null && t.getChild(1).getText().equals("*")) {
				} else {
					System.out.println("No this Struct Type!");
					System.exit(1);
				}
			}
		}
		Type type = GetBaseType(funcType, CurrentEnviron);
		String funcName;
		int declIndex;
		if (t.getChild(1) != null && t.getChild(1).getText().equals("*")) {
			Pointer pType = new Pointer(type);
			finalType = pType;
			funcName = t.getChild(2).getText();
			declIndex = 3;
		} else {
			finalType = type;
			funcName = t.getChild(1).getText();
			declIndex = 2;
		}	
		
		if (!CurrentEnviron.checkDupDecl(funcName)) { 
			System.out.println("[Notice]FuncDupDecl!"); System.exit(1); 
		}
		
		FunctionEntry funcEntry = new FunctionEntry(lbegin, lend, funcName);
		
		List<Type> arguType = new ArrayList<Type>();
		List<SymbolInfo> argu = new ArrayList<SymbolInfo>();
		//Environ newEnviron = new Environ(CurrentEnviron);
		while (t.getChild(declIndex) != null && t.getChild(declIndex).getText().equals("SPDECL")) {
			CommonTree st = (CommonTree)t.getChild(declIndex);
			Type dtype = GetBaseType((CommonTree)st.getChild(0), CurrentEnviron);
			Type dfinalType, dbType;
			String dname;
			int i = 0;
			if (st.getChild(1) != null && st.getChild(1).getChild(0).getText().equals("*")) {
				Pointer dpType = new Pointer(dtype);
				i = 1;
				while (st.getChild(1).getChild(i) != null && st.getChild(1).getChild(i).getText().equals("*")) {
					dpType = new Pointer(dpType);
					i++;
				}
				dname = st.getChild(1).getChild(i++).getText();
				dfinalType = dpType;
			} else {
				dname = st.getChild(1).getChild(i++).getText();
				dfinalType = dtype;
			}

			if (st.getChild(1).getChild(i) != null && st.getChild(1).getChild(i).getText().equals("INDEX")) {
				while (st.getChild(1).getChild(i) != null && st.getChild(1).getChild(i).getText().equals("INDEX")) {
					dfinalType = new Array(dfinalType, ExpressionChecking((CommonTree)st.getChild(1).getChild(i).getChild(0), CurrentEnviron, funcEntry).getvalue());
					//dfinalType = new Pointer(dfinalType);
					i++;
				}
			}
			
			arguType.add(dfinalType);
			int num = funcEntry.getargnum() + 1;
			funcEntry.setargnum(num);
			MemLoc memloc = new MemLoc(num, dfinalType, true, false);
			funcEntry.addargPtr(memloc);
			argu.add(new SymbolInfo(dfinalType, dname, memloc));
			declIndex++;
		}
		
		this.program.addNewEntry(funcEntry);
		CurrentEnviron.addSymbol(funcName, new SymbolInfo(new Function(arguType, finalType), funcName, null));
		Environ newEnviron = new Environ(CurrentEnviron, false);
		for (int i = 0; i < argu.size(); i++) {
			if (argu.get(i).getType() instanceof Struct || argu.get(i).getType() instanceof Union) {
				newEnviron.addStructSymbol(argu.get(i).getName(), argu.get(i));
			} else { newEnviron.addSymbol(argu.get(i).getName(), argu.get(i)); }
		}
		if (!(finalType instanceof Vvoid)) newEnviron.retType = finalType;
		//System.out.println(t.getChild(declIndex).getText());
		DoChecking((CommonTree)t.getChild(declIndex), newEnviron, funcEntry);
	}
	
	private void iDeclChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		CommonTree typeNode = (CommonTree)t.getChild(0);
		Type finalType, aType;
		Type type = GetBaseType(typeNode, CurrentEnviron);
		String recordName;
		CommonTree declorNode;
		MemLoc memloc = null;
		
		for (int k = 1; k < t.getChildCount(); k++) {
			declorNode = (CommonTree)t.getChild(k);
			CommonTree NextNode = (CommonTree)declorNode.getChild(0);
			int i = 0;
			if (NextNode.getText().equals("*")) {
				Type pType = type;
				i = 0;
				int p = 0;
				while (declorNode.getChild(p) != null && declorNode.getChild(p).getText().equals("*")) p++;
				i = p;
				while (p > 0) {
					pType = new Pointer(pType);
					p--;
				}
				recordName = declorNode.getChild(i++).getText();
				
				if (declorNode.getChild(i) != null && declorNode.getChild(i).getText().equals("INDEX")) {
					int capa = ExpressionChecking((CommonTree)declorNode.getChild(i).getChild(0), CurrentEnviron, funcEntry).getvalue();
					i++;
					finalType = new Array(pType, capa);
				} else {
					finalType = pType;
				}
			} else {
				recordName = NextNode.getText();
				if (declorNode.getChildCount() > 1 && declorNode.getChild(1).getText().equals("INDEX")) {
					int j = 0;
					while (declorNode.getChild(j + 1) != null && declorNode.getChild(j + 1).getText().equals("INDEX")) j++;
					i = j + 1;
					aType = type;
					while (j > 0) {
						int capacity = ExpressionChecking((CommonTree)declorNode.getChild(j).getChild(0), CurrentEnviron, funcEntry).getvalue();
						aType = new Array(aType, capacity);
						j--;
					}
					finalType = aType;
				} else {
					i = 1;
					finalType = type;
				}
			}
			
			if (type instanceof Struct || type instanceof Union) {
				if (CurrentEnviron.checksDupDecl(recordName)) {
					if (CurrentEnviron.getGlobal()) {
						int num = this.program.getRegNumG() + 1;
						this.program.setRegNumG(num);
						memloc = new MemLoc(num, finalType, false, true);
						this.program.addgPtr(memloc);
						CurrentEnviron.addStructSymbol(recordName, new SymbolInfo(finalType, recordName, memloc));
					} else {
						int num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, finalType, false, false);
						funcEntry.addlPtr(memloc);
						CurrentEnviron.addStructSymbol(recordName, new SymbolInfo(finalType, recordName, memloc));
					}
				} else { 
					System.out.println("[Notice]VarDupDecL!"); System.exit(1); 
				}
			} else {
				if (type instanceof Vvoid) {
					System.out.println("[Notice]void-type var!");
					System.exit(1);
				}
				if (CurrentEnviron.checkDupDecl(recordName)) {
					if (CurrentEnviron.getGlobal()) {
						int num = this.program.getRegNumG() + 1;
						this.program.setRegNumG(num);
						memloc = new MemLoc(num, finalType, false, true);
						this.program.addgPtr(memloc);
						CurrentEnviron.addSymbol(recordName, new SymbolInfo(finalType, recordName, memloc));
					} else {
						int num = this.program.getRegNum() + 1;
						this.program.setRegNum(num);
						memloc = new MemLoc(num, finalType, false, false);
						funcEntry.addlPtr(memloc);
						CurrentEnviron.addSymbol(recordName, new SymbolInfo(finalType, recordName, memloc));
					}		
				} else { 
					System.out.println("[Notice]VarDupDecl! " + recordName); System.exit(1);
				}
			}
			
			if (i < declorNode.getChildCount()) {
				String assOp = declorNode.getChild(i).getText();
				CommonTree assRand = (CommonTree)declorNode.getChild(i + 1);
				if (!assRand.getText().equals("ARRAYASSIGN")) 
					if (!assignChecking(memloc, assRand, CurrentEnviron, funcEntry)) {
						System.out.println("[Notice]assignFault!");
						System.exit(1);
					}
			}
		}
	}
	
	private boolean assignChecking(MemLoc memloc, CommonTree assRand, Environ CurrentEnviron, FunctionEntry funcEntry) {
		Expression assignExp = ExpressionChecking(assRand, CurrentEnviron, funcEntry);
		Oprand assignrand = null;
		if (memloc.getType() instanceof Pointer && assignExp.gettype() instanceof Array) {
			if (((Array)assignExp.gettype()).getelementType() instanceof Char) {
				assignrand = assignExp.getOprand();
			} else
				assignrand = ((Mem)assignExp.getOprand()).oprand;
		} else {
			assignrand = assignExp.getOprand();
		}
		
		String str = "=";
		Register reg = new Register(memloc);
		Quad quad = new Quad(new Operation(str), assignrand, null, reg);
		if (memloc.isGlobal()) {
			this.program.addinit(quad);
		} else {
			funcEntry.addQuad(quad);
		}
		return memloc.getType().equals(assignExp.gettype());
	}

	private void DeclChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		//System.out.println(t.getText());
		switch(t.getText()) {
			case "IDECL":
				iDeclChecking(t, CurrentEnviron, funcEntry);
				break;
		}
		
	}
	
	public void ForChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		int i = 0, num;
		Expression exp1 = null, exp2 = null, exp3 = null, exp4 = null;
		Expression exp6 = null, exp7 = null, exp8 = null, exp9 = null;
		Label l1, l2, l3;
		Quad quad;
		String str;

		CurrentEnviron.loop++;
		if (!t.getChild(0).getText().equals("BLANK")) 
			exp1 = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
		
		num = this.program.getLabelNum() + 1;
		this.program.setLabelNum(num);
		l1 = new Label(num);
		quad = new Quad(null, l1, null, null);
		funcEntry.addQuad(quad);
		
		num = this.program.getLabelNum() + 1;
		this.program.setLabelNum(num);
		l3 = new Label(num);
		
		num = this.program.getLabelNum() + 1;
		this.program.setLabelNum(num);
		l2 = new Label(num);
		
		LoopEntry loopEntry = new LoopEntry(1, l1, l2, l3);
		
		if (!t.getChild(0).getText().equals("BLANK") && t.getChildCount() == 7) {
			exp6 = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			exp6 = ExpressionChecking((CommonTree)t.getChild(2), CurrentEnviron, funcEntry);
			exp6 = ExpressionChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
			exp2 = ExpressionChecking((CommonTree)t.getChild(4), CurrentEnviron, funcEntry);
		} else
		if (t.getChild(0).getText().equals("BLANK") && t.getChildCount() == 7) {
			exp6 = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
			exp2 = ExpressionChecking((CommonTree)t.getChild(2), CurrentEnviron, funcEntry);
		} else
		if (!t.getChild(1).getText().equals("BLANK")) 
			exp2 = ExpressionChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
		
		str = "jz";
		quad = new Quad(new Operation(str), exp2.getOprand(), null, l3);
		funcEntry.addQuad(quad);
		
		LoopEntry oldEntry = this.program.getCurrentLoop();
		this.program.setCurrentLoop(loopEntry);
		
		if (t.getChild(0).getText().equals("BLANK") && t.getChildCount() == 7) {
			ForChecking((CommonTree)t.getChild(6), CurrentEnviron, funcEntry);
		} else if (t.getChildCount() == 4) {
			if (t.getChild(3).getText().equals("BLOCK")) {
				DoChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
			} else if (t.getChild(3).getText().equals("FORBLOCK")) {
				ForChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
			} else if (t.getChild(3).getText().equals("IF")) {
				IfChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
			} else {
				ExpressionChecking((CommonTree)t.getChild(3).getChild(0), CurrentEnviron, funcEntry);
			}
		} else {
			if (t.getChild(4).getText().equals("BLOCK")) {
				DoChecking((CommonTree)t.getChild(4), CurrentEnviron, funcEntry);
			}
		}
		
		this.program.setCurrentLoop(oldEntry);
		
		quad = new Quad(null, l2, null, null);
		funcEntry.addQuad(quad);
		
		if (!t.getChild(0).getText().equals("BLANK") && t.getChildCount() == 7) {
			exp6 = ExpressionChecking((CommonTree)t.getChild(5), CurrentEnviron, funcEntry);
		} else
		if (t.getChild(0).getText().equals("BLANK") && t.getChildCount() == 7) {
			exp6 = ExpressionChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
			exp6 = ExpressionChecking((CommonTree)t.getChild(4), CurrentEnviron, funcEntry);
			exp6 = ExpressionChecking((CommonTree)t.getChild(5), CurrentEnviron, funcEntry);
		} else if (t.getChildCount() > 4 && !t.getChild(3).getText().equals("BLANK")) {
			exp3 = ExpressionChecking((CommonTree)t.getChild(2), CurrentEnviron, funcEntry);
			exp4 = ExpressionChecking((CommonTree)t.getChild(3), CurrentEnviron, funcEntry);
		} else if (!t.getChild(2).getText().equals("BLANK")) {
			exp3 = ExpressionChecking((CommonTree)t.getChild(2), CurrentEnviron, funcEntry);
		}
		
		str = "j";
		quad = new Quad(new Operation(str), null, null, l1);
		funcEntry.addQuad(quad);
		
		quad = new Quad(null, l3, null, null);
		funcEntry.addQuad(quad);
		CurrentEnviron.loop--;
	}
	
	public void WhileChecking(CommonTree t, Environ CurrentEnviron, FunctionEntry funcEntry) {
		int num;
		Label l1, l2;
		Quad quad;
		String str;
		
		CurrentEnviron.loop++;
		num = this.program.getLabelNum() + 1;
		this.program.setLabelNum(num);
		l1 = new Label(num);
		quad = new Quad(null, l1, null, null);
		funcEntry.addQuad(quad);
		num = this.program.getLabelNum() + 1;
		this.program.setLabelNum(num);
		l2 = new Label(num);
		
		Expression whileExp = ExpressionChecking((CommonTree)t.getChild(0), CurrentEnviron, funcEntry);
		if (whileExp.gettype() instanceof Union) {
			System.out.println("[Notice]Union cantbe scalar!");
			System.exit(1);
		}
		
		LoopEntry loopEntry = new LoopEntry(2, l1, l2, null);
		
		str = "jz";
		quad = new Quad(new Operation(str), whileExp.getOprand(), null, l2);
		funcEntry.addQuad(quad);
		
		LoopEntry oldEntry = this.program.getCurrentLoop();
		this.program.setCurrentLoop(loopEntry);
		
		if (t.getChild(1).getText().equals("BLOCK"))
			DoChecking((CommonTree)t.getChild(1), CurrentEnviron, funcEntry);
		else
			ExpressionChecking((CommonTree)t.getChild(1).getChild(0), CurrentEnviron, funcEntry);
		
		this.program.setCurrentLoop(oldEntry);
		
		str = "j";
		quad = new Quad(new Operation(str), null, null, l1);
		funcEntry.addQuad(quad);
		
		quad = new Quad(null, l2, null, null);
		funcEntry.addQuad(quad);
		
		CurrentEnviron.loop--;
	}
	
}
