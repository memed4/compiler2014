// $ANTLR 3.5.1 C.g 2014-05-22 06:25:18

	package appetizer.syntactic;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

@SuppressWarnings("all")
public class CLexer extends Lexer {
	public static final int EOF=-1;
	public static final int T__33=33;
	public static final int T__34=34;
	public static final int T__35=35;
	public static final int T__36=36;
	public static final int T__37=37;
	public static final int T__38=38;
	public static final int T__39=39;
	public static final int T__40=40;
	public static final int T__41=41;
	public static final int T__42=42;
	public static final int T__43=43;
	public static final int T__44=44;
	public static final int T__45=45;
	public static final int T__46=46;
	public static final int T__47=47;
	public static final int T__48=48;
	public static final int T__49=49;
	public static final int T__50=50;
	public static final int T__51=51;
	public static final int T__52=52;
	public static final int T__53=53;
	public static final int T__54=54;
	public static final int T__55=55;
	public static final int T__56=56;
	public static final int T__57=57;
	public static final int T__58=58;
	public static final int T__59=59;
	public static final int T__60=60;
	public static final int T__61=61;
	public static final int T__62=62;
	public static final int T__63=63;
	public static final int T__64=64;
	public static final int T__65=65;
	public static final int T__66=66;
	public static final int T__67=67;
	public static final int T__68=68;
	public static final int T__69=69;
	public static final int T__70=70;
	public static final int T__71=71;
	public static final int T__72=72;
	public static final int T__73=73;
	public static final int T__74=74;
	public static final int T__75=75;
	public static final int T__76=76;
	public static final int T__77=77;
	public static final int T__78=78;
	public static final int T__79=79;
	public static final int T__80=80;
	public static final int T__81=81;
	public static final int T__82=82;
	public static final int T__83=83;
	public static final int T__84=84;
	public static final int T__85=85;
	public static final int T__86=86;
	public static final int T__87=87;
	public static final int T__88=88;
	public static final int ARRAYASSIGN=4;
	public static final int BLANK=5;
	public static final int BLANKSTATEMENT=6;
	public static final int BLOCK=7;
	public static final int BLOCK_COMMENT=8;
	public static final int CALL=9;
	public static final int CAST=10;
	public static final int CHARACTER=11;
	public static final int DECLARATOR=12;
	public static final int ELSE=13;
	public static final int EXPRESSION=14;
	public static final int FORBLOCK=15;
	public static final int FUNC=16;
	public static final int FUNCDECLARATOR=17;
	public static final int IDECL=18;
	public static final int IDENTIFIER=19;
	public static final int IF=20;
	public static final int INCLUDE=21;
	public static final int INDEX=22;
	public static final int INTEGER=23;
	public static final int LINE_COMMENT=24;
	public static final int MEMBER=25;
	public static final int PDECL=26;
	public static final int RETURN=27;
	public static final int SPDECL=28;
	public static final int STRING=29;
	public static final int STRUCTPTR=30;
	public static final int WHILEBLOCK=31;
	public static final int WS=32;

	// delegates
	// delegators
	public Lexer[] getDelegates() {
		return new Lexer[] {};
	}

	public CLexer() {} 
	public CLexer(CharStream input) {
		this(input, new RecognizerSharedState());
	}
	public CLexer(CharStream input, RecognizerSharedState state) {
		super(input,state);
	}
	@Override public String getGrammarFileName() { return "C.g"; }

	// $ANTLR start "T__33"
	public final void mT__33() throws RecognitionException {
		try {
			int _type = T__33;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:11:7: ( '!' )
			// C.g:11:9: '!'
			{
			match('!'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__33"

	// $ANTLR start "T__34"
	public final void mT__34() throws RecognitionException {
		try {
			int _type = T__34;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:12:7: ( '!=' )
			// C.g:12:9: '!='
			{
			match("!="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__34"

	// $ANTLR start "T__35"
	public final void mT__35() throws RecognitionException {
		try {
			int _type = T__35;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:13:7: ( '%' )
			// C.g:13:9: '%'
			{
			match('%'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__35"

	// $ANTLR start "T__36"
	public final void mT__36() throws RecognitionException {
		try {
			int _type = T__36;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:14:7: ( '%=' )
			// C.g:14:9: '%='
			{
			match("%="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__36"

	// $ANTLR start "T__37"
	public final void mT__37() throws RecognitionException {
		try {
			int _type = T__37;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:15:7: ( '&&' )
			// C.g:15:9: '&&'
			{
			match("&&"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__37"

	// $ANTLR start "T__38"
	public final void mT__38() throws RecognitionException {
		try {
			int _type = T__38;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:16:7: ( '&' )
			// C.g:16:9: '&'
			{
			match('&'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__38"

	// $ANTLR start "T__39"
	public final void mT__39() throws RecognitionException {
		try {
			int _type = T__39;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:17:7: ( '&=' )
			// C.g:17:9: '&='
			{
			match("&="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__39"

	// $ANTLR start "T__40"
	public final void mT__40() throws RecognitionException {
		try {
			int _type = T__40;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:18:7: ( '(' )
			// C.g:18:9: '('
			{
			match('('); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__40"

	// $ANTLR start "T__41"
	public final void mT__41() throws RecognitionException {
		try {
			int _type = T__41;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:19:7: ( ')' )
			// C.g:19:9: ')'
			{
			match(')'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__41"

	// $ANTLR start "T__42"
	public final void mT__42() throws RecognitionException {
		try {
			int _type = T__42;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:20:7: ( '*' )
			// C.g:20:9: '*'
			{
			match('*'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__42"

	// $ANTLR start "T__43"
	public final void mT__43() throws RecognitionException {
		try {
			int _type = T__43;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:21:7: ( '*=' )
			// C.g:21:9: '*='
			{
			match("*="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__43"

	// $ANTLR start "T__44"
	public final void mT__44() throws RecognitionException {
		try {
			int _type = T__44;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:22:7: ( '+' )
			// C.g:22:9: '+'
			{
			match('+'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__44"

	// $ANTLR start "T__45"
	public final void mT__45() throws RecognitionException {
		try {
			int _type = T__45;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:23:7: ( '++' )
			// C.g:23:9: '++'
			{
			match("++"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__45"

	// $ANTLR start "T__46"
	public final void mT__46() throws RecognitionException {
		try {
			int _type = T__46;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:24:7: ( '+=' )
			// C.g:24:9: '+='
			{
			match("+="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__46"

	// $ANTLR start "T__47"
	public final void mT__47() throws RecognitionException {
		try {
			int _type = T__47;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:25:7: ( ',' )
			// C.g:25:9: ','
			{
			match(','); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__47"

	// $ANTLR start "T__48"
	public final void mT__48() throws RecognitionException {
		try {
			int _type = T__48;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:26:7: ( '-' )
			// C.g:26:9: '-'
			{
			match('-'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__48"

	// $ANTLR start "T__49"
	public final void mT__49() throws RecognitionException {
		try {
			int _type = T__49;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:27:7: ( '--' )
			// C.g:27:9: '--'
			{
			match("--"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__49"

	// $ANTLR start "T__50"
	public final void mT__50() throws RecognitionException {
		try {
			int _type = T__50;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:28:7: ( '-=' )
			// C.g:28:9: '-='
			{
			match("-="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__50"

	// $ANTLR start "T__51"
	public final void mT__51() throws RecognitionException {
		try {
			int _type = T__51;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:29:7: ( '->' )
			// C.g:29:9: '->'
			{
			match("->"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__51"

	// $ANTLR start "T__52"
	public final void mT__52() throws RecognitionException {
		try {
			int _type = T__52;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:30:7: ( '.' )
			// C.g:30:9: '.'
			{
			match('.'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__52"

	// $ANTLR start "T__53"
	public final void mT__53() throws RecognitionException {
		try {
			int _type = T__53;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:31:7: ( '/' )
			// C.g:31:9: '/'
			{
			match('/'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__53"

	// $ANTLR start "T__54"
	public final void mT__54() throws RecognitionException {
		try {
			int _type = T__54;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:32:7: ( '/=' )
			// C.g:32:9: '/='
			{
			match("/="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__54"

	// $ANTLR start "T__55"
	public final void mT__55() throws RecognitionException {
		try {
			int _type = T__55;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:33:7: ( ';' )
			// C.g:33:9: ';'
			{
			match(';'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__55"

	// $ANTLR start "T__56"
	public final void mT__56() throws RecognitionException {
		try {
			int _type = T__56;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:34:7: ( '<' )
			// C.g:34:9: '<'
			{
			match('<'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__56"

	// $ANTLR start "T__57"
	public final void mT__57() throws RecognitionException {
		try {
			int _type = T__57;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:35:7: ( '<<' )
			// C.g:35:9: '<<'
			{
			match("<<"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__57"

	// $ANTLR start "T__58"
	public final void mT__58() throws RecognitionException {
		try {
			int _type = T__58;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:36:7: ( '<<=' )
			// C.g:36:9: '<<='
			{
			match("<<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__58"

	// $ANTLR start "T__59"
	public final void mT__59() throws RecognitionException {
		try {
			int _type = T__59;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:37:7: ( '<=' )
			// C.g:37:9: '<='
			{
			match("<="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__59"

	// $ANTLR start "T__60"
	public final void mT__60() throws RecognitionException {
		try {
			int _type = T__60;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:38:7: ( '=' )
			// C.g:38:9: '='
			{
			match('='); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__60"

	// $ANTLR start "T__61"
	public final void mT__61() throws RecognitionException {
		try {
			int _type = T__61;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:39:7: ( '==' )
			// C.g:39:9: '=='
			{
			match("=="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__61"

	// $ANTLR start "T__62"
	public final void mT__62() throws RecognitionException {
		try {
			int _type = T__62;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:40:7: ( '>' )
			// C.g:40:9: '>'
			{
			match('>'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__62"

	// $ANTLR start "T__63"
	public final void mT__63() throws RecognitionException {
		try {
			int _type = T__63;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:41:7: ( '>=' )
			// C.g:41:9: '>='
			{
			match(">="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__63"

	// $ANTLR start "T__64"
	public final void mT__64() throws RecognitionException {
		try {
			int _type = T__64;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:42:7: ( '>>' )
			// C.g:42:9: '>>'
			{
			match(">>"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__64"

	// $ANTLR start "T__65"
	public final void mT__65() throws RecognitionException {
		try {
			int _type = T__65;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:43:7: ( '>>=' )
			// C.g:43:9: '>>='
			{
			match(">>="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__65"

	// $ANTLR start "T__66"
	public final void mT__66() throws RecognitionException {
		try {
			int _type = T__66;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:44:7: ( '[' )
			// C.g:44:9: '['
			{
			match('['); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__66"

	// $ANTLR start "T__67"
	public final void mT__67() throws RecognitionException {
		try {
			int _type = T__67;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:45:7: ( ']' )
			// C.g:45:9: ']'
			{
			match(']'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__67"

	// $ANTLR start "T__68"
	public final void mT__68() throws RecognitionException {
		try {
			int _type = T__68;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:46:7: ( '^' )
			// C.g:46:9: '^'
			{
			match('^'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__68"

	// $ANTLR start "T__69"
	public final void mT__69() throws RecognitionException {
		try {
			int _type = T__69;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:47:7: ( '^=' )
			// C.g:47:9: '^='
			{
			match("^="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__69"

	// $ANTLR start "T__70"
	public final void mT__70() throws RecognitionException {
		try {
			int _type = T__70;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:48:7: ( 'break' )
			// C.g:48:9: 'break'
			{
			match("break"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__70"

	// $ANTLR start "T__71"
	public final void mT__71() throws RecognitionException {
		try {
			int _type = T__71;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:49:7: ( 'char' )
			// C.g:49:9: 'char'
			{
			match("char"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__71"

	// $ANTLR start "T__72"
	public final void mT__72() throws RecognitionException {
		try {
			int _type = T__72;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:50:7: ( 'continue' )
			// C.g:50:9: 'continue'
			{
			match("continue"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__72"

	// $ANTLR start "T__73"
	public final void mT__73() throws RecognitionException {
		try {
			int _type = T__73;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:51:7: ( 'else' )
			// C.g:51:9: 'else'
			{
			match("else"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__73"

	// $ANTLR start "T__74"
	public final void mT__74() throws RecognitionException {
		try {
			int _type = T__74;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:52:7: ( 'for' )
			// C.g:52:9: 'for'
			{
			match("for"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__74"

	// $ANTLR start "T__75"
	public final void mT__75() throws RecognitionException {
		try {
			int _type = T__75;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:53:7: ( 'if' )
			// C.g:53:9: 'if'
			{
			match("if"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__75"

	// $ANTLR start "T__76"
	public final void mT__76() throws RecognitionException {
		try {
			int _type = T__76;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:54:7: ( 'int' )
			// C.g:54:9: 'int'
			{
			match("int"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__76"

	// $ANTLR start "T__77"
	public final void mT__77() throws RecognitionException {
		try {
			int _type = T__77;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:55:7: ( 'return' )
			// C.g:55:9: 'return'
			{
			match("return"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__77"

	// $ANTLR start "T__78"
	public final void mT__78() throws RecognitionException {
		try {
			int _type = T__78;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:56:7: ( 'sizeof' )
			// C.g:56:9: 'sizeof'
			{
			match("sizeof"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__78"

	// $ANTLR start "T__79"
	public final void mT__79() throws RecognitionException {
		try {
			int _type = T__79;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:57:7: ( 'struct' )
			// C.g:57:9: 'struct'
			{
			match("struct"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__79"

	// $ANTLR start "T__80"
	public final void mT__80() throws RecognitionException {
		try {
			int _type = T__80;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:58:7: ( 'union' )
			// C.g:58:9: 'union'
			{
			match("union"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__80"

	// $ANTLR start "T__81"
	public final void mT__81() throws RecognitionException {
		try {
			int _type = T__81;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:59:7: ( 'void' )
			// C.g:59:9: 'void'
			{
			match("void"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__81"

	// $ANTLR start "T__82"
	public final void mT__82() throws RecognitionException {
		try {
			int _type = T__82;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:60:7: ( 'while' )
			// C.g:60:9: 'while'
			{
			match("while"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__82"

	// $ANTLR start "T__83"
	public final void mT__83() throws RecognitionException {
		try {
			int _type = T__83;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:61:7: ( '{' )
			// C.g:61:9: '{'
			{
			match('{'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__83"

	// $ANTLR start "T__84"
	public final void mT__84() throws RecognitionException {
		try {
			int _type = T__84;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:62:7: ( '|' )
			// C.g:62:9: '|'
			{
			match('|'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__84"

	// $ANTLR start "T__85"
	public final void mT__85() throws RecognitionException {
		try {
			int _type = T__85;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:63:7: ( '|=' )
			// C.g:63:9: '|='
			{
			match("|="); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__85"

	// $ANTLR start "T__86"
	public final void mT__86() throws RecognitionException {
		try {
			int _type = T__86;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:64:7: ( '||' )
			// C.g:64:9: '||'
			{
			match("||"); 

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__86"

	// $ANTLR start "T__87"
	public final void mT__87() throws RecognitionException {
		try {
			int _type = T__87;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:65:7: ( '}' )
			// C.g:65:9: '}'
			{
			match('}'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__87"

	// $ANTLR start "T__88"
	public final void mT__88() throws RecognitionException {
		try {
			int _type = T__88;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:66:7: ( '~' )
			// C.g:66:9: '~'
			{
			match('~'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "T__88"

	// $ANTLR start "IDENTIFIER"
	public final void mIDENTIFIER() throws RecognitionException {
		try {
			int _type = IDENTIFIER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:227:11: ( ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )* )
			// C.g:227:13: ( 'a' .. 'z' | 'A' .. 'Z' | '_' ) ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			{
			if ( (input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
				input.consume();
			}
			else {
				MismatchedSetException mse = new MismatchedSetException(null,input);
				recover(mse);
				throw mse;
			}
			// C.g:227:37: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '_' )*
			loop1:
			while (true) {
				int alt1=2;
				int LA1_0 = input.LA(1);
				if ( ((LA1_0 >= '0' && LA1_0 <= '9')||(LA1_0 >= 'A' && LA1_0 <= 'Z')||LA1_0=='_'||(LA1_0 >= 'a' && LA1_0 <= 'z')) ) {
					alt1=1;
				}

				switch (alt1) {
				case 1 :
					// C.g:
					{
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'Z')||input.LA(1)=='_'||(input.LA(1) >= 'a' && input.LA(1) <= 'z') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop1;
				}
			}

			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "IDENTIFIER"

	// $ANTLR start "INTEGER"
	public final void mINTEGER() throws RecognitionException {
		try {
			int _type = INTEGER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:230:8: ( ( '0' .. '9' )+ | ( '0x' | '0X' ) ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )+ )
			int alt5=2;
			int LA5_0 = input.LA(1);
			if ( (LA5_0=='0') ) {
				int LA5_1 = input.LA(2);
				if ( (LA5_1=='X'||LA5_1=='x') ) {
					alt5=2;
				}

				else {
					alt5=1;
				}

			}
			else if ( ((LA5_0 >= '1' && LA5_0 <= '9')) ) {
				alt5=1;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 5, 0, input);
				throw nvae;
			}

			switch (alt5) {
				case 1 :
					// C.g:230:10: ( '0' .. '9' )+
					{
					// C.g:230:10: ( '0' .. '9' )+
					int cnt2=0;
					loop2:
					while (true) {
						int alt2=2;
						int LA2_0 = input.LA(1);
						if ( ((LA2_0 >= '0' && LA2_0 <= '9')) ) {
							alt2=1;
						}

						switch (alt2) {
						case 1 :
							// C.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt2 >= 1 ) break loop2;
							EarlyExitException eee = new EarlyExitException(2, input);
							throw eee;
						}
						cnt2++;
					}

					}
					break;
				case 2 :
					// C.g:231:7: ( '0x' | '0X' ) ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )+
					{
					// C.g:231:7: ( '0x' | '0X' )
					int alt3=2;
					int LA3_0 = input.LA(1);
					if ( (LA3_0=='0') ) {
						int LA3_1 = input.LA(2);
						if ( (LA3_1=='x') ) {
							alt3=1;
						}
						else if ( (LA3_1=='X') ) {
							alt3=2;
						}

						else {
							int nvaeMark = input.mark();
							try {
								input.consume();
								NoViableAltException nvae =
									new NoViableAltException("", 3, 1, input);
								throw nvae;
							} finally {
								input.rewind(nvaeMark);
							}
						}

					}

					else {
						NoViableAltException nvae =
							new NoViableAltException("", 3, 0, input);
						throw nvae;
					}

					switch (alt3) {
						case 1 :
							// C.g:231:8: '0x'
							{
							match("0x"); 

							}
							break;
						case 2 :
							// C.g:231:15: '0X'
							{
							match("0X"); 

							}
							break;

					}

					// C.g:231:21: ( '0' .. '9' | 'a' .. 'f' | 'A' .. 'F' )+
					int cnt4=0;
					loop4:
					while (true) {
						int alt4=2;
						int LA4_0 = input.LA(1);
						if ( ((LA4_0 >= '0' && LA4_0 <= '9')||(LA4_0 >= 'A' && LA4_0 <= 'F')||(LA4_0 >= 'a' && LA4_0 <= 'f')) ) {
							alt4=1;
						}

						switch (alt4) {
						case 1 :
							// C.g:
							{
							if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'A' && input.LA(1) <= 'F')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
								input.consume();
							}
							else {
								MismatchedSetException mse = new MismatchedSetException(null,input);
								recover(mse);
								throw mse;
							}
							}
							break;

						default :
							if ( cnt4 >= 1 ) break loop4;
							EarlyExitException eee = new EarlyExitException(4, input);
							throw eee;
						}
						cnt4++;
					}

					}
					break;

			}
			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INTEGER"

	// $ANTLR start "CHARACTER"
	public final void mCHARACTER() throws RecognitionException {
		try {
			int _type = CHARACTER;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:234:10: ( '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\n' | ' ' | '\\\\0' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\x' ( '0' .. '9' | 'a' .. 'f' ) ( '0' .. '9' | 'a' .. 'f' ) ) '\\'' )
			// C.g:234:12: '\\'' ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\n' | ' ' | '\\\\0' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\x' ( '0' .. '9' | 'a' .. 'f' ) ( '0' .. '9' | 'a' .. 'f' ) ) '\\''
			{
			match('\''); 
			// C.g:234:17: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\n' | ' ' | '\\\\0' ( '0' .. '7' ) ( '0' .. '7' ) | '\\\\x' ( '0' .. '9' | 'a' .. 'f' ) ( '0' .. '9' | 'a' .. 'f' ) )
			int alt6=7;
			switch ( input.LA(1) ) {
			case 'a':
			case 'b':
			case 'c':
			case 'd':
			case 'e':
			case 'f':
			case 'g':
			case 'h':
			case 'i':
			case 'j':
			case 'k':
			case 'l':
			case 'm':
			case 'n':
			case 'o':
			case 'p':
			case 'q':
			case 'r':
			case 's':
			case 't':
			case 'u':
			case 'v':
			case 'w':
			case 'x':
			case 'y':
			case 'z':
				{
				alt6=1;
				}
				break;
			case 'A':
			case 'B':
			case 'C':
			case 'D':
			case 'E':
			case 'F':
			case 'G':
			case 'H':
			case 'I':
			case 'J':
			case 'K':
			case 'L':
			case 'M':
			case 'N':
			case 'O':
			case 'P':
			case 'Q':
			case 'R':
			case 'S':
			case 'T':
			case 'U':
			case 'V':
			case 'W':
			case 'X':
			case 'Y':
			case 'Z':
				{
				alt6=2;
				}
				break;
			case '0':
			case '1':
			case '2':
			case '3':
			case '4':
			case '5':
			case '6':
			case '7':
			case '8':
			case '9':
				{
				alt6=3;
				}
				break;
			case '\\':
				{
				switch ( input.LA(2) ) {
				case 'n':
					{
					alt6=4;
					}
					break;
				case '0':
					{
					alt6=6;
					}
					break;
				case 'x':
					{
					alt6=7;
					}
					break;
				default:
					int nvaeMark = input.mark();
					try {
						input.consume();
						NoViableAltException nvae =
							new NoViableAltException("", 6, 4, input);
						throw nvae;
					} finally {
						input.rewind(nvaeMark);
					}
				}
				}
				break;
			case ' ':
				{
				alt6=5;
				}
				break;
			default:
				NoViableAltException nvae =
					new NoViableAltException("", 6, 0, input);
				throw nvae;
			}
			switch (alt6) {
				case 1 :
					// C.g:234:18: 'a' .. 'z'
					{
					matchRange('a','z'); 
					}
					break;
				case 2 :
					// C.g:234:29: 'A' .. 'Z'
					{
					matchRange('A','Z'); 
					}
					break;
				case 3 :
					// C.g:234:40: '0' .. '9'
					{
					matchRange('0','9'); 
					}
					break;
				case 4 :
					// C.g:234:51: '\\\\n'
					{
					match("\\n"); 

					}
					break;
				case 5 :
					// C.g:234:58: ' '
					{
					match(' '); 
					}
					break;
				case 6 :
					// C.g:234:63: '\\\\0' ( '0' .. '7' ) ( '0' .. '7' )
					{
					match("\\0"); 

					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '7') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;
				case 7 :
					// C.g:234:93: '\\\\x' ( '0' .. '9' | 'a' .. 'f' ) ( '0' .. '9' | 'a' .. 'f' )
					{
					match("\\x"); 

					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					if ( (input.LA(1) >= '0' && input.LA(1) <= '9')||(input.LA(1) >= 'a' && input.LA(1) <= 'f') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

			}

			match('\''); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "CHARACTER"

	// $ANTLR start "STRING"
	public final void mSTRING() throws RecognitionException {
		try {
			int _type = STRING;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:237:7: ( '\"' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\' | '%' | ' ' | '-' | '>' | '!' | ',' | '.' | ':' | '<' | '(' | ')' | '#' | ';' | '*' | '{' | '}' | '=' | '+' | '\\'' )+ '\"' )
			// C.g:237:9: '\"' ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\' | '%' | ' ' | '-' | '>' | '!' | ',' | '.' | ':' | '<' | '(' | ')' | '#' | ';' | '*' | '{' | '}' | '=' | '+' | '\\'' )+ '\"'
			{
			match('\"'); 
			// C.g:237:13: ( options {greedy=false; } : 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '\\\\' | '%' | ' ' | '-' | '>' | '!' | ',' | '.' | ':' | '<' | '(' | ')' | '#' | ';' | '*' | '{' | '}' | '=' | '+' | '\\'' )+
			int cnt7=0;
			loop7:
			while (true) {
				int alt7=24;
				switch ( input.LA(1) ) {
				case 'a':
				case 'b':
				case 'c':
				case 'd':
				case 'e':
				case 'f':
				case 'g':
				case 'h':
				case 'i':
				case 'j':
				case 'k':
				case 'l':
				case 'm':
				case 'n':
				case 'o':
				case 'p':
				case 'q':
				case 'r':
				case 's':
				case 't':
				case 'u':
				case 'v':
				case 'w':
				case 'x':
				case 'y':
				case 'z':
					{
					alt7=1;
					}
					break;
				case 'A':
				case 'B':
				case 'C':
				case 'D':
				case 'E':
				case 'F':
				case 'G':
				case 'H':
				case 'I':
				case 'J':
				case 'K':
				case 'L':
				case 'M':
				case 'N':
				case 'O':
				case 'P':
				case 'Q':
				case 'R':
				case 'S':
				case 'T':
				case 'U':
				case 'V':
				case 'W':
				case 'X':
				case 'Y':
				case 'Z':
					{
					alt7=2;
					}
					break;
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					{
					alt7=3;
					}
					break;
				case '\\':
					{
					alt7=4;
					}
					break;
				case '%':
					{
					alt7=5;
					}
					break;
				case ' ':
					{
					alt7=6;
					}
					break;
				case '-':
					{
					alt7=7;
					}
					break;
				case '>':
					{
					alt7=8;
					}
					break;
				case '!':
					{
					alt7=9;
					}
					break;
				case ',':
					{
					alt7=10;
					}
					break;
				case '.':
					{
					alt7=11;
					}
					break;
				case ':':
					{
					alt7=12;
					}
					break;
				case '<':
					{
					alt7=13;
					}
					break;
				case '(':
					{
					alt7=14;
					}
					break;
				case ')':
					{
					alt7=15;
					}
					break;
				case '#':
					{
					alt7=16;
					}
					break;
				case ';':
					{
					alt7=17;
					}
					break;
				case '*':
					{
					alt7=18;
					}
					break;
				case '{':
					{
					alt7=19;
					}
					break;
				case '}':
					{
					alt7=20;
					}
					break;
				case '=':
					{
					alt7=21;
					}
					break;
				case '+':
					{
					alt7=22;
					}
					break;
				case '\'':
					{
					alt7=23;
					}
					break;
				case '\"':
					{
					alt7=24;
					}
					break;
				}
				switch (alt7) {
				case 1 :
					// C.g:237:41: 'a' .. 'z'
					{
					matchRange('a','z'); 
					}
					break;
				case 2 :
					// C.g:237:50: 'A' .. 'Z'
					{
					matchRange('A','Z'); 
					}
					break;
				case 3 :
					// C.g:237:59: '0' .. '9'
					{
					matchRange('0','9'); 
					}
					break;
				case 4 :
					// C.g:237:68: '\\\\'
					{
					match('\\'); 
					}
					break;
				case 5 :
					// C.g:237:73: '%'
					{
					match('%'); 
					}
					break;
				case 6 :
					// C.g:237:77: ' '
					{
					match(' '); 
					}
					break;
				case 7 :
					// C.g:237:81: '-'
					{
					match('-'); 
					}
					break;
				case 8 :
					// C.g:237:85: '>'
					{
					match('>'); 
					}
					break;
				case 9 :
					// C.g:237:89: '!'
					{
					match('!'); 
					}
					break;
				case 10 :
					// C.g:237:93: ','
					{
					match(','); 
					}
					break;
				case 11 :
					// C.g:237:97: '.'
					{
					match('.'); 
					}
					break;
				case 12 :
					// C.g:237:101: ':'
					{
					match(':'); 
					}
					break;
				case 13 :
					// C.g:237:105: '<'
					{
					match('<'); 
					}
					break;
				case 14 :
					// C.g:237:109: '('
					{
					match('('); 
					}
					break;
				case 15 :
					// C.g:237:113: ')'
					{
					match(')'); 
					}
					break;
				case 16 :
					// C.g:237:117: '#'
					{
					match('#'); 
					}
					break;
				case 17 :
					// C.g:237:121: ';'
					{
					match(';'); 
					}
					break;
				case 18 :
					// C.g:237:125: '*'
					{
					match('*'); 
					}
					break;
				case 19 :
					// C.g:237:129: '{'
					{
					match('{'); 
					}
					break;
				case 20 :
					// C.g:237:133: '}'
					{
					match('}'); 
					}
					break;
				case 21 :
					// C.g:237:137: '='
					{
					match('='); 
					}
					break;
				case 22 :
					// C.g:237:141: '+'
					{
					match('+'); 
					}
					break;
				case 23 :
					// C.g:237:145: '\\''
					{
					match('\''); 
					}
					break;

				default :
					if ( cnt7 >= 1 ) break loop7;
					EarlyExitException eee = new EarlyExitException(7, input);
					throw eee;
				}
				cnt7++;
			}

			match('\"'); 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "STRING"

	// $ANTLR start "INCLUDE"
	public final void mINCLUDE() throws RecognitionException {
		try {
			int _type = INCLUDE;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:240:8: ( '#' (~ ( '\\r' | '\\n' ) )* ( '\\r\\n' | '\\r' | '\\n' ) )
			// C.g:240:10: '#' (~ ( '\\r' | '\\n' ) )* ( '\\r\\n' | '\\r' | '\\n' )
			{
			match('#'); 
			// C.g:240:14: (~ ( '\\r' | '\\n' ) )*
			loop8:
			while (true) {
				int alt8=2;
				int LA8_0 = input.LA(1);
				if ( ((LA8_0 >= '\u0000' && LA8_0 <= '\t')||(LA8_0 >= '\u000B' && LA8_0 <= '\f')||(LA8_0 >= '\u000E' && LA8_0 <= '\uFFFF')) ) {
					alt8=1;
				}

				switch (alt8) {
				case 1 :
					// C.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop8;
				}
			}

			// C.g:240:30: ( '\\r\\n' | '\\r' | '\\n' )
			int alt9=3;
			int LA9_0 = input.LA(1);
			if ( (LA9_0=='\r') ) {
				int LA9_1 = input.LA(2);
				if ( (LA9_1=='\n') ) {
					alt9=1;
				}

				else {
					alt9=2;
				}

			}
			else if ( (LA9_0=='\n') ) {
				alt9=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 9, 0, input);
				throw nvae;
			}

			switch (alt9) {
				case 1 :
					// C.g:240:31: '\\r\\n'
					{
					match("\r\n"); 

					}
					break;
				case 2 :
					// C.g:240:40: '\\r'
					{
					match('\r'); 
					}
					break;
				case 3 :
					// C.g:240:47: '\\n'
					{
					match('\n'); 
					}
					break;

			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "INCLUDE"

	// $ANTLR start "LINE_COMMENT"
	public final void mLINE_COMMENT() throws RecognitionException {
		try {
			int _type = LINE_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:243:13: ( '//' (~ ( '\\r' | '\\n' ) )* ( '\\r\\n' | '\\r' | '\\n' ) )
			// C.g:243:15: '//' (~ ( '\\r' | '\\n' ) )* ( '\\r\\n' | '\\r' | '\\n' )
			{
			match("//"); 

			// C.g:243:20: (~ ( '\\r' | '\\n' ) )*
			loop10:
			while (true) {
				int alt10=2;
				int LA10_0 = input.LA(1);
				if ( ((LA10_0 >= '\u0000' && LA10_0 <= '\t')||(LA10_0 >= '\u000B' && LA10_0 <= '\f')||(LA10_0 >= '\u000E' && LA10_0 <= '\uFFFF')) ) {
					alt10=1;
				}

				switch (alt10) {
				case 1 :
					// C.g:
					{
					if ( (input.LA(1) >= '\u0000' && input.LA(1) <= '\t')||(input.LA(1) >= '\u000B' && input.LA(1) <= '\f')||(input.LA(1) >= '\u000E' && input.LA(1) <= '\uFFFF') ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					break loop10;
				}
			}

			// C.g:243:36: ( '\\r\\n' | '\\r' | '\\n' )
			int alt11=3;
			int LA11_0 = input.LA(1);
			if ( (LA11_0=='\r') ) {
				int LA11_1 = input.LA(2);
				if ( (LA11_1=='\n') ) {
					alt11=1;
				}

				else {
					alt11=2;
				}

			}
			else if ( (LA11_0=='\n') ) {
				alt11=3;
			}

			else {
				NoViableAltException nvae =
					new NoViableAltException("", 11, 0, input);
				throw nvae;
			}

			switch (alt11) {
				case 1 :
					// C.g:243:37: '\\r\\n'
					{
					match("\r\n"); 

					}
					break;
				case 2 :
					// C.g:243:46: '\\r'
					{
					match('\r'); 
					}
					break;
				case 3 :
					// C.g:243:53: '\\n'
					{
					match('\n'); 
					}
					break;

			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "LINE_COMMENT"

	// $ANTLR start "BLOCK_COMMENT"
	public final void mBLOCK_COMMENT() throws RecognitionException {
		try {
			int _type = BLOCK_COMMENT;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:246:14: ( '/*' ( options {greedy=false; } : . )* '*/' )
			// C.g:246:16: '/*' ( options {greedy=false; } : . )* '*/'
			{
			match("/*"); 

			// C.g:246:21: ( options {greedy=false; } : . )*
			loop12:
			while (true) {
				int alt12=2;
				int LA12_0 = input.LA(1);
				if ( (LA12_0=='*') ) {
					int LA12_1 = input.LA(2);
					if ( (LA12_1=='/') ) {
						alt12=2;
					}
					else if ( ((LA12_1 >= '\u0000' && LA12_1 <= '.')||(LA12_1 >= '0' && LA12_1 <= '\uFFFF')) ) {
						alt12=1;
					}

				}
				else if ( ((LA12_0 >= '\u0000' && LA12_0 <= ')')||(LA12_0 >= '+' && LA12_0 <= '\uFFFF')) ) {
					alt12=1;
				}

				switch (alt12) {
				case 1 :
					// C.g:246:50: .
					{
					matchAny(); 
					}
					break;

				default :
					break loop12;
				}
			}

			match("*/"); 

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "BLOCK_COMMENT"

	// $ANTLR start "WS"
	public final void mWS() throws RecognitionException {
		try {
			int _type = WS;
			int _channel = DEFAULT_TOKEN_CHANNEL;
			// C.g:249:3: ( ( ' ' | '\\t' | '\\n' | '\\r' )+ )
			// C.g:249:5: ( ' ' | '\\t' | '\\n' | '\\r' )+
			{
			// C.g:249:5: ( ' ' | '\\t' | '\\n' | '\\r' )+
			int cnt13=0;
			loop13:
			while (true) {
				int alt13=2;
				int LA13_0 = input.LA(1);
				if ( ((LA13_0 >= '\t' && LA13_0 <= '\n')||LA13_0=='\r'||LA13_0==' ') ) {
					alt13=1;
				}

				switch (alt13) {
				case 1 :
					// C.g:
					{
					if ( (input.LA(1) >= '\t' && input.LA(1) <= '\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
						input.consume();
					}
					else {
						MismatchedSetException mse = new MismatchedSetException(null,input);
						recover(mse);
						throw mse;
					}
					}
					break;

				default :
					if ( cnt13 >= 1 ) break loop13;
					EarlyExitException eee = new EarlyExitException(13, input);
					throw eee;
				}
				cnt13++;
			}

			 _channel=HIDDEN; 
			}

			state.type = _type;
			state.channel = _channel;
		}
		finally {
			// do for sure before leaving
		}
	}
	// $ANTLR end "WS"

	@Override
	public void mTokens() throws RecognitionException {
		// C.g:1:8: ( T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | IDENTIFIER | INTEGER | CHARACTER | STRING | INCLUDE | LINE_COMMENT | BLOCK_COMMENT | WS )
		int alt14=64;
		alt14 = dfa14.predict(input);
		switch (alt14) {
			case 1 :
				// C.g:1:10: T__33
				{
				mT__33(); 

				}
				break;
			case 2 :
				// C.g:1:16: T__34
				{
				mT__34(); 

				}
				break;
			case 3 :
				// C.g:1:22: T__35
				{
				mT__35(); 

				}
				break;
			case 4 :
				// C.g:1:28: T__36
				{
				mT__36(); 

				}
				break;
			case 5 :
				// C.g:1:34: T__37
				{
				mT__37(); 

				}
				break;
			case 6 :
				// C.g:1:40: T__38
				{
				mT__38(); 

				}
				break;
			case 7 :
				// C.g:1:46: T__39
				{
				mT__39(); 

				}
				break;
			case 8 :
				// C.g:1:52: T__40
				{
				mT__40(); 

				}
				break;
			case 9 :
				// C.g:1:58: T__41
				{
				mT__41(); 

				}
				break;
			case 10 :
				// C.g:1:64: T__42
				{
				mT__42(); 

				}
				break;
			case 11 :
				// C.g:1:70: T__43
				{
				mT__43(); 

				}
				break;
			case 12 :
				// C.g:1:76: T__44
				{
				mT__44(); 

				}
				break;
			case 13 :
				// C.g:1:82: T__45
				{
				mT__45(); 

				}
				break;
			case 14 :
				// C.g:1:88: T__46
				{
				mT__46(); 

				}
				break;
			case 15 :
				// C.g:1:94: T__47
				{
				mT__47(); 

				}
				break;
			case 16 :
				// C.g:1:100: T__48
				{
				mT__48(); 

				}
				break;
			case 17 :
				// C.g:1:106: T__49
				{
				mT__49(); 

				}
				break;
			case 18 :
				// C.g:1:112: T__50
				{
				mT__50(); 

				}
				break;
			case 19 :
				// C.g:1:118: T__51
				{
				mT__51(); 

				}
				break;
			case 20 :
				// C.g:1:124: T__52
				{
				mT__52(); 

				}
				break;
			case 21 :
				// C.g:1:130: T__53
				{
				mT__53(); 

				}
				break;
			case 22 :
				// C.g:1:136: T__54
				{
				mT__54(); 

				}
				break;
			case 23 :
				// C.g:1:142: T__55
				{
				mT__55(); 

				}
				break;
			case 24 :
				// C.g:1:148: T__56
				{
				mT__56(); 

				}
				break;
			case 25 :
				// C.g:1:154: T__57
				{
				mT__57(); 

				}
				break;
			case 26 :
				// C.g:1:160: T__58
				{
				mT__58(); 

				}
				break;
			case 27 :
				// C.g:1:166: T__59
				{
				mT__59(); 

				}
				break;
			case 28 :
				// C.g:1:172: T__60
				{
				mT__60(); 

				}
				break;
			case 29 :
				// C.g:1:178: T__61
				{
				mT__61(); 

				}
				break;
			case 30 :
				// C.g:1:184: T__62
				{
				mT__62(); 

				}
				break;
			case 31 :
				// C.g:1:190: T__63
				{
				mT__63(); 

				}
				break;
			case 32 :
				// C.g:1:196: T__64
				{
				mT__64(); 

				}
				break;
			case 33 :
				// C.g:1:202: T__65
				{
				mT__65(); 

				}
				break;
			case 34 :
				// C.g:1:208: T__66
				{
				mT__66(); 

				}
				break;
			case 35 :
				// C.g:1:214: T__67
				{
				mT__67(); 

				}
				break;
			case 36 :
				// C.g:1:220: T__68
				{
				mT__68(); 

				}
				break;
			case 37 :
				// C.g:1:226: T__69
				{
				mT__69(); 

				}
				break;
			case 38 :
				// C.g:1:232: T__70
				{
				mT__70(); 

				}
				break;
			case 39 :
				// C.g:1:238: T__71
				{
				mT__71(); 

				}
				break;
			case 40 :
				// C.g:1:244: T__72
				{
				mT__72(); 

				}
				break;
			case 41 :
				// C.g:1:250: T__73
				{
				mT__73(); 

				}
				break;
			case 42 :
				// C.g:1:256: T__74
				{
				mT__74(); 

				}
				break;
			case 43 :
				// C.g:1:262: T__75
				{
				mT__75(); 

				}
				break;
			case 44 :
				// C.g:1:268: T__76
				{
				mT__76(); 

				}
				break;
			case 45 :
				// C.g:1:274: T__77
				{
				mT__77(); 

				}
				break;
			case 46 :
				// C.g:1:280: T__78
				{
				mT__78(); 

				}
				break;
			case 47 :
				// C.g:1:286: T__79
				{
				mT__79(); 

				}
				break;
			case 48 :
				// C.g:1:292: T__80
				{
				mT__80(); 

				}
				break;
			case 49 :
				// C.g:1:298: T__81
				{
				mT__81(); 

				}
				break;
			case 50 :
				// C.g:1:304: T__82
				{
				mT__82(); 

				}
				break;
			case 51 :
				// C.g:1:310: T__83
				{
				mT__83(); 

				}
				break;
			case 52 :
				// C.g:1:316: T__84
				{
				mT__84(); 

				}
				break;
			case 53 :
				// C.g:1:322: T__85
				{
				mT__85(); 

				}
				break;
			case 54 :
				// C.g:1:328: T__86
				{
				mT__86(); 

				}
				break;
			case 55 :
				// C.g:1:334: T__87
				{
				mT__87(); 

				}
				break;
			case 56 :
				// C.g:1:340: T__88
				{
				mT__88(); 

				}
				break;
			case 57 :
				// C.g:1:346: IDENTIFIER
				{
				mIDENTIFIER(); 

				}
				break;
			case 58 :
				// C.g:1:357: INTEGER
				{
				mINTEGER(); 

				}
				break;
			case 59 :
				// C.g:1:365: CHARACTER
				{
				mCHARACTER(); 

				}
				break;
			case 60 :
				// C.g:1:375: STRING
				{
				mSTRING(); 

				}
				break;
			case 61 :
				// C.g:1:382: INCLUDE
				{
				mINCLUDE(); 

				}
				break;
			case 62 :
				// C.g:1:390: LINE_COMMENT
				{
				mLINE_COMMENT(); 

				}
				break;
			case 63 :
				// C.g:1:403: BLOCK_COMMENT
				{
				mBLOCK_COMMENT(); 

				}
				break;
			case 64 :
				// C.g:1:417: WS
				{
				mWS(); 

				}
				break;

		}
	}


	protected DFA14 dfa14 = new DFA14(this);
	static final String DFA14_eotS =
		"\1\uffff\1\50\1\52\1\55\2\uffff\1\57\1\62\1\uffff\1\66\1\uffff\1\72\1"+
		"\uffff\1\75\1\77\1\102\2\uffff\1\104\12\41\1\uffff\1\124\34\uffff\1\126"+
		"\5\uffff\1\130\3\uffff\5\41\1\136\7\41\7\uffff\4\41\1\152\1\uffff\1\153"+
		"\7\41\1\163\1\41\1\165\2\uffff\4\41\1\172\1\41\1\174\1\uffff\1\41\1\uffff"+
		"\3\41\1\u0081\1\uffff\1\u0082\1\uffff\1\41\1\u0084\1\u0085\1\u0086\2\uffff"+
		"\1\41\3\uffff\1\u0088\1\uffff";
	static final String DFA14_eofS =
		"\u0089\uffff";
	static final String DFA14_minS =
		"\1\11\2\75\1\46\2\uffff\1\75\1\53\1\uffff\1\55\1\uffff\1\52\1\uffff\1"+
		"\74\2\75\2\uffff\1\75\1\162\1\150\1\154\1\157\1\146\1\145\1\151\1\156"+
		"\1\157\1\150\1\uffff\1\75\34\uffff\1\75\5\uffff\1\75\3\uffff\1\145\1\141"+
		"\1\156\1\163\1\162\1\60\2\164\1\172\1\162\3\151\7\uffff\1\141\1\162\1"+
		"\164\1\145\1\60\1\uffff\1\60\1\165\1\145\1\165\1\157\1\144\1\154\1\153"+
		"\1\60\1\151\1\60\2\uffff\1\162\1\157\1\143\1\156\1\60\1\145\1\60\1\uffff"+
		"\1\156\1\uffff\1\156\1\146\1\164\1\60\1\uffff\1\60\1\uffff\1\165\3\60"+
		"\2\uffff\1\145\3\uffff\1\60\1\uffff";
	static final String DFA14_maxS =
		"\1\176\3\75\2\uffff\2\75\1\uffff\1\76\1\uffff\1\75\1\uffff\2\75\1\76\2"+
		"\uffff\1\75\1\162\1\157\1\154\1\157\1\156\1\145\1\164\1\156\1\157\1\150"+
		"\1\uffff\1\174\34\uffff\1\75\5\uffff\1\75\3\uffff\1\145\1\141\1\156\1"+
		"\163\1\162\1\172\2\164\1\172\1\162\3\151\7\uffff\1\141\1\162\1\164\1\145"+
		"\1\172\1\uffff\1\172\1\165\1\145\1\165\1\157\1\144\1\154\1\153\1\172\1"+
		"\151\1\172\2\uffff\1\162\1\157\1\143\1\156\1\172\1\145\1\172\1\uffff\1"+
		"\156\1\uffff\1\156\1\146\1\164\1\172\1\uffff\1\172\1\uffff\1\165\3\172"+
		"\2\uffff\1\145\3\uffff\1\172\1\uffff";
	static final String DFA14_acceptS =
		"\4\uffff\1\10\1\11\2\uffff\1\17\1\uffff\1\24\1\uffff\1\27\3\uffff\1\42"+
		"\1\43\13\uffff\1\63\1\uffff\1\67\1\70\1\71\1\72\1\73\1\74\1\75\1\100\1"+
		"\2\1\1\1\4\1\3\1\5\1\7\1\6\1\13\1\12\1\15\1\16\1\14\1\21\1\22\1\23\1\20"+
		"\1\26\1\76\1\77\1\25\1\uffff\1\33\1\30\1\35\1\34\1\37\1\uffff\1\36\1\45"+
		"\1\44\15\uffff\1\65\1\66\1\64\1\32\1\31\1\41\1\40\5\uffff\1\53\13\uffff"+
		"\1\52\1\54\7\uffff\1\47\1\uffff\1\51\4\uffff\1\61\1\uffff\1\46\4\uffff"+
		"\1\60\1\62\1\uffff\1\55\1\56\1\57\1\uffff\1\50";
	static final String DFA14_specialS =
		"\u0089\uffff}>";
	static final String[] DFA14_transitionS = {
			"\2\46\2\uffff\1\46\22\uffff\1\46\1\1\1\44\1\45\1\uffff\1\2\1\3\1\43\1"+
			"\4\1\5\1\6\1\7\1\10\1\11\1\12\1\13\12\42\1\uffff\1\14\1\15\1\16\1\17"+
			"\2\uffff\32\41\1\20\1\uffff\1\21\1\22\1\41\1\uffff\1\41\1\23\1\24\1\41"+
			"\1\25\1\26\2\41\1\27\10\41\1\30\1\31\1\41\1\32\1\33\1\34\3\41\1\35\1"+
			"\36\1\37\1\40",
			"\1\47",
			"\1\51",
			"\1\53\26\uffff\1\54",
			"",
			"",
			"\1\56",
			"\1\60\21\uffff\1\61",
			"",
			"\1\63\17\uffff\1\64\1\65",
			"",
			"\1\71\4\uffff\1\70\15\uffff\1\67",
			"",
			"\1\73\1\74",
			"\1\76",
			"\1\100\1\101",
			"",
			"",
			"\1\103",
			"\1\105",
			"\1\106\6\uffff\1\107",
			"\1\110",
			"\1\111",
			"\1\112\7\uffff\1\113",
			"\1\114",
			"\1\115\12\uffff\1\116",
			"\1\117",
			"\1\120",
			"\1\121",
			"",
			"\1\122\76\uffff\1\123",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\125",
			"",
			"",
			"",
			"",
			"",
			"\1\127",
			"",
			"",
			"",
			"\1\131",
			"\1\132",
			"\1\133",
			"\1\134",
			"\1\135",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\137",
			"\1\140",
			"\1\141",
			"\1\142",
			"\1\143",
			"\1\144",
			"\1\145",
			"",
			"",
			"",
			"",
			"",
			"",
			"",
			"\1\146",
			"\1\147",
			"\1\150",
			"\1\151",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\154",
			"\1\155",
			"\1\156",
			"\1\157",
			"\1\160",
			"\1\161",
			"\1\162",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\164",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"",
			"\1\166",
			"\1\167",
			"\1\170",
			"\1\171",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\1\173",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\175",
			"",
			"\1\176",
			"\1\177",
			"\1\u0080",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"\1\u0083",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			"",
			"",
			"\1\u0087",
			"",
			"",
			"",
			"\12\41\7\uffff\32\41\4\uffff\1\41\1\uffff\32\41",
			""
	};

	static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
	static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
	static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
	static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
	static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
	static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
	static final short[][] DFA14_transition;

	static {
		int numStates = DFA14_transitionS.length;
		DFA14_transition = new short[numStates][];
		for (int i=0; i<numStates; i++) {
			DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
		}
	}

	protected class DFA14 extends DFA {

		public DFA14(BaseRecognizer recognizer) {
			this.recognizer = recognizer;
			this.decisionNumber = 14;
			this.eot = DFA14_eot;
			this.eof = DFA14_eof;
			this.min = DFA14_min;
			this.max = DFA14_max;
			this.accept = DFA14_accept;
			this.special = DFA14_special;
			this.transition = DFA14_transition;
		}
		@Override
		public String getDescription() {
			return "1:1: Tokens : ( T__33 | T__34 | T__35 | T__36 | T__37 | T__38 | T__39 | T__40 | T__41 | T__42 | T__43 | T__44 | T__45 | T__46 | T__47 | T__48 | T__49 | T__50 | T__51 | T__52 | T__53 | T__54 | T__55 | T__56 | T__57 | T__58 | T__59 | T__60 | T__61 | T__62 | T__63 | T__64 | T__65 | T__66 | T__67 | T__68 | T__69 | T__70 | T__71 | T__72 | T__73 | T__74 | T__75 | T__76 | T__77 | T__78 | T__79 | T__80 | T__81 | T__82 | T__83 | T__84 | T__85 | T__86 | T__87 | T__88 | IDENTIFIER | INTEGER | CHARACTER | STRING | INCLUDE | LINE_COMMENT | BLOCK_COMMENT | WS );";
		}
	}

}
