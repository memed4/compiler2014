grammar C;

options {
	output=AST;
	backtrack=true;
	memoize = true;
    language=Java;
}

tokens {
SPDECL;
PDECL;
IDECL;
FUNC;
FUNCDECLARATOR;
DECLARATOR;
EXPRESSION;
ARRAYASSIGN;
INDEX;
BLOCK;
IF;
ELSE;
FORBLOCK;
WHILEBLOCK;
RETURN;
CALL;
STRUCTPTR;
MEMBER;
CAST;
BLANK;
BLANKSTATEMENT;
}

@header {
	package appetizer.syntactic;
}

@lexer::header {
	package appetizer.syntactic;
}

program:  ( declaration | function_definition )+
       ;
       
declaration: (type_specifier init_declarators? ';') -> ^(IDECL type_specifier init_declarators?)
           ; 

function_definition: (type_specifier plain_declarator '(' parameters? ')' compound_statement) -> ^(FUNC type_specifier plain_declarator parameters? compound_statement) 
                   ; 

parameters: plain_declaration (','! plain_declaration)*        
          ; 

declarators: declarator (','! declarator)* 
           ; 

init_declarators: init_declarator (','! init_declarator)* 
		; 

init_declarator: declarator^ '=' initializer
               | declarator  
	           ; 

initializer: assignment_expression 
           | ('{' initializer (',' initializer)* '}') -> ^(ARRAYASSIGN initializer (initializer)*)
           ; 

type_specifier: 'void' 
              | 'char' 
              | 'int' 
              | (struct_or_union IDENTIFIER? '{' plain_declarations '}' ) -> ^(struct_or_union IDENTIFIER? plain_declarations)
              | (struct_or_union IDENTIFIER) -> ^(struct_or_union IDENTIFIER) 
              ; 

struct_or_union: 'struct' 
               | 'union' 
               ; 

plain_declarations: ((type_specifier declarators ';')+) -> ^(PDECL type_specifier declarators)+
                  ;

plain_declaration: (type_specifier declarator) -> ^(SPDECL type_specifier declarator) 
                 ; 

declarator: (plain_declarator '(' parameters? ')') -> ^(FUNCDECLARATOR plain_declarator parameters?) 
          | (plain_declarator ('[' constant_expression ']')*) -> ^(DECLARATOR plain_declarator ^(INDEX constant_expression)*)
          ; 

plain_declarator: '*'* IDENTIFIER
                ; 

/* Statements */ 
statement: expression_statement 
         | compound_statement 
         | selection_statement 
         | iteration_statement 
         | jump_statement 
         ; 

expression_statement: ';' -> BLANKSTATEMENT
				    | (expression+ ';') -> ^(EXPRESSION expression)+
                    ; 

compound_statement: ('{' declaration* statement* '}') -> ^(BLOCK declaration* statement* ) 
                  ; 

selection_statement: ('if' '(' expression ')' statement ('else' statement)? ) -> ^(IF expression statement ^(ELSE statement)? )  
                    ;

iteration_statement: ('while' '(' expression ')' statement) -> ^(WHILEBLOCK expression statement)
					| ('for' '(' first=expression ';' ';' ')' statement) -> ^(FORBLOCK $first BLANK BLANK statement)
					| ('for' '(' ';' second=expression ';' ')' statement) -> ^(FORBLOCK BLANK $second BLANK statement)
					| ('for' '(' ';' ';' third=expression ')' statement) -> ^(FORBLOCK BLANK BLANK $third statement)
					|  ('for' '(' first=expression ';' second=expression ';' ')' statement) -> ^(FORBLOCK $first $second BLANK statement)
                   |  ('for' '(' ';' second=expression ';' third=expression ')' statement) -> ^(FORBLOCK BLANK $second $third statement)
                   | ('for' '(' first=expression ';' ';' third=expression ')' statement) -> ^(FORBLOCK $first BLANK $third statement)
				   | ('for' '(' first=expression ';' second=expression ';' third=expression ')' statement) -> ^(FORBLOCK $first $second $third statement) 
                   ; 

jump_statement: 'continue' ';'! 
              | 'break' ';'! 
              | 'return' expression? ';' -> ^(RETURN expression?) 
              ; 

/* Expressions */ 
expression: assignment_expression (','! assignment_expression)* 
          ; 

assignment_expression: unary_expression assignment_operator^ assignment_expression  
                     | logical_or_expression 
                     ; 

assignment_operator: '=' | '*=' | '/=' | '%=' | '+=' | '-=' | '<<=' | '>>=' | '&=' | '^=' | '|=' 
                   ; 

constant_expression: logical_or_expression 
                   ; 

logical_or_expression: logical_and_expression ('||'^ logical_and_expression)* 
                     ; 

logical_and_expression: inclusive_or_expression ('&&'^ inclusive_or_expression)* 
                      ; 

inclusive_or_expression: exclusive_or_expression ('|'^ exclusive_or_expression)* 
                       ; 

exclusive_or_expression: and_expression ('^'^ and_expression)* 
                       ; 

and_expression: equality_expression ('&'^ equality_expression)* 
              ; 

equality_expression: relational_expression (equality_operator^ relational_expression)* 
                   ; 

equality_operator: '==' | '!='
                 ;

relational_expression: shift_expression (relational_operator^ shift_expression)*
                     ;

relational_operator: '<' | '>' | '<=' | '>='
                   ;

shift_expression: additive_expression (shift_operator^ additive_expression)*
                ;

shift_operator: '<<' | '>>'
              ;

additive_expression: multiplicative_expression (additive_operator^ multiplicative_expression)*
                   ;

additive_operator: '+' | '-'
                 ;

multiplicative_expression: cast_expression (multiplicative_operator^ cast_expression)*
                         ;

multiplicative_operator: '*' | '/' | '%'
                       ;

cast_expression: unary_expression
               | ('(' type_name ')' cast_expression) -> ^(CAST type_name cast_expression)
               ;
type_name: type_specifier '*'*
		 ;
		 
unary_expression: postfix_expression
                | '++'^ unary_expression
                | '--'^ unary_expression
                | unary_operator^ cast_expression
                | 'sizeof'^ unary_expression
                | 'sizeof'^ '('! type_name ')'!
                ;

unary_operator: '&' | '*' | '+' | '-' | '~' | '!'
              ;

postfix_expression: primary_expression^ postfix+
  	          | primary_expression
                  ;

postfix: ('[' expression ']') -> ^(INDEX expression) 
       | ('(' arguments? ')') -> ^(CALL arguments?)
       | ('.' IDENTIFIER) -> ^(MEMBER IDENTIFIER)
       | '->' IDENTIFIER -> ^(STRUCTPTR IDENTIFIER)
       | '++' 
       | '--'
       ;

arguments: assignment_expression (','! assignment_expression)*
         ;

primary_expression
		  : IDENTIFIER
                  | constant
                  | STRING 
		  | '('! expression ')'!
                  ;


constant: INTEGER
        | CHARACTER
        ;

IDENTIFIER: ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
          ;

INTEGER: ('0'..'9')+ 
	   | ('0x' | '0X') ('0'..'9'|'a'..'f'|'A'..'F')+
       ;

CHARACTER: '\'' ('a'..'z' | 'A'..'Z' | '0'..'9' | '\\n' |' '| '\\0' ('0'..'7') ('0'..'7') | '\\x' ('0'..'9'|'a'..'f') ('0'..'9'|'a'..'f')) '\''
         ;

STRING: '"' (options { greedy=false; }: 'a'..'z'|'A'..'Z'|'0'..'9'|'\\'|'%'|' '|'-'|'>'|'!'|','|'.'|':'|'<'|'('|')'|'#'|';'|'*'|'{'|'}'|'='|'+'|'\'')+ '"'
      ;

INCLUDE: '#' ~('\r' | '\n')* ('\r\n' | '\r' | '\n') { $channel=HIDDEN; }
       ;

LINE_COMMENT: '//' ~('\r' | '\n')* ('\r\n' | '\r' | '\n') { $channel=HIDDEN; }
            ;

BLOCK_COMMENT: '/*' ( options { greedy=false; }: .)* '*/' { $channel=HIDDEN; }
	     ;

WS: ( ' ' | '\t' | '\n' | '\r' )+ { $channel=HIDDEN; }
  ;
