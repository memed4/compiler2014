package appetizer.interform;

public class ConstString extends Oprand {
	String s;
	
	public ConstString(String s) {
		this.s = s;
	}
	
	public String getIR() {
		return "\"" + this.s + "\"";
	}
}
