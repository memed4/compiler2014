package appetizer.interform;

import appetizer.environ.*;

public class MemLoc {
	private boolean global;
	private boolean args;
	private int num;
	private Type type;
	public int offset;
	
	public MemLoc(int i, Type type, boolean args, boolean global) {
		this.num = i;
		this.type = type;
		this.args = args;
		this.global = global;
	}
	
	public Type getType() {
		return this.type;
	}
	
	public boolean isArgs() {
		return this.args;
	}
	
	public boolean isGlobal() {
		return this.global;
	}
	
	public int getNum() {
		return this.num;
	}
	
	public void setGlobal(boolean b) {
		this.global = b;
	}
	
	public void setType(Type t) {
		this.type = t;
	}
}
