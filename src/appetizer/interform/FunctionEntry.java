package appetizer.interform;

import java.util.HashMap;
import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import appetizer.environ.SymbolInfo;

public class FunctionEntry {
	public Quad fbegin, fend;
	public Quad fexit;
	String fName;
	public QuadChain quadChain;
	int argnum;
	private List<MemLoc>lPtr = new ArrayList<MemLoc>();
	public List<MemLoc>argPtr = new ArrayList<MemLoc>();
	boolean isMain;
	public int minreg = 2147483647, maxreg = 0;
	public HashMap<Integer, Register> regMap = new HashMap<Integer, Register>();
	public int stacksize;
	public int innerArgSpace;
	public HashMap<Integer, Quad> regbegin = new HashMap<Integer, Quad>();
	public HashMap<Integer, Quad> regend = new HashMap<Integer, Quad>();
	public Set<Integer> waitforallo = new HashSet<Integer>();
	public HashMap<Integer, Integer> allo = new HashMap<Integer, Integer>();
	public HashMap<Integer, Integer> regbegi = new HashMap<Integer, Integer>();
	public HashMap<Integer, Integer> regendi = new HashMap<Integer, Integer>();
	public HashMap<Integer, Integer> busyto = new HashMap<Integer, Integer>();
	public Set<Integer> occupied = new HashSet<Integer>();
	public Label fLabel = null;
	
	public FunctionEntry(int i, int j, String fName) {
		Label lbegin = new Label(i); 
		Label lend = new Label(j);
		this.fbegin = new Quad(null, lbegin, null, null);
		this.fend = new Quad(null, lend, null, null);
		this.fName = fName;
		if (this.fName.equals("main")) {
			this.isMain = true;
		} else {
			this.isMain = false;
		}
		quadChain = new QuadChain();
		quadChain.addQuad(this.fbegin);
		quadChain.addQuad(this.fend);
		this.argnum = 0;
	}
	
	public void setFuncLabel(HashMap<String, FunctionEntry> map) {
		quadChain.setFuncLabel(map);
	}
	
	public int getInnerArgSpace() {
		innerArgSpace = quadChain.getInnerArgSpace();
		return innerArgSpace;
	}
	
	private int getAlignOffset(int i) {
		int tmp = i;
		tmp += 3;
		tmp >>= 2;
		tmp <<= 2;
		return tmp;
	}
	
	public int argsize() {
		int x = 0;
		for (int i = 0; i < argPtr.size(); ++i) {
			//x += getAlignOffset(argPtr.get(i).getType().getSize());
			x += 4;
		}
		return x;
	}
	
	public Quad getfbegin(){
		return this.fbegin;
	}
	
	public void printIR() {
		this.quadChain.printIR();
	}
	
	public void printASM(){
		this.quadChain.printASM(this);
	}
	
	public String getName() {
		return this.fName;
	}
	
	public int getargnum() {
		return this.argnum;
	}
	
	public void addQuad(Quad quad) {
		if (quad.oprand1 instanceof Register) {
			if (!((Register)quad.oprand1).getMemloc().isArgs() && !((Register)quad.oprand1).getMemloc().isGlobal()) {
				int tmp = ((Register)quad.oprand1).getMemloc().getNum();
				if (tmp < minreg)
					minreg = tmp;
				if (tmp > maxreg)
					maxreg = tmp;
				if (!this.regMap.containsKey(tmp))
					this.regMap.put(tmp, ((Register)quad.oprand1));
			}
		}
		if (quad.oprand2 instanceof Register) {
			if (!((Register)quad.oprand2).getMemloc().isArgs() && !((Register)quad.oprand2).getMemloc().isGlobal()) {
				int tmp = ((Register)quad.oprand2).getMemloc().getNum();
				if (tmp < minreg)
					minreg = tmp;
				if (tmp > maxreg)
					maxreg = tmp;
				if (!this.regMap.containsKey(tmp))
					this.regMap.put(tmp, ((Register)quad.oprand2));
			}
		}
		if (quad.result instanceof Register) {
			if (!((Register)quad.result).getMemloc().isArgs() && !((Register)quad.result).getMemloc().isGlobal()) {
				int tmp = ((Register)quad.result).getMemloc().getNum();
				if (tmp < minreg)
					minreg = tmp;
				if (tmp > maxreg)
					maxreg = tmp;
				if (!this.regMap.containsKey(tmp))
					this.regMap.put(tmp, ((Register)quad.result));
			}
		}
		quadChain.addQuad_(quad, fend);
	}
	
	public void setargnum(int i) {
		this.argnum = i;
	}
	
	public void addlPtr(MemLoc memloc) {
		this.lPtr.add(memloc);
	}
	
	public void addargPtr(MemLoc memloc) {
		this.argPtr.add(memloc);
	}
	
	public void scan() {
		this.quadChain.scan(this);
	}
}
