package appetizer.interform;

public class Label extends Oprand {
	public String labelName;
	
	public Label(int i) {
		this.labelName = "label" + String.valueOf(i);
	}
	
	public String getlabelName() {
		return this.labelName;
	}
	
	public String getIR() {
		return this.labelName;
	}
}
