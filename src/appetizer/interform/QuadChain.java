package appetizer.interform;

import java.util.HashMap;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Set;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import appetizer.environ.Array;
import appetizer.environ.Pointer;
import appetizer.environ.Struct;
import appetizer.environ.Type;
import appetizer.translate.Asm;
import appetizer.environ.*; 

public class QuadChain {
	private class Node {
		public Node next;
		public Quad quad;
		public Node prev;
		
		public Node(Node p, Quad q) { 
			this.next = p;
			this.quad = q;
		}
	}
	
	public Node chainStart;
	public Node chainEnd;
	
	public void printIR() {
		Node p = chainStart;
		while(p.next != null) {
			p = p.next;
			p.quad.printIR();
		}
	}
	
	public void printASM(FunctionEntry funcEntry) {
		Node p = chainStart.next;
		//while(p.quad != funcEntry.fend) {
		while(p.next.quad != funcEntry.fend) {
			 p = p.next;
			 p.quad.printIR();
			 p.quad.printASM(funcEntry);
		}
	}
	
	public void setFuncLabel(HashMap<String, FunctionEntry> map) {
		Node p = chainStart;
		while(p.next != null) {
			p = p.next;
			if (p.quad.oprand1 instanceof Func) {
				Func fc = (Func)p.quad.oprand1;
				fc.entry = map.get(fc.fname);
			}
		}
	}
	
	public int getInnerArgSpace() {
		Node p = chainStart;
		int space = 0;
		while (p.next != null) {
			p = p.next;
			if (p.quad.oprand1 instanceof Func) {
				Func fc = (Func)p.quad.oprand1;
				int x = 0;
				for (int i = 0; i < fc.argument.size(); ++i) {
					Oprand opr = fc.argument.get(i);
					if (opr instanceof Register && ((Register)opr).memloc.getType() instanceof Struct) {
						x += ((Register)opr).memloc.getType().getSize();
						continue;
					}
					
					if ( opr instanceof Mem && ((Mem)opr).oprand instanceof Register && ((Register) ((Mem)opr).oprand).memloc.getType() instanceof Pointer) {
						if (((Pointer) ((Register) ((Mem)opr).oprand).memloc.getType()).getelementType() instanceof Struct) {
							Type tmpType = ((Pointer) ((Register) ((Mem)opr).oprand).memloc.getType()).getelementType();
							int size = ((Struct) tmpType).getSize();
							x += size;
						} else {
							x += 4;
						}
					} else {
						x += 4;
					}
				}
				if (x > space)
					space = x;
				//if (fc.entry != null && fc.entry.argsize() > space)
				//	space = fc.entry.argsize();
			}
		}
		return space;
	}
	
	public void getprev(FunctionEntry funcEntry) {
		Node p = chainStart;
		while(p.next != null) {
			p.next.prev = p;
			p = p.next;
		}
		chainEnd = p;
	}
	
	public Quad findlabel(String s) {
		Node p = chainStart;
		while (p.next != null) {
			p = p.next;
			if (p.quad.oprand1 instanceof Label && ((Label) p.quad.oprand1).labelName.equals(s)) {
				Node q = p.next;
				//while (q != null && (q.quad.oprand1 instanceof Label || q.quad.op.getOpName().equals("=") || q.quad.op.getOpName().equals("&"))) q = q.next;
				while(q != null && q.quad.oprand1 instanceof Label) q = q.next;
				if (q != null)
					return q.quad;
				else return null;
			}
		}
			
		return null;
	}
	
	public Quad findnext(Node p) {
		p = p.next;
		//while (p != null && (p.quad.oprand1 instanceof Label || p.quad.op.getOpName().equals("&"))) p = p.next;
		while(p != null && p.quad.oprand1 instanceof Label) p = p.next;
		if (p != null)
			return p.quad;
		else return null;
	}
	
	public int getRealReg(Oprand opr) {
		if (opr instanceof Register) {
			if (((Register)opr).getMemloc().isArgs() && ((Register)opr).getMemloc().isGlobal()) return 0;
			return ((Register)opr).getMemloc().getNum();
		} else if (opr instanceof Mem) {
			Oprand x = ((Mem) opr).oprand;
			if (x instanceof Register && !((Register)x).getMemloc().isArgs() && !((Register)x).getMemloc().isGlobal()) {
				return ((Register)x).getMemloc().getNum();
			} else {
				return 0;
			}
		} else {
			return 0;
		}
	}

	public void scan(FunctionEntry funcEntry) {
		boolean flag = true;
		Quad quad;
		Quad nextq;
		Node p = chainEnd;
		Oprand opr1, opr2, result;
		Register reg;
		int i;
		getprev(funcEntry);
		while(flag) {
			flag = false;
			p = chainEnd;
			while(p != chainStart) {
				quad = p.quad;
				Operation op = quad.op;
				Set<Integer> oriin = new HashSet<Integer>();
				oriin.addAll(quad.livein);
				Set<Integer> oriout = new HashSet<Integer>();
				oriout.addAll(quad.liveout);
				if (op != null) {
					switch (op.getOpName()) {
						case "j":
							result = quad.result;
							nextq = findlabel(result.getIR());
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							quad.livein.addAll(quad.liveout);
							break;
						case "jz":
							opr1 = quad.oprand1;
							result = quad.result;
							nextq = findnext(p);
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							nextq = findlabel(result.getIR());
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							quad.livein.addAll(quad.liveout);
							i = getRealReg(opr1);
							if (i != 0) {
								quad.livein.add(i);
							}
							break;
						case "jnz":
							opr1 = quad.oprand1;
							result = quad.result;
							nextq = findnext(p);
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							nextq = findlabel(result.getIR());
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							quad.livein.addAll(quad.liveout);
							i = getRealReg(opr1);
							if (i != 0) {
								quad.livein.add(i);
							}
							break;
						case "ret":
							result = quad.result;
							if (result != null) {
								i = getRealReg(result);
								quad.livein.add(i);
							}
							break;
						case "=":
							opr1 = quad.oprand1;
							nextq = findnext(p);
							if (nextq != null) quad.liveout.addAll(nextq.livein);
							quad.livein.addAll(quad.liveout);
							if (opr1 instanceof Func) {
								int x = getRealReg(quad.result);
								if (quad.result instanceof Register) {
									if (x != 0) quad.livein.remove(x);
								} else if (quad.result instanceof Mem) {
									if (x != 0) quad.livein.add(x);
								}
								for (int j = 0; j < ((Func)opr1).argument.size(); j++) {
									Oprand opr = ((Func)opr1).argument.get(j);
									x = getRealReg(opr);
									if (x != 0) {
										quad.livein.add(x);
									}
								}
							} else {
								int x = getRealReg(quad.result);
								if (quad.result instanceof Register) {
									if (x != 0) quad.livein.remove(x);
								} else if (quad.result instanceof Mem) {
									if (x != 0) quad.livein.add(x);
								}
								x = getRealReg(quad.oprand1);
								if (x != 0) quad.livein.add(x);
							}
						//case "&":
						//	opr1 = quad.oprand1;
							
						default:
							opr1 = quad.oprand1;
							opr2 = quad.oprand2;
							result = quad.result;
							if (opr2 != null) {
								nextq = findnext(p);
								if (nextq != null) quad.liveout.addAll(nextq.livein);
								quad.livein.addAll(quad.liveout);
								i = getRealReg(result);
								if (result instanceof Register) {
									if (i != 0) quad.livein.remove(i);
								} else if (result instanceof Mem) {
									if (i != 0) quad.livein.add(i);
								}
								i = getRealReg(opr1);
								if (i != 0) quad.livein.add(i);
								i = getRealReg(opr2);
								if (i != 0) quad.livein.add(i);
							} else {
								nextq = findnext(p);
								if (nextq != null) quad.liveout.addAll(nextq.livein);
								quad.livein.addAll(quad.liveout);
								i = getRealReg(result);
								if (result instanceof Register) {
									if (i != 0) quad.livein.remove(i);
								} else if (result instanceof Mem) {
									if (i != 0) quad.livein.add(i);
								}
								i = getRealReg(opr1);
								if (i != 0) quad.livein.add(i);
							}
							break;
							
					}
				}
				
				if (!oriin.containsAll(quad.livein)) flag = true;
				if (!oriout.containsAll(quad.liveout)) flag = true;
				p = p.prev;
			}
		}	
		
		getAllRegBeginAndEnd(funcEntry);
		delReg(funcEntry);
		Listwaitforallo(funcEntry);
		regAllocate(funcEntry);
	}
	
	public void delprintf(Func func, FunctionEntry funcEntry) {
		for (int i = 0; i < func.argument.size(); ++i) {
			Oprand op = func.argument.get(i);
			if ( op instanceof Register) {
				MemLoc memloc = ((Register) op).getMemloc();
				if (memloc.getType() instanceof Array && !((Register) op).getMemloc().isGlobal() && !((Register) op).getMemloc().isArgs()) {
					funcEntry.waitforallo.remove(memloc.getNum());
				}
			}
		}
	}
	
	public void delfunc(Func func, FunctionEntry funcEntry) {
		for (int i = 0; i < func.argument.size(); ++i) {
			Oprand op = func.argument.get(i);
			if (op instanceof Register) {
				MemLoc memloc = ((Register) op).getMemloc();
				if (memloc.getType() instanceof Struct) {
					if (!memloc.isArgs() && !memloc.isGlobal())
						funcEntry.waitforallo.remove(memloc.getNum());
				}
				if (memloc.getType() instanceof Array && !((Register) op).getMemloc().isGlobal() && !((Register) op).getMemloc().isArgs()) {
					funcEntry.waitforallo.remove(memloc.getNum());
				}
			} else if (op instanceof Mem) {
				if ( ((Mem)op).oprand instanceof Register && ((Register) ((Mem)op).oprand).memloc.getType() instanceof Pointer) { 
					if (((Pointer) ((Register) ((Mem)op).oprand).memloc.getType()).getelementType() instanceof Struct) {
						if (!((Register) ((Mem)op).oprand).memloc.isArgs() && !((Register) ((Mem)op).oprand).memloc.isGlobal())
							funcEntry.waitforallo.remove(((Register) ((Mem)op).oprand).memloc.getNum());
					}
				}
			}
		}
	}
	
	public void deloperation1(Operation op, Oprand oprand1, Oprand result, FunctionEntry funcEntry) {
		if (op.getOpName().equals("&")) {
			if (oprand1 instanceof Register) {
				MemLoc memloc = ((Register) oprand1).memloc;
				if (!memloc.isArgs() && !memloc.isGlobal()) funcEntry.waitforallo.remove(memloc.getNum());
			} else if (oprand1 instanceof Mem && ((Mem) oprand1).oprand instanceof Register) {
				Register reg = (Register)((Mem) oprand1).oprand;
				MemLoc memloc = reg.memloc;
				if (!memloc.isArgs() && !memloc.isGlobal()) funcEntry.waitforallo.remove(memloc.getNum());
			}
		}
	}
	
	public void delassign(Oprand oprand1, Oprand result, FunctionEntry funcEntry) {
		if (oprand1 instanceof Register && result instanceof Register) {
			if (((Register)result).getMemloc().getType() instanceof Struct) {
				MemLoc memloc = ((Register) result).memloc;
				if (!memloc.isArgs() && !memloc.isGlobal()) funcEntry.waitforallo.remove(memloc.getNum());
				return;
			}
		}
		
		if (oprand1 instanceof Register) {
			MemLoc memloc = ((Register) oprand1).getMemloc();
			if (memloc.getType() instanceof Array) {
				if (!memloc.isGlobal() && !memloc.isArgs()) {
					funcEntry.waitforallo.remove(memloc.getNum());
				}
			}
		}
	}
	
	public int getQuadNumber(Quad quad) {
		Node p = chainStart.next;
		int i = 1;
		while (p != null) {
			if ( p.quad == quad ) return i;
			p = p.next;
			i++;
		}
		return -1;
	}
	
	public void delReg(FunctionEntry funcEntry) {
		Node p = chainStart.next;
		Quad quad;
		//while(p.quad != funcEntry.fend) {
		while(p.next.quad != funcEntry.fend) {
			 p = p.next;
			 quad = p.quad;
			 Operation op = quad.op;
			 if (quad.oprand1 instanceof Label) {
				 continue;
			 } else if ( op.getOpName().equals("=") ) {
				 if (quad.oprand1 instanceof Func) {
					 if (((Func) quad.oprand1).fname.equals("printf")) {
							delprintf((Func) quad.oprand1, funcEntry);
					 } else {
						 delfunc((Func) quad.oprand1, funcEntry);
					 }
				 } else {
					 delassign(quad.oprand1, quad.result, funcEntry);
				 }
			 } else if ( op.getOpName().equals("jz")) {
				 
			 } else if (op.getOpName().equals("jnz")) {
				 
			 } else if (op.getOpName().equals("j")) {
				 
			 } else if (op.getOpName().equals("ret")) {
				 
			 } else if ( quad.oprand2 != null) {
				 
			 } else {
				 deloperation1(op, quad.oprand1, quad.result, funcEntry);
			 }
		}
	}
	
	public void Listwaitforallo(FunctionEntry funcEntry) {
		for (int x : funcEntry.waitforallo) {
			funcEntry.regbegi.put(x, getQuadNumber(funcEntry.regbegin.get(x)));
			funcEntry.regendi.put(x,  getQuadNumber(funcEntry.regend.get(x)));
		}
	}
	
	public void regAllocate(FunctionEntry funcEntry) {
		List<Map.Entry<Integer, Integer>> infoIds =
			    new ArrayList<Map.Entry<Integer, Integer>>(funcEntry.regbegi.entrySet());
		
		Collections.sort(infoIds, new Comparator<Map.Entry<Integer, Integer>>() {   
		    public int compare(Map.Entry<Integer, Integer> o1, Map.Entry<Integer, Integer> o2) {      
		        if (o1.getValue() > o2.getValue()) return 1;
		        else if (o1.getValue() == o2.getValue()) return 0;
		        else return -1;
		    }
		}); 
		
		/*for (int i = 0; i < infoIds.size(); ++i) {
			int x = infoIds.get(i).getKey();
			System.out.println("t" + infoIds.get(i).getKey());
			System.out.println(infoIds.get(i).getValue());
			System.out.println(funcEntry.regendi.get(x));
		}*/
		
		int regnum = 12;
		//init reg
		for (int i = 0; i < regnum; i++) funcEntry.busyto.put(i, 0);
		for (int i = 0; i < infoIds.size(); ++i) {
			int x = infoIds.get(i).getKey();
			int begin = funcEntry.regbegi.get(x);
			int end = funcEntry.regendi.get(x);
			//find a free register
			for (int j = 0; j < regnum; j++) {
				if (begin > funcEntry.busyto.get(j)) {
					funcEntry.allo.put(x, j);
					funcEntry.busyto.put(j, end);
					break;
				}
			}
		}
		
		/*for (int i = 0; i < infoIds.size(); ++i) {
			int x = infoIds.get(i).getKey();
			System.out.println("t" + infoIds.get(i).getKey());
			System.out.println(infoIds.get(i).getValue());
			System.out.println(funcEntry.regendi.get(x));
			System.out.println(funcEntry.allo.get(x));
		}*/
		
	}
	
	public void getAllRegBeginAndEnd(FunctionEntry funcEntry) {
		Node p = chainStart;
		Quad quad;
		while (p.next != chainEnd) {
			p = p.next;
			quad = p.quad;
			for (int x : quad.liveout) {
				if (!funcEntry.regbegin.containsKey(x)) {
					funcEntry.regbegin.put(x, quad);
				}
			}
		}
		
		p = chainEnd;
		while (p.prev != chainStart) {
			p = p.prev;
			quad = p.quad;
			for (int x : quad.livein) {
				if (!funcEntry.regend.containsKey(x)) {
					funcEntry.regend.put(x, quad);
				}
			}
		}
		
		for (int i = funcEntry.minreg; i <= funcEntry.maxreg; i++) {
			if (funcEntry.regbegin.containsKey(i)) {
				Node begin = chainStart;
				while (begin.quad != funcEntry.regbegin.get(i)) begin = begin.next;
				while(begin.quad != funcEntry.regend.get(i)) {
					quad = begin.quad;
					if (quad.op != null && quad.op.getOpName().equals("=") && quad.oprand1 instanceof Func) {
						funcEntry.regbegin.remove(i);
						funcEntry.regend.remove(i);
						break;
					}
					begin = begin.next;
				}
				
			}
		}
		
		for (int i = funcEntry.minreg; i <= funcEntry.maxreg; i++) {
			//System.out.println("For t" + Integer.toString(i) + ":");
			if (funcEntry.regbegin.containsKey(i)) {
				//System.out.println("Begin:");
				//funcEntry.regbegin.get(i).printIR();
				//funcEntry.regbegi.put(i, getQuadNumber(funcEntry.regbegin.get(i)));
				funcEntry.waitforallo.add(i);
			}
			if (funcEntry.regend.containsKey(i)) {
				//System.out.println("End:");
				//funcEntry.regend.get(i).printIR();
				//funcEntry.regendi.put(i,  getQuadNumber(funcEntry.regend.get(i)));
				funcEntry.waitforallo.add(i);
			}
		}
	}
	
	
	
	public QuadChain() {
		chainStart = new Node(null, null);
	}
	
	public void addQuad(Quad q) {
		Node p = chainStart;
		while (p.next != null) p = p.next;
		p.next = new Node(null, q);
	}
	
	public void addQuad_(Quad q, Quad flag) {
		Node p = chainStart;
		while (p.next.quad != flag) p = p.next;
		Node j = new Node(p.next, q);
		p.next = j;
	}
}
