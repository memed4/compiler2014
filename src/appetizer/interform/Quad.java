package appetizer.interform;

import appetizer.translate.*;
import java.util.Set;
import java.util.HashSet;

public class Quad {
	public Operation op;
	public Oprand oprand1;
	public Oprand oprand2;
	public Oprand result;
	public Set<Integer> livein = new HashSet<Integer>();
	public Set<Integer> liveout = new HashSet<Integer>();
	
	public Quad(Operation op, Oprand oprand1, Oprand oprand2, Oprand result) {
		this.op = op;
		this.oprand1 = oprand1;
		this.oprand2 = oprand2;
		this.result = result;
	}
	
	public void printIR() {
		System.out.print("#");
		if (this.oprand1 instanceof Label) {  //label
			System.out.println(this.oprand1.getIR() + ":");
		} else if (this.op.getOpName().equals("j")) { // j
			if (this.result!=null)
				System.out.println("j " + this.result.getIR());
			else 
				System.out.println("j");
		} else if (this.op.getOpName().equals("jz")) {
			System.out.println("if " + this.oprand1.getIR() + "==0 j " + this.result.getIR());
		} else if (this.op.getOpName().equals("jnz")) { // cond j
			System.out.println("if " + this.oprand1.getIR() + "!=0 j " + this.result.getIR());
		} else if (this.op.getOpName().equals("=")) {
			if (this.oprand1 != null && this.result != null) 
				System.out.println(this.result.getIR() + " = " + this.oprand1.getIR());
		} else if (this.op.getOpName().equals("ret")) {
			if (this.result!=null) {
				System.out.println("ret " + this.result.getIR());
			} else {
				System.out.println("ret");
			}
		} else if (this.oprand2 != null) {
			System.out.println(this.result.getIR() + " = " + this.oprand1.getIR() + this.op.getOpName() + this.oprand2.getIR());
		} else {
			System.out.println(this.result.getIR() + " = " + this.op.getOpName() + this.oprand1.getIR());
		}
	}
	
	public void printASM(FunctionEntry funcEntry) {
		if (oprand1 instanceof Label) {
			Asm.label(oprand1.getIR());
		} else if (op.getOpName().equals("j")) {
			Asm.jmp(result.getIR());
		} else if (op.getOpName().equals("jz")) {
			Asm.jz(oprand1, result.getIR(), funcEntry);
		} else if (op.getOpName().equals("jnz")) {
			Asm.jnz(oprand1, result.getIR(), funcEntry);
		} else if (op.getOpName().equals("=")) {
			if (oprand1 instanceof Func) {
				if (((Func) oprand1).fname.equals("printf")) {
					Asm.printf((Func)oprand1, funcEntry);
				} else if (((Func) oprand1).fname.equals("malloc")) {
					Asm.malloc((Func)oprand1, result, funcEntry);
				} else if (((Func) oprand1).fname.equals("memcpy")) {
					Asm.memcpy((Func) oprand1, funcEntry);
				} else if (((Func) oprand1).fname.equals("comp1")) {
					Asm.comp1((Func) oprand1, result);
				} else {
					Asm.assignFunc((Func)oprand1, result, funcEntry);
				} 
			} else {
				Asm.assign(oprand1, result, funcEntry);
			}
		} else if (this.op.getOpName().equals("ret")) {
			Asm.ret(this.result, funcEntry);
		} else if (this.oprand2 != null) {
			Asm.operation2(op, oprand1, oprand2, result, funcEntry);
		} else {
			Asm.operation1(op, oprand1, result, funcEntry);
		}
	}
}
