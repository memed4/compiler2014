package appetizer.interform;

public class LoopEntry {
	int type; // 1: For 2: While
	Label l1, l2, l3;
	
	public LoopEntry(int type, Label l1, Label l2, Label l3) {
		this.type = type;
		this.l1 = l1;
		this.l2 = l2;
		this.l3 = l3;
	}
	
	public int getType() {
		return this.type;
	}
	
	public Label getl1() {
		return this.l1;
	}
	
	public Label getl2() {
		return this.l2;
	}
	
	public Label getl3() {
		return this.l3;
	}
}
