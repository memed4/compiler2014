package appetizer.interform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Program {
	public List<FunctionEntry> funcList = new ArrayList<FunctionEntry>();
	public List<Quad> init = new ArrayList<Quad>();
	private int labelNum;
	private int regNum;
	public List<MemLoc> gPtr = new ArrayList<MemLoc>();
	private int regNumG;
	private LoopEntry currentLoop;
	public HashMap<String, FunctionEntry> funcLabel = new HashMap<String, FunctionEntry>();
	
	public Program() {
		labelNum = 0;
		regNum = 0;
		regNumG = 0;
		currentLoop = null;
	}
	
	public Quad CheckDupConstStr(String s) {
		for (int i = 0; i < this.init.size(); ++i) {
			if (this.init.get(i).oprand1.getIR().equals(s)) {
				return this.init.get(i);
			}
		}
		return null;
	}
	
	public void setCurrentLoop(LoopEntry e) {
		this.currentLoop = e;
	}
	
	public LoopEntry getCurrentLoop() {
		return this.currentLoop;
	}
	
	public void addinit(Quad quad) {
		this.init.add(quad);
	}
	
	public void addgPtr(MemLoc memloc) {
		this.gPtr.add(memloc);
	}
	
	public int getRegNumG() {
		return this.regNumG;
	}
	
	public void setRegNumG(int i) {
		this.regNumG = i;
	}
	
	public int getLabelNum() {
		return labelNum;
	}
	
	public int getRegNum() {
		return regNum;
	}
	
	public void setLabelNum(int i) {
		this.labelNum = i;
	}
	
	public void setRegNum(int i) {
		this.regNum = i;
	}
	
	public void addNewEntry(FunctionEntry funcEntry) {
		funcList.add(funcEntry);
		if (!funcLabel.containsKey(funcEntry.fName))
			funcLabel.put(funcEntry.fName, funcEntry);
	}
	
	public void printIR() {
		for (int i = 0; i < this.init.size(); ++i) {
			this.init.get(i).printIR();
		}
		System.out.println();
		for (int i = 0; i < this.funcList.size(); ++i) {
			this.funcList.get(i).quadChain.printIR();
			System.out.println();
		}
	}
}
