package appetizer.interform;

public class Register extends Oprand {
	public MemLoc memloc;
	
	public Register(MemLoc memloc) {
		this.memloc = memloc;
	}

	public MemLoc getMemloc() {
		return this.memloc;
	}
	
	public void setMemloc(MemLoc m) {
		this.memloc = m;
	}
	
	public String getIR() {
		if (this.memloc.isGlobal()) {
			return "g" + Integer.toString(this.memloc.getNum());
		}
		
		if (this.memloc.isArgs()) {
			return "arg" + Integer.toString(this.memloc.getNum());
		}
		
		return "t" + Integer.toString(this.memloc.getNum());
	}
}
