package appetizer.interform;

import java.util.ArrayList;
import java.util.List;

public class Func extends Oprand {
	public String fname;
	public List<Oprand> argument;
	public FunctionEntry entry;
	
	public Func(String fname) {
		this.fname = fname;
		this.argument = new ArrayList<Oprand>();
	}
	
	public void addArgu(Oprand arg) {
		argument.add(arg);
	}
	
	public int argsize() {
		return entry.argsize();
	}
	
	public String getIR() {
		String ir = this.fname + "( ";
		for (int i = 0; i < argument.size(); ++i) {
			ir += argument.get(i).getIR() + " ";
		}
		ir += ")";
		return ir;
	}

}
