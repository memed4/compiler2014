package appetizer.interform;

public class Mem extends Oprand {
	public Oprand oprand;
	
	public Mem(Oprand oprand) {
		this.oprand = oprand;
	}
	
	public String getIR() {
		return "[" + this.oprand.getIR() + "]";
	}
}
