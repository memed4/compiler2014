package appetizer.interform;

public class Const extends Oprand {
	public int value;
	
	public Const(int value) {
		this.value = value;
	}
	
	public String getIR() {
		return Integer.toString(value);
	}

}
