package appetizer.interform;

public class Operation {
	private String opName;
	
	public Operation(String s) {
		this.opName = s;
	}
	
	public void setOpName(String s) {
		this.opName = s;
	}
	
	public String getOpName() {
		return this.opName;
	}
}
